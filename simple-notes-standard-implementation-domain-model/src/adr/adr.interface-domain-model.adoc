== Domain Model based on interfaces

=== Status

[badge accepted]*accepted*

=== Context

The domain model must be independent from possible serialization mechanisms.
Such serialization mechanism should serialize the Domain Events or the enities with common serialization methods.
Such common serializations can be supported by Java annotations for JSON serialization like https://github.com/FasterXML/jackson[FasterXML Jackson], https://github.com/google/gson[Google GSON] or the https://jakarta.ee/specifications/jsonb/[Jakarta JSON-B] standard.

Such binding technologies may change in the future.
Therefor the project should support an easier switch of such serializationn methods.

=== Decision

The Domain Model should based on Java interfaces only.
This will support adaptability against serialization mechanisms and Entity an Event implementations bby hiding such implementation details.


==== Alternatives

Direct implementation of the Entity objects and the Events may also raise API dependencies to external APIs to improve development speed.


=== Consequences

There must be a https://en.wikipedia.org/wiki/Abstract_factory_pattern[factory] interface for creating at least the https://en.wikipedia.org/wiki/Domain-driven_design[Aggregate Root] entity.
