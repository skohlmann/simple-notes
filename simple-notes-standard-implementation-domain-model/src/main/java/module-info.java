/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */

module speexx.simplenotes.standard.impl.domain.model {
    requires speexx.common;
    requires speexx.common.annotation;
    requires speexx.domain.model;
    requires speexx.event.sourcing;
    requires speexx.json.serializer;
    requires jakarta.validation;
    requires jmolecules.events;
    requires jmolecules.ddd;
    requires jmolecules.onion.architecture;
    requires org.apiguardian.api;
    requires com.fasterxml.jackson.annotation;
    requires transitive com.fasterxml.jackson.databind;
    
    requires speexx.simplenotes.domain.model.api;
    
    exports de.speexx.simplenotes.domain.impl.standard.model;
    exports de.speexx.simplenotes.domain.impl.standard.model.creator;
    exports de.speexx.simplenotes.domain.impl.standard.model.note;
    exports de.speexx.simplenotes.domain.impl.standard.service.note;
    
    opens de.speexx.simplenotes.domain.impl.standard.model.creator to com.fasterxml.jackson.databind, speexx.domain.model;
    opens de.speexx.simplenotes.domain.impl.standard.model.note to com.fasterxml.jackson.databind, speexx.domain.model;
}
