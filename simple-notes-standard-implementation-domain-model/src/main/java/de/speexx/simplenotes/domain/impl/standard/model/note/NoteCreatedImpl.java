/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.model.note;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import de.speexx.common.annotation.design.gof.Adapter;
import de.speexx.domain.annotation.PublishedLanguage;
import de.speexx.domain.model.EventName;
import de.speexx.simplenotes.domain.impl.standard.model.creator.CreatorImpl;
import de.speexx.simplenotes.domain.model.creator.Creator;
import de.speexx.simplenotes.domain.model.note.NoteCreated;
import de.speexx.simplenotes.domain.model.note.Text;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import java.time.OffsetDateTime;
import org.jmolecules.event.annotation.DomainEvent;

/**
 *
 * @author sascha.kohlmann
 */
@DomainEvent
@EventName("note-created")
@PublishedLanguage
public final class NoteCreatedImpl extends NoteEventImpl implements NoteCreated {
    
    @NotNull
    @JsonProperty("text")
    private TextImpl text;

    @NotNull
    @JsonProperty("created-at")
    @JsonPropertyDescription("The ISO 8601 date-time format with an offset, such as '2011-12-03T10:15:30.39623Z' or '2011-12-03T10:15:30.39623+01:00'. See also date-time with time-secfrac in RFC 3339 section 5.6. E.g. Required.")
    @Pattern(regexp="^(?<fullyear>\\d{4})-(?<month>0[1-9]|1[0-2])-(?<mday>0[1-9]|[12][0-9]|3[01])T(?<hour>[01][0-9]|2[0-3]):(?<minute>[0-5][0-9]):(?<second>[0-5][0-9]|60)(?<secfrac>\\.[0-9]+)?(Z|(\\+|-)(?<offsethour>[01][0-9]|2[0-3]):(?<offsetminute>[0-5][0-9]))$")
    private OffsetDateTime createdAt;

    @NotNull
    @JsonProperty("creator")
    private CreatorImpl creator;
    
    public NoteCreatedImpl(final @NotNull @JsonProperty("note-id") NoteIdImpl id,
                           final @NotNull @JsonProperty("text") TextImpl text,
                           final @NotNull @JsonProperty("created-at") OffsetDateTime createdAt,
                           final @NotNull @JsonProperty("creator") CreatorImpl creator) {
        super(id);
        this.text = text;
        this.createdAt = createdAt;
        this.creator = creator;
    }

    @Override
    public Text text() {
        return this.text;
    }

    @Override
    public OffsetDateTime createdAt() {
        return this.createdAt;
    }

    @Override
    public Creator creator() {
        return this.creator;
    }

    @Override
    public String toString() {
        return "NoteCreatedImpl{" + "text=" + text + ", createdAt=" + createdAt + ", creator=" + creator + '}';
    }

    @Adapter
    public static NoteCreatedImpl adaptFrom(final NoteCreated noteCreated) {
        if (noteCreated instanceof NoteCreatedImpl impl) {
            return impl;
        }
        
        return new NoteCreatedImpl(NoteIdImpl.adaptFrom(noteCreated.noteId()),
                                   TextImpl.adaptFrom(noteCreated.text()),
                                   noteCreated.createdAt(),
                                   CreatorImpl.adaptFrom(noteCreated.creator()));
    }
}
