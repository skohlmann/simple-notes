/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.service.note;

import de.speexx.simplenotes.domain.command.note.CreateNote;
import de.speexx.simplenotes.domain.impl.standard.model.EventImpl;
import de.speexx.simplenotes.domain.impl.standard.model.note.NoteCreatedImpl;
import de.speexx.simplenotes.domain.impl.standard.model.note.NoteImpl;
import de.speexx.simplenotes.domain.model.note.Note;
import de.speexx.simplenotes.domain.model.note.NoteCreated;
import de.speexx.simplenotes.domain.model.note.NoteEvent;
import de.speexx.simplenotes.domain.service.note.NoteFactory;
import de.speexx.simplenotes.domain.service.note.NoteIdentityService;
import java.util.List;

/**
 *
 * @author sascha.kohlmann
 */
public class StandardNoteFactory implements NoteFactory, NoteIdentityService {
    
    @Override
    public List<NoteEvent> create(final CreateNote command) {
        final var note = NoteImpl.create(command, this);
        return List.of(note);
    }

    @Override
    public Note reconstitute(final List<NoteEvent> events) {
        return new NoteImpl(events.stream().map(event -> adapt(event)).toList());
    }
    
    EventImpl adapt(final NoteEvent event) {
        if (event instanceof EventImpl impl) {
            return impl;
        } else if (event instanceof NoteCreated created) {
            return NoteCreatedImpl.adaptFrom(created);
        }
        throw new IllegalArgumentException("Event Type not supported: " + event);
    }
}
