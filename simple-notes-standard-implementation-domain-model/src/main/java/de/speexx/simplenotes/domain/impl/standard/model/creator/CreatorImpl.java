/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.model.creator;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.speexx.common.annotation.design.gof.Adapter;
import de.speexx.simplenotes.domain.model.creator.Creator;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import static de.speexx.common.Assertions.assertLength;
import static de.speexx.common.Assertions.assertNotBlank;
import static de.speexx.common.Assertions.assertNotNull;

/**
 *
 * @author sascha.kohlmann
 */
public record CreatorImpl(@JsonProperty("creator-id") @NotNull CreatorIdImpl creatorId,
                          @JsonProperty("name") @NotBlank @Max(128) String name) implements Creator {

    public CreatorImpl {
        assertNotBlank(name, "Name must not be null and not blank");
        assertLength(name, 128, "Name must not longer 128 characters. Is: " + name.length());
        assertNotNull(creatorId, "Creator ID must be provided");
    }

    @Adapter
    public static CreatorImpl adaptFrom(final Creator creator) {
        if (creator instanceof CreatorImpl impl) {
            return impl;
        }
        
        return new CreatorImpl(CreatorIdImpl.adaptFrom(creator.creatorId()), creator.name());
    }
}
