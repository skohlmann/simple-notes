/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.model.creator;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.speexx.common.annotation.design.gof.Adapter;
import de.speexx.simplenotes.domain.model.creator.CreatorId;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;

import static de.speexx.common.Assertions.assertLength;
import static de.speexx.common.Assertions.assertNotBlank;

/**
 *
 * @author sascha.kohlmann
 */
public record CreatorIdImpl(@JsonProperty("id") @NotBlank @Max(64) String id) implements CreatorId {
    
    public CreatorIdImpl {
        assertNotBlank(id, "ID must be provided");
        assertLength(id, 64, "ID must not longer 64 characters. Is: " + id.length());
    }

    @Adapter
    public static CreatorIdImpl adaptFrom(final CreatorId creatorId) {
        if (creatorId instanceof CreatorIdImpl impl) {
            return impl;
        }
        
        return new CreatorIdImpl(creatorId.id());
    }
}
