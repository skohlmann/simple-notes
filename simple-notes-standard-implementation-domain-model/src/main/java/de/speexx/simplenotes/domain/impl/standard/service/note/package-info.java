/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * The standard implementation supports an event serialization mechanism based on FasterXML Jackson annotations.
 * 
 * <p><strong>Note:</strong> The package may be later located in an own project/jar file.</p>
 */
@DomainServiceRing
package de.speexx.simplenotes.domain.impl.standard.service.note;

import org.jmolecules.architecture.onion.classical.DomainServiceRing;

