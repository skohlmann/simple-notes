/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.speexx.domain.model.DomainEvent;
import de.speexx.event.sourcing.Source;
import de.speexx.simplenotes.domain.model.Event;
import java.time.OffsetDateTime;

/**
 *
 * @author sascha.kohlmann
 */
public abstract class EventImpl<T> extends DomainEvent implements Event, Source<T> {
    
    @Override
    public int sourceVersion() {
        return eventVersion();
    }

    @Override
    public OffsetDateTime sourcedAt() {
        return occuredOn();
    }

    @Override
    @JsonIgnore
    public boolean isNull() {
        return false;
    }
}
