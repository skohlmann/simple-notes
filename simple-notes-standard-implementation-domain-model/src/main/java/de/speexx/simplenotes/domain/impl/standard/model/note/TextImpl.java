/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.model.note;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.speexx.common.annotation.design.gof.Adapter;
import de.speexx.simplenotes.domain.model.note.Text;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;

import static de.speexx.common.Assertions.assertLength;
import static de.speexx.common.Assertions.assertNotBlank;
import static de.speexx.common.Assertions.assertNotNull;
import static de.speexx.simplenotes.domain.impl.standard.model.note.NoteImpl.MAX_NOTE_LENGTH;
import static java.lang.String.format;

/**
 *
 * @author sascha.kohlmann
 */
public record TextImpl(@NotBlank @Max(MAX_NOTE_LENGTH) @JsonProperty("value") String value) implements Text {
    
    public TextImpl {
        // Note: Suppliers for assertion methjods are a better choice but currently doesn't work with records
        // (compact constructor must not have return statements).
        assertNotNull(value, "Text value must be provided");
        assertNotBlank(value, "Text value must not be whitespace only");
        assertLength(value, MAX_NOTE_LENGTH, format("Text value must be have a maximum length of %d. Is: %d", MAX_NOTE_LENGTH, value.length()));
    }

    @Adapter
    public static TextImpl adaptFrom(final Text text) {
        if (text instanceof TextImpl impl) {
            return impl;
        }
        
        return new TextImpl(text.value());
    }
}
