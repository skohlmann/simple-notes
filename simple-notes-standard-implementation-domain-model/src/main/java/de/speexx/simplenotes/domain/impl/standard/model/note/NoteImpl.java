/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.model.note;

import de.speexx.domain.model.sourced.EventSourcedEntity;
import de.speexx.domain.model.sourced.Mutator;
import de.speexx.simplenotes.domain.command.note.CreateNote;
import de.speexx.simplenotes.domain.impl.standard.model.EventImpl;
import de.speexx.simplenotes.domain.model.creator.Creator;
import de.speexx.simplenotes.domain.model.note.Note;
import de.speexx.simplenotes.domain.model.note.NoteCreated;
import de.speexx.simplenotes.domain.model.note.NoteId;
import de.speexx.simplenotes.domain.model.note.Text;
import de.speexx.simplenotes.domain.service.note.NoteIdentityService;
import java.time.OffsetDateTime;
import java.util.List;

import static de.speexx.common.Assertions.assertLength;
import static de.speexx.common.Assertions.assertNotBlank;
import static de.speexx.common.Assertions.assertTrue;
import static de.speexx.simplenotes.domain.impl.standard.model.creator.CreatorImpl.adaptFrom;
import static java.lang.String.format;
import static java.time.OffsetDateTime.now;

/**
 *
 * @author sascha.kohlmann
 */
public class NoteImpl extends EventSourcedEntity implements Note {

    static final int MAX_NOTE_LENGTH = 240;
    
    private NoteId noteId;
    private Text text;
    private OffsetDateTime createdAt;
    private Creator creator;
    
    public static NoteCreated create(final CreateNote command, final NoteIdentityService identityService) {
        final var note = new NoteImpl(command, identityService);
        return (NoteCreated) note.mutatingEvents().get(0);
    }

    public NoteImpl(final List<EventImpl> eventStream) {
        super(eventStream, 1);
    }
    
    private NoteImpl(final CreateNote command, final NoteIdentityService identityService) {
        assertNotBlank(command.text.orElse(null), "Note text must nop be blank");
        assertLength(command.text.get(), 1, MAX_NOTE_LENGTH, () -> format("Note text not in length range of 1 and %d", MAX_NOTE_LENGTH));
        assertTrue(command.creator.isPresent(), "Crator must be provided");

        final var noteCreated = new NoteCreatedImpl(new NoteIdImpl(identityService.nextNoteId().id()), 
                                                    new TextImpl(command.text.get()),
                                                    command.creationDatetime.orElse(now()),
                                                    adaptFrom(command.creator.get()));
        apply(noteCreated);
    }
    
    @Override
    public Text text() {
        return this.text;
    }    

    @Override
    public OffsetDateTime createdAt() {
        return this.createdAt;
    }    

    @Override
    public NoteId noteId() {
        return this.noteId;
    }
    
    @Override
    public Creator creator() {
        return this.creator;
    }
    
    @Mutator
    protected void when(final NoteCreatedImpl event) {
        this.text = event.text();
        this.noteId = event.noteId();
        this.createdAt = event.createdAt();
        this.creator = event.creator();
    }

    @Override
    public String toString() {
        return "NoteImpl{" + "noteId=" + noteId + ", text=" + text + ", createdAt=" + createdAt + '}';
    }
}
