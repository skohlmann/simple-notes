/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.model.creator;

import de.speexx.simplenotes.domain.model.creator.CreatorId;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author sascha.kohlmann
 */
public class CreatorIdImplTest {

    @Test
    public void simple_create() {
        // When
        final var creatorId = new CreatorIdImpl("123");
        
        // Then
        assertThat(creatorId.id()).isEqualTo("123");
    }

    @Test
    public void create_with_null() {
        // When
        final var ex = assertThrows(IllegalArgumentException.class, () -> new CreatorIdImpl(null));
        
        // Then
        assertThat(ex.getMessage()).isEqualTo("ID must be provided");
    }

    @Test
    public void create_with_blank() {
        // When
        final var ex = assertThrows(IllegalArgumentException.class, () -> new CreatorIdImpl("\t "));
        
        // Then
        assertThat(ex.getMessage()).isEqualTo("ID must be provided");
    }

    @Test
    public void create_with_too_long() {
        // Given
        final var tooLongId = IntStream.range(0, 64 + 1).mapToObj(c -> "a").collect(joining());
        
        // When
        final var ex = assertThrows(IllegalArgumentException.class, () -> new CreatorIdImpl(tooLongId));
        
        // Then
        assertThat(ex.getMessage()).isEqualTo("ID must not longer 64 characters. Is: 65");
    }

    @Test
    public void adapt_with_not_CreatorIdImpl() {
        // Given
        final var creatorId = new CreatorId() {
            @Override
            public String id() {
                return "123";
            }
        };
        
        // When
        final var adapted = CreatorIdImpl.adaptFrom(creatorId);
        
        // Then
        assertThat(adapted.id()).isEqualTo("123");
    }

    @Test
    public void adapt_with_CreatorIdImpl() {
        // Given
        final var creatorId = new CreatorIdImpl("abc");
        
        // When
        final var adapted = CreatorIdImpl.adaptFrom(creatorId);
        
        // Then
        assertThat(adapted).isSameInstanceAs(creatorId);
    }
}
