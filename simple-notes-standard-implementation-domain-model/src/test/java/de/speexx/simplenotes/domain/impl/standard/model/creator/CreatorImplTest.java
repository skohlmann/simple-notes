/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.model.creator;

import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author sascha.kohlmann
 */
public class CreatorImplTest {

    @Test
    public void create() {
        // When
        final var creator = new CreatorImpl(new CreatorIdImpl("123"), "nick name");
        
        // Then
        assertThat(creator.creatorId().id()).isEqualTo("123");
        assertThat(creator.name()).isEqualTo("nick name");
    }

    @Test
    public void create_with_null_id() {
        // When
        assertThrows(IllegalArgumentException.class, () -> new CreatorImpl(null, "nick name"));
    }

    @Test
    public void create_with_null_nick_name() {
        // When
        assertThrows(IllegalArgumentException.class, () -> new CreatorImpl(new CreatorIdImpl("123"), null));
    }

    @Test
    public void create_with_blank_nick_name() {
        // When
        assertThrows(IllegalArgumentException.class, () -> new CreatorImpl(new CreatorIdImpl("123"), "\t "));
    }

    @Test
    public void create_with_too_long_nick_name() {
        // Given
        final var tooLongNickName = IntStream.range(0, 128 + 1).mapToObj(c -> "a").collect(joining());
        
        // When
        assertThrows(IllegalArgumentException.class, () -> new CreatorImpl(new CreatorIdImpl("123"), "\t "));
    }

    @Test
    public void adapt_from_not_CreatorImpl() {
        // Given
        final var tooLongNickName = IntStream.range(0, 128 + 1).mapToObj(c -> "a").collect(joining());
        
        // When
        assertThrows(IllegalArgumentException.class, () -> new CreatorImpl(new CreatorIdImpl("123"), "\t "));
    }
}
