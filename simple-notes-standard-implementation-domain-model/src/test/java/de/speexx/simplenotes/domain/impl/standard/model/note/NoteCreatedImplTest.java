/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.model.note;

import de.speexx.simplenotes.domain.impl.standard.model.creator.CreatorIdImpl;
import de.speexx.simplenotes.domain.impl.standard.model.creator.CreatorImpl;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static de.speexx.json.seralizer.JsonSerializer.instance;
import static java.time.OffsetDateTime.now;

/**
 *
 * @author sascha.kohlmann
 */
public class NoteCreatedImplTest {

    @Test
    public void serialize_deserialize() {
        // Given
        final var datetime = now();
        final var event = new NoteCreatedImpl(new NoteIdImpl("test"),
                                              new TextImpl("simple text"),
                                              datetime,
                                              new CreatorImpl(new CreatorIdImpl("1"), "nickname"));
        
        // When
        final var json = instance().serialize(event);
        final var deserialized = instance().deserialize(json, NoteCreatedImpl.class);
        
        // Then
        assertThat(deserialized.createdAt()).isEqualTo(datetime);
        assertThat(deserialized.text().value()).isEqualTo("simple text");
        assertThat(deserialized.noteId().id()).isEqualTo("test");
        assertThat(deserialized.creator().creatorId().id()).isEqualTo("1");
        assertThat(deserialized.creator().name()).isEqualTo("nickname");
    }
}
