/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.service.note;

import de.speexx.simplenotes.domain.command.note.CreateNote;
import de.speexx.simplenotes.domain.model.note.NoteCreated;
import de.speexx.simplenotes.domain.impl.standard.model.creator.CreatorIdImpl;
import de.speexx.simplenotes.domain.impl.standard.model.creator.CreatorImpl;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author sascha.kohlmann
 */
public class StandardNoteFactoryTest {
/* because of a Netbenas problem with record imports
import de.speexx.simplenotes.domain.impl.standard.model.creator.CreatorIdImpl;
import de.speexx.simplenotes.domain.impl.standard.model.creator.CreatorImpl;
*/
    
    @Test
    public void create_note() {
        // Given
        final var factory = new StandardNoteFactory();
        final var createCommand = CreateNote.builder()
                .withText("simple text")
                .withCreator(new CreatorImpl(new CreatorIdImpl("1"), "nick name"))
                .build();
        
        // When
        final var createdEvent = factory.create(createCommand);
        
        // Then
        assertThat(createdEvent).isNotEmpty();
        assertThat(((NoteCreated) createdEvent.get(0)).text().value()).isEqualTo("simple text");
        assertThat(((NoteCreated) createdEvent.get(0)).noteId().id()).isNotEmpty();
        assertThat(((NoteCreated) createdEvent.get(0)).creator().creatorId().id()).isEqualTo("1");
        assertThat(((NoteCreated) createdEvent.get(0)).creator().name()).isEqualTo("nick name");
    }
    
    @Test
    public void create_note_with_null_text() {
        // Given
        final var factory = new StandardNoteFactory();
        final var createCommand = CreateNote.builder()
                .withText(null)
                .withCreator(new CreatorImpl(new CreatorIdImpl("1"), "nick name"))
                .build();
        
        // When
        assertThrows(IllegalArgumentException.class, () -> factory.create(createCommand));
    }
    
    @Test
    public void create_note_with_null_creator() {
        // Given
        final var factory = new StandardNoteFactory();
        final var createCommand = CreateNote.builder()
                .withText("simple data")
                .withCreator(null)
                .build();
        
        // When
        assertThrows(IllegalArgumentException.class, () -> factory.create(createCommand));
    }
}
