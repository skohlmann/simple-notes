/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.model.note;

import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static de.speexx.simplenotes.domain.impl.standard.model.note.NoteImpl.MAX_NOTE_LENGTH;
import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author sascha.kohlmann
 */
public class TextImplTest {
    
    @Test
    public void create_with_null() {
        // When
        final var ex = assertThrows(IllegalArgumentException.class, () -> new TextImpl(null));
        
        // Then
        assertThat(ex.getLocalizedMessage()).isEqualTo("Text value must be provided");
    }

    @Test
    public void create_with_blank() {
        // When
        final var ex = assertThrows(IllegalArgumentException.class, () -> new TextImpl("\t "));
        
        // Then
        assertThat(ex.getLocalizedMessage()).isEqualTo("Text value must not be whitespace only");
    }

    @Test
    public void create_with_too_long_value() {
        // Given
        final var value = IntStream.range(0, 240 + 1).mapToObj(c -> "a").collect(joining());

        // When
        final var ex = assertThrows(IllegalArgumentException.class, () -> new TextImpl(value));
        
        // Then
        assertThat(ex.getLocalizedMessage()).isEqualTo("Text value must be have a maximum length of " + MAX_NOTE_LENGTH + ". Is: 241");
    }
}
