<#if licenseFirst??>
${licenseFirst}
</#if>
${licensePrefix}This file is part of the SpeexX Simple Notes Project.
${licensePrefix}
${licensePrefix}SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
${licensePrefix}it under the terms of the GNU Affero General Public License as published by the
${licensePrefix}Free Software Foundation, either version 3 of the License, or any later version.
${licensePrefix}
${licensePrefix}SpeexX Simple Notes Project is distributed in the hope that it will be,
${licensePrefix}useful but WITHOUT ANY WARRANTY; without even the implied warranty of
${licensePrefix}MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
${licensePrefix}GNU Affero General Public License for more details.
${licensePrefix}
${licensePrefix}You should have received a copy of the GNU Affero General Public License along
${licensePrefix}along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
<#if licenseLast??>
${licenseLast}
</#if>
