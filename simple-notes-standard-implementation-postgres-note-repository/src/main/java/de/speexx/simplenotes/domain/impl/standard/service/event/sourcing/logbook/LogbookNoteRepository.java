/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.service.event.sourcing.logbook;

import de.speexx.common.executor.TrialExecutor;
import de.speexx.event.sourcing.Source;
import de.speexx.event.sourcing.store.logbook.Logbook;
import de.speexx.simplenotes.domain.model.note.Note;
import de.speexx.simplenotes.domain.model.note.NoteEvent;
import de.speexx.simplenotes.domain.model.note.NoteId;
import de.speexx.simplenotes.domain.service.note.NoteEvents;
import de.speexx.simplenotes.domain.service.note.NoteFactory;
import de.speexx.simplenotes.domain.service.note.NoteRepository;
import de.speexx.simplenotes.domain.service.note.RepositoryException;
import java.util.Optional;

import static de.speexx.common.Assertions.assertNotNull;
import static de.speexx.event.sourcing.store.ResultType.FAILURE;

/**
 *
 * @author sascha.kohlmann
 */
public final class LogbookNoteRepository implements NoteRepository {
    
    private final Logbook<String> logbook;
    private final NoteFactory noteFactory;
    
    public LogbookNoteRepository(final Logbook<String> logbook, final NoteFactory noteFactory) {
        this.logbook = assertNotNull(logbook, "Logbook must be provided");
        this.noteFactory = assertNotNull(noteFactory, "NoteFactory must be provided");
    }

    /**
     * The provided events must implement the {@code Source} interface.
     * @param noteEvents the note events to add to the note stores
     * @throws RepositoryException besides the description of the interface
     *                             the exception will be thrown if the provided
     *                             events doesn't implement the {@code Source}
     *                             interface.
     * @see NoteRepository#add(java.util.List)
     */
    @Override
    public void add(final NoteEvents noteEvents) {
        final var sources = noteEvents.events().stream().map(event -> eventToSource(event)).toList();
        final var result = new LogbookAppendResultInterest();
        TrialExecutor.of().execute(() -> logbook().appendAll(noteEvents.noteId().id(), 1, sources, result));
        handleResult(result, noteEvents);
    }

    private void handleResult(final LogbookAppendResultInterest result, final NoteEvents noteEvents) throws RepositoryException {
        if (result.result().resultType() == FAILURE) {
            if (result.result().exception().isPresent()) {
                throw new RepositoryException("Problem during add operation", result.result().exception().get());
            }
            throw new RepositoryException("An unknown problem occurs while adding " + noteEvents);
        }
    }

    
    private Source eventToSource(final NoteEvent event) {
        if (event instanceof Source source) {
            return source;
        }
        throw new RepositoryException("NoteEvent not of type Source: " + event.getClass().getName());
    }
    
    @Override
    public Optional<Note> noteOf(final NoteId noteId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private Logbook logbook() {
        return this.logbook;
    }
}
