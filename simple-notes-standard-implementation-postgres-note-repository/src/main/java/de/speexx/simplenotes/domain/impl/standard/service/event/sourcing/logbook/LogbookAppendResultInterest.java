/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.impl.standard.service.event.sourcing.logbook;

import de.speexx.common.annotation.Generated;
import de.speexx.event.sourcing.Source;
import de.speexx.event.sourcing.store.StoreResult;
import de.speexx.event.sourcing.store.logbook.Logbook;
import java.util.List;
import java.util.Optional;

import static java.util.List.copyOf;

/**
 *
 * @author sascha.kohlmann
 */
public final class LogbookAppendResultInterest implements Logbook.AppendResultInterest {
    
    private StoreResult result;
    private String streamName;
    private List<Source> sources;

    @Override
    public <S, ST> void appendResultedIn(final StoreResult result, final String streamName, final int streamVersion, final List<Source<S>> sources, final Optional<ST> snapshot) {
        this.result = result;
        this.streamName = streamName;
        this.sources = copyOf(sources);
    }

    public StoreResult result() {
        return result;
    }

    public String streamName() {
        return streamName;
    }

    public List<Source> sources() {
        return sources;
    }

    @Override
    @Generated
    public String toString() {
        return "LogbookAppendResultInterest{" + "result=" + result + ", streamName=" + streamName + ", sources=" + sources + '}';
    }
}
