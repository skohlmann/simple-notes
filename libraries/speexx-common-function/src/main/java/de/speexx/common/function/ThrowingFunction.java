/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.function;

import java.util.function.Function;

/**
 * Exception throwing replacement of {@link Function}.
 * {@link de.speexx.common.function.Functions#rethrowUnchecked(de.speexx.common.rethrow.ThrowingFunction)} supports
 * rethrowing of an unchecked exception for a checked exception.
 * @author Sascha Kohlmann
 */
@FunctionalInterface
public interface ThrowingFunction<T, R, E extends Exception> {

    R apply(T t) throws E;
}
