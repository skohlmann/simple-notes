/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.function;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.function.Function;

import static de.speexx.common.Assertions.assertNotNull;

/**
 *
 * @author sascha.kohlmann
 */
public final class Functions {
    
    private Functions() { throw new AssertionError(); }
    
    /**
     * Creates a function which transforms all checked exceptions to unchecked expceptions.
     * In standard cases a {@link RuntimeException} is thrown if and only if
     * the thrown exception is not of type {@code RuntimeExceptiony}.
     * <p>If and only if the thrown exception is of type {@link IOException} the
     * implementation throws an {@link UncheckedIOException}.</p>
     * @param <T> type to apply
     * @param <E> exception to be thrown
     * @param func an exception throwing function
     * @return a function
     */
    public static <T, R, E extends Exception> Function<T, R> rethrowUnchecked(final ThrowingFunction<T, R, E> func) {
        assertNotNull(func, "Function must be provided");
        return t -> {
            try {
                return func.apply(t);
            } catch (final Exception e) {
                if (e instanceof RuntimeException runtimeException) {throw runtimeException;}
                if (e instanceof IOException ioException) {throw new UncheckedIOException(ioException);}
                throw new RuntimeException(e);
            }
        };
    }
}
