/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.function;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.function.Consumer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ThrowingConsumerTest {

    @Test
    public void rethrowRuntimeException() {
        final var toThrow = new IllegalStateException();
        
        try {
            createConsumer(toThrow).accept("");
        } catch (final Exception e) {
            assertSame(toThrow, e);
        }
    }

    @Test
    public void encapsulateInRuntimeException() {
        final var toThrow = new InterruptedException();
        
        try {
            createConsumer(toThrow).accept("");
        } catch (final Exception e) {
            assertSame(RuntimeException.class, e.getClass());
            assertSame(toThrow, e.getCause());
        }
    }

    @Test
    public void encapsulateIOExceptionInUncheckedIOException() {
        final var toThrow = new IOException();
        
        try {
            createConsumer(toThrow).accept("");
        } catch (final Exception e) {
            assertSame(UncheckedIOException.class, e.getClass());
            assertSame(toThrow, e.getCause());
        }
    }
    
    @Test
    public void failFast() {
        assertThrows(IllegalArgumentException.class, () -> Consumers.rethrowUnchecked(null));
    }

    Consumer<String> createConsumer(final Exception toThrow) {
        return Consumers.rethrowUnchecked((t) -> {throw toThrow;});
    }
}
