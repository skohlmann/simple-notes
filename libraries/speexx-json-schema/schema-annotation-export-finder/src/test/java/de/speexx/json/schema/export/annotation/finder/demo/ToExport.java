/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.export.annotation.finder.demo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import de.speexx.domain.annotation.PublishedLanguage;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * @author sascha.kohlmann
 */
@PublishedLanguage(version = "1")
@JsonRootName(value = "root-name")
public class ToExport {
    
    @Deprecated
    private static String MONSTER;
    
    {
        MONSTER = "monster";
    }

    public ToExport () {}
    
    protected String local;
    
    {{
        local = "test";
    }}
    
    @PublishedLanguage(version = "2", group = "test", name = "masterpiece")
    @Deprecated
    @JsonRootName("simple-name")
    public static final class Master {
        @JsonProperty("name")
        @NotNull
        private String name;
    }
    
    @IntegerAnnotation(values = {1, 2})
    class Local {}

    @Deprecated
    static final class Servant implements Serializable {
        @SuppressWarnings("why?")
        private String field;
    }
}
