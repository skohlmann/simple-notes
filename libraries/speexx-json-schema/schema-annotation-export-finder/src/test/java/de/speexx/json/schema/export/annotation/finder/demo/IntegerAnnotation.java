/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.export.annotation.finder.demo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.SOURCE;
import java.lang.annotation.Target;

/**
 *
 * @author sascha.kohlmann
 */
@Retention(SOURCE)
@Target({ElementType.TYPE})
public @interface IntegerAnnotation {
    int[] values();
}
