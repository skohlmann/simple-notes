/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.export.annotation.finder;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.schema.export.annotation.finder.demo.ToExport;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class SchemaExportingClassFinderTest {
    
    @Test
    public void extractSchema() {
        
        // Given
        final Path sourcePath = Paths.get(System.getProperty("schema-export-annotated-classes"));
        
        // When
        final Set<SchemaExportingClass> exportingClasses = new SchemaExportingClassFinder().findClasses(sourcePath);

        // Then
        assertThat(exportingClasses.size()).isEqualTo(2);
        
        exportingClasses.forEach(exporting -> {
            if (exporting.className().equals(ToExport.class.getName())) {
                assertThat(exporting.isValidSchemaExportingClass()).isTrue();
                assertThat(exporting.attributes().get().version().version()).isEqualTo("1");
                assertThat(exporting.attributes().get().group().group().isEmpty()).isTrue();
                assertThat(exporting.attributes().get().name().name().isEmpty()).isTrue();
            } else if (exporting.className().equals(ToExport.Master.class.getName())) {
                assertThat(exporting.isValidSchemaExportingClass()).isTrue();
                assertThat(exporting.attributes().get().version().version()).isEqualTo("2");
                assertThat(exporting.attributes().get().group().group()).isEqualTo("test");
                assertThat(exporting.attributes().get().name().name()).isEqualTo("masterpiece");
            }
        });
    }
}
