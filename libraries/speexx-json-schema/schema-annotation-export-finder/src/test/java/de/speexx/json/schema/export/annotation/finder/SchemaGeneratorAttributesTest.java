/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.export.annotation.finder;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.schema.export.annotation.finder.SchemaGeneratorAttributes.Group;
import de.speexx.json.schema.export.annotation.finder.SchemaGeneratorAttributes.Name;
import de.speexx.json.schema.export.annotation.finder.SchemaGeneratorAttributes.Version;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class SchemaGeneratorAttributesTest {
    
    @Test
    public void create_simple_Version() {
        // When
        final Version v = new Version("v");
        
        // Then
        assertThat(v.version()).isEqualTo("v");
        
    }

    @Test
    public void createVersion() {
        assertThrows(IllegalArgumentException.class , () -> new Version(" "));
    }
    
    @Test
    public void create_SchemaGeneratorAttributes_with_all_parameters() {
        // Given
        final Version v = new Version("v");
        final Group g = new Group("g");
        final Name n = new Name("n");
        
        // When
        final SchemaGeneratorAttributes attributes = new SchemaGeneratorAttributes(v, n, g);

        // Then
        assertThat(attributes.version()).isEqualTo(v);
        assertThat(attributes.group()).isEqualTo(g);
        assertThat(attributes.name()).isEqualTo(n);
    }
    
    @Test
    public void create_SchemaGeneratorAttributes_with_minimum_required_parameters() {
        // Given
        final Version v = new Version("v");
        
        // When
        final SchemaGeneratorAttributes attributes = new SchemaGeneratorAttributes(v, null, null);

        // Then
        assertThat(attributes.group()).isEqualTo(Group.DEFAULT_GROUP);
        assertThat(attributes.name()).isEqualTo(Name.DEFAULT_NAME);
    }
    
    @Test
    public void create_SchemaGeneratorAttributes_with_null_version() {
        // Then
        assertThrows(NullPointerException.class, () -> new SchemaGeneratorAttributes(null, null, null));
    }
}
