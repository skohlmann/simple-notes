/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.export.annotation.finder;

import de.speexx.common.annotation.Generated;
import de.speexx.common.annotation.Immutable;
import java.util.Objects;
import java.util.Optional;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 * Holder or the {@code de.speexx.domain.annotation.PublishedLanguage} annotation properties.
 * @author sascha.kohlmann
 */
@Immutable
@API(status=EXPERIMENTAL)
public final class SchemaGeneratorAttributes {
    
    private final Version version;
    private final Group group;
    private final Name name;

    SchemaGeneratorAttributes(final Version version, final Name name, final Group group) {
        this.version = Objects.requireNonNull(version, "Version must be provided");
        this.name = name != null ? name : Name.DEFAULT_NAME;
        this.group = group != null ? group : Group.DEFAULT_GROUP;
    }
    
    /**
     * The version property content.
     * @return the version. Never {@code null}.
     */
    public Version version() {
        return this.version;
    }

    /**
     * The name property content.
     * @return the name. Never {@code null}.
     */
    public Name name() {
        return this.name;
    }

    /**
     * The group property content.
     * @return the group. Never {@code null}.
     */
    public Group group() {
        return this.group;
    }

    /**
     * Represents the version of a {@code SchemaExport}.
     * A version is never {@code null} or {@linkplain String#isBlank() blank}.
     */
    public static final class Version {

        private String version;

        Version(final String version) {
            this.version = Objects.requireNonNull(version, "Version must be provided");
            if (this.version.isBlank()) {
                throw new IllegalArgumentException("Version can't be an empty String");
            }
        }

        /**
         * @return the version
         */
        public String version() {
            return this.version;
        }

        @Override
        @Generated
        public int hashCode() {
            int hash = 7;
            hash = 37 * hash + Objects.hashCode(this.version);
            return hash;
        }

        @Override
        @Generated
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Version other = (Version) obj;
            if (!Objects.equals(this.version, other.version)) {
                return false;
            }
            return true;
        }

        @Override
        @Generated
        public String toString() {
            return "Version{" + "version=" + version + '}';
        }
    }

    @Override
    @Generated
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.version);
        hash = 97 * hash + Objects.hashCode(this.group);
        hash = 97 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SchemaGeneratorAttributes other = (SchemaGeneratorAttributes) obj;
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        if (!Objects.equals(this.group, other.group)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    @Generated
    public String toString() {
        return "SchemaExportAttributes{" + "version=" + version + ", group=" + group + ", name=" + name + '}';
    }

    /**
     * Represents the group of a {@code SchemaExport}.
     */
    public static final class Group {

        static final Group DEFAULT_GROUP = new Group(null);

        private String group;

        Group(final String group) {
            this.group = group;
        }

        /**
         * @return an optional group
         */
        public Optional<String> group() {
            return Optional.ofNullable(this.group);
        }

        @Override
        @Generated
        public int hashCode() {
            int hash = 5;
            hash = 97 * hash + Objects.hashCode(this.group);
            return hash;
        }

        @Override
        @Generated
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Group other = (Group) obj;
            if (!Objects.equals(this.group, other.group)) {
                return false;
            }
            return true;
        }

        @Override
        @Generated
        public String toString() {
            return "Group{" + "group=" + group + '}';
        }
    }

    /**
     * Represents the name of a {@code SchemaExport}.
     */
    public static final class Name {
        
        static final Name DEFAULT_NAME = new Name(null);

        private String name;

        Name(final String name) {
            this.name = name;
        }
        
        /**
         * @return an optional name
         */
        public Optional<String> name() {
            return Optional.ofNullable(this.name);
        }

        @Override
        @Generated
        public int hashCode() {
            int hash = 5;
            hash = 29 * hash + Objects.hashCode(this.name);
            return hash;
        }

        @Override
        @Generated
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Name other = (Name) obj;
            if (!Objects.equals(this.name, other.name)) {
                return false;
            }
            return true;
        }

        @Override
        @Generated
        public String toString() {
            return "Name{" + "name=" + name + '}';
        }
    }
}
