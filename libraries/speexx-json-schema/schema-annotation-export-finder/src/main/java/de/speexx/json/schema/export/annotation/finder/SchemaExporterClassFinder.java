/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.export.annotation.finder;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import de.speexx.common.annotation.Mutable;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 * Analyse a Java source file for availability of the {@code de.speexx.domain.annotation.PublishedLanguage} 
 * annotation. Descriptios of the classes found are generated if at least one class with {@code PublishedLanguage} is available
 * in the source file.
 * <p>Not thread safe.</p>
 * @author sascha.kohlmann
 */
@Mutable
@API(status=EXPERIMENTAL)
public class SchemaExporterClassFinder {

    private static final String SCHEMA_EXPORTER_PACKAGE_NAME = "de.speexx.domain.annotation";
    private static final String SCHEMA_EXPORTER_SIMPLE_NAME = "PublishedLanguage";
    private static final String SCHEMA_EXPORTER_CLASS_NAME = SCHEMA_EXPORTER_PACKAGE_NAME + "." + SCHEMA_EXPORTER_SIMPLE_NAME;
    
    private static final String SCHEMA_EXPORTER_NAME_ATTRIBUTE_NAME = "name";
    private static final String SCHEMA_EXPORTER_VERSION_ATTRIBUTE_NAME = "version";
    private static final String SCHEMA_EXPORTER_GROUP_ATTRIBUTE_NAME = "group";
    
    private final Set<SchemaExportingClass> classNames = new HashSet<>();
    private final AtomicReference<String> schemaExportImport = new AtomicReference<>();

    /** Analyse a Java source file for availability of the {@code de.speexx.domain.annotation.PublishedLanguage} 
     * annotation.
     * @param javaSourceFile the path to the source file
     * @return a set of {@code SchemaExportingClass} or empty if no {@code SchemaExport} annotation found. Never {@code null}
     */
    public Set<SchemaExportingClass> findSchemaExporterAnnotatedClasses(final Path javaSourceFile) {
        
        try {
            final CompilationUnit compilationUnit = StaticJavaParser.parse(javaSourceFile);

            compilationUnit.walk(Node.TreeTraversal.DIRECT_CHILDREN, n -> {
                doWalkForClassOrInterfaceDeclaration(n, null);
            });

        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
        
        return this.classNames;
    }
    
    void doWalkForClassOrInterfaceDeclaration(final Node n, final SchemaExportingClass candidate) {

        if (n instanceof ImportDeclaration) {
            final String importValue = ((ImportDeclaration) n).getNameAsString();
            if (SCHEMA_EXPORTER_PACKAGE_NAME.equals(importValue) || SCHEMA_EXPORTER_CLASS_NAME.equals(importValue)) {
                this.schemaExportImport.set(SCHEMA_EXPORTER_SIMPLE_NAME);
            }

        } else if (n instanceof ClassOrInterfaceDeclaration) {
            final var clDecl = (ClassOrInterfaceDeclaration) n;
            final var schemaClassCandidate = new SchemaExportingClass(clDecl.getFullyQualifiedName().get(), clDecl.isNestedType());
            
            clDecl.walk(Node.TreeTraversal.DIRECT_CHILDREN, schemaClassCandidateNode -> {
                doWalkForClassOrInterfaceDeclaration(schemaClassCandidateNode, schemaClassCandidate);
            });

        }  else if (n instanceof NormalAnnotationExpr && isSchemaExportAnnotation((AnnotationExpr) n) && candidate != null) {

            String version = null;
            String group = null;
            String name = null;

            for (final MemberValuePair pair : ((NormalAnnotationExpr) n).getPairs()) {
                final var pairName = pair.getNameAsString();
                final var valueExpression = pair.getValue();
                
                if (valueExpression instanceof StringLiteralExpr) {
                    final var value = ((StringLiteralExpr) valueExpression).getValue();
                    switch(pairName) {
                        case SCHEMA_EXPORTER_NAME_ATTRIBUTE_NAME:    name = value;    break;
                        case SCHEMA_EXPORTER_GROUP_ATTRIBUTE_NAME:   group = value;   break;
                        case SCHEMA_EXPORTER_VERSION_ATTRIBUTE_NAME: version = value; break;
                        default: break; // ignore unknown attributes
                    }
                }
            }
            final SchemaExportingClass clazz = new SchemaExportingClass(candidate,
                    new SchemaGeneratorAttributes(new SchemaGeneratorAttributes.Version(version),
                                               new SchemaGeneratorAttributes.Name(name),
                                               new SchemaGeneratorAttributes.Group(group)));
            this.classNames.add(clazz);
        }
    }
    
    boolean isSchemaExportAnnotation(final AnnotationExpr annotationExpression) {
        final var name = annotationExpression.getNameAsString();
        if (SCHEMA_EXPORTER_CLASS_NAME.equals(name)) {
            return true;
        }
        
        return this.schemaExportImport.get() != null && SCHEMA_EXPORTER_SIMPLE_NAME.equals(name);
    }
}
