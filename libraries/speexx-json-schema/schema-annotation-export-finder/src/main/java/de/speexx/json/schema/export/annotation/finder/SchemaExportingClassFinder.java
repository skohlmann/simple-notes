/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.export.annotation.finder;

import de.speexx.common.annotation.Immutable;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 * Tree walker through a tree of Java source classed to find all classes annotated with
 * {@code de.speexx.domain.annotation.PublishedLanguage} .
 * @author sascha.kohlmann
 */
@Immutable
@API(status=EXPERIMENTAL)
public final class SchemaExportingClassFinder {

    /**
     * Walks trough the given directory tree to find all class annotated with
     * {@code de.speexx.domain.annotation.PublishedLanguage}.
     * @param rootDirectory the root directory to search all schema exporting classes.
     * @return a set of {@code SchemaExportingClass}. May be empty. Never {@code null}.
     */
    public Set<SchemaExportingClass> findClasses(final Path rootDirectory) {
        try {
            return Files.walk(rootDirectory)
                    .filter(path -> path.getFileName().toString().endsWith(".java"))
                    .map(path -> toSchemaExportingClasses(path))
                    .filter(classes -> classes != null)
                    .flatMap(classes -> classes.stream())
                    .collect(Collectors.toSet());
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }
    
    Set<SchemaExportingClass> toSchemaExportingClasses(final Path javaSourceFile) {
        return new SchemaExporterClassFinder().findSchemaExporterAnnotatedClasses(javaSourceFile);
    }
}
