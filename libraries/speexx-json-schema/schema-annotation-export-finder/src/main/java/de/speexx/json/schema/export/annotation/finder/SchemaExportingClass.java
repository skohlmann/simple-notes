/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.export.annotation.finder;

import de.speexx.common.annotation.Generated;
import de.speexx.common.annotation.Immutable;
import java.util.Objects;
import java.util.Optional;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 * Description of a class annotated with {@code de.speexx.domain.annotation.PublishedLanguage} .
 * @author sascha.kohlmann
 * @see de.speexx.domain.annotation.PublishedLanguage
 */
@Immutable
@API(status=EXPERIMENTAL)
public final class SchemaExportingClass {

    private final String className;
    private final boolean nested;
    private SchemaGeneratorAttributes attributes;
    
    SchemaExportingClass(final String className, final boolean nested) {
        this(className, null, nested);
    }
    
    private SchemaExportingClass(final String className, final SchemaGeneratorAttributes attributes, final boolean nested) {
        this.className = Objects.requireNonNull(className, "Class name must be provided");
        this.attributes = attributes;
        this.nested = nested;
    }
    
    SchemaExportingClass(final SchemaExportingClass clazz, final SchemaGeneratorAttributes attributes) {
        this(clazz.className(), attributes, clazz.isNested());
    }

    /** The name of the annotated class.
     * @return the class name */
    public String className() {
        return this.className;
    }
    
    public boolean isNested() {
        return this.nested;
    }

    /**
     * Available attributes of the class. Represents the properties of {@code de.speexx.domain.annotation.PublishedLanguage} .
     * @return the attributes of {@code SchemaExport}
     */
    public Optional<SchemaGeneratorAttributes> attributes() {
        return Optional.ofNullable(this.attributes);
    }

    /**
     * An instance of this class only represents a valid schema exporting class if this method returns {@code true}. Otherwise
     * relevant information are missed.
     * @return {@code true} if and only if the represented class is a valid schema exporting class. {@code false} otherwise.
     */
    public boolean isValidSchemaExportingClass() {
        return attributes().isPresent();
    }

    @Override
    @Generated
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.className);
        return hash;
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SchemaExportingClass other = (SchemaExportingClass) obj;
        if (!Objects.equals(this.className, other.className)) {
            return false;
        }
        return true;
    }

    @Override
    @Generated
    public String toString() {
        return "SchemaExportingClass{" + "className=" + className + ", nested=" + nested + ", attributes=" + attributes + '}';
    }
}
