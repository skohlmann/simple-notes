/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.generator.demo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import de.speexx.domain.annotation.PublishedLanguage;
import jakarta.validation.constraints.Pattern;
import java.io.Serializable;
import java.net.URI;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 *
 * @author sascha.kohlmann
 */
@PublishedLanguage(version = "1")
@JsonRootName(value = "root-name")
public class ToExport {
    
    @Deprecated
    private static String MONSTER;
    
    {
        MONSTER = "monster";
    }

    public ToExport () {}
    
    protected String local;
    
    {{
        local = "test";
    }}
    
    @Deprecated
    @JsonRootName("simple-name")
    @PublishedLanguage(version = "2", group = "test", name = "masterpiece")
    public static final class Master {
        @JsonProperty("uri")
        @jakarta.validation.constraints.NotNull
        private URI uri;
        
        @JsonProperty
        @javax.validation.constraints.NotNull
        @Pattern(regexp="^(?<fullyear>\\d{4})-(?<month>0[1-9]|1[0-2])-(?<mday>0[1-9]|[12][0-9]|3[01])T(?<hour>[01][0-9]|2[0-3]):(?<minute>[0-5][0-9]):(?<second>[0-5][0-9]|60)(?<secfrac>\\.[0-9]+)?(Z|(\\+|-)(?<offsethour>[01][0-9]|2[0-3]):(?<offsetminute>[0-5][0-9]))$")
        private OffsetDateTime datetime;

        @JsonProperty
        private UUID uuid;

        @JsonProperty
        @javax.validation.constraints.NotNull
        @Pattern(regexp = "[A-za-z0-9]+")
        private String pattern;
    }
    
    @IntegerAnnotation(values = {1, 2})
    class Local {}

    @Deprecated
    static final class Servant implements Serializable {
        @SuppressWarnings("why?")
        private String field;
    }
}
