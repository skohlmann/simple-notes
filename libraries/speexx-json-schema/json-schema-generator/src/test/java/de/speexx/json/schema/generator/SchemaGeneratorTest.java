/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.generator;

import de.speexx.json.adapter.JsonAdapter;
import de.speexx.json.adapter.JsonPointerSupport;
import de.speexx.json.schema.generator.demo.ToExport;
import java.io.StringWriter;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;


public class SchemaGeneratorTest {

    @Test
    public void simple_export() throws Exception {
        // Given
        final var writer = new StringWriter();
        
        // When
        SchemaGenerator.of().createSchema(ToExport.Master.class, writer);
        final var schema = writer.toString();
        final var json = new JsonAdapter().jsonFrom(schema);

        // Then
        assertThat(support().stringForPointer(json, "/$schema").get()).isEqualTo("http://json-schema.org/draft-07/schema#");
        assertThat(support().stringForPointer(json, "/definitions/Master/properties/datetime/type").get()).isEqualTo("string");
        assertThat(support().stringForPointer(json, "/definitions/Master/properties/datetime/format").get()).isEqualTo("date-time");
        
        assertThat(support().stringForPointer(json, "/definitions/Master/properties/pattern/pattern").get()).isEqualTo("[A-za-z0-9]+");
        
        assertThat(support().stringForPointer(json, "/definitions/Master/properties/uri/format").get()).isEqualTo("uri");

        assertThat(support().arrayForPointer(json, "/definitions/Master/required").asStringStream().collect(Collectors.toSet())).containsAnyOf("datetime", "pattern", "uri");
    }
    
    private JsonPointerSupport support() {
        return new JsonPointerSupport();
    }
}
