/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.generator;

import de.speexx.json.schema.generator.impl.victools.VictoolsSchemaGenerator;
import java.io.IOException;
import java.io.Writer;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * Generates a JSON schema (<a href='https://json-schema.org/'>Draft #7 or higher</a>).
 * The generator supports the FasterXML Jackson JSON annotations and the Javax Bean Validation (version 2.0) annotations.
 * @author sascha.kohlmann
 */
@API(status=STABLE)
public interface SchemaGenerator {

    /**
     * Generates a JSON schema (Draft #7 or higher) for the given class. 
     * @param clazz the class to generate the JSON schema for
     * @param target a writer to write the JSON schema to
     * @throws IOException if and only if it is not possible to write the schema.
     */
    void createSchema(final Class clazz, final Writer target) throws IOException;
    
    /**
     * Creates an instance.
     * @return a schema generator 
     */
    public static SchemaGenerator of() {
        return new VictoolsSchemaGenerator();
    }
}
