/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.schema.generator.impl.victools;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.victools.jsonschema.generator.Option;
import static com.github.victools.jsonschema.generator.OptionPreset.PLAIN_JSON;
import com.github.victools.jsonschema.generator.SchemaGeneratorConfigBuilder;
import static com.github.victools.jsonschema.generator.SchemaVersion.DRAFT_7;
import com.github.victools.jsonschema.generator.impl.module.SimpleTypeModule;
import com.github.victools.jsonschema.module.jackson.JacksonModule;
import com.github.victools.jsonschema.module.jakarta.validation.JakartaValidationModule;
import com.github.victools.jsonschema.module.jakarta.validation.JakartaValidationOption;
import com.github.victools.jsonschema.module.javax.validation.JavaxValidationModule;
import com.github.victools.jsonschema.module.javax.validation.JavaxValidationOption;
import de.speexx.common.annotation.Immutable;
import de.speexx.json.schema.generator.SchemaGenerator;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;


/**
 * Generates a JSON schema (<a href='https://json-schema.org/'>Draft #7 or higher</a>).
 * The generator supports the FasterXML Jackson JSON annotations and the Javax Bean validation annotations.
 * @author sascha.kohlmann
 */
@Immutable
@API(status=STABLE)
public class VictoolsSchemaGenerator implements SchemaGenerator {

    /**
     * Generates a JSON schema (Draft #7 or higher) for the given class. 
     * @param clazz the class to generate the JSON schema for
     * @param target a writer to write the JSON schema to
     * @throws IOException if and only if it is not possible to write the schema.
     */
    @Override
    public void createSchema(final Class clazz, final Writer target) throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter();
        final var configBuilder = new SchemaGeneratorConfigBuilder(objectMapper, DRAFT_7, PLAIN_JSON);
        final var config = configBuilder.with(Option.SCHEMA_VERSION_INDICATOR,
                                              Option.DEFINITION_FOR_MAIN_SCHEMA,
                                              Option.ADDITIONAL_FIXED_TYPES,
                                              Option.EXTRA_OPEN_API_FORMAT_VALUES,
                                              Option.PLAIN_DEFINITION_KEYS)
                                        .without(Option.NONSTATIC_NONVOID_NONGETTER_METHODS,
                                                 Option.PLAIN_DEFINITION_KEYS,
                                                 Option.TRANSIENT_FIELDS,
                                                 Option.VOID_METHODS)
                                        .with(new JacksonModule())
                                        .with(SimpleTypeModule.forPrimitiveAndAdditionalTypes().withStringType(URI.class, "uri"))
                                        .with(new JavaxValidationModule(JavaxValidationOption.INCLUDE_PATTERN_EXPRESSIONS, JavaxValidationOption.NOT_NULLABLE_FIELD_IS_REQUIRED))
                                        .with(new JakartaValidationModule(JakartaValidationOption.INCLUDE_PATTERN_EXPRESSIONS, JakartaValidationOption.NOT_NULLABLE_FIELD_IS_REQUIRED))
                                        .build();
        final var generator = new com.github.victools.jsonschema.generator.SchemaGenerator(config);
        final var jsonSchema = generator.generateSchema(clazz); 

        target.write(jsonSchema.toPrettyString());
        target.flush();
    }
}
