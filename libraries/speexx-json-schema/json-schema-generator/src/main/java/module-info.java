/*
 * This file is part of the SpeexX JSON Schema.
 * 
 * SpeexX JSON Schema is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Schema is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
 */

module speexx.json.schema.generator {
    requires com.fasterxml.jackson.databind;
    requires jakarta.validation;
    requires java.validation;
    requires jsonschema.generator;
    requires jsonschema.module.jakarta.validation;
    requires jsonschema.module.javax.validation;
    requires jsonschema.module.jackson;
    requires speexx.common;
    requires speexx.common.annotation;
    requires org.apiguardian.api;

    exports de.speexx.json.schema.generator;
}
