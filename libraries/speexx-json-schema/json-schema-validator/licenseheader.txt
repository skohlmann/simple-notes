<#if licenseFirst??>
${licenseFirst}
</#if>
${licensePrefix}This file is part of the SpeexX JSON Schema.
${licensePrefix}
${licensePrefix}SpeexX JSON Schema is free software: you can redistribute it and/or modify
${licensePrefix}it under the terms of the GNU General Public License as published by the
${licensePrefix}Free Software Foundation, either version 3 of the License, or any later version.
${licensePrefix}
${licensePrefix}SpeexX JSON Schema is distributed in the hope that it will be useful,
${licensePrefix}but WITHOUT ANY WARRANTY; without even the implied warranty of
${licensePrefix}MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
${licensePrefix}GNU General Public License for more details.
${licensePrefix}
${licensePrefix}You should have received a copy of the GNU General Public License along
${licensePrefix}with SpeexX JSON Schema. If not, see <https://www.gnu.org/licenses/>.
<#if licenseLast??>
${licenseLast}
</#if>
