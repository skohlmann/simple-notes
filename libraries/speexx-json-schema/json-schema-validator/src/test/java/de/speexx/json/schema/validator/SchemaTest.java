package de.speexx.json.schema.validator;

import de.speexx.json.schema.validator.ValidationException;
import de.speexx.json.schema.validator.Schema;
import static com.google.common.truth.Truth.assertThat;
import static de.speexx.json.schema.validator.Schema.of;
import static java.util.Arrays.fill;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class SchemaTest {

    @Test
    public void schema_not_null() throws Exception {
        assertThat(Schema.of(SIMPLE_SCHEMA)).isNotNull();
    }

    @Test
    public void valid_schema_test() throws Exception {
        // Given
        final Schema schema = Schema.of(SIMPLE_SCHEMA);
        
        // Then
        assertDoesNotThrow(() -> schema.validate("{\"name\":\"09\"}"));
    }
    
    @Test
    public void invalid_schema_test_1() throws Exception {
        // Given
        final Schema schema = Schema.of(SIMPLE_SCHEMA);
        
        // Then
        final ValidationException ex = assertThrows(ValidationException.class, () -> schema.validate("{\"name\":\"\"}"));
    }

    @Test
    public void invalid_schema_test_2() throws Exception {
        // Given
        final Schema schema = Schema.of(SIMPLE_SCHEMA);
        
        // Then
        final ValidationException ex = assertThrows(ValidationException.class, () -> schema.validate("{\"name\":\"0a\"}"));
    }

    @Test
    public void schema_too_big() throws Exception {
        // Given
        final var array = new char[250_001];
        fill(array, 'd');
        final var schema = new String(array);        
        
        // Then
        final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> of(schema));
    }
    
    private final static String SIMPLE_SCHEMA = 
            "{\n" +
            "  \"$schema\" : \"http://json-schema.org/draft-07/schema#\",\n" +
            "  \"type\" : \"object\",\n" +
            "  \"properties\" : {\n" +
            "    \"name\" : {\n" +
            "      \"type\" : \"string\",\n" +
            "      \"pattern\" : \"^[0-9]{2}\"\n" +
            "    }\n" +
            "  },\n" +
            "  \"required\" : [ \"name\" ]\n" +
            "}";
}
