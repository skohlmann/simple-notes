package de.speexx.json.schema.validator.demo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;

/**
 *
 * @author sascha.kohlmann
 */
@JsonRootName(value = "root-name")
public class ToExport {
    
    @JsonProperty("name")
    @NotBlank
    @Pattern(regexp="[0-9]{1,2}")
    private String name;
}
