package de.speexx.json.schema.validator.impl.everit;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import static java.util.stream.Collectors.joining;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class EveritSchemaValidatorTest {
    
    @Test
    public void remove_null_fields() {
        // Given
        final var schema = fromResource("/order-created.schema.json");
        final var validator = new EveritSchemaValidator(schema);
        
        final var json = fromResource("/order-created.event.json");
        
        // Then
        validator.validate(json);
    }

    String fromResource(final String resource) {
        final var stream = getClass().getResourceAsStream(resource);
        final var reader = new InputStreamReader(stream);
        final var buffered = new BufferedReader(reader);
        return buffered.lines().collect(joining(System.lineSeparator()));
    }
}
