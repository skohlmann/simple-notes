package de.speexx.json.schema.validator;

import de.speexx.json.schema.validator.ValidationException;
import static com.google.common.truth.Truth.assertThat;
import static java.lang.System.lineSeparator;
import static java.util.Arrays.asList;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class ValidationExceptionTest {

    @Test
    public void create_with_null_message() {
        // When
        final ValidationException ex = new ValidationException((String) null);
        
        // Then
        assertThat(ex.getMessage()).isNull();
    }
    
    @Test
    public void create_with_null_all_messages() {
        // When
        final ValidationException ex = new ValidationException((List<String>) null);
        
        // Then
        assertThat(ex.getMessage()).isNull();
    }
    
    @Test
    public void create_with_simple_message() {
        // When
        final ValidationException ex = new ValidationException("test");
        
        // Then
        assertThat(ex.getMessage()).isEqualTo("test");
        assertThat(ex.getLocalizedMessage()).isEqualTo("test");
    }
    
    @Test
    public void create_with_message_list() {
        // When
        final ValidationException ex = new ValidationException(asList("test", "msg"));
        
        // Then
        assertThat(ex.getMessage()).isEqualTo("test" + lineSeparator() + "msg");
        
        // And Then
        assertThat(ex.allMessages().size()).isEqualTo(2);
        assertThat(ex.allMessages().get(0)).isEqualTo("test");
        assertThat(ex.allMessages().get(1)).isEqualTo("msg");
    }
    
    @Test
    public void create_with_default_constructor() {
        // When
        final ValidationException ex = new ValidationException();
        
        // Then
        assertThat(ex.getMessage()).isNull();
    }
}
