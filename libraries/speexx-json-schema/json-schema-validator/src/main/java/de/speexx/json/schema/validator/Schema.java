package de.speexx.json.schema.validator;

import de.speexx.json.schema.validator.impl.everit.EveritSchemaValidator;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * Simple <a href='https://json-schema.org/'>JSON Schema</a> validator.
 * <p>Supports currently Draft #7 of JSON schema or lower.</p>
 * @author sascha.kohlmann
 */
@API(status=STABLE)
public interface Schema {
    
    /**
     * Validates a given <em>json</em> against the also given <em>schema</em>.
     * @param json a serialized JSON object to validate
     * @throws ValidationException if the validation fails
     */
    public abstract void validate(final String json) throws ValidationException;

    /**
     * Creates a new {@code Schema} instance based on the descriptor.
     * @param schemaDescriptor the schema descriptor.
     * @return a schema
     * @throws IllegalArgumentException if and only if schema is to big (&gt; 250.000 characters)
     */
    public static Schema of(final String schemaDescriptor) {
        if (schemaDescriptor.length() > 250_000) {
            throw new IllegalArgumentException("Scheme too big (" + schemaDescriptor.length() + "). Maximum 250,000 characters are allowed.");
        }
        return new EveritSchemaValidator(schemaDescriptor);
    }
}
