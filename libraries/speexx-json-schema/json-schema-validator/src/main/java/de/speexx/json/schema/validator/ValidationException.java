package de.speexx.json.schema.validator;

import de.speexx.common.annotation.Nullable;
import static java.lang.System.lineSeparator;
import static java.util.Arrays.asList;
import java.util.List;
import java.util.stream.Collectors;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;

/**
 *
 * @author sascha.kohlmann
 */
@API(status=STABLE)
public class ValidationException extends RuntimeException {

    private final List<String> messages;
    
    /**
     * Creates a new instance of <code>ValidationException</code> without detail message.
     */
    public ValidationException() {
        this((List) null    );
    }

    /**
     * Constructs an instance of <code>ValidationException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public ValidationException(@Nullable final String msg) {
        this(msg != null ? asList(msg) : List.of());
    }

    /**
     * Constructs an instance of <code>ValidationException</code> with a list of specified detail message.
     *
     * @param messages list of detail messages.
     */
        public ValidationException(@Nullable final List<String> messages) {
        if (messages != null) {
            this.messages = messages.stream().filter(msg -> msg != null).collect(Collectors.toList());
        } else {
            this.messages = List.of();
        }
    }

    /**
     * Returns a list with all messages.
     * @return never {@code null}
     */
    public List<String> allMessages() {
        return List.copyOf(this.messages);
    }
    
    @Override
    public String getMessage() {
        if (!this.messages.isEmpty()) {
            return this.messages.stream().collect(Collectors.joining(lineSeparator()));
        }
        return null;
    }
    
    /**
     * Calls {@link #getMessage() }
     * @return a message
     */
    @Override
    public String getLocalizedMessage() {
        return getMessage();
    }
}
