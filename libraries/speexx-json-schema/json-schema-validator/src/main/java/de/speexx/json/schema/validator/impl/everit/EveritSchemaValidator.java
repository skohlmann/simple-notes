package de.speexx.json.schema.validator.impl.everit;

import java.util.LinkedHashMap;
import java.util.Map;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import de.speexx.json.schema.validator.Schema;
import de.speexx.json.schema.validator.ValidationException;
import org.json.JSONArray;

/**
 *
 * @author sascha.kohlmann
 */
public final class EveritSchemaValidator implements Schema {
    
    private final static Map<String, org.everit.json.schema.Schema> CACHE = new LinkedHashMap<String, org.everit.json.schema.Schema>() {
        @Override
        protected boolean removeEldestEntry(final Map.Entry eldest) {
            return this.size() > 100;
        }
    };

    private final org.everit.json.schema.Schema validator;
    
    public EveritSchemaValidator(final String descriptor) {
        this.validator = fetchSchema(descriptor);
    }
    
    @Override
    public void validate(final String object) throws ValidationException {
        try {
            this.validator.validate(stringToJSONObject(object));
        } catch (final org.everit.json.schema.ValidationException e) {
            throw new ValidationException(e.getAllMessages());
        }
    }
    
    final org.everit.json.schema.Schema fetchSchema(final String schemaDescriptor) {
        final var schema = CACHE.get(schemaDescriptor);
        if (schema == null) {
            return putSchema(schemaDescriptor);
        }
        
        return schema;
    }

    org.everit.json.schema.Schema putSchema(final String schemaDescriptor) throws JSONException {
        final var schemaTokener = new JSONTokener(schemaDescriptor);
        final var schemaObject = new JSONObject(schemaTokener);
        final var loader = SchemaLoader.builder().schemaJson(schemaObject).build();
        final var validator = loader.load().build();
        CACHE.put(schemaDescriptor, validator);
        return validator;
    }

    JSONObject stringToJSONObject(final String toValidate) {
        final JSONTokener objectTokener = new JSONTokener(toValidate);
        final var jsonObject = new JSONObject(objectTokener);
        removeNullFields(jsonObject);
        return jsonObject;
    }
    
    /**
     * Remove 'null' fields from a JSONObject. This method calls itself recursively until all the
     * fields have been looked at. */
    private static void removeNullFields(Object object) throws JSONException {
        if (object instanceof JSONArray) {
            final var array = (JSONArray) object;
            for (int i = 0; i < array.length(); ++i) {
                removeNullFields(array.get(i));
            }
        } else if (object instanceof JSONObject) {
            final var json = (JSONObject) object;
            final var names = json.names();
            if (names == null) {
                return;
            }
            for (int i = 0; i < names.length(); ++i) {
                final var key = names.getString(i);
                if (json.isNull(key)) {
                   json.remove(key);
                } else {
                    removeNullFields(json.get(key));
                }
            }
        }
    }
}
