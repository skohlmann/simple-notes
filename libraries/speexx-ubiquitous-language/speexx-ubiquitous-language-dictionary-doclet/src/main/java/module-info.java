/*
 * This file is part of the SpeexX Ubiquitous Language.
 * 
 * SpeexX Ubiquitous Language is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Ubiquitous Language is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Ubiquitous Language. If not, see <https://www.gnu.org/licenses/>.
 */
module speexx.ubiquitous.language.doclet.dictionary {
    requires jdk.javadoc;
    
    exports de.speexx.ubiquitous.language.doclet.dictionary;
}
    