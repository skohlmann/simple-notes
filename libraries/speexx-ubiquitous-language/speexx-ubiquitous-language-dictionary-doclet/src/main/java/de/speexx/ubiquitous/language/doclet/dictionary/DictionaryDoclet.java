/*
 * This file is part of the SpeexX Ubiquitous Language.
 * 
 * SpeexX Ubiquitous Language is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Ubiquitous Language is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Ubiquitous Language. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.ubiquitous.language.doclet.dictionary;

import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.util.DocTrees;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.tools.Diagnostic;
import jdk.javadoc.doclet.Doclet;
import jdk.javadoc.doclet.DocletEnvironment;
import jdk.javadoc.doclet.Reporter;
import jdk.javadoc.doclet.StandardDoclet;

/**
 * Yet this is a hack!
 * @author sascha.kohlmann
 */
public class DictionaryDoclet extends StandardDoclet {

    private Reporter reporter;
    private String outputDirectory;

    @Override
    public void init(final Locale locale, final Reporter reporter) {
        this.reporter.print(Diagnostic.Kind.NOTE, "Doclet using locale: " + locale);
        this.reporter.print(Diagnostic.Kind.NOTE, "Source version: " + getSupportedSourceVersion());
        this.reporter = reporter;
    }

    @Override
    public boolean run(final DocletEnvironment docEnv) {
        this.reporter.print(Diagnostic.Kind.NOTE, "Module mode: " + docEnv.getModuleMode());
        this.reporter.print(Diagnostic.Kind.NOTE, "Dictionary file: " + asciiDocOutputFilePath());
        // get the DocTrees utility class to access document comments
        final DocTrees docTrees = docEnv.getDocTrees();

        final Map<String, DictionaryEntry> conceptDatas = new TreeMap<>((name1, name2) -> name1.compareTo(name2));
        
        this.reporter.print(Diagnostic.Kind.NOTE, "Pass 1: find dictionary entries");
        for (final TypeElement t : ElementFilter.typesIn(docEnv.getIncludedElements())) {
            if (t.getKind().isClass() || t.getKind().isInterface()) {
                findAllConcepts(t, docTrees, conceptDatas);
                findAllBehavior(t, docTrees, conceptDatas);
            }
        }
        
        final Map<String, DictionaryEntry> toRemove = new HashMap<>();
        for (final DictionaryEntry behaviorEntry : conceptDatas.values()) {
            if (behaviorEntry.type == DictionaryEntry.EntryType.BEHAVIOR) {
                conceptDatas.values().stream().filter((conceptEntry) -> (behaviorEntry.qualifiedName.equals(conceptEntry.qualifiedName))).map((conceptEntry) -> {
                    conceptEntry.behaviors.add(behaviorEntry);
                    return conceptEntry;
                }).map((item) -> {
                    behaviorEntry.behaviorInConcept = true;
                    return item;
                }).forEach((item) -> {
                    toRemove.put(behaviorEntry.entryName(), behaviorEntry);
                });
            }
        }
        toRemove.keySet().forEach(key -> conceptDatas.remove(key));
        
        this.reporter.print(Diagnostic.Kind.NOTE, "Pass 2: Search references");
        conceptDatas.values().stream().filter(conceptData -> conceptData.type == DictionaryEntry.EntryType.CONCEPT).forEach(entry -> {
            for (final Element element : entry.typeElement.getEnclosedElements()) {
                if (element.getKind() == ElementKind.FIELD) {
                    final Set<DictionaryEntry> references = 
                            findAllReferencesForFieldElement(element, new TreeSet<>((DictionaryEntry d1, DictionaryEntry d2) -> d1.name.compareTo(d2.name)), conceptDatas);
                    entry.contains.addAll(references);
                }
            }
        });
        
        this.reporter.print(Diagnostic.Kind.NOTE, "Pass 3: generate dictionary");
        
        final String asciiDoc = createAsciiDoc(conceptDatas).toString();
        final Path asciiDocOutFile = asciiDocOutputFilePath();

        try {
            Files.writeString(asciiDocOutFile, asciiDoc, Charset.forName("UTF-8"));
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
        
        return true;
    }

    void findAllBehavior(final TypeElement t, final DocTrees docTrees, final Map<String, DictionaryEntry> conceptDatas) {
        for (final Element enclosedElement : t.getEnclosedElements()) {
            if (enclosedElement.getKind() == ElementKind.METHOD || enclosedElement.getKind() == ElementKind.CONSTRUCTOR) {
                for (final AnnotationMirror annotationMirror : enclosedElement.getAnnotationMirrors()) {
                    final Element annotationElement = annotationMirror.getAnnotationType().asElement();
                    
                    if ("de.speexx.ubiquitous.language.annotation.Behavior".equals(annotationElement.toString())) {
                        final DictionaryEntry conceptData = new DictionaryEntry(DictionaryEntry.EntryType.BEHAVIOR);
                        conceptData.simpleName = t.getSimpleName().toString();
                        conceptData.qualifiedName = t.getQualifiedName().toString();
                        final Map<? extends ExecutableElement, ? extends AnnotationValue> values =  annotationMirror.getElementValues();
                        
                        for (final Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> value : values.entrySet()) {
                            if ("value".equals(value.getKey().getSimpleName().toString())) {
                                conceptData.dictionaryName = value.getValue().toString();
                            }
                        }
                        
                        if (conceptData.dictionaryName == null) {
                            conceptData.dictionaryName = enclosedElement.getSimpleName().toString();
                        }
                        
                        conceptData.dictionaryDocument = extractComment(docTrees, enclosedElement);
                        final DictionaryEntry cd = conceptDatas.put(conceptData.entryName(), conceptData);
                        if (cd != null) {
                            this.reporter.print(Diagnostic.Kind.ERROR.NOTE, "Dictonary name doubled in domain: " + cd.entryName());
                        }
                    }
                }
            }
        }
    }

    void findAllConcepts(final TypeElement t, final DocTrees docTrees, final Map<String, DictionaryEntry> conceptDatas) {
        for (final AnnotationMirror annotationMirror : t.getAnnotationMirrors()) {
            final Element annotationElement =  annotationMirror.getAnnotationType().asElement();
            if ("de.speexx.ubiquitous.language.annotation.Concept".equals(annotationElement.toString())) {
                final DictionaryEntry conceptData = new DictionaryEntry(DictionaryEntry.EntryType.CONCEPT);
                conceptData.simpleName = t.getSimpleName().toString();
                conceptData.qualifiedName = t.getQualifiedName().toString();
                conceptData.typeElement = t;
                final Map<? extends ExecutableElement, ? extends AnnotationValue> values =  annotationMirror.getElementValues();
                
                for (final Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> value : values.entrySet()) {
                    if ("value".equals(value.getKey().getSimpleName().toString())) {
                        conceptData.dictionaryName = value.getValue().toString();
                    } else if ("alternatives".equals(value.getKey().getSimpleName().toString())) {
                        conceptData.alternativeNames.add(value.getValue().toString());
                    }
                }
                
                conceptData.dictionaryDocument = extractComment(docTrees, t);
                conceptData.name = conceptData.entryName();
                final DictionaryEntry cd = conceptDatas.put(conceptData.entryName(), conceptData);
                if (cd != null) {
                    this.reporter.print(Diagnostic.Kind.ERROR.NOTE, "Dictonary name doubled in domain: " + cd.entryName());
                }
            }
        }
    }
    
    Set<DictionaryEntry> findAllReferencesForFieldElement(final Element fieldElement, final Set<DictionaryEntry> entries, final Map<String, DictionaryEntry> conceptDatas) {
        
        if (fieldElement.getKind().isField()) {
            if (fieldElement.asType().getKind() == TypeKind.DECLARED) {
                final DeclaredType dt = (DeclaredType) fieldElement.asType();
                final Element dtElement = dt.asElement();
                if (dtElement.getKind().isClass() || dtElement.getKind().isInterface()) {
                    final String name = ((TypeElement) dt.asElement()).getQualifiedName().toString();
                    final DictionaryEntry ref = isInSet(name, conceptDatas.values());
                    if (ref != null) {
                        entries.add(ref);
                    }
                } else {
                    for (final TypeMirror mirror : dt.getTypeArguments()) {
                        if (mirror.getKind() == TypeKind.DECLARED) {
                            final Element e = ((DeclaredType) mirror).asElement();
                            return findAllReferencesForFieldElement(e, entries, conceptDatas);
                        }
                    }
                }
            }
        } else if (fieldElement.getKind().isClass() || fieldElement.getKind().isInterface()) {
            final String name = ((TypeElement) fieldElement).getQualifiedName().toString();
            final DictionaryEntry ref = isInSet(name, conceptDatas.values());
            if (ref != null) {
                entries.add(ref);
            }
        }
        return entries;
    }
    
    DictionaryEntry isInSet(final String qualifiedName, final Collection<DictionaryEntry> entries) {
        for (final DictionaryEntry entry : entries) {
            if (entry.qualifiedName.equals(qualifiedName)) {
                return entry;
            }
        }
        
        return null;
    }

    String extractComment(final DocTrees docTrees, final Element element) {
        final StringBuilder sb = new StringBuilder();
        final DocCommentTree docCommentTree = docTrees.getDocCommentTree(element);
        
        if (docCommentTree != null) {
            final List<? extends DocTree> commentTree = docCommentTree.getFullBody();
            commentTree.forEach(entry -> sb.append(entry));
        }

        return removeLeadingWhitespaceIf(sb.toString());
    }
    
    String removeLeadingWhitespaceIf(final String comment) {
        final StringBuilder sb = new StringBuilder();
        
        comment.lines().forEach(line -> {
            if (line.length() > 0) {
                if (Character.isWhitespace(line.charAt(0))) {
                    if (line.length() > 1) {
                        sb.append(line.substring(1));
                    }
                } else {
                   sb.append(line);
                }
            } else {
                sb.append(line);
            }
            sb.append(System.lineSeparator());
        });
        
        return sb.toString();
    }
    
    Path outputDirectory() {
        final Path outFile = Paths.get(this.outputDirectory);
        if (outFile.endsWith("apidocs")) {
            return outFile.getParent();
        }
        return outFile;
    }

    Path markdownOutputFilePath() {
        return Paths.get(outputDirectory().toString(), "dictionary.rd");
    }

    Path asciiDocOutputFilePath() {
        return Paths.get(outputDirectory().toString(), "dictionary.adoc");
    }

    StringBuilder createAsciiDoc(final Map<String, DictionaryEntry> conceptDatas) {
        final StringBuilder sb = new StringBuilder();
        final int dictionaryTitleLength = getName().length();
        sb.append(":encoding: UTF-8\n\n");
        sb.append(getName()).append("\n").append(titleLine(dictionaryTitleLength, "=")).append("\n\n");
        
        // Build ToC
        conceptDatas.entrySet().forEach(entry -> {
            sb.append("* <<").append(idForIdentity(entry.getValue())).append(",").append(removeEncapsulatingQuotationMarks(entry.getKey())).append(">>\n");
        });
        
        sb.append("\n");
        
        conceptDatas.keySet().stream().forEachOrdered(name -> {
            final int conectNameLength = removeEncapsulatingQuotationMarks(name).length();
            final DictionaryEntry conceptData = conceptDatas.get(name);
            sb.append("[[").append(idForIdentity(conceptData)).append("]]\n")  // Toc anchor
                .append(removeEncapsulatingQuotationMarks(name)).append("\n").append(titleLine(conectNameLength, "-")).append("\n\n")  // Title
                .append("*Type:* ").append(conceptData.type().isConcept() ? "Concept" : "Behavior").append("\n\n")
                .append(".Definition\n").append(conceptData.entryDefinition().orElse("_undefined_")).append("\n\n")
//                .append(".Alternative Names\n").append(alternativeNames(conceptData.alternativeNames))
                    ;
            
            if (!conceptData.behaviors.isEmpty()) {
                sb.append(conceptData.behaviors.size() == 1 ? "Behavior" : "Behaviors").append("\n").append(titleLine("Behaviors".length(), "~")).append("\n\n");
                
                conceptData.behaviors.stream().forEachOrdered(behavior -> {
                    final String behaviorName = removeEncapsulatingQuotationMarks(behavior.entryName());
                    sb.append(behaviorName).append("\n").append(titleLine(behaviorName.length(), "^")).append("\n\n")
                        .append(".Definition\n").append(behavior.entryDefinition().orElse("_undefined_")).append("\n\n");
                });
            }
            
            if (!conceptData.contains.isEmpty()) {
                sb.append("Contains::\n");
                final String seeAlso = conceptData.contains.stream()
                        .map(see -> new StringBuilder()
                                .append("<<")
                                .append(idForIdentity(see))
                                .append(",")
                                .append(removeEncapsulatingQuotationMarks(see.name))
                                .append(">>").toString())
                        .collect(Collectors.joining(", "));
                sb.append(seeAlso).append("\n\n");
            }
        });
        return sb;
    }
    
    String alternativeNames(final Set<String> alternativeNames) {
        return alternativeNames.stream().collect(Collectors.joining(", "));
    }
    
    String idForIdentity(final Object o) {
        return "i" + System.identityHashCode(o);
    }
    
    StringBuilder titleLine(final int length, final String character) {
        final StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(character);
        }
        return sb;
    }
    
    @Override
    public String getName() {
        return "Dictionary";
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }
    
    @Override
    public Set<Doclet.Option> getSupportedOptions() {
        final Set<Doclet.Option> retval = new HashSet<>();
        super.getSupportedOptions().stream()
                .filter(option -> (!"-d".equals(option.getNames().get(0))))
                .forEachOrdered(option -> retval.add(option));

        final Doclet.Option overwrite = new LocalOption("-d", true, "Output directory", null) {
            @Override
            public boolean process(final String option, final List<String> arguments) {
                DictionaryDoclet.this.outputDirectory = arguments.get(0);
                return true;
            }
        };

        retval.add(overwrite);
        return retval;
    }

    /**
     * A base class for declaring options.
     * Subtypes for specific options should implement
     * the {@link #process(String,List) process} method
     * to handle instances of the option found on the
     * command line.
     */
    abstract class LocalOption implements Doclet.Option {
        private final String name;
        private final boolean hasArg;
        private final String description;
        private final String parameters;
 
        LocalOption(final String name, final boolean hasArg, final String description, final String parameters) {
            this.name = Objects.requireNonNull(name);
            this.hasArg = hasArg;
            this.description = Objects.requireNonNull(description);
            this.parameters = parameters;
        }
 
        @Override
        public int getArgumentCount() {
            return hasArg ? 1 : 0;
        }
 
        @Override
        public String getDescription() {
            return description;
        }
 
        @Override
        public Kind getKind() {
            return Kind.STANDARD;
        }
 
        @Override
        public List<String> getNames() {
            return List.of(name);
        }
 
        @Override
        public String getParameters() {
            return hasArg ? parameters : null;
        }
    }

    /**
     * Removes leading and trailing quotation marks from a string.
     * @param string the String to remove the quotations marks from
     * @return a string without leading and trailing quotation marks.
     *         {@code null} if and only if the parameter is {@code null}.
     */
    public static String removeEncapsulatingQuotationMarks(final String string) {
        if (string != null) {
            return string.replaceAll("^\"|\"$", "");
        }
        return null;
    }
}
