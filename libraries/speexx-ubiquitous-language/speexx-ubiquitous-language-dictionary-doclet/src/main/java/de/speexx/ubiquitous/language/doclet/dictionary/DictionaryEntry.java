/*
 * This file is part of the SpeexX Ubiquitous Language.
 * 
 * SpeexX Ubiquitous Language is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Ubiquitous Language is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Ubiquitous Language. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.ubiquitous.language.doclet.dictionary;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import javax.lang.model.element.TypeElement;

/**
 *
 * @author sascha.kohlmann
 */
public class DictionaryEntry {

    public TypeElement typeElement;
    public String qualifiedName;
    public String simpleName;
    public String dictionaryName;
    public final Set<String> alternativeNames = new TreeSet<>((String alt1, String alt2) -> alt1.compareToIgnoreCase(alt2));
    public final Set<DictionaryEntry> contains = new TreeSet<>((DictionaryEntry d1, DictionaryEntry d2) -> d1.entryName().compareTo(d2.entryName()));
    public final Set<DictionaryEntry> behaviors = new TreeSet<>((DictionaryEntry d1, DictionaryEntry d2) -> d1.entryName().compareTo(d2.entryName()));

    public String dictionaryDocument;
    public final EntryType type;
    public String name;
    public boolean behaviorInConcept = false;
    
    public DictionaryEntry(final EntryType type) {
        this.type = Objects.requireNonNull(type);
    }
    
    public String entryName() {
        if (this.dictionaryName != null && !this.dictionaryName.isBlank()) {
            return this.dictionaryName;
        }
        return this.simpleName;
    }
    
    public Optional<String> entryDefinition() {
        if (this.dictionaryDocument == null || this.dictionaryDocument.isBlank()) {
            return Optional.empty();
        }
        return Optional.of(this.dictionaryDocument);
    }
    
    public EntryType type() {
        return this.type;
    }
    
    public String qualifiedClassName() {    
        return this.qualifiedName;
    }

    public TypeElement typeElement() {
        return typeElement;
    }    

    @Override
    public String toString() {
        return "DictionaryEntry{" + "typeElement=" + typeElement + ", qualifiedName=" + qualifiedName + ", simpleName=" + simpleName + ", dictionaryName=" + dictionaryName + ", contains=" + contains + ", behaviors=" + behaviors + ", dictionaryDocument=" + dictionaryDocument + ", type=" + type + ", name=" + name + '}';
    }


    public enum EntryType {
        
        CONCEPT {
            @Override
            public boolean isConcept() {
                return true;
            }
        },
        BEHAVIOR {
            @Override
            public boolean isBehavior() {
                return true;
            }
        };
        
        public boolean isConcept() {
            return false;
        }

        public boolean isBehavior() {
            return false;
        }
    }
}
