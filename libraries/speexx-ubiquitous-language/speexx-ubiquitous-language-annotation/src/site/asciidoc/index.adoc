= SpeexX Ubiquitous Language

This project supports the development of Java projects with annotations to define a Domain Driven Design defined Ubiquitous Language.
It follows the idea of living documentation.

All annotations are only retentioned on `SOURCE` or `CLASS` level.
This means: the dependency to this project can be of scope _compile_.
Additional no runtime behavior is part of this project expect if you use the `enum` implementation of the project.

A change of this behavior will reflect in a major version change.

See link:apidocs/index.html[Javadoc] for all annotations.

== Development

* Releases are based on the lastest release Java version.

=== Version schema

A https://semver.org[semantic version schema] where

* MAJOR is the version of the latest compatible Java version
* MINOR backward compatible API changes
* PATCH backward compatible bug fixes

E.g. a version might be 17.0.1 which means: this version is Java 17 or higher compatble.

New features might be backported to older versions with increased MINOR version.

See Maven https://maven.apache.org/enforcer/enforcer-rules/versionRanges.html[Version Range Specification] specification (Enforcer Pluging).

