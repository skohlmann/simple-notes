/*
 * This file is part of the SpeexX Ubiquitous Language.
 * 
 * SpeexX Ubiquitous Language is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Ubiquitous Language is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Ubiquitous Language. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Support for creating dictionaries based an types, methods and constructors in Java.
 * The idea is to have the documentation of the <a href='https://en.wikipedia.org/wiki/Domain-driven_design'>Ubiquitous Language</a>
 * direct in the code as the only source of truth of a system.
 * 
 * <p>Evaluating programs should also be able to interpret the annotations from <a href='https://github.com/xmolecules/jmolecules'>jmolecules</a>.</p>
 */
module speexx.ubiquitous.language.annotation {
    requires speexx.common.annotation;
    requires org.apiguardian.api;
    
    exports de.speexx.ubiquitous.language.annotation;
}
