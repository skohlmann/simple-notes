/*
 * This file is part of the SpeexX Ubiquitous Language.
 * 
 * SpeexX Ubiquitous Language is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Ubiquitous Language is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Ubiquitous Language. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.ubiquitous.language.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.SOURCE;
import java.lang.annotation.Target;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * Defines a business concept and should support tools in autogenerate dictionaries.
 * Tools handling annotated types should use the Javadoc documentation to create the dictionary content.
 * @author sascha.kohlmann
 * @see Behavior
 */
@Inherited
@Documented
@Retention(SOURCE)
@API(status=STABLE)
@Target({ElementType.TYPE, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
public @interface Concept {
    /**
     * Overwriting technical name of the concept.
     * If used documentation tools should use the given value.
     * Otherwise the {@linkplain Class#getSimpleName() simple name} of the annotated type must be used.
     * This name might be splitted.
     * @return the human readable business name of the technical type.
     */
    String value() default "";
    
    /**
     * One or more alternative names of the concept.
     * Alternative names should not be part of other concept names.
     * <p><strong>Warning:</strong> Usage of alternatives is always in indicator of an unclear domain language.</p>
     * <p>Apart from documentation purposes this annotation is intended to be used by static analysis tools to validate
     * against unclear domain language violations.</p>
     * @return a list of alternative human readable business names
     */
    String[] alternatives() default "";
}
