package architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.jupiter.api.Test;
import static com.google.common.truth.Truth.assertThat;
import de.speexx.ubiquitous.language.annotation.Behavior;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author sascha.kohlmann
 */
public class RetentionTest {

    @Test
    public void all_classes() {
        final JavaClasses importedClasses = new ClassFileImporter().importPackages(Behavior.class.getPackageName());
        importedClasses.forEach(javaClass -> {
            final Class<?> annotation = javaClass.reflect();
            if (!"package-info".equals(annotation.getSimpleName())) {
                assertThat(annotation.isAnnotation() || annotation.isEnum()).isTrue();

                if (annotation.isAnnotation()) {
                    final Retention retention = annotation.getAnnotation(Retention.class);
                    assertThat(retention).isNotNull();
            
                    final RetentionPolicy policy = retention.value();
                    assertThat(policy).isNotEqualTo(RetentionPolicy.RUNTIME);
                }
            }
        });
    }
}
