/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.executor;

import org.apiguardian.api.API;

import static org.apiguardian.api.API.Status.STABLE;

/**
 * Thrown if an execution failed.
 */
@API(status=STABLE)
public class ExecutionException extends RuntimeException {

    /**
     * Constructs an instance of <code>ExecutionException</code> with the specified detail message and the causal throwable.
     * @param message the detail message
     * @param cause the cause
     */
    public ExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs an instance of <code>ExecutionException</code> with the causal throwable.
     * @param cause the cause
     */
    public ExecutionException(Throwable cause) {
        super(cause);
    }
    
    /**
     * Return the exception message auf the {@linkplain #getCause() cause} if available. Otherwise the message of this.
     * @return the exception message
     */
    @Override
    public String getMessage() {
        final Throwable cause = getCause();
        if (cause != null) {
            return cause.getMessage();
        }
        return super.getMessage();
    }

    /**
     * Return the localized exception message auf the {@linkplain #getCause() cause} if available.
     * Otherwise the localized message of this.
     * @return the localized exception message
     */
    @Override
    public String getLocalizedMessage() {
        final Throwable cause = getCause();
        if (cause != null) {
            return cause.getLocalizedMessage();
        }
        return super.getLocalizedMessage();
    }
}
