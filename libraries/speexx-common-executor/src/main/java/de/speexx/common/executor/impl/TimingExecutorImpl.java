/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.executor.impl;

import de.speexx.common.executor.ExecutionException;
import de.speexx.common.executor.Executor;
import de.speexx.common.executor.TimingExecutor;
import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;

import static de.speexx.common.Assertions.assertNotNull;
import static java.lang.System.nanoTime;
import static java.time.Duration.ofNanos;

/**
 *
 * @author sascha.kohlmann
 */
public final class TimingExecutorImpl extends ChainExecutorSupport implements TimingExecutor {

    private static final ThreadLocal<AtomicLong> DURATION_HOLDER = new ThreadLocal<AtomicLong>(){
        @Override
        protected AtomicLong initialValue() {
            return new AtomicLong();
        }
    };

    public TimingExecutorImpl() {
        super(null);
    }
    
    public TimingExecutorImpl(final Executor chain) {
        super(chain);
        assertNotNull(chain, "Chain Executor must be provided");
    }

    @Override
    public <V> V execute(final Callable<V> executable) throws ExecutionException {
        assertNotNull(executable, "Executable (Callable) must be provided");
        final var start = nanoTime();
        try {
            return doExecute(executable);
        } catch (final Exception e) {
            if (e instanceof ExecutionException executionException) {
                throw executionException;
            }
            throw new ExecutionException(e);
        } finally {
            final var finish = nanoTime();
            final var durationHolder = DURATION_HOLDER.get();
            durationHolder.set(finish - start);
        }
    }

    @Override
    public Duration duration() {
        return ofNanos(DURATION_HOLDER.get().get());
    }
}
