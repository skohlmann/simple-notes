/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.executor.impl;

import de.speexx.common.executor.ExecutionException;
import de.speexx.common.executor.Executor;
import java.util.concurrent.Callable;

/**
 *
 * @author sascha.kohlmann
 */
public abstract class ChainExecutorSupport {
    
    private final Executor executor;

    protected ChainExecutorSupport(final Executor chain) {
        this.executor = chain;
    }
    
    protected final <V> V doExecute(final Callable<V> executable) throws Exception {
        final var exec = executor();
        if (exec == null) {
            return executable.call();
        }
        return exec.execute(executable);
    }

    protected final Executor executor() {
        return this.executor;
    }

    protected void throwExecutionException(final Exception e) throws ExecutionException {
        if (e instanceof ExecutionException executionException) {
            throw executionException;
        }
        
        throw new ExecutionException(e != null ? e.getMessage() : null, e);
    } 
}
