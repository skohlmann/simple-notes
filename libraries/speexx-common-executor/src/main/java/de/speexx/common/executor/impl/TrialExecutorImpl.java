/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.executor.impl;

import de.speexx.common.executor.ExecutionException;
import de.speexx.common.executor.Executor;
import de.speexx.common.executor.TrialExecutor;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

import static de.speexx.common.Assertions.assertNotNull;
import static de.speexx.common.Assertions.assertRange;
import static java.lang.Integer.MAX_VALUE;

public final class TrialExecutorImpl extends ChainExecutorSupport implements TrialExecutor {

    private static final ThreadLocal<AtomicInteger> TRIAL_HOLDER = new ThreadLocal<AtomicInteger>(){
        @Override
        protected AtomicInteger initialValue() {
            return new AtomicInteger();
        }
    };
    
    private final int trials;
    
    public TrialExecutorImpl(final int trials) {
        super(null);
        this.trials = assertRange(trials, 1, MAX_VALUE, () -> "Trials must be > 0. Is: " + trials);
    }

    public TrialExecutorImpl(final int trials, final Executor chain) {
        super(chain);
        assertNotNull(chain, "Chain Executor must be provided");
        this.trials = assertRange(trials, 1, MAX_VALUE, () -> "Trials must be > 0. Is: " + trials);
    }

    @Override
    public <V> V execute(final Callable<V> executable) throws ExecutionException {
        assertNotNull(executable, "Executable (Callable) must be provided");
        final AtomicInteger trialHolder = TRIAL_HOLDER.get();
        trialHolder.set(0);
        int localTrialCounter = 0;
        do {
            try {
                trialHolder.incrementAndGet();
                return doExecute(executable);
            } catch (final Exception e) {
                localTrialCounter = trialsExhausted(localTrialCounter, e);
            }
        } while(true);
    } 

    int trialsExhausted(final int tries, final Exception toThrow) throws ExecutionException {
        if (tries == toTry()) {
            throwExecutionException(toThrow);
        }
        return tries + 1;
    }

    int toTry() {
        return this.trials;
    }

    @Override
    public int trials() {
        return TRIAL_HOLDER.get().intValue();
    }
}
