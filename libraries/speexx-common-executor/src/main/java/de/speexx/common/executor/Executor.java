/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.executor;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import org.apiguardian.api.API;

import static de.speexx.common.Assertions.assertNotNull;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * An {@code Executor} instance will execute a given {@code Runnable} or {@code Callable} for different behaviors.
 */
@API(status=STABLE)
public interface Executor {

    /**
     * Executes the given {@code Runnable} and throws an exception in case of failure.
     * @param executable the {@code Runbnable} to execute
     * @throws ExecutionException if an exception raise during execution
     * @throws IllegalArgumentException if and only if the <em>executable</em> is {@code null} */
    public default void execute(final Runnable executable) throws ExecutionException {
        assertNotNull(executable, "Executable (Runnable) must be provided");
        execute(Executors.callable(executable));
    }

    /**
     * Executes the given {@code Runnable} and returns the result or throws an exception in case of failure.
     * @param executable the {@code Callable} to execute
     * @param <V> the return type
     * @return the result of the execution
     * @throws ExecutionException if an exception raise during execution
     * @throws IllegalArgumentException if and only if thrown if may be the <em>executable</em> is {@code null}. Implementation dependend. */
    <V> V execute(final Callable<V> executable) throws ExecutionException;
}
