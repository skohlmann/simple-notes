/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.executor;

import de.speexx.common.executor.impl.TimingExecutorImpl;
import java.time.Duration;
import org.apiguardian.api.API;

import static org.apiguardian.api.API.Status.STABLE;

/**
 * Measures the duration of an execution and returns the value <em>after</em> the last execution.
 */
@API(status=STABLE)
public interface TimingExecutor extends Executor {

    /**
     * The duration after the last call of {@link #execute(java.util.concurrent.Callable)} or {@link #execute(java.lang.Runnable)}.
     * Before an execution call, the behavior is not defined and may raise a {@link IllegalStateException RuntimeException}.
     * @return the execution duration
     */
    Duration duration();

    /** Returns a thread safe instance measuring the execution time.
     * @return a TimingExecutor
     */
    static TimingExecutor of() {
        return new TimingExecutorImpl();
    }

    /** Returns a thread safe instance measuring the execution time. {@link #execute(java.lang.Runnable)} and 
     * {@link #execute(java.util.concurrent.Callable)} calls there corresponding method of the given {@code Executor}.
     * <p>The implementation starts the measure before calling the execution method of the given {@link Executor} and stops after
     * return of the called {@code Executor}.</p>
     * @param executor an executor to chain the call to
     * @return a TimingExecutor
     */
    static TimingExecutor of(final Executor executor) {
        return new TimingExecutorImpl(executor);
    }
}