/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.executor.impl;

import de.speexx.common.executor.ExecutionException;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 *
 * @author sascha.kohlmann
 */
public class ExecutionExceptionTest {

    @Test
    public void getMessage_with_cause() {
        
        // Given
        final var t = new Throwable("test");
        
        // When
        final var ex = new ExecutionException("out", t);
        
        // Then
        assertThat(ex.getMessage()).isEqualTo("test");
    }

    @Test
    public void with_cause() {
        
        // Given
        final var t = new Throwable("test");
        
        // When
        final var ex = new ExecutionException(t);
        
        // Then
        assertThat(ex.getMessage()).isEqualTo("test");
    }

    @Test
    public void getMessage_with_null_cause() {
        
        // When
        final var ex = new ExecutionException("out", null);
        
        // Then
        assertThat(ex.getMessage()).isEqualTo("out");
    }

    @Test
    public void getLocalizedMessage_with_cause() {
        
        // Given
        final var t = new Throwable("test");
        
        // When
        final var ex = new ExecutionException("out", t);
        
        // Then
        assertThat(ex.getLocalizedMessage()).isEqualTo("test");
    }

    @Test
    public void getLocalizedMessage_with_null_cause() {
        
        // When
        final var ex = new ExecutionException("out", null);
        
        // Then
        assertThat(ex.getLocalizedMessage()).isEqualTo("out");
    }
}
