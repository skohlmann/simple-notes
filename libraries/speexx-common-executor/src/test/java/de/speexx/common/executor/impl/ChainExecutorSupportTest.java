/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.executor.impl;

import de.speexx.common.executor.ExecutionException;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author sascha.kohlmann
 */
public class ChainExecutorSupportTest {

    @Test
    public void rethrow_ExecutionException() {
        // Given
        final var chainSuport = new ChainExecutorSupport(null) {};
        final var ex = new ExecutionException(new Throwable());
        
        // When
        final var thrown = assertThrows(Exception.class, () -> chainSuport.throwExecutionException(ex));
        
        // Then
        assertThat(thrown).isSameInstanceAs(ex);
    }
}
