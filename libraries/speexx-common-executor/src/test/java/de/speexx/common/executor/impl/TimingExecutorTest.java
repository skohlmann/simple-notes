/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.executor.impl;

import de.speexx.common.executor.ExecutionException;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static de.speexx.common.executor.TimingExecutor.of;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author sascha.kohlmann
 */
public class TimingExecutorTest {

    @Test
    public void simple_timing() {
        // Given
        final var executor = of();
        
        // When
        executor.execute(() -> {;});

        // Then
        assertThat(executor.duration().getNano()).isGreaterThan(0);
    }

    @Test
    public void simple_chained_timing() {
        // Given
        final var base = of();
        final var executor = of(base);
        
        // When
        executor.execute(() -> {;});

        // Then
        assertThat(base.duration().getNano()).isGreaterThan(0);
        assertThat(executor.duration().getNano()).isGreaterThan(0);
    }

    @Test
    public void catch_not_ExecutionException_throwing() {
        // Given
        final var executor = of();
        
        // When
        final var ex = assertThrows(RuntimeException.class, () -> executor.execute(() -> {throw new RuntimeException("test");}));

        // Then
        assertThat(ex).isInstanceOf(ExecutionException.class);
        assertThat(ex.getCause().getMessage()).isEqualTo("test");
        assertThat(executor.duration().getNano()).isGreaterThan(0);
    }

    @Test
    public void catch_ExecutionException_throwing() {
        // Given
        final var executor = of();
        
        // When
        final var ex = assertThrows(RuntimeException.class, () -> executor.execute(() -> {throw new ExecutionException("execution", new Throwable("throwable"));}));

        // Then
        assertThat(ex).isInstanceOf(ExecutionException.class);
        assertThat(ex.getMessage()).isEqualTo("throwable");
        assertThat(executor.duration().getNano()).isGreaterThan(0);
    }
}
