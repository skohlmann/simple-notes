/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store;

import static com.google.common.truth.Truth.assertThat;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class ResultTypeTest {
    
    @Test
    public void ResultType_CONCURRENCY_VIOLATION() {
        assertThat(ResultType.CONCURRENCY_VIOLATION.isConcurrencyViolation()).isTrue();
        assertThat(ResultType.CONCURRENCY_VIOLATION.isFailure()).isFalse();
        assertThat(ResultType.CONCURRENCY_VIOLATION.isSuccess()).isFalse();
        assertThat(ResultType.CONCURRENCY_VIOLATION.isNotFound()).isFalse();
    }
    
    @Test
    public void ResultType_FAILURE() {
        assertThat(ResultType.FAILURE.isConcurrencyViolation()).isFalse();
        assertThat(ResultType.FAILURE.isFailure()).isTrue();
        assertThat(ResultType.FAILURE.isSuccess()).isFalse();
        assertThat(ResultType.FAILURE.isNotFound()).isFalse();
    }
    
    @Test
    public void ResultType_NOT_FOUND() {
        assertThat(ResultType.NOT_FOUND.isConcurrencyViolation()).isFalse();
        assertThat(ResultType.NOT_FOUND.isFailure()).isFalse();
        assertThat(ResultType.NOT_FOUND.isSuccess()).isFalse();
        assertThat(ResultType.NOT_FOUND.isNotFound()).isTrue();
    }
    
    @Test
    public void ResultType_SUCCESS() {
        assertThat(ResultType.SUCCESS.isConcurrencyViolation()).isFalse();
        assertThat(ResultType.SUCCESS.isFailure()).isFalse();
        assertThat(ResultType.SUCCESS.isSuccess()).isTrue();
        assertThat(ResultType.SUCCESS.isNotFound()).isFalse();
    }
}
