/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store;

import static com.google.common.truth.Truth.assertThat;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class StoreResultTest {
    
    @Test
    public void of_success() {
        final StoreResult result = StoreResult.ofSuccess();
        
        assertThat(result.resultType()).isSameInstanceAs(ResultType.SUCCESS);
        assertThat(result.exception().isPresent()).isFalse();
    }
    
    @Test
    public void of_not_found_without_exception() {
        final StoreResult result = StoreResult.ofNotFound();
        
        assertThat(result.resultType()).isSameInstanceAs(ResultType.NOT_FOUND);
        assertThat(result.exception().isPresent()).isFalse();
    }
    
    @Test
    public void of_not_found_with_exception() {
        final StoreResult result = StoreResult.ofNotFound(new IllegalStateException());
        
        assertThat(result.resultType()).isSameInstanceAs(ResultType.NOT_FOUND);
        assertThat(result.exception().get()).isInstanceOf(IllegalStateException.class);
    }
    
    @Test
    public void of_not_found_with_exception_null() {
        final StoreResult result = StoreResult.ofNotFound(null);
        
        assertThat(result.resultType()).isSameInstanceAs(ResultType.NOT_FOUND);
        assertThat(result.exception().isPresent()).isFalse();
    }
    
    @Test
    public void of_failure() {
        final StoreResult result = StoreResult.ofFailure(new IllegalArgumentException());
        
        assertThat(result.resultType()).isSameInstanceAs(ResultType.FAILURE);
        assertThat(result.exception().get()).isInstanceOf(IllegalArgumentException.class);
    }
    
    @Test
    public void of_failure_exception_null() {
        final StoreResult result = StoreResult.ofFailure(null);
        
        assertThat(result.resultType()).isSameInstanceAs(ResultType.FAILURE);
        assertThat(result.exception().isPresent()).isFalse();
    }
    
    @Test
    public void toString_test() {
        
        final StoreResult result = StoreResult.ofFailure(new IllegalArgumentException());
        assertThat(result.toString()).isNotNull();

    }
}
