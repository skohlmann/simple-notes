/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object.jdbc.postgres;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.event.sourcing.BaseMemento.TextMemento;
import static de.speexx.event.sourcing.store.jdbc.postgres.PostgresBase.SERVER;
import de.speexx.event.sourcing.store.object.ObjectState;
import java.time.LocalDateTime;
import static java.time.Month.JULY;
import static java.time.Month.JUNE;
import static java.time.Month.MAY;
import java.time.OffsetDateTime;
import static java.time.OffsetDateTime.of;
import java.time.ZoneOffset;
import static java.util.Arrays.asList;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import de.speexx.event.sourcing.store.object.ObjectReader;
import de.speexx.event.sourcing.store.object.ObjectStore;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.Timeout;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 *
 * @author sascha.kohlmann
 */
@Tag("slow")
@Testcontainers
@Timeout(value = 60, unit = TimeUnit.SECONDS)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PostgresObjectReaderIT extends ObjectStoreTestSupport {

    @Test
    @Order(1)
    public void insert_single_data() throws Exception {
        // Given
        final LocalDateTime local = LocalDateTime.of(2020, JULY, 25, 16, 32, 14);
        final OffsetDateTime datetime = of(local, ZoneOffset.UTC);
        final DateTimeSource dateSource = new DateTimeSource(1, datetime, local);

        final ObjectStore objectStore = new PostgresObjectStore(mementoAdapterProvider(), newConnection(true));
        
        // When
        final StoreResultInterestImpl result = storeResultInterest();
        objectStore.store("object-1", dateSource, result);
 
        // And When
        final ObjectReader<TextMemento> objectReader = objectStore.objectReader();
        final ObjectState<TextMemento> object = objectReader.objectFor("object-1");
        
        // Then
        assertThat(result.result.resultType().isSuccess()).isTrue();
        assertThat(object.isNull()).isFalse();
        assertThat(object.objectName()).isEqualTo("object-1");
        assertThat(object.mementos()).hasSize(1);
        assertThat(object.mementos().get(0).typeName()).isEqualTo(dateSource.getClass().getName());
    }

    @Test
    @Order(1)
    public void insert_multiple_data() throws Exception {
        // Given
        final LocalDateTime local1 = LocalDateTime.of(2020, JULY, 25, 16, 32, 14);
        final OffsetDateTime datetime1 = of(local1, ZoneOffset.UTC);
        final OffsetDateTime datetime2 = of(local1.plusNanos(2), ZoneOffset.UTC);
        final IntegerSource intSource = new IntegerSource(1, datetime1, 17);
        final DateTimeSource dateSource = new DateTimeSource(1, datetime2, local1);

        final ObjectStore objectStore = new PostgresObjectStore(mementoAdapterProvider(), newConnection(true));
        
        // When
        objectStore.storeAll("object-2", asList(intSource, dateSource), storeResultInterest());
 
        // And When
        final ObjectReader<TextMemento> objectReader = objectStore.objectReader();
        final ObjectState<TextMemento> object = objectReader.objectFor("object-2");
        
        // Then
        assertThat(object.isNull()).isFalse();
        assertThat(object.objectName()).isEqualTo("object-2");
        assertThat(object.mementos()).hasSize(2);
        assertThat(object.mementos().get(0).typeName()).isEqualTo(IntegerSource.class.getName());
        assertThat(object.mementos().get(1).typeName()).isEqualTo(DateTimeSource.class.getName());
    }

    @Test
    @Order(3)
    public void dont_upsert_before() throws Exception {
        // Given
        final LocalDateTime local1 = LocalDateTime.of(2020, JULY, 25, 16, 32, 14);
        final OffsetDateTime datetime1 = of(local1, ZoneOffset.UTC);
        final DateTimeSource dateSource = new DateTimeSource(1, datetime1.minusNanos(1), LocalDateTime.of(2020, JUNE, 25, 16, 32, 14));

        final ObjectStore objectStore = new PostgresObjectStore(mementoAdapterProvider(), newConnection(true));
        
        // When
        objectStore.store("object-2", dateSource, storeResultInterest());
 
        // And When
        final ObjectReader<TextMemento> objectReader = objectStore.objectReader();
        final ObjectState<TextMemento> object = objectReader.objectFor("object-2");
        
        // Then
        assertThat(object.mementos().get(0).typeName()).isEqualTo(IntegerSource.class.getName());
        assertThat(object.mementos().get(1).typeName()).isEqualTo(DateTimeSource.class.getName());
        assertThat(object.mementos().get(1).mementoData()).isEqualTo("2020-07-25T16:32:14");
    }

    @Test
    @Order(4)
    public void upsert_equal() throws Exception {
        // Given
        final LocalDateTime local1 = LocalDateTime.of(2020, JULY, 25, 16, 32, 14);
        final OffsetDateTime datetime1 = of(local1, ZoneOffset.UTC);
        final DateTimeSource dateSource = new DateTimeSource(1, datetime1.plusNanos(2), LocalDateTime.of(2020, JUNE, 25, 16, 32, 14));

        final ObjectStore objectStore = new PostgresObjectStore(mementoAdapterProvider(), newConnection(true));
        
        // When
        objectStore.store("object-2", dateSource, storeResultInterest());
 
        // And When
        final ObjectReader<TextMemento> objectReader = objectStore.objectReader();
        final ObjectState<TextMemento> object = objectReader.objectFor("object-2");
        
        // Then
        assertThat(object.mementos().get(0).typeName()).isEqualTo(IntegerSource.class.getName());
        assertThat(object.mementos().get(1).typeName()).isEqualTo(DateTimeSource.class.getName());
        assertThat(object.mementos().get(1).mementoData()).isEqualTo("2020-06-25T16:32:14");
    }

    @Test
    @Order(5)
    public void upsert_newer() throws Exception {
        // Given
        final LocalDateTime local1 = LocalDateTime.of(2020, JULY, 25, 16, 32, 14);
        final OffsetDateTime datetime1 = of(local1, ZoneOffset.UTC);
        final DateTimeSource dateSource = new DateTimeSource(1, datetime1.plusNanos(3), LocalDateTime.of(1969, MAY, 15, 16, 32, 14));

        final ObjectStore objectStore = new PostgresObjectStore(mementoAdapterProvider(), newConnection(true));
        
        // When
        objectStore.store("object-2", dateSource, storeResultInterest());
 
        // And When
        final ObjectReader<TextMemento> objectReader = objectStore.objectReader();
        final ObjectState<TextMemento> object = objectReader.objectFor("object-2");
        
        // Then
        assertThat(object.mementos().get(0).typeName()).isEqualTo(IntegerSource.class.getName());
        assertThat(object.mementos().get(1).typeName()).isEqualTo(DateTimeSource.class.getName());
        assertThat(object.mementos().get(1).mementoData()).isEqualTo("1969-05-15T16:32:14");
    }


    @Test
    @Order(5)
    public void unknown_object() throws Exception {
        // Given
        final ObjectStore objectStore = new PostgresObjectStore(mementoAdapterProvider(), newConnection(true));
        
        // When
        final ObjectReader<TextMemento> objectReader = objectStore.objectReader();
        final ObjectState<TextMemento> object = objectReader.objectFor("unknown");
        
        // Then
        assertThat(object.isNull()).isTrue();
    }

    @BeforeAll
    public static void startContainer() throws Exception {
        SERVER.beforeAll(null);
    }

    @AfterAll
    public static void stopContainer() throws Exception {
        SERVER.afterAll(null);
    }
}
