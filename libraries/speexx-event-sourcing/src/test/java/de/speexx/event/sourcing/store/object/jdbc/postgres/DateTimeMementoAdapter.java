/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object.jdbc.postgres;

import de.speexx.event.sourcing.BaseMemento.TextMemento;
import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.MementoAdapter;
import java.time.LocalDateTime;
import static java.time.OffsetDateTime.now;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;

/**
 *
 * @author sascha.kohlmann
 */
public class DateTimeMementoAdapter implements MementoAdapter<DateTimeSource, Memento<String>> {

    @Override
    public DateTimeSource fromMemento(Memento<String> memento) {
        return new DateTimeSource(memento.mementoVersion(), now(), LocalDateTime.parse(memento.mementoData(), ISO_LOCAL_DATE_TIME));
    }

    @Override
    public Memento<String> toMemento(DateTimeSource source) {
        return new TextMemento(source.getClass(), source.sourceVersion(), source.value().format(ISO_LOCAL_DATE_TIME));
    }

    @Override
    public Memento<String> toMemento(DateTimeSource source, String id) {
        return new TextMemento(id, source.getClass(), source.sourceVersion(), source.value().format(ISO_LOCAL_DATE_TIME));
    }
}
