/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author sascha.kohlmann
 */
public class SnapshotTest {

    @Test
    public void create_without_id() {
        final var ex = assertThrows(NullPointerException.class, () -> new Snapshot.TextSnapshot(null, getClass(), 1, "String", 2));
        assertThat(ex.getMessage()).contains("ID must be provided");
    }

    @Test
    public void create_with_typeVersion_lower_one() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> new Snapshot.TextSnapshot("abc", getClass(), 0, "String", 2));
        assertThat(ex.getMessage()).contains("Snapshot typeVersion must be greater than 0.");
    }

    @Test
    public void create_without_type() {
        final var ex = assertThrows(NullPointerException.class, () -> new Snapshot.TextSnapshot("abc", (Class<?>) null, 1, "String", 2));
        assertThat(ex.getMessage()).contains("Type must be provided");
    }

    @Test
    public void create_without_data() {
        final var ex = assertThrows(NullPointerException.class, () -> new Snapshot.TextSnapshot("abc", getClass(), 1, null, 2));
        assertThat(ex.getMessage()).contains("Data must be provided");
    }

    @Test
    public void create_with_dataVersion_lower_one() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> new Snapshot.TextSnapshot("abc", getClass(), 1, "String", 0));
        assertThat(ex.getMessage()).contains("Snapshot dataVersion must be greater than 0.");
    }

    @Test
    public void simple_create() {
        final var snapshot = new Snapshot.TextSnapshot("abc", getClass(), 1, "String", 2);
        
        assertThat(snapshot.id()).isEqualTo("abc");
        assertThat(snapshot.dataVersion()).isEqualTo(2);
        assertThat(snapshot.typeVersion()).isEqualTo(1);
        assertThat(snapshot.typeName()).isEqualTo(getClass().getName());
        assertThat(snapshot.snapshotData()).isEqualTo("String");
        
    }

    @Test
    public void simple_textsnapshot_create_not_null() {
        final var snapshot = new Snapshot.TextSnapshot("abc", getClass(), 1, "String", 2);
        assertThat(snapshot.isNull()).isFalse();
    }

    @Test
    public void simple_textsnapshot_create_not_empty() {
        final var snapshot = new Snapshot.TextSnapshot("abc", getClass(), 1, "String", 2);
        assertThat(snapshot.isEmpty()).isFalse();
    }

    @Test
    public void nullInstance_textsnapshot_is_null() {
        assertThat(Snapshot.TextSnapshot.NULL.isNull()).isTrue();
        
    }

    @Test
    public void nullInstance_textsnapshot_is_empty() {
        assertThat(Snapshot.TextSnapshot.NULL.isEmpty()).isTrue();
    }
    
    @Test
    public void simple_binarysnapshot_create_not_null() {
        final var snapshot = new Snapshot.BinarySnapshot("abc", getClass(), 1, new byte[] {1, 2}, 2);
        assertThat(snapshot.isNull()).isFalse();
    }

    @Test
    public void simple_binarysnapshot_create_not_empty() {
        final var snapshot = new Snapshot.BinarySnapshot("abc", getClass(), 1, new byte[] {1, 2}, 2);
        assertThat(snapshot.isEmpty()).isFalse();
    }

    @Test
    public void nullInstance_binarysnapshot_is_null() {
        assertThat(Snapshot.TextSnapshot.NULL.isNull()).isTrue();
        
    }

    @Test
    public void nullInstance_binarysnapshot_is_empty() {
        assertThat(Snapshot.TextSnapshot.NULL.isEmpty()).isTrue();
    }

    @Test
    public void isEmpty_abstract() {
        assertThat(new DummySnapshot("id", String.class, 1, "abc", 1).isEmpty()).isFalse();
    }
    
    @Test
    public void isNull_abstract() {
        assertThat(new DummySnapshot("id", String.class, 1, "abc", 1).isNull()).isFalse();
    }
    
    private static final class DummySnapshot extends Snapshot<String> {
        public DummySnapshot(final String id, final Class<?> type, final int typeVersion, final String data, final int dataVersion) {
            super(id, type, typeVersion, id, dataVersion);
        }
    }
}
