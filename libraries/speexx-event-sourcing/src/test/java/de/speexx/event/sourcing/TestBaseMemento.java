/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

/**
 *
 * @author sascha.kohlmann
 */
public class TestBaseMemento implements Memento<String> {
    
    private final String id;
    private final String mementoData;
    private final String typeName;
    private final int mementoVersion;

    protected TestBaseMemento(final Class<?> nativeType, final int mementoVersion, final String mementoData) {
        this.id = "";
        this.typeName = nativeType.getName();
        this.mementoVersion = mementoVersion;
        this.mementoData = mementoData;
    }

    @Override
    public String mementoData() {
        return this.mementoData;
    }

    @Override
    public String typeName() {
        return this.typeName;
    }
    
    @Override
    public int mementoVersion() {
        return this.mementoVersion;
    }

    @Override
    public String id() {
        return this.id;
    }

    @Override
    public boolean isEmpty() {
        return mementoData().isBlank();
    }

    @Override
    public boolean isNull() {
        return Memento.super.isNull();
    }
}
