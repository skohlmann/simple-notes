/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store;

import static com.google.common.truth.Truth.assertThat;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class StoreExceptionTest {
    
    @Test
    public void create() {
        final StoreException ex = new StoreException(new StoreResult(ResultType.FAILURE, new IllegalStateException()), "test");
        
        assertThat(ex.storeResult().isPresent()).isTrue();
        assertThat(ex.getLocalizedMessage()).isEqualTo("test");
    }

    @Test
    public void create_with_message_null() {
        final StoreException ex = new StoreException(new StoreResult(ResultType.FAILURE, new IllegalStateException()), null);
        
        assertThat(ex.getLocalizedMessage()).isNull();
    }

    @Test
    public void create_with_storeresult_null() {
        final StoreException ex = new StoreException(null, "message");
        
        assertThat(ex.storeResult().isPresent()).isFalse();
    }
}
