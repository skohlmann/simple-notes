/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.inmemory;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.OffsetDateTime;

/**
 *
 * @author sascha.kohlmann
 */
public class SecondEvent extends BaseEvent {

    @JsonProperty("second-data")
    private String secondData;
    
    public SecondEvent(@JsonProperty("second-data") final String secondData) {
        this();
        this.secondData = secondData;
    }
    
    public SecondEvent(@JsonProperty("second-data") final String secondData, @JsonProperty("sourced-at") final OffsetDateTime sourcedAt) {
        super(sourcedAt);
        this.secondData = secondData;
    }
    
    // For deserialization
    private SecondEvent() {
        super();
    }
}
