/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.inmemory;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author sascha.kohlmann
 */
public class SimpleSnapshotable {
    
    @JsonProperty("data")
    public String data;

    @Override
    public String toString() {
        return "SimpleSnapshotable{" + "data=" + data + '}';
    }
}
