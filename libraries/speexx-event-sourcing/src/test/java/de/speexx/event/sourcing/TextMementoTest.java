/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import de.speexx.event.sourcing.BaseMemento.TextMemento;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author sascha.kohlmann
 */
public class TextMementoTest {

    @Test
    public void create_with_mementoVersion_lower_one() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> new TextMemento(getClass(), 0, "String"));
        assertThat(ex.getMessage()).isEqualTo("Version must be > 0");
    }

    @Test
    public void create_with_type_null() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> new TextMemento((Class<?>) null, 1, "String"));
        assertThat(ex.getMessage()).isEqualTo("A native type must be provided");
    }

    @Test
    public void create_with_data_null() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> new TextMemento(getClass(), 1, null));
        assertThat(ex.getMessage()).isEqualTo("Memento data must be provided");
    }

    @Test
    public void simple_create() {
        final TextMemento  memento = new TextMemento(getClass(), 2, "String");

        assertThat(memento.id()).isEmpty();
        assertThat(memento.mementoData()).isEqualTo("String");
        assertThat(memento.mementoVersion()).isEqualTo(2);
        assertThat(memento.typeName()).isEqualTo(getClass().getName());
        assertThat(memento.typeName()).isEqualTo(getClass().getName());
        assertThat(memento.isNull()).isFalse();
        assertThat(memento.isEmpty()).isFalse();
    }

    @Test
    public void not_null() {
        final TextMemento  memento = new TextMemento(getClass(), 2, "String");

        assertThat(memento.isNull()).isFalse();
    }

    @Test
    public void is_empty() {
        final TextMemento  memento = new TextMemento(getClass(), 2, "");

        assertThat(memento.isEmpty()).isTrue();
    }
}
