/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object.jdbc.postgres;

import de.speexx.event.sourcing.Source;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

/**
 *
 * @author sascha.kohlmann
 */
public class DateTimeSource implements Source<LocalDateTime> {

    private int sourceVersion;
    private OffsetDateTime datetime;
    private LocalDateTime value;
    
    public DateTimeSource(final int sourceVersion, final OffsetDateTime datetime, final LocalDateTime value) {
        this.sourceVersion = sourceVersion;
        this.datetime = datetime;
        this.value = value;
    }
    
    public LocalDateTime value() {
        return this.value;
    }
    
    @Override
    public int sourceVersion() {
        return sourceVersion;
    }

    public String sourceName() {
        return getClass().getName();
    }

    @Override
    public OffsetDateTime sourcedAt() {
        return datetime;
    }

    @Override
    public boolean isNull() {
        return false;
    }
}
