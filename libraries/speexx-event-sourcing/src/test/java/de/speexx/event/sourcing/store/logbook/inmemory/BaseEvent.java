/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.inmemory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.speexx.event.sourcing.Source;
import java.time.OffsetDateTime;

import static java.time.OffsetDateTime.now;

/**
 *
 * @author sascha.kohlmann
 */
public abstract class BaseEvent implements Source<BaseEvent> {

    @JsonProperty("version")
    private int version = 1;
    
    @JsonProperty("sourced-at")
    private OffsetDateTime sourcedAt;
    
    
    public BaseEvent() {
        this(now());
    }
    
    public BaseEvent(final OffsetDateTime sourcedAt) {
        this.sourcedAt = sourcedAt;
    }
    
    @Override
    public final int sourceVersion() {
        return this.version;
    }

    @Override
    public final OffsetDateTime sourcedAt() {
        return this.sourcedAt;
    }

    @Override
    @JsonIgnore
    public boolean isNull() {
        return Source.super.isNull();
    }
}
