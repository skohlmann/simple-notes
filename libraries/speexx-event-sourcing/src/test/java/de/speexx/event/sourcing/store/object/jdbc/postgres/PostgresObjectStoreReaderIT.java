/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object.jdbc.postgres;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.event.sourcing.Memento;
import static de.speexx.event.sourcing.store.MementoReader.BEGIN;
import static de.speexx.event.sourcing.store.jdbc.postgres.PostgresBase.SERVER;
import static de.speexx.event.sourcing.store.object.jdbc.postgres.PostgresObjectStore.toNanos;
import static java.lang.String.valueOf;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import static java.time.Month.JULY;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import static java.util.Arrays.asList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.Timeout;
import de.speexx.event.sourcing.store.object.ObjectStore;
import de.speexx.event.sourcing.store.object.ObjectStoreReader;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 *
 * @author sascha.kohlmann
 */
@Tag("slow")
@Testcontainers
@Timeout(value = 60, unit = TimeUnit.SECONDS)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PostgresObjectStoreReaderIT extends ObjectStoreTestSupport {
    
    private final static String READER_NAME = "reader";

    private static String FIRST_ID;
    private static String SECOND_ID;
    private static String THIRD_ID;

    @Test
    @Order(1)
    public void flow() throws Exception {
        initDb();
        try (Connection connReader = newConnection(true)) {
            // Given
            final ObjectStoreReader reader = new PostgresObjectStore(mementoAdapterProvider(), connReader).objectStoreReader(READER_NAME);
            
            // When
            final OptionalLong size = reader.size();
            
            // Then
            assertThat(size.getAsLong()).isEqualTo(5);
            
            // And When
            final Optional<Memento> memento1 = reader.readNext();
            
            // And Then
            assertThat(memento1.get().mementoData()).isEqualTo("17");
        }
        
        try (Connection connNew = newConnection(true)) {
            // And When
            final ObjectStore stNew = new PostgresObjectStore(mementoAdapterProvider(), connNew);
            final ObjectStoreReader readerNew = stNew.objectStoreReader(READER_NAME);
            final Optional<Memento> memento2 = readerNew.readNext();
            
            // And Then
            assertThat(memento2.get().mementoData()).isEqualTo("2020-07-25T16:32:14");
        }

        try (Connection connMulti = newConnection(true)) {
             // And When
            final ObjectStore stMulti = new PostgresObjectStore(mementoAdapterProvider(), connMulti);
            final ObjectStoreReader readerMulti = stMulti.objectStoreReader(READER_NAME);
            final List<Memento> mementos = readerMulti.readNext(2);
            
            // And Then
            assertThat(mementos).hasSize(2);
            assertThat(mementos.get(0).mementoData()).isEqualTo("18");
            assertThat(mementos.get(1).mementoData()).isEqualTo("15");
            
            // And When
            final Optional<Memento> memento3 = readerMulti.readNext();
            // And Then
            assertThat(memento3.get().mementoData()).isEqualTo("2022-07-25T16:32:14");
            
            
            // And When
            final Optional<Memento> memento4 = readerMulti.readNext();
            // And Then
            assertThat(memento4.isEmpty()).isTrue();
        }
    }

    @Test
    @Order(2)
    public void rewind() throws Exception {
        try (Connection connReader = newConnection(true)) {
            // Given
            final ObjectStoreReader reader = new PostgresObjectStore(mementoAdapterProvider(), connReader).objectStoreReader(READER_NAME);
            
            // When
            reader.rewind();
            final Optional<Memento> memento = reader.readNext();
            
            // And Then
            assertThat(memento.get().mementoData()).isEqualTo("17");
        }
    }

    @Test
    @Order(3)
    public void seekTo() throws Exception {
        try (Connection connReader = newConnection(true)) {
            // Given
            final ObjectStoreReader reader = new PostgresObjectStore(mementoAdapterProvider(), connReader).objectStoreReader(READER_NAME);
            
            // When
            reader.seekTo(THIRD_ID);
            final Optional<Memento> memento = reader.readNext();
            
            // And Then
            assertThat(memento.get().mementoData()).isEqualTo("15");
        }
    }

    @Test
    @Order(4)
    public void nextWithId() throws Exception {
        try (Connection connReader = newConnection(true)) {
            // Given
            final ObjectStoreReader reader = new PostgresObjectStore(mementoAdapterProvider(), connReader).objectStoreReader(READER_NAME);
            
            // When
            final Optional<Memento> memento = reader.readNext(SECOND_ID);
            
            // And Then
            assertThat(memento.get().mementoData()).isEqualTo("18");
        }
    }

    @Test
    @Order(5)
    public void read_multiple_from_id() throws Exception {
        try (Connection connReader = newConnection(true)) {
            // Given
            final ObjectStoreReader reader = new PostgresObjectStore(mementoAdapterProvider(), connReader).objectStoreReader(READER_NAME);
            
            // When
            final List<Memento> mementos = reader.readNext(SECOND_ID, 2);
            
            // And Then
            assertThat(mementos).hasSize(2);
            assertThat(mementos.get(0).mementoData()).isEqualTo("18");
            assertThat(mementos.get(1).mementoData()).isEqualTo("15");
        }
    }

    @Test
    @Order(6)
    public void read_multiple_one() throws Exception {
        try (Connection connReader = newConnection(true)) {
            // Given
            final ObjectStoreReader reader = new PostgresObjectStore(mementoAdapterProvider(), connReader).objectStoreReader(READER_NAME);
            
            // When
            reader.seekTo(SECOND_ID);
            final List<Memento> mementos1 = reader.readNext(1);
            
            // Then
            assertThat(mementos1.get(0).mementoData()).isEqualTo("18");
            
            // And When
            final List<Memento> mementos2 = reader.readNext(1);
            assertThat(mementos2.get(0).mementoData()).isEqualTo("15");
        }
    }

    @Test
    @Order(7)
    public void read_multiple_from_begin() throws Exception {
        try (Connection connReader = newConnection(true)) {
            // Given
            final ObjectStoreReader reader = new PostgresObjectStore(mementoAdapterProvider(), connReader).objectStoreReader(READER_NAME);
            
            // When
            final List<Memento> mementos = reader.readNext(PostgresObjectStoreReader.BEGIN, 3);
            
            // Then
            assertThat(mementos).hasSize(3);
            assertThat(mementos.get(0).mementoData()).isEqualTo("17");
            assertThat(mementos.get(1).mementoData()).isEqualTo("2020-07-25T16:32:14");
            assertThat(mementos.get(2).mementoData()).isEqualTo("18");
        }
    }
    
    @Test
    @Order(8)
    public void read_multiple_from_begin_with_first_readNext_only() throws Exception {
        try (Connection connReader = newConnection(true)) {
            // Given
            final ObjectStoreReader reader = new PostgresObjectStore(mementoAdapterProvider(), connReader).objectStoreReader(READER_NAME);
            
            // When
            final Optional<Memento> memento = reader.readNext(BEGIN);
            final List<Memento> mementos = reader.readNext(2);
            
            // Then
            assertThat(mementos).hasSize(2);
            assertThat(mementos.get(0).mementoData()).isEqualTo("2020-07-25T16:32:14");
            assertThat(mementos.get(1).mementoData()).isEqualTo("18");
        }
    }

    
    @Test
    @Order(9)
    public void read_after_delete() throws Exception {
        try (Connection connReader = newConnection(true)) {
            // Given
            final ObjectStoreReader reader = new PostgresObjectStore(mementoAdapterProvider(), connReader).objectStoreReader(READER_NAME);
            
            // When
            reader.delete();
            final Optional<Memento> memento = reader.readNext(BEGIN);
            
            // Then
            assertThat(memento.isPresent()).isTrue();
        }
    }
    
    void initDb() throws SQLException {
        try (final Connection conn = newConnection(true)) {
            final ObjectStore st = new PostgresObjectStore(mementoAdapterProvider(), conn);
            
            final OffsetDateTime first = OffsetDateTime.of(1970, 1, 1, 0, 0, 0, 2, ZoneOffset.UTC);
            final OffsetDateTime second = OffsetDateTime.of(1970, 1, 1, 0, 0, 0, 3, ZoneOffset.UTC);
            final OffsetDateTime third = OffsetDateTime.of(1970, 1, 1, 0, 0, 0, 4, ZoneOffset.UTC);
            
            final LocalDateTime local = LocalDateTime.of(2020, JULY, 25, 16, 32, 14);
            final IntegerSource intSource1 = new IntegerSource(1, first, 17);
            final DateTimeSource dateSource1 = new DateTimeSource(1, first, local);
            final String objectName1 = "object-1";
            st.storeAll(objectName1, asList(intSource1, dateSource1), storeResultInterest());
            FIRST_ID = valueOf(toNanos(first));
            
            final String objectName2 = "object-2";
            final IntegerSource intSource2 = new IntegerSource(1, second, 18);
            st.store(objectName2, intSource2, storeResultInterest());
            SECOND_ID = valueOf(toNanos(second));
            
            final LocalDateTime loca3 = LocalDateTime.of(2020, JULY, 25, 16, 32, 14);
            final IntegerSource intSource3 = new IntegerSource(1, third, 15);
            final DateTimeSource dateSource3 = new DateTimeSource(1, third, local.plusYears(2));
            final String objectName3 = "object-3";
            st.storeAll(objectName3, asList(intSource3, dateSource3), storeResultInterest());
            THIRD_ID = valueOf(toNanos(third));
            
            st.objectStoreReader(READER_NAME);
        }
    }

    @BeforeAll
    public static void startContainer() throws Exception {
        SERVER.beforeAll(null);
    }

    @AfterAll
    public static void stopContainer() throws Exception {
        SERVER.afterAll(null);
    }
}
