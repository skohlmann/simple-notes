/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.inmemory;

import de.speexx.event.sourcing.Source;
import de.speexx.event.sourcing.store.StoreResult;
import de.speexx.event.sourcing.store.logbook.Logbook;
import java.util.List;
import java.util.Optional;

import static java.util.List.copyOf;

/**
 *
 * @author sascha.kohlmann
 */
public class AppendResultInterestImpl implements Logbook.AppendResultInterest {

    public StoreResult result;
    public String streamName;
    public int streamVersion;
    public List<Source<?>> sources;
    public Optional snapshot;
    
    @Override
    public <S, ST> void appendResultedIn(StoreResult result, String streamName, int streamVersion, List<Source<S>> sources, Optional<ST> snapshot) {
        this.result = result;
        this.streamName = streamName;
        this.streamVersion = streamVersion;
        this.sources = copyOf(sources);
        this.snapshot = snapshot;
    }
}
