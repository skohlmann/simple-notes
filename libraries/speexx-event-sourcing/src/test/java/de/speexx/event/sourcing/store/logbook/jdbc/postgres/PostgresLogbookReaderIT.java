/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.jdbc.postgres;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.truth.Truth.assertThat;
import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.Source;
import de.speexx.event.sourcing.store.StoreResult;
import de.speexx.event.sourcing.store.jdbc.postgres.EventSourceSupport;
import static de.speexx.event.sourcing.store.jdbc.postgres.PostgresBase.SERVER;
import de.speexx.event.sourcing.store.logbook.Logbook;
import de.speexx.event.sourcing.store.logbook.LogbookReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.Timeout;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 *
 * @author sascha.kohlmann
 */
@Tag("slow")
@Testcontainers
@Timeout(value = 60, unit = TimeUnit.SECONDS)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PostgresLogbookReaderIT extends EventSourceSupport {
    
    @Test
    @Order(1)
    public void append_event() throws Exception {
        try (final Connection connection = newConnection(false)) {
            // Given
            final Event event1 = Event.Builder.of().add("Test 1").add(OffsetDateTime.now()).build();
            final Event event2 = Event.Builder.of().add("Test 2").add(OffsetDateTime.now()).build();
            
            // When
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, noopOutboxController(), noopRouter());
            logBook.appendAll("simple-stream-name", 1, Arrays.asList(event1, event2), new CollectingAppendResultInterest());
            connection.commit();
        }
        
        try (final Connection connection = newConnection(false)) {
            // Then
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, noopOutboxController(), noopRouter());
            final LogbookReader reader = logBook.logbookReader("unknown");
            
            assertThat(reader.size().getAsLong()).isEqualTo(2);
            assertThat(reader.readNext().isPresent()).isTrue();
            assertThat(reader.readNext().isPresent()).isTrue();
            reader.rewind();
            assertThat(reader.readNext().toString()).contains("Test 1");
            assertThat(reader.readNext().toString()).contains("Test 2");
        }
    }

    @Test
    @Order(2)
    public void add_event_with_snapshot() throws Exception {
        try (final Connection connection = newConnection(false)) {
            // Given
            final Event event = Event.Builder.of().add("Test 3").add(OffsetDateTime.now()).build();
            final Entity entity = Entity.Builder.of().add("Rutschehof").add(new BigDecimal("1234567890.98765432")).build();
            
            // When
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, noopOutboxController(), noopRouter());
            logBook.appendWith("simple-stream-name", 2, event, entity, new CollectingAppendResultInterest());
            connection.commit();
        }
        try (final Connection connection = newConnection(false)) {
            // Then
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, noopOutboxController(), noopRouter());
            final LogbookReader reader = logBook.logbookReader("unknown");

            assertThat(reader.readNext().toString()).contains("Test 3");
        }
    }

    @Test
    @Order(3)
    public void append_3rd_event() throws Exception {
        // Given
        try (final Connection connection = newConnection(false)) {
            final Event event = Event.Builder.of().add("Test 4").add(OffsetDateTime.now()).build();
            
            // When
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, noopOutboxController(), noopRouter());
            logBook.append("simple-stream-name", 3, event, new CollectingAppendResultInterest());
            connection.commit();
        }
        try (final Connection connection = newConnection(false)) {
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, noopOutboxController(), noopRouter());
            final LogbookReader reader = logBook.logbookReader("unknown");
            
            assertThat(reader.size().getAsLong()).isEqualTo(4);
            final Optional<Memento> memento1 = reader.readNext();
            assertThat(memento1.isPresent()).isTrue();
            assertThat(memento1.get().isEmpty()).isFalse();

            reader.rewind();
            final List<Memento> mementos = reader.readNext(5);
            assertThat(mementos.size()).isEqualTo(4);

            assertThat(mementos.get(0).mementoVersion()).isEqualTo(1);
            assertThat(mementos.get(0).typeName()).isEqualTo(Event.class.getName());
            assertThat(mementos.get(2).mementoData().toString()).contains("Test 3");
        }
    }
    
    public static class Entity {
        
        @JsonProperty private String someData;
        @JsonProperty private BigDecimal amount;

        public Entity() {
            super();
        }

        private Entity(final Builder builder) {
            this();
            this.amount = builder.amount;
            this.someData = builder.someData;
        }

        public String someData() {
            return this.someData;
        }

        public BigDecimal amount() {
            return this.amount;
        }

        public static final class Builder {
            private String someData;
            private BigDecimal amount;
            
            private Builder() {}

            public static Builder of() {
                return new Builder();
            }

            public Builder add(final String someData) {
                this.someData = someData;
                return this;
            }

            public Builder add(final BigDecimal amount) {
                this.amount = amount;
                return this;
            }

            public Entity build() {
                return new Entity(this);
            }
        }

        @Override
        public String toString() {
            return "Entity{" + "someData=" + someData + ", amount=" + amount + '}';
        }
    }

    public static class Event implements Source<Object> {

        @JsonProperty private String data;
        @JsonProperty private OffsetDateTime datatime;

        public Event() {
            super();
        }

        private Event(final Builder builder) {
            this();
            this.data = builder.data;
            this.datatime = builder.datatime;
        }

        public String data() {
            return this.data;
        }

        public OffsetDateTime datatime() {
            return this.datatime;
        }

        @Override
        public int sourceVersion() {
            return 1;
        }

        @Override
        public OffsetDateTime sourcedAt() {
            return this.datatime;
        }

        @Override
        public boolean isNull() {
            return false;
        }

        public static final class Builder {
            String data;
            OffsetDateTime datatime;

            private Builder() {}

            public static Builder of() {
                return new Builder();
            }

            public Builder add(final String data) {
                this.data = data;
                return this;
            }

            public Builder add(final OffsetDateTime datatime) {
                this.datatime = datatime;
                return this;
            }

            public Event build() {
                return new Event(this);
            }
        }

        @Override
        public String toString() {
            return "Event{" + "data=" + data + ", datatime=" + datatime + '}';
        }
    }
    
    
    public static final class CollectingAppendResultInterest implements Logbook.AppendResultInterest {
        
        @Override
        public <S, ST> void appendResultedIn(StoreResult result, String streamName, int streamVersion, List<Source<S>> sources, Optional<ST> snapshot) {
            return;
        }
    }

    @BeforeAll
    public static void startContainer() throws Exception {
        SERVER.beforeAll(null);
    }

    @AfterAll
    public static void stopContainer() throws Exception {
        SERVER.afterAll(null);
    }
}
