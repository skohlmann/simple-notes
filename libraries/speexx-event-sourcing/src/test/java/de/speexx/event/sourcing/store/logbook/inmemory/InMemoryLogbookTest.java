/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.inmemory;

import de.speexx.event.sourcing.BaseMemento;
import de.speexx.event.sourcing.MementoAdapterProvider;
import de.speexx.event.sourcing.Snapshot;
import de.speexx.event.sourcing.SnapshotAdapterProvider;
import de.speexx.event.sourcing.store.ResultType;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static java.util.List.of;

/**
 *
 * @author sascha.kohlmann
 */
public class InMemoryLogbookTest {

    @Test
    public void appendAll_with_two_events() {
        // Given
        final var inMemory = new InMemoryLogbook<String, Snapshot.TextSnapshot>(new MementoAdapterProvider(), new SnapshotAdapterProvider());
        final var firstEvent = new FirstEvent("123");
        final var secondEvent = new SecondEvent("abc");
        final var result = new AppendResultInterestImpl();

        // When
        inMemory.appendAll("dummy", 1, of(firstEvent, secondEvent), result);
        
        // Then
        assertThat(result.result.resultType()).isEqualTo(ResultType.SUCCESS);
        
        // And When
        final var stream = inMemory.streamReader().streamFor("dummy");
        
        // Then
        assertThat(stream.hasSnapshot()).isFalse();
        assertThat(stream.mementos().get(0)).isInstanceOf(BaseMemento.TextMemento.class);
        assertThat(stream.mementos().get(0).typeName()).isEqualTo(FirstEvent.class.getName());
        assertThat(stream.mementos().get(1).typeName()).isEqualTo(SecondEvent.class.getName());
    }

    @Test
    public void appendAllWith_with_two_events() {
        // Given
        final var inMemory = new InMemoryLogbook<String, Snapshot.TextSnapshot>(new MementoAdapterProvider(), new SnapshotAdapterProvider());
        final var firstEvent = new FirstEvent("123");
        final var snapshot = new SimpleSnapshotable();
        snapshot.data = "snapshot data";
        final var result = new AppendResultInterestImpl();

        // When
        inMemory.appendAllWith("dummy", 1, of(firstEvent), snapshot, result);
        
        // Then
        assertThat(result.result.resultType()).isEqualTo(ResultType.SUCCESS);

        // And Given
        final var secondEvent = new SecondEvent("abc");

        // And When
        inMemory.appendAll("dummy", 2, of(secondEvent), result);
        final var stream2 = inMemory.streamReader().streamFor("dummy", 2);
        
        // And Then
        assertThat(stream2.hasSnapshot()).isTrue();
        assertThat(stream2.mementos().get(0).typeName()).isEqualTo(SecondEvent.class.getName());

        // And Given
        final var thirdEvent = new ThirdEvent("#+!");

        // And When
        inMemory.appendAll("dummy", 3, of(thirdEvent), result);
        final var stream3 = inMemory.streamReader().streamFor("dummy", 2);
        
        // And Then
        assertThat(stream3.hasSnapshot()).isTrue();
        assertThat(stream3.mementos().get(0).typeName()).isEqualTo(SecondEvent.class.getName());
        assertThat(stream3.mementos().get(1).typeName()).isEqualTo(ThirdEvent.class.getName());

        // And When
        final var stream4 = inMemory.streamReader().streamFor("dummy", 3);
        
        // And Then
        assertThat(stream4.hasSnapshot()).isTrue();
        assertThat(stream4.mementos().get(0).typeName()).isEqualTo(SecondEvent.class.getName());
    }
}
