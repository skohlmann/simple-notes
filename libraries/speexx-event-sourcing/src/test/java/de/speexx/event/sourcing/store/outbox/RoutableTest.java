/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.outbox;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.event.sourcing.BaseMemento;
import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.Snapshot;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class RoutableTest {
    
    @Test
    public void simple_create() {
        final LocalDateTime now = LocalDateTime.now();
        final Routable routable = new Routable("abc", now, Snapshot.TextSnapshot.NULL, Arrays.asList(BaseMemento.NullMemento.TEXT));
        
        assertThat(routable.id()).isEqualTo("abc");
        assertThat(routable.createdOn()).isEqualTo(now);
        assertThat(routable.typedSnapshot()).isEqualTo(Snapshot.TextSnapshot.NULL);
        assertThat(routable.mementos().size()).isEqualTo(1);
        assertThat(routable.mementos().get(0)).isEqualTo(BaseMemento.NullMemento.TEXT);
        assertThat(routable.snapshot().get()).isEqualTo(Snapshot.TextSnapshot.NULL);
        assertThat(routable.toString()).isNotNull();

    }

    @Test
    public void create_with_null_id() {
        final NullPointerException ex = assertThrows(NullPointerException.class, () -> new Routable(null, LocalDateTime.now(), Snapshot.TextSnapshot.NULL, Arrays.asList(BaseMemento.NullMemento.TEXT)));
        assertThat(ex.getMessage()).contains("ID must be provided");
    }
    
    @Test
    public void create_with_null_createdOn() {
        final NullPointerException ex = assertThrows(NullPointerException.class, () -> new Routable("abc", null, Snapshot.TextSnapshot.NULL, Arrays.asList(BaseMemento.NullMemento.TEXT)));
        assertThat(ex.getMessage()).contains("Created On must be provided");
    }
    
    @Test
    public void create_with_null_snapshot() {
        final Routable routable = new Routable("abc", LocalDateTime.now(), null, Arrays.asList(BaseMemento.NullMemento.TEXT));
        assertThat(routable.snapshot().isPresent()).isFalse();
    }
    
    @Test
    public void create_with_null_mementos() {
        final Routable routable = new Routable("abc", LocalDateTime.now(), Snapshot.TextSnapshot.NULL, null);
        assertThat(routable.mementos().isEmpty()).isTrue();
    }
    
    @Test
    public void create_with_modifiable_memento_list() {
        final List<Memento> mementos = new ArrayList<>();
        final Routable routable = new Routable("abc", LocalDateTime.now(), Snapshot.TextSnapshot.NULL, mementos);
        assertThat(routable.mementos()).isNotSameInstanceAs(mementos.getClass());
    }
}
