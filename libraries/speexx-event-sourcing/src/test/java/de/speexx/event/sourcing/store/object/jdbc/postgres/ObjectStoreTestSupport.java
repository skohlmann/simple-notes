/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object.jdbc.postgres;

import de.speexx.event.sourcing.MementoAdapterProvider;
import de.speexx.event.sourcing.Source;
import de.speexx.event.sourcing.store.StoreResult;
import de.speexx.event.sourcing.store.jdbc.postgres.PostgresBase;
import de.speexx.event.sourcing.store.object.ObjectStore.StoreResultInterest;
import java.util.List;

/**
 *
 * @author sascha.kohlmann
 */
public abstract class ObjectStoreTestSupport extends PostgresBase {

    protected MementoAdapterProvider mementoAdapterProvider() {
        final MementoAdapterProvider provider = new MementoAdapterProvider();
        provider.registerAdapter("integer", new IntegerMementoAdapter())
                .registerAdapter(DateTimeSource.class, new DateTimeMementoAdapter());
        return provider;
    }

    protected StoreResultInterestImpl storeResultInterest() {
        return new StoreResultInterestImpl();
    }
    
    public static final class StoreResultInterestImpl implements StoreResultInterest {
        
        public StoreResult result;
        public String objectName;
        public List<Source> sources;

        @Override
        public <S> void storeResultedIn(final StoreResult result, final String objectName, final List<Source<S>> sources) {
            this.result = result;
            this.objectName = objectName;
            if (sources != null) {
                this.sources = (List<Source>) List.copyOf((List<? extends S>) sources);
            }
        }

        @Override
        public String toString() {
            return "StoreResultInterestImpl{" + "result=" + result + ", objectName=" + objectName + ", sources=" + sources + '}';
        }
    }
}
