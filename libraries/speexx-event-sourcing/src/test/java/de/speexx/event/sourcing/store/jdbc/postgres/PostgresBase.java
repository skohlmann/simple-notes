/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.jdbc.postgres;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Collectors;
import javax.sql.DataSource;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.postgresql.ds.PGSimpleDataSource;
import org.testcontainers.containers.PostgreSQLContainer;
import static org.testcontainers.utility.DockerImageName.parse;

/**
 *
 * @author sascha.kohlmann
 */
public abstract class PostgresBase {

    @RegisterExtension
    public static final PostgresqlServerExtension SERVER = new PostgresqlServerExtension();

    public Connection newConnection(final boolean autoCommit) throws SQLException {
        final Connection conn = SERVER.newConnection();
        conn.setAutoCommit(autoCommit);
        return conn;
    }

    public static final class PostgresqlServerExtension implements BeforeAllCallback, AfterAllCallback {

        private final PostgreSQLContainer<?> container = new PostgreSQLContainer<>(parse("postgres:latest"));

        private DataSource dataSource;

        @Override
        public void afterAll(final ExtensionContext context) {
            this.container.stop();
        }

        @Override
        public void beforeAll(final ExtensionContext context) throws SQLException, IOException {
            this.container.start();
            this.dataSource = dataSource(this);


            final String initScript = loadInitScript();
            final Statement stmt = newConnection().createStatement();
            stmt.execute(initScript);
        }

        String getDatabase() {
            return this.container.getDatabaseName();
        }

        String getHost() {
            return this.container.getContainerIpAddress();
        }

        Connection newConnection() throws SQLException {
            return this.dataSource.getConnection();
        }

        String getPassword() {
            return this.container.getPassword();
        }

        int getPort() {
            return this.container.getMappedPort(5432);
        }

        String getUsername() {
            return this.container.getUsername();
        }
        
        String jdbcUrl() {
            return this.container.getJdbcUrl();
        }

       private DataSource dataSource(final PostgresqlServerExtension extension) {
            final PGSimpleDataSource ds = new PGSimpleDataSource();

            ds.setURL(extension.jdbcUrl());
            ds.setDatabaseName(extension.getDatabase());
            ds.setUser(extension.getUsername());
            ds.setPassword(extension.getPassword());

            return ds;
        }

        private String loadInitScript() throws IOException {
            final String testHeader = Files.lines(Paths.get(System.getProperty("db-test-script"))).collect(Collectors.joining("\n"));
            final String createDb = Files.lines(Paths.get(System.getProperty("db-init-script"))).collect(Collectors.joining("\n"));
            return testHeader + createDb;
        }
    }
}
