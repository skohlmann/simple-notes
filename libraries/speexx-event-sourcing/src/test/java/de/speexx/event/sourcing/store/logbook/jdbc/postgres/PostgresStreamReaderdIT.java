/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.jdbc.postgres;

import de.speexx.event.sourcing.store.jdbc.postgres.EventSourceSupport;
import static com.google.common.truth.Truth.assertThat;
import static de.speexx.event.sourcing.store.jdbc.postgres.PostgresBase.SERVER;
import de.speexx.event.sourcing.store.logbook.Stream;
import java.math.BigDecimal;
import java.sql.Connection;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import de.speexx.event.sourcing.store.logbook.Logbook;
import java.util.Arrays;

import de.speexx.event.sourcing.store.logbook.jdbc.postgres.PostgresLogbookReaderIT.Event;
import de.speexx.event.sourcing.store.logbook.jdbc.postgres.PostgresLogbookReaderIT.Entity;
import de.speexx.event.sourcing.store.logbook.jdbc.postgres.PostgresLogbookReaderIT.CollectingAppendResultInterest;
import java.time.OffsetDateTime;
import de.speexx.event.sourcing.store.logbook.StreamReader;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.Timeout;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 *
 * @author sascha.kohlmann
 */
@Tag("slow")
@Testcontainers
@Timeout(value = 60, unit = TimeUnit.SECONDS)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PostgresStreamReaderdIT extends EventSourceSupport {
 
    @Test
    @Order(1)
    public void append_events_for_stream_base() throws Exception {
        // Given
        try (final Connection connection = newConnection(false)) {
            final Event event1 = Event.Builder.of().add("Base 1").add(OffsetDateTime.now()).build();
            final Event event2 = Event.Builder.of().add("Base 2").add(OffsetDateTime.now()).build();
            
            // When
            final Logbook logBook = provideAsynchronousPostgresLogbook(connection, false);
            logBook.appendAll("base", 1, Arrays.asList(event1, event2), new CollectingAppendResultInterest());
            connection.commit();
        }
        try (final Connection connection = newConnection(false)) {
            // Then
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, noopOutboxController(), noopRouter());
            final StreamReader reader = logBook.streamReader();
            final Stream<String> stream = reader.streamFor("base");
            assertThat(stream.size()).isEqualTo(2);
        }
    }

    @Test
    @Order(2)
    public void append_events_with_snapshot_for_stream_base() throws Exception {
        // Given
        try (final Connection connection = newConnection(false)) {
            final Event event1 = Event.Builder.of().add("Base 3").add(OffsetDateTime.now()).build();
            final Event event2 = Event.Builder.of().add("Base 4").add(OffsetDateTime.now()).build();
            final Entity entity = Entity.Builder.of().add("Rutschehof").add(new BigDecimal("1234567890.98765432")).build();
            
            // When
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, noopOutboxController(), noopRouter());
            logBook.appendAllWith("base", 3, Arrays.asList(event1, event2), entity, new CollectingAppendResultInterest());
            connection.commit();
        }
        try (final Connection connection = newConnection(false)) {
            // Then
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, noopOutboxController(), noopRouter());
            final StreamReader reader = logBook.streamReader();

            final Stream<String> stream = reader.streamFor("base");
            assertThat(stream.hasSnapshot()).isTrue();
            assertThat(stream.size()).isEqualTo(2);

            final Stream<String> stream1 = reader.streamFor("base", 1);
            assertThat(stream1.hasSnapshot()).isTrue();
            assertThat(stream1.size()).isEqualTo(4);
        }
    }

    @Test
    @Order(3)
    public void append_events_with_snapshot_for_stream_base_and_stream_base2() throws Exception {
        // Given
        try (final Connection connection = newConnection(false)) {
            final Event event1 = Event.Builder.of().add("Base 5").add(OffsetDateTime.now()).build();
            final Event event2 = Event.Builder.of().add("Base 6").add(OffsetDateTime.now()).build();
            
            // When
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, noopOutboxController(), noopRouter());
            logBook.appendAll("base2", 1, Arrays.asList(event1, event2), new CollectingAppendResultInterest());
            logBook.appendAll("base", 5, Arrays.asList(event1, event2), new CollectingAppendResultInterest());
            connection.commit();
        }
        try (final Connection connection = newConnection(false)) {
            // Then
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, noopOutboxController(), noopRouter());
            final StreamReader reader = logBook.streamReader();

            final Stream<String> stream = reader.streamFor("base");
            assertThat(stream.hasSnapshot()).isTrue();
            assertThat(stream.size()).isEqualTo(4);

            final Stream<String> stream1 = reader.streamFor("base", 1);
            assertThat(stream1.hasSnapshot()).isTrue();
            assertThat(stream1.size()).isEqualTo(6);

            final Stream<String> streamBase2 = reader.streamFor("base2");
            assertThat(streamBase2.hasSnapshot()).isFalse();
            assertThat(streamBase2.size()).isEqualTo(2);

            final Stream<String> streamBase21 = reader.streamFor("base2", 1);
            assertThat(streamBase21.size()).isEqualTo(2);
        }
    }

    @BeforeAll
    public static void startContainer() throws Exception {
        SERVER.beforeAll(null);
    }

    @AfterAll
    public static void stopContainer() throws Exception {
        SERVER.afterAll(null);
    }
}
