/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static com.google.common.truth.Truth.assertThat;

/**
 *
 * @author sascha.kohlmann
 */
public class NullSourceTest {

    @Test
    public void unsupported_sourceVersion() {
        assertThrows(UnsupportedOperationException.class, () -> new NullSource<>().sourceVersion());
    }

    @Test
    public void unsupported_sourcedAt() {
        assertThrows(UnsupportedOperationException.class, () -> new NullSource<>().sourcedAt());
    }

    @Test
    public void sourceName() {
        assertThat(new NullSource().sourceName()).isEqualTo("NullSource");
    }
}
