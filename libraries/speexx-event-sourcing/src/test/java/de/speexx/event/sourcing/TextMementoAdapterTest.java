/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.speexx.json.seralizer.JsonSerializer;
import java.time.OffsetDateTime;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author sascha.kohlmann
 */
public class TextMementoAdapterTest {

    @Test
    public void simple_asMemento() {
        final Event event = new Event("MementoTests");
        final BaseMemento.TextMemento memento = new MementoAdapterProvider().asMemento(event);
        
        assertThat(memento.id()).isEmpty();
        assertThat(memento.mementoVersion()).isEqualTo(1);
        assertThat(memento.typeName()).isEqualTo(Event.class.getName());
        assertThat(memento.mementoData()).contains("MementoTests");
    }

    @Test
    public void simple_asSource() {
        final Event event = new Event("MementoTests");
        final BaseMemento.TextMemento memento = new MementoAdapterProvider().asMemento(event);
        final Source source = new MementoAdapterProvider().asSource(memento);
        
        assertThat(source).isInstanceOf(Event.class);
        assertThat(((Event) source).value).isEqualTo("MementoTests");
    }
    
    @Test
    public void simple_toMemento_with_id() {
        final Event event = new Event("MementoTests");
        final BaseMemento.TextMemento memento = new MementoAdapterProvider().asMemento(event);
        final Source source = new MementoAdapterProvider().asSource(memento);

        final Memento newMemento = new TextMementoAdapter(JsonSerializer.instance()).toMemento(source, "123");
        assertThat(newMemento.id()).isEqualTo("123");
    }
    
    @Test
    public void class_not_found() {
        final Memento memento = new DummyMemento(String.class, 1, "abc");
        final IllegalStateException execption = assertThrows(IllegalStateException.class, () -> new TextMementoAdapter(JsonSerializer.instance()).fromMemento(memento));
        
        assertThat(execption.getLocalizedMessage()).contains("Cannot convert to type: test");
    }
    
    private static final class DummyMemento extends TestBaseMemento {

        public DummyMemento(final Class<?> type, final int mementoVersion, final String mementoData) {
            super(type, mementoVersion, mementoData);
        }
        
        @Override
        public String typeName() {
            return "test";
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
    }
    
    public static final class Event implements Source<Object> {
        @JsonProperty("value") String value;
        @JsonProperty("source-version") private int sourceVersion = 1;
        @JsonProperty("sourced-at") private OffsetDateTime sourcedAt = OffsetDateTime.now();
        @JsonProperty("sourced-name") private String sourceName = getClass().getSimpleName();
        
        public Event(final String value) {
            this.value = value;
        }
        
        private Event() {}

        @Override public int sourceVersion() {return 1;}
        @Override public OffsetDateTime sourcedAt() {return OffsetDateTime.now();}
        @Override @JsonIgnore public boolean isNull() {return false;}
    }
}
