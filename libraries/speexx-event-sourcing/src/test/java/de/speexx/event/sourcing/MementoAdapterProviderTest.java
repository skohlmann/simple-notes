/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import de.speexx.json.seralizer.JsonSerializer;
import java.time.OffsetDateTime;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 *
 * @author sascha.kohlmann
 */
public class MementoAdapterProviderTest {
    
    @Test
    public void register_adapter() {
        // Given
        final MementoAdapterProvider provider = new MementoAdapterProvider();
        provider.registerAdapter(DummySource.class, new DummySourceTextMementoAdapter());
        
        // When
        final Memento memento = provider.asMemento(new DummySource());
        
        // Then
        assertThat(memento.mementoData()).isEqualTo("12345");
    }
    
    
    private static final class DummySource implements Source<String> {

        @Override public int sourceVersion() {return 1;}
        @Override public OffsetDateTime sourcedAt() {return OffsetDateTime.now();}
        @Override public boolean isNull() {return false;}
    }
    
    private static final class DummySourceTextMementoAdapter extends TextMementoAdapter {

        public DummySourceTextMementoAdapter() {
            super(JsonSerializer.instance());
        }
        
        @Override
        public BaseMemento.TextMemento toMemento(final Source source) {
            return new BaseMemento.TextMemento(source.getClass(), 1, "12345");
        }
    }
}
