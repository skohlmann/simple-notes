/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.jdbc.postgres;

import de.speexx.event.sourcing.MementoAdapterProvider;
import de.speexx.event.sourcing.SnapshotAdapterProvider;
import de.speexx.event.sourcing.store.logbook.jdbc.postgres.PostgresLogbook;
import de.speexx.event.sourcing.store.logbook.jdbc.postgres.PostgresOutboxController;
import de.speexx.event.sourcing.store.logbook.jdbc.postgres.PostgresRoutingResult;
import de.speexx.event.sourcing.store.outbox.Routable;
import de.speexx.event.sourcing.store.outbox.Router;
import de.speexx.event.sourcing.store.outbox.RoutingResult;
import java.sql.Connection;

/**
 *
 * @author sascha.kohlmann
 */
public abstract class EventSourceSupport extends PostgresBase {
    
    protected MementoAdapterProvider mementoAdapterProvider() {
        return new MementoAdapterProvider();
    }

    protected SnapshotAdapterProvider snapshotAdapterProvider() {
        return new SnapshotAdapterProvider();
    }

    protected PostgresLogbook provideSynchronousPostgresLogbook(final Connection connection, final PostgresOutboxController controller, final Router router) {
        return new PostgresLogbook(controller, new PostgresRoutingResult(), router, mementoAdapterProvider(), snapshotAdapterProvider(), connection);
    }

    protected PostgresLogbook provideSynchronousPostgresLogbook(final Connection connection, final PostgresOutboxController controller, final PostgresRoutingResult result, final Router router) {
        return new PostgresLogbook(controller, result, router, mementoAdapterProvider(), snapshotAdapterProvider(), connection);
    }

    protected PostgresLogbook provideAsynchronousPostgresLogbook(final Connection connection, final boolean toOutbox) {
        return new PostgresLogbook(toOutbox, mementoAdapterProvider(), snapshotAdapterProvider(), connection);
    }

    protected PostgresOutboxController noopOutboxController() {
        return new PostgresOutboxController(null) {
            @Override
            public void process(final Router router, final RoutingResult result) {}
        };
    }
    
    protected Router noopRouter() {
        return (Router) (Routable routable) -> {};
    }
}
