/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.seralizer.JsonSerializer;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class TextSnapshotAdapterTest {

    @Test
    public void simple_asRaw() {
        final Entity entity = new Entity("SnapshotTests");
        final Snapshot<String> snapshot = new SnapshotAdapterProvider().asRaw("abc", entity, 2);
        
        assertThat(snapshot.dataVersion()).isEqualTo(2);
        assertThat(snapshot.typeName()).isEqualTo(Entity.class.getName());
        assertThat(snapshot.typeVersion()).isEqualTo(1);
    }
    
    @Test
    public void simple_fromRaw() {
        final Entity entity = new Entity("SnapshotTests");
        final Snapshot<String> snapshot = new SnapshotAdapterProvider().asRaw("abc", entity, 2);
        final Object fromRaw = new SnapshotAdapterProvider().fromRaw(snapshot);

        assertThat(fromRaw).isInstanceOf(Entity.class);
        assertThat(((Entity) fromRaw).value).isEqualTo("SnapshotTests");
    }
    
    @Test
    public void toRawSnapshot_with_version() {
        final Entity entity = new Entity("SnapshotTests");
        final Snapshot<String> snapshot = new TextSnapshotAdapter(JsonSerializer.instance()).toRawSnapshot(entity, 2);
        
        assertThat(snapshot.dataVersion()).isEqualTo(2);
    }

    public static final class Entity {
        @JsonProperty("entity-value") final String value;
        
        public Entity(@JsonProperty("entity-value") final String value) {
            this.value = value;
        }
    }
}
