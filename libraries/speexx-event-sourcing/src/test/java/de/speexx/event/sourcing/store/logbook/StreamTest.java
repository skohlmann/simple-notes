/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook;

import de.speexx.event.sourcing.BaseMemento;
import de.speexx.event.sourcing.Snapshot;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * 
 * @author sascha.kohlmann
 */
public class StreamTest {
 
    @Test
    public void create_Stream() {
        // Given
        final BaseMemento baseMemento = new BaseMemento.TextMemento(String.class, 2, "data");
        final List<BaseMemento<String>> mementos = Arrays.asList(baseMemento);
        final Snapshot<String> snapshot = new DummySnapshot("id", String.class, 1, "abc", 3);
        
        // When
        final Stream stream = new Stream("uuid", 4, mementos, snapshot);
        
        // Then
        assertThat(stream.streamName()).isEqualTo("uuid");
        assertThat(stream.streamVersion()).isEqualTo(4);
        assertThat(stream.size()).isEqualTo(1);
        assertThat(stream.mementos().get(0)).isEqualTo(baseMemento);
        assertThat(stream.hasSnapshot()).isTrue();
        assertThat(stream.snapshot()).isEqualTo(snapshot);
        assertThat(stream.toString()).isNotNull();
        
        assertThrows(UnsupportedOperationException.class, () -> stream.mementos().remove(0));
        
    }
    
    @Test
    public void create_Stream_with_null_streamName() {
        final BaseMemento baseMemento = new BaseMemento.TextMemento(String.class, 2, "data");
        final List<BaseMemento<String>> mementos = Arrays.asList(baseMemento);
        final Snapshot<String> snapshot = new DummySnapshot("id", String.class, 1, "abc", 3);

        assertThrows(IllegalArgumentException.class, () -> new Stream(null, 4, mementos, snapshot));
    }
    
    @Test
    public void create_Stream_with_null_snapshot() {
        final BaseMemento baseMemento = new BaseMemento.TextMemento(String.class, 2, "data");
        final List<BaseMemento<String>> mementos = Arrays.asList(baseMemento);

        assertThrows(IllegalArgumentException.class, () -> new Stream("uuid", 4, mementos, null));
    }
    
    @Test
    public void create_Stream_with_Snapshot_TextSnapshot_ull() {
        final BaseMemento baseMemento = new BaseMemento.TextMemento(String.class, 2, "data");
        final List<BaseMemento<String>> mementos = Arrays.asList(baseMemento);

        assertThat(new Stream("uuid", 4, mementos, Snapshot.TextSnapshot.NULL).hasSnapshot()).isFalse();
    }
    
    @Test
    public void create_Stream_with_null_mementos() {
        final Snapshot<String> snapshot = new DummySnapshot("id", String.class, 1, "abc", 3);
        assertThrows(NullPointerException.class, () -> new Stream("uuid", 4, null, snapshot));
    }
    
    private static final class DummySnapshot extends Snapshot<String> {
        public DummySnapshot(final String id, final Class<?> type, final int typeVersion, final String data, final int dataVersion) {
            super(id, type, typeVersion, id, dataVersion);
        }
    }
}
