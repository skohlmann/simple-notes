/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.jdbc.postgres;

import de.speexx.event.sourcing.store.jdbc.postgres.EventSourceSupport;
import de.speexx.event.sourcing.store.logbook.Logbook;
import de.speexx.event.sourcing.store.outbox.Routable;
import de.speexx.event.sourcing.store.outbox.RoutedResultType;
import de.speexx.event.sourcing.store.outbox.Router;
import java.sql.Connection;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.Timeout;
import org.testcontainers.junit.jupiter.Testcontainers;

import static com.google.common.truth.Truth.assertThat;
import static de.speexx.event.sourcing.store.jdbc.postgres.PostgresBase.SERVER;

/**
 *
 * @author sascha.kohlmann
 */
@Tag("slow")
@Testcontainers
@Timeout(value = 60, unit = TimeUnit.SECONDS)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PostgresOutboxIT extends EventSourceSupport {
    
    @Test
    public void append_two_events_with_one_call_synchronous() throws Exception {
        
        // Given
        try (final Connection connection = newConnection(false)) {
            final PostgresLogbookReaderIT.Event event1 = PostgresLogbookReaderIT.Event.Builder.of().add("Base 1").add(OffsetDateTime.now()).build();
            final PostgresLogbookReaderIT.Event event2 = PostgresLogbookReaderIT.Event.Builder.of().add("Base 2").add(OffsetDateTime.now()).build();
            
            // When
            final CollectingRouter router = new CollectingRouter();
            final PostgresRoutingResult firstRoutingResult = new PostgresRoutingResult();
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, new PostgresOutboxController(connection), firstRoutingResult, router);
            logBook.appendAll("append_two_events_with_one_call", 1, Arrays.asList(event1, event2), new PostgresLogbookReaderIT.CollectingAppendResultInterest());
            connection.commit();

            // Then
            assertThat(router.routables().size()).isEqualTo(1);
            assertThat(router.routables().get(0).mementos().size()).isEqualTo(2);
            assertThat(router.routables().get(0).mementos().toString()).contains("Base 1");
            assertThat(router.routables().get(0).mementos().toString()).contains("Base 2");
            assertThat(router.routables().get(0).mementos().get(0).toString()).contains("Base 1");
            assertThat(router.routables().get(0).mementos().get(1).toString()).contains("Base 2");
            
            assertThat(firstRoutingResult.routed()).isEqualTo(1);
            
            // And When
            final PostgresRoutingResult secondRoutingResult = new PostgresRoutingResult();
            new PostgresOutboxController(connection).process(router, secondRoutingResult);
            assertThat(secondRoutingResult.routed()).isEqualTo(0);
            
            // Then
            assertThat(router.routables().size()).isEqualTo(1);
        }
    }
    
    @Test
    public void append_two_events_with_one_call_each_asynchronous() throws Exception {

        // Given
        try (final Connection connection = newConnection(false)) {
            final PostgresLogbookReaderIT.Event event1 = PostgresLogbookReaderIT.Event.Builder.of().add("Base 1").add(OffsetDateTime.now()).build();
            final PostgresLogbookReaderIT.Event event2 = PostgresLogbookReaderIT.Event.Builder.of().add("Base 2").add(OffsetDateTime.now()).build();
            
            // When
            final Logbook logBook = provideAsynchronousPostgresLogbook(connection, true);
            logBook.append("append_two_events_with_one_call_each", 1, event1, new PostgresLogbookReaderIT.CollectingAppendResultInterest());
            logBook.append("append_two_events_with_one_call_each", 2, event2, new PostgresLogbookReaderIT.CollectingAppendResultInterest());
            connection.commit();

            final PostgresRoutingResult routingResult = new PostgresRoutingResult();
            final CollectingRouter router = new CollectingRouter();
            new PostgresOutboxController(connection).process(router, routingResult);

            // Then
            assertThat(router.routables().size()).isEqualTo(2);
            assertThat(router.routables().get(0).mementos().get(0).toString()).contains("Base 1");
            assertThat(router.routables().get(1).mementos().get(0).toString()).contains("Base 2");
            
            assertThat(routingResult.routed()).isEqualTo(2);
            assertThat(routingResult.exception().isPresent()).isFalse();
            assertThat(routingResult.resultType()).isEqualTo(RoutedResultType.SUCCESS);
        }
    }

    @Test
    public void append_two_events_with_one_call_each_asynchronous_and_routing_limit() throws Exception {

        // Given
        try (final Connection connection = newConnection(false)) {
            final PostgresLogbookReaderIT.Event event1 = PostgresLogbookReaderIT.Event.Builder.of().add("Base 1").add(OffsetDateTime.now()).build();
            final PostgresLogbookReaderIT.Event event2 = PostgresLogbookReaderIT.Event.Builder.of().add("Base 2").add(OffsetDateTime.now()).build();
            
            // When
            final Logbook logBook = provideAsynchronousPostgresLogbook(connection, true);
            logBook.append("append_two_events_with_one_call_each", 1, event1, new PostgresLogbookReaderIT.CollectingAppendResultInterest());
            logBook.append("append_two_events_with_one_call_each", 2, event2, new PostgresLogbookReaderIT.CollectingAppendResultInterest());
            connection.commit();

            final CollectingRouter router = new CollectingRouter();
            final PostgresRoutingResult firstRoutingResult = new PostgresRoutingResult();
            new PostgresOutboxController(connection, 1).process(router, firstRoutingResult);

            // Then
            assertThat(router.routables().size()).isEqualTo(1);
            assertThat(router.routables().get(0).mementos().get(0).toString()).contains("Base 1");
            
            assertThat(firstRoutingResult.routed()).isEqualTo(1);

            // And When
            final PostgresRoutingResult secondRoutingResult = new PostgresRoutingResult();
            new PostgresOutboxController(connection, 1).process(router, secondRoutingResult);

            // And Then
            assertThat(router.routables().size()).isEqualTo(2);
            assertThat(router.routables().get(1).mementos().get(0).toString()).contains("Base 2");
            
            assertThat(secondRoutingResult.routed()).isEqualTo(1);
        }
    }
    
    @Test
    public void append_event_with_one_call_each_asynchronous_no_routing() throws Exception {

        // Given
        try (final Connection connection = newConnection(false)) {
            final PostgresLogbookReaderIT.Event event1 = PostgresLogbookReaderIT.Event.Builder.of().add("Base 1").add(OffsetDateTime.now()).build();
            
            // When
            final Logbook logBook = provideAsynchronousPostgresLogbook(connection, false);
            logBook.append("append_two_events_with_one_call_each_no_routing", 1, event1, new PostgresLogbookReaderIT.CollectingAppendResultInterest());
            connection.commit();

            final CollectingRouter router = new CollectingRouter();
            new PostgresOutboxController(connection).process(router);

            // Then
            assertThat(router.routables().size()).isEqualTo(0);
        }
    }
    
    @Test
    public void append_two_events_with_one_call_each_and_failing_routing_first() throws Exception {

        // Given
        try (final Connection connection = newConnection(false)) {
            final PostgresLogbookReaderIT.Event event1 = PostgresLogbookReaderIT.Event.Builder.of().add("Base 1").add(OffsetDateTime.now()).build();
            final PostgresLogbookReaderIT.Event event2 = PostgresLogbookReaderIT.Event.Builder.of().add("Base 2").add(OffsetDateTime.now()).build();
            
            // When
            final CollectingRouter router = new FirstCallFailingCollectingRouter();
            final Logbook logBook = provideSynchronousPostgresLogbook(connection, new PostgresOutboxController(connection), router);
            logBook.append("append_two_events_with_one_call_each_and_failing_routing_first", 1, event1, new PostgresLogbookReaderIT.CollectingAppendResultInterest());

            // Then
            assertThat(router.routables().size()).isEqualTo(0);
 
            // And When
            logBook.append("append_two_events_with_one_call_each_and_failing_routing_first", 2, event2, new PostgresLogbookReaderIT.CollectingAppendResultInterest());
            connection.commit();

            // Then
            assertThat(router.routables().size()).isEqualTo(2);
            assertThat(router.routables().get(0).mementos().get(0).toString()).contains("Base 1");
            assertThat(router.routables().get(1).mementos().get(0).toString()).contains("Base 2");
            
            // And When
            new PostgresOutboxController(connection).process(router);
            
            // Then
            assertThat(router.routables().size()).isEqualTo(2);
        }
    }
    
    public static class CollectingRouter implements Router<Routable> {

        private final List<Routable> routables = new ArrayList<>();

        
        public List<Routable> routables() {
            return this.routables;
        }

        @Override
        public void route(Routable routable) throws RuntimeException {
            this.routables.add(routable);
        }
    }

    public static class FirstCallFailingCollectingRouter extends CollectingRouter {
        
        private boolean failed = false;

        @Override
        public void route(Routable routable) throws RuntimeException {
            if (!this.failed) {
                this.failed = true;
                throw new IllegalStateException("Because it's a test!");
            }
            super.route(routable);
        }
    }

    @BeforeAll
    public static void startContainer() throws Exception {
        SERVER.beforeAll(null);
    }

    @AfterAll
    public static void stopContainer() throws Exception {
        SERVER.afterAll(null);
    }
}
