/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object.jdbc.postgres;

import de.speexx.event.sourcing.Source;
import java.time.OffsetDateTime;

/**
 *
 * @author sascha.kohlmann
 */
public class IntegerSource implements Source<Integer> {

    private int sourceVersion;
    private OffsetDateTime datetime;
    private int value;
    
    public IntegerSource(final int sourceVersion, final OffsetDateTime datetime, final int value) {
        this.sourceVersion = sourceVersion;
        this.datetime = datetime;
        this.value = value;
    }
    
    public Integer value() {
        return this.value;
    }
    
    @Override
    public int sourceVersion() {
        return this.sourceVersion;
    }

    @Override
    public OffsetDateTime sourcedAt() {
        return this.datetime;
    }

    @Override
    public String sourceName() {
        return "integer";
    }

    @Override
    public boolean isNull() {
        return false;
    }
}
