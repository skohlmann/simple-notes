DROP USER IF EXISTS event;
CREATE USER event CREATEDB PASSWORD 'test';

DROP DATABASE IF EXISTS event_sourcing;
CREATE DATABASE event_sourcing WITH OWNER event;

GRANT ALL PRIVILEGES ON DATABASE event_sourcing to event;

-- \connect event_sourcing

