
CREATE TABLE tbl_event_logbook (
    id                 UUID         PRIMARY KEY,
    stream_name        VARCHAR(128) NOT NULL,
    stream_version     INTEGER      NOT NULL,
    memento_data       TEXT         NOT NULL,
    memento_type       VARCHAR(256) NOT NULL,
    memento_type_version INTEGER    NOT NULL,
    memento_created_at        TIMESTAMP WITH TIME ZONE NOT NULL,
    memento_created_at_offset INTEGER NOT NULL,
    event_timestamp           BIGINT NOT NULL
);

CREATE INDEX ON tbl_event_logbook (event_timestamp);
CREATE INDEX ON tbl_event_logbook (stream_name, stream_version);

CREATE TABLE tbl_event_logbook_snapshots (
    stream_name           VARCHAR(128) PRIMARY KEY,
    snapshot_type         VARCHAR(256) NOT NULL,
    snapshot_type_version INTEGER      NOT NULL,
    snapshot_data         JSONB        NOT NULL,
    snapshot_data_version INTEGER      NOT NULL
);

CREATE TABLE tbl_event_logbook_offsets (
    reader_name    VARCHAR(128) PRIMARY KEY,
    reader_offset  BIGINT NOT NULL,
    reader_last_op TIMESTAMP
);

CREATE TABLE tbl_event_logbook_outbox (
    id                    UUID PRIMARY KEY,
    created_timestamp     TIMESTAMP NOT NULL,
    route_id              VARCHAR(128) NOT NULL,
    snapshot_id           VARCHAR(128) NULL,
    snapshot_type         VARCHAR(256) NULL,
    snapshot_type_version INTEGER NULL,
    snapshot_data         TEXT NULL,
    snapshot_data_version INT NULL,
    memento_ids           TEXT NOT NULL
);

CREATE INDEX ON tbl_event_logbook_outbox (created_timestamp);
CREATE INDEX ON tbl_event_logbook_outbox (route_id);

ALTER TABLE tbl_event_logbook OWNER TO event;
ALTER TABLE tbl_event_logbook_outbox OWNER TO event;
ALTER TABLE tbl_event_logbook_offsets OWNER TO event;
ALTER TABLE tbl_event_logbook_snapshots OWNER TO event;



CREATE TABLE IF NOT EXISTS tbl_event_object (
    id                   UUID          PRIMARY KEY,
    object_name          VARCHAR(128)  NOT NULL,
    memento_data         TEXT          NOT NULL,
    memento_type         VARCHAR(256)  NOT NULL,
    memento_type_version INTEGER       NOT NULL,
    memento_created_at_nanos BIGINT    NOT NULL,
    memento_created_zone VARCHAR(10)   NOT NULL
);

CREATE TABLE IF NOT EXISTS tbl_event_object_offsets (
    reader_name    VARCHAR(128) PRIMARY KEY,
    reader_offset  BIGINT       NOT NULL,
    reader_counter INTEGER      NOT NULL,
    reader_last_op TIMESTAMP    NOT NULL
);

CREATE UNIQUE INDEX CONCURRENTLY IF NOT EXISTS idx_object_name_type ON tbl_event_object (object_name, memento_type);
CREATE INDEX IF NOT EXISTS idx_object_name_nanos ON tbl_event_object (object_name, memento_created_at_nanos ASC);
CREATE INDEX IF NOT EXISTS idx_nanos ON tbl_event_object (memento_created_at_nanos ASC);

ALTER TABLE tbl_event_object OWNER TO event;
ALTER TABLE tbl_event_object_offsets OWNER TO event;

ALTER TABLE tbl_event_object ADD CONSTRAINT unique_object_name_type UNIQUE USING INDEX idx_object_name_type;
