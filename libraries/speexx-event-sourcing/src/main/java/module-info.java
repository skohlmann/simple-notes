/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */

module speexx.event.sourcing {
    requires java.sql;
    requires java.naming;
    requires speexx.json.serializer;
    requires speexx.serializer.api;
    requires speexx.common;
    requires speexx.common.annotation;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.datatype.jdk8;
    requires org.apiguardian.api;
    requires static org.postgresql.jdbc;
    
    exports de.speexx.event.sourcing;
    exports de.speexx.event.sourcing.store;
    exports de.speexx.event.sourcing.store.logbook;
    exports de.speexx.event.sourcing.store.object;
    exports de.speexx.event.sourcing.store.outbox;
    exports de.speexx.event.sourcing.store.logbook.inmemory;
    exports de.speexx.event.sourcing.store.logbook.jdbc.postgres;
    exports de.speexx.event.sourcing.store.object.jdbc.postgres;

    opens de.speexx.event.sourcing;
    opens de.speexx.event.sourcing.store.logbook.inmemory;
    opens de.speexx.event.sourcing.store.logbook.jdbc.postgres;
}
