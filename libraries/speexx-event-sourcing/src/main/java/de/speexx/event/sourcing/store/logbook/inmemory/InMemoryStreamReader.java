/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.inmemory;

import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.Snapshot;
import de.speexx.event.sourcing.store.logbook.Stream;
import de.speexx.event.sourcing.store.logbook.StreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static de.speexx.common.Assertions.assertNotNull;

/**
 *
 * @author sascha.kohlmann
 */
public final class InMemoryStreamReader<T> implements StreamReader<T> {
    
    private final List<Memento<T>> logbook;
    private final Map<String, Map<Integer, Integer>> indices;
    private final Map<String, Snapshot<T>> snapshots;
    
    InMemoryStreamReader(final List<Memento<T>> logbook, final Map<String, Map<Integer, Integer>> indices, final Map<String, Snapshot<T>> snapshots) {
        this.logbook = assertNotNull(logbook, "Logbook must be provided");
        this.indices = assertNotNull(indices, "Indices must be provided");
        this.snapshots = assertNotNull(snapshots, "Snapshots must be provided");
    }

    @Override
    public Stream<T> streamFor(final String streamName) {
        return streamFor(streamName, 1);
    }

    @Override
    public Stream<T> streamFor(final String streamName, final int fromStreamVersion) {
        var version = fromStreamVersion;
        var snapshot = snapshotForStream(streamName);

        if (!snapshot.isNull()) {
            if (snapshot.dataVersion() <= version) {
                version = snapshot.dataVersion() + 1;
            } else {
                snapshot = (Snapshot<T>) Snapshot.TextSnapshot.NULL;
            }
        }

        final var mementos = new ArrayList<Memento<T>>();
        final var versionIndices = this.indices.get(streamName);
        if (versionIndices != null) {
            var logbookIndex = versionIndices.get(version);

            while (logbookIndex != null) {
                final var memento = this.logbook.get(logbookIndex);
                mementos.add(memento);
                logbookIndex = versionIndices.get(++version);
            }
        }
 
        final var stream = new Stream(streamName, version - 1, mementos, snapshot);
        return stream;
    }

    private Snapshot<T> snapshotForStream(final String streamName) {
        var snapshot = this.snapshots.get(streamName);
        return snapshot != null ? snapshot : (Snapshot<T>) Snapshot.TextSnapshot.NULL;
    }
}
