/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store;

import java.util.Optional;

/**
 *
 * @author sascha.kohlmann
 */
public class StoreException extends RuntimeException {
    
    public final StoreResult result;

    public StoreException(final StoreResult result, final String message) {
        super(message);
        this.result = result;
    }

    public Optional<StoreResult> storeResult() {
        return Optional.ofNullable(this.result);
    }
}
