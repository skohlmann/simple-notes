/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object;

import de.speexx.event.sourcing.BaseMemento;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * The {@code Mementos} of a given named object.
 * @author sascha.kohlmann
 */
public final class ObjectState<T> {
    
    private static final ObjectState NULL_STATE = new ObjectState();
    
    private final List<BaseMemento<T>> mementos;
    private final String objectName;

    private ObjectState() {
        this.mementos = Collections.emptyList();
        this.objectName = "";
    }
    
    /**
     * Creates a new stream
     * @param objectName the name of the object
     * @param mementos the mementos of the object
     */
    public ObjectState(final String objectName, final List<BaseMemento<T>> mementos) {
        this.mementos = Collections.unmodifiableList(new ArrayList<>(Objects.requireNonNull(mementos, "Mementos must be provided")));
        this.objectName = Objects.requireNonNull(objectName, "Object Name must be provided");
    }

    /** The {@code List<Memento<T>>} of the mementos of the named object, and possibly just a sub-object. 
     * The list is unmodifiable */
    public List<BaseMemento<T>> mementos() {
        return mementos;
    }

    /** The strean name. */
    public String objectName() {
        return objectName;
    }

    /** Amount of mementos within this stream. */
    public int size() {
        return mementos().size();
    }
    
    public boolean isNull() {
        return mementos().isEmpty() && "".equals(objectName);
    }

    @Override
    public String toString() {
        return "ObjectState{" + "mementos=" + mementos + ", objectName=" + objectName + '}';
    }
    
    public static final ObjectState nullState() {
        return NULL_STATE;
    }
}
