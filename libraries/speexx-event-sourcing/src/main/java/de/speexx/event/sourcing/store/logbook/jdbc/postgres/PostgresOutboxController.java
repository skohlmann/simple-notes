/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.jdbc.postgres;

import de.speexx.event.sourcing.BaseMemento;
import de.speexx.event.sourcing.Memento;
import static de.speexx.event.sourcing.Memento.typed;
import de.speexx.event.sourcing.Snapshot;
import de.speexx.event.sourcing.store.StoreException;
import de.speexx.event.sourcing.store.StoreResult;
import de.speexx.event.sourcing.store.outbox.Routable;
import de.speexx.event.sourcing.store.outbox.Router;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import de.speexx.event.sourcing.store.outbox.OutboxController;
import de.speexx.event.sourcing.store.outbox.RoutingResult;
import static java.util.Objects.requireNonNull;

/**
 * Propagats all not yet routed {@link Routable}s to the accociated router.
 * The implementation may or may not share a {@link Connection} with the eventually
 * used clients (see {@link PostgresLogbook}.
 * The implementation commits a transaction after executing each available routing sucessful.
 * @author sascha.kohlmann
 */
public class PostgresOutboxController implements OutboxController {

    public static final String ROUTABLE_MEMENTOS_ID_DELIMITER = "|";

    private final static String QUERY_ROUTABLE =
            "SELECT id, created_timestamp, route_id, snapshot_id, snapshot_type, snapshot_type_version, snapshot_data, snapshot_data_version, memento_ids "
            + "FROM tbl_event_logbook_outbox "
            + "ORDER BY created_timestamp ASC LIMIT ?";

    private final static String DELETE_ROUTABLE = 
            "DELETE FROM tbl_event_logbook_outbox "
            + "WHERE route_id = ?";
    
    private static final String QUERY_MEMENTO =
            "SELECT id, memento_data, memento_type, memento_type_version, event_timestamp "
            + "FROM tbl_event_logbook "
            + "WHERE id = ?";
    
    private Connection connection;
    private long limit;

    /**
     * Creates a new instance of the controller for the given input. Sets the default <em>limit</em> to {@link Long#MAX_VALUE}.
     * @param connection the connection
     */
    public PostgresOutboxController(final Connection connection) {
        this(connection, Long.MAX_VALUE);
    }
    
    /**
     * Creates a new instance of the controller for the given input and a limit of routings to perform per
     * {@linkplain de.speexx.event.sourcing.store.outbox.Router#route(de.speexx.event.sourcing.store.outbox.Routable) ) route(…)} call.
     * @param connection the connection
     * @param limit the limit between 1 and {@link Long#MAX_VALUE}.
     * @throws IllegalStateException if and only if <em>limit</em> is not greater 0.
     */
    public PostgresOutboxController(final Connection connection, final long limit) throws IllegalStateException {
        setConnection(connection);
        setLimit(limit);
    }
    
    @Override
    public void process(final Router router, final RoutingResult routingResult) {
        if (routingResult != null && !(routingResult instanceof PostgresRoutingResult)) {
            throw new IllegalArgumentException("RoutingResult not instance of PostgresRoutingResult: " + routingResult.getClass().getSimpleName());
        }
        doRouting(requireNonNull(router, "Router must be provided"), routingResultIfNull((PostgresRoutingResult) routingResult));
    }

    private void doRouting(final Router router, final PostgresRoutingResult routingResult) {
        assert routingResult != null;
        try {
            final List<Routable<Memento<String>, Snapshot.TextSnapshot>> unconfirmedRoutables = allUnconfirmedRoutables();
            for (final Routable<Memento<String>, Snapshot.TextSnapshot> routable : unconfirmedRoutables) {
                router.route(routable);
                deleteRoutable(routable);
                routingResult.increaseRouted();
            }
        } catch (final Exception ex) {
            routingResult.withException(ex);
            return;
        }
        routingResult.success();
    }
    
    private PostgresRoutingResult routingResultIfNull(final PostgresRoutingResult routingResult) {
        if (routingResult == null) {
            return new PostgresRoutingResult();
        }
        return routingResult;
    }
    
    private void deleteRoutable(final Routable routable) throws Exception {
        try (final PreparedStatement stmt = connection().prepareStatement(DELETE_ROUTABLE)) {
            stmt.setString(1, routable.id());
            stmt.execute();
            commitIf();
        }
    }
    
    private void commitIf() {
        try {
            final Connection con = connection();
            if (!con.getAutoCommit()) {
                con.commit();
            }
        } catch (final SQLException e) {
            throw new StoreException(StoreResult.ofFailure(e), "Unable to commit because: " + e.getLocalizedMessage());
        }
    }

    
    private List<Routable<Memento<String>, Snapshot.TextSnapshot>> allUnconfirmedRoutables() throws Exception {
        final List<Routable<Memento<String>, Snapshot.TextSnapshot>> routables = new ArrayList<>();

        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_ROUTABLE)) {
            stmt.setLong(1, limit());
            try(final ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    final Routable<Memento<String>, Snapshot.TextSnapshot> routable = routableFor(rs);
                    routables.add(routable);
                }
            }
        }
        
        return routables;
    }
    
    
    private Routable<Memento<String>, Snapshot.TextSnapshot> routableFor(final ResultSet rs)  throws SQLException, ClassNotFoundException {
        
        final Snapshot.TextSnapshot snapshot;
        final String snapshotId = rs.getString("snapshot_id");
        if (snapshotId != null) {
            final String snapshotTypeName = rs.getString("snapshot_type");
            final int snapshotTypeVersion = rs.getInt("snapshot_type_version");
            final String snapshotData = rs.getString("snapshot_data");
            final int snapshotDataVersion = rs.getInt("snapshot_data_version");
            
            snapshot = new Snapshot.TextSnapshot(snapshotId, typed(snapshotTypeName), snapshotTypeVersion, snapshotData, snapshotDataVersion);
        
        } else {
            snapshot = null;
        }

        final String mementoIds = rs.getString("memento_ids");
        final List<Memento<String>> mementos = new ArrayList<>();
        final String[] ids = mementoIds.split("\\" + ROUTABLE_MEMENTOS_ID_DELIMITER);
        
        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_MEMENTO)) {
            for (final String mementoId : ids) {
                stmt.clearParameters();
                stmt.setObject(1, UUID.fromString(mementoId));
                
                try (final ResultSet innerRs = stmt.executeQuery()) {
                    if (innerRs.next()) {
                        mementos.add(mementoFrom(innerRs));
                    }
                }
            }
        }

        final String routeId = rs.getString("route_id");
        final LocalDateTime createdAt = rs.getTimestamp("created_timestamp").toLocalDateTime();
        
        return new Routable<>(routeId, createdAt, snapshot, mementos);
    }

    private Memento<String> mementoFrom(final ResultSet resultSet) throws SQLException, ClassNotFoundException {
        final String id = resultSet.getString("id");
        final String mementoData = resultSet.getString("memento_data");
        final String mementoTypeName = resultSet.getString("memento_type");
        final int typeVersion = resultSet.getInt("memento_type_version");

        final BaseMemento.TextMemento memento = new BaseMemento.TextMemento(id, typed(mementoTypeName), typeVersion, mementoData);
        return memento;
    }

    private Connection connection() {
        return this.connection;
    }
    
    private long limit() {
        return this.limit;
    }
    
    private void setLimit(final long limit) {
        if (limit < 1) {
            throw new IllegalStateException("Limit must be greater 0. Is: " + limit);
        }
        this.limit = limit;
    }

    private void setConnection(final Connection con) {
        this.connection = con;
    }
}
