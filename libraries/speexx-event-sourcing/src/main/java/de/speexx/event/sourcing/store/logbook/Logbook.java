/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook;

import de.speexx.common.annotation.behavior.CommandMethod;
import de.speexx.common.annotation.behavior.QueryMethod;
import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.Source;
import de.speexx.event.sourcing.store.StoreException;
import de.speexx.event.sourcing.store.StoreResult;
import de.speexx.event.sourcing.store.outbox.OutboxController;
import de.speexx.event.sourcing.store.outbox.Router;
import java.util.List;
import java.util.Optional;

import static java.util.List.of;

/**
 * The top-level logbook used to store all of its {@code Memento<M>} instances for
 * {@code event sourced} and/or {@code command sourced} components. Each use of
 * the logbook appends some number of {@code Memento<M>} instances and an
 * optional single snapshot {@code Snapshot<S>}.
 * 
 * <p>The logbook may also be queried for a {@link #logbookReader(String) LogBookReader}
 * and a {@link #streamReader() StreamReader}.</p>
 * 
 * <p>Assuming that all successfully appended {@code Memento<M>} instances should be
 * routed in some way after each write transaction, you should register a 
 * {@link OutboxController} when first creating your {@code Logbook<S>}.
 *
 * @param <S> the concrete type of {@code Memento<M>} and {@code Snapshot<?>} stored, which maybe be {@code String}
 * @see OutboxController
 * @author sascha.kohlmann
 */
public interface Logbook<S> {

   /**
    * Appends the single {@code Source<S>} as an {@code Memento<M>} to the end of the
    * logbook creating an association to {@code streamName} with {@code streamVersion}.
    * <p>If a {@link Router} is available it will be informed of the appended
    * {@code Memento<M>} and it will have an assigned valid ID. Router handling is
    * implementation dependend.</p>
    * @param streamName the String name of the stream to append
    * @param streamVersion the version of the stream to append
    * @param source the {@code Source<S>} to append as a {@code Memento<M>}
    * @param interest the {@code AppendResultInterest<ST>} used to convey the result of the operation
    * @param <S> the {@code Source} type
    * @see OutboxController
    */
    @CommandMethod
    default <S> void append(final String streamName, final int streamVersion, final Source<S> source, final AppendResultInterest interest) {
        appendAll(streamName, streamVersion, of(source), interest);
    }

   /**
    * Appends all {@code Source<S>} of the {@code List} as {@code Memento<M>}s to the
    * end of the logbook creating an association to {@code streamName}
    * with {@code streamVersion}.
    * <p>If a {@link Router} is available it will be informed of the appended
    * {@code Memento<M>}s and they will have an assigned valid ID. Router handling is
    * implementation dependend.</p>
    * @param streamName the String name of the stream to append
    * @param streamVersion the version of the stream to append
    * @param sources the {@code Source<S>}s to append as a {@code Memento<M>}s
    * @param interest the {@code AppendResultInterest<ST>} used to convey the result of the operation
    * @param <S> the {@code Source} type
    * @see OutboxController
    */
    @CommandMethod
    <S> void appendAll(final String streamName, final int streamVersion, final List<Source<S>> sources, final AppendResultInterest interest);

   /**
    * Appends the single {@code Source<S>} as an {@code Memento<M>} to the end of the
    * logbook creating an association to {@code streamName} with {@code streamVersion},
    * and storing the full state {@code snapshot}. The source and the snapshot are
    * consistently persisted together or neither at all.
    * <p>If a {@link Router} is available it will be informed of the appended
    * {@code Memento<M>} and it will have an assigned valid ID. Router handling is
    * implementation dependend.</p>
    * @param streamName the String name of the stream to append
    * @param streamVersion the version of the stream to append
    * @param source the {@code Source<S>} to append as a {@code Memento<M>}
    * @param snapshot the current {@code ST} state of the stream <em>before</em> the {@code Source} is applied
    * @param interest the {@code AppendResultInterest<ST>} used to convey the result of the operation
    * @param <S> the {@code Source} type
    * @param <ST> the {@code Snapshot} type
    * @see OutboxController
    */
    @CommandMethod
    default <S, ST> void appendWith(final String streamName, final int streamVersion, final Source<S> source, final ST snapshot, final AppendResultInterest interest) {
        appendAllWith(streamName, streamVersion, of(source), snapshot, interest);
    }

   /**
    * Appends all {@code Source<S>} of the {@code List} as {@code Memento<M>}s to the
    * end of the logbook creating an association to {@code streamName}
    * with {@code streamVersion}, and storing the full state {@code snapshot}.
    * The sources and the snapshot are consistently persisted together or neither at all.
    * <p>If a {@link Router} is available it will be informed of the appended
    * {@code Memento<M>}s and they will have an assigned valid ID. Router handling is
    * implementation dependend.</p>
    * @param streamName the String name of the stream to append
    * @param streamVersion the version of the stream to append
    * @param sources the {@code Source<S>}s to append as a {@code Memento<M>}s
    * @param snapshot the current {@code ST} state of the stream <em>before</em> the {@code Source} is applied
    * @param interest the {@code AppendResultInterest<ST>} used to convey the result of the operation
    * @param <S> the {@code Source} type
    * @param <ST> the {@code Snapshot} type
    * @see OutboxController
    */
    @CommandMethod
    <S, ST> void appendAllWith(final String streamName, final int streamVersion, final List<Source<S>> sources, final ST snapshot, final AppendResultInterest interest);

   /**
    * Gives the {@code LogbookReader<M>} named {@code name} for this logbook. If
    * the reader named {@code name} does not yet exist, it is first created. Readers
    * with different names enables reading from different positions and for different
    * reasons.
    * <p>E.g. such an instance might be used for a feed reader getting all new events.</p>
    * @param name the String name of the {@code LogbookReader<M>} to answer
    * @param <M> the concrete type of {@code Memento<?>} of the {@code LogbookReader<M>}
    * @return a logbook reader instance
    * @throws StoreException if and only if a reader can't be created
    */
    @QueryMethod
    <M extends Memento<?>> LogbookReader<M> logbookReader(final String name) throws StoreException;

    /** Gives a {@code StreamReader} for this logbook. */
    @QueryMethod
    <M extends Memento<?>> StreamReader<M> streamReader();

    /** Default {@code String} based {@code Memento} logbook. */
    public static interface TextLogBook extends Logbook<String> { }

    @FunctionalInterface
    public static interface AppendResultInterest {
        <S, ST> void appendResultedIn(final StoreResult result, final String streamName, final int streamVersion, final List<Source<S>> sources, final Optional<ST> snapshot);
    }
}
