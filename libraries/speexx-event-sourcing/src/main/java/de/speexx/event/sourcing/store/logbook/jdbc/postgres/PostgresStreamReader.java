/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.jdbc.postgres;

import de.speexx.event.sourcing.BaseMemento;
import static de.speexx.event.sourcing.Memento.typed;
import de.speexx.event.sourcing.Snapshot;
import de.speexx.event.sourcing.store.ResultType;
import de.speexx.event.sourcing.store.StoreException;
import de.speexx.event.sourcing.store.StoreResult;
import de.speexx.event.sourcing.store.logbook.Stream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import de.speexx.event.sourcing.store.logbook.StreamReader;

/**
 *
 * @author sascha.kohlmann
 */
public class PostgresStreamReader implements StreamReader<BaseMemento<String>> {

    private static final String QUERY_EVENTS =
            "SELECT id, memento_data, memento_type, memento_type_version, stream_version "
            + "FROM tbl_event_logbook "
            + "WHERE stream_name = ? AND stream_version >= ? ORDER BY event_timestamp ASC";

    
    private static final String QUERY_SNAPSHOT =
            "SELECT snapshot_type, snapshot_type_version, snapshot_data, snapshot_data_version "
            + "FROM tbl_event_logbook_snapshots "
            + "WHERE stream_name = ?";

    private Connection connection;

    public PostgresStreamReader(final Connection con) {
        setConnection(con);
    }
    
    @Override
    public Stream<BaseMemento<String>> streamFor(final String streamName) {
        try {
            return eventsFromOffset(streamName, 1, false);
        } catch (final Exception ex) {
            throw new StoreException(new StoreResult(ResultType.FAILURE, ex), "Unable to fetch stream for " + streamName + " and version 1 because: " + ex.getLocalizedMessage());
        }
    }

    @Override
    public Stream<BaseMemento<String>> streamFor(final String streamName, final int fromStreamVersion) {
        try {
            return eventsFromOffset(streamName, fromStreamVersion, true);
        } catch (final Exception ex) {
            throw new StoreException(new StoreResult(ResultType.FAILURE, ex), "Unable to fetch stream for " + streamName + " and version " + fromStreamVersion + " because: " + ex.getLocalizedMessage());
        }
    }
    
    private Stream<BaseMemento<String>> eventsFromOffset(final String streamName, final int offset, final boolean ignoreSnapshot) throws Exception {
        final Snapshot<String> snapshot = latestSnapshotOf(streamName);

        int dataVersion = offset;
        Snapshot<String> referenceSnapshot = Snapshot.TextSnapshot.NULL;

        if (!ignoreSnapshot) {
            if (snapshot != Snapshot.TextSnapshot.NULL) {
                if (snapshot.dataVersion() > offset) {
                    dataVersion = snapshot.dataVersion();
                    referenceSnapshot = snapshot;
                }
            }
        } else {
            if (snapshot != Snapshot.TextSnapshot.NULL) {
                referenceSnapshot = snapshot;
            }
        }

        final List<BaseMemento<String>> events = new ArrayList<>();
        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_EVENTS)) {
            stmt.setString(1, streamName);
            stmt.setInt(2, dataVersion);
            try (ResultSet resultSet = stmt.executeQuery()) {
                while (resultSet.next()) {
                    final String id = resultSet.getString("id");
                    final String eventData = resultSet.getString("memento_data");
                    final String eventType = resultSet.getString("memento_type");
                    final int eventTypeVersion = resultSet.getInt("memento_type_version");

                    // Debug
                    // final int streamVersion = resultSet.getInt("stream_version");

                    final BaseMemento.TextMemento memento = new BaseMemento.TextMemento(id, typed(eventType), eventTypeVersion, eventData);
                    events.add(memento);
                }
            }
        }

        return new Stream(streamName, dataVersion + events.size(), events, referenceSnapshot);
    }

        
    private Snapshot<String> latestSnapshotOf(final String streamName) throws Exception {
        
        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_SNAPSHOT)) {
            stmt.setString(1, streamName);
            try (final ResultSet resultSet = stmt.executeQuery()) {

                if (resultSet.next()) {
                    final String snapshotDataType = resultSet.getString(1);
                    final int snapshotTypeVersion = resultSet.getInt(2);
                    final String snapshotData = resultSet.getString(3);
                    final int snapshotDataVersion = resultSet.getInt(4);

                    return new Snapshot.TextSnapshot(streamName, typed(snapshotDataType), snapshotTypeVersion, snapshotData, snapshotDataVersion);
                }
            }
        }

        return Snapshot.TextSnapshot.NULL;
    }

    private Connection connection() {
        return connection;
    }

    private void setConnection(final Connection con) {
        this.connection = con;
    }
}
