/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook;

import de.speexx.common.annotation.behavior.QueryMethod;

/**
 * The reader for a specific named {@code Stream<T>} of the {@code Logbook<T>}
 * as provided by {@link Logbook#streamReader() streamReader()}.
 * The reader can read all existing {@code Memento<T>} instances of a given
 * {@code Stream<T>}, or from a specific {@code Stream<T>} version.
 * If {@code snapshots} are used for the given {@code Stream<T>}, the result may include
 * a snapshot {@code Snapsshot<T>} instance if one is available within the given
 * {@code Stream<T>} version span ({@code fromStreamVersion} to the stream end).
 * 
 * @param <T> the concrete type of {@code Stream<T>} stored and read, which maybe be {@code String}
 * @author sascha.kohlmann
 */
public interface StreamReader<T> {
 
    /** Gives the full {@code Stream<T>} of the stream with {@code streamName}; or if
     * a {@code State<T>} snapshot is available, only the snapshot and any 
     * {@code Memento<T>} instances that exist since the snapshot.
     * @param streamName the name of the {@code Stream<T>} to answer */
    @QueryMethod
    Stream<T> streamFor(final String streamName);
    
   /**
    * Gives the {@code Stream<T>} of the stream with {@code streamName}, starting with
    * {@code fromStreamVersion} until the end of the stream. Any existing snapshot within
    * this sub-stream is ignored. This method enables reading the entire stream without
    * snapshot optimizations (e.g. {@code reader.streamFor(id, 1)} reads the entire
    * stream of {@code ID}).
    * @param streamName the {@code String} name of the {@code Stream<T>} to answer
    * @param fromStreamVersion the {@code int} version from which to begin reading, inclusive
    * @return the {@code Stream<T>} of the full stream
    */
    @QueryMethod
    Stream<T> streamFor(final String streamName, final int fromStreamVersion);
}
