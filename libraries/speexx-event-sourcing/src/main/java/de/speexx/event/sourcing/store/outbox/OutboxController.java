/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.outbox;

/**
 * {@link de.speexx.event.sourcing.store.logbook.Logbook} implementation may support an optional controller
 * to process appended elements to associated {@link Router Routers}. Such {@code Logbook} implementation
 * must support this interface to handle the routing.
 * <p>This interface supports the <a href='../../../../../../../outbox.html'>Outbox Pattern</a>.</p>
 * @author sascha.kohlmann
 */
public interface OutboxController {

    /**
     * Implementation should process the appended data as {@link Routable Routables}.
     * <p>The default implementation call
     * {@link #process(Router, iRoutingResult) }
     * with {@code RoutingResult = null}.</p>
     * @param router 
     */
    public default void process(final Router router) {
        process(router, null);
    }
    
    /**
     * Implementation should process the appended data as {@link Routable Routables}.
     * <p>The {@code RoutingResult} can finally return successful executed routings but also show
     * {@linkplain RoutedResultType#FAILURE failures}.
     * <p><strong>Implementation note:</strong> Neither is it necessary for the Controller to work with one or more {@code Routers},
     * nor for it to continue routing {@code Routables}.</p>
     * @param router not {@code null}
     * @param result can be {@code null}
     */
    void process(final Router router, final RoutingResult result);
}
