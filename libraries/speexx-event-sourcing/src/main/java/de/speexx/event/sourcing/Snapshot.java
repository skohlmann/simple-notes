/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import java.util.Objects;

/**
 * Encapsulates serializable data of a common Java class and represents a the result of
 * a series of {@code Source} of concrete instance.
 * <p>{@code Snapshot} should be stored and fast retrieved from a database.</p>
 * @author sascha.kohlmann
 * @param <T> the serializable representation of the snapshot
 * @see TextSnapshot as default implementation
 */
public abstract class Snapshot<T> {
    
    private String id;
    private T data;
    private int dataVersion;
    private String typeName;
    private int typeVersion;

    /**
     * <strong>Note:</strong> native types for snapshots is a bad pattern on this level of abstraction.
     * Usage of native types should be handled on higher levels. Therefor use this constructor with care.
     * @param id The ID data element
     * @param type The type of encapsulated object
     * @param typeVersion the version of the {@code Snapshot} implementation
     * @param data the concrete serializable data of the snapshot
     * @param dataVersion the version index of the {@link Source} this snapshot based on.
     */
    protected Snapshot(final String id, final Class<?> type, final int typeVersion, final T data, final int dataVersion) {
        this(id, Objects.requireNonNull(type, "Type must be provided").getName(), typeVersion, data, dataVersion);
    }

    /**
     * @param id The ID data element
     * @param type The type of encapsulated object
     * @param typeVersion the version of the {@code Snapshot} implementation
     * @param data the concrete serializable data of the snapshot
     * @param dataVersion the version index of the {@link Source} this snapshot based on.
     */
    private Snapshot(final String id, final String type, final int typeVersion, final T data, final int dataVersion) {
        this.id = Objects.requireNonNull(id, "ID must be provided");
        this.typeName = Objects.requireNonNull(type, "Type must be provided");
        if (typeVersion <= 0) throw new IllegalArgumentException("Snapshot typeVersion must be greater than 0.");
        this.typeVersion = typeVersion;
        this.data = Objects.requireNonNull(data, "Data must be provided");
        if (dataVersion <= 0) throw new IllegalArgumentException("Snapshot dataVersion must be greater than 0.");
        this.dataVersion = dataVersion;
    }

    /** The serializable representation of the snapshot. */
    public T snapshotData() {
        return this.data;
    }

    /** The version of the data of the {@code Source} stream. */
    public int dataVersion() {
        return this.dataVersion;
    }

    /** The name of the type. Default implementation returns {@link Class#getName()} of the */
    public String typeName() {
        return this.typeName;
    }

    /** The the version of the {@code Snapshot} implementation. */
    public int typeVersion() {
        return this.typeVersion;
    }

    /** Identifies the emptiness of the {@linkplain #snapshotData() snapshot data}.
     * Default implementatiion always returns {@code false}. */
    public boolean isEmpty() {
        return false;
    }
    
    /** The ID of the underlying {@linkplain #snapshotData() snapshot data}. */
    public String id() {
        return this.id;
    }

    /** Support for Null Object pattern. */
    public boolean isNull() {
        return false;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" + "id=" + id + ", data=" + data + ", dataVersion=" + dataVersion + ", typeName=" + typeName + ", typeVersion=" + typeVersion + '}';
    }

    /** Contains a {@code String} representation of the
     * {@linkplain Snapshot#snapshotData() snapshot data}. */
    public static final class TextSnapshot extends Snapshot<String> {

        /** Support for Null Object pattern. */
        public static final TextSnapshot NULL = new TextSnapshot();

        public TextSnapshot(final String id, final Class<?> type, final int typeVersion, final String data, final int dataVersion) {
            super(id, type, typeVersion, data, dataVersion);
        }
        
        private TextSnapshot() {
            super("", Object.class, 1, "", 1);
        }

        /** Returns {@code true} if and only if the {@code String} represenation of
         * {@linkplain Snapshot#snapshotData() snaphot data} is an empty {@code String}.
         */
        @Override
        public boolean isEmpty() {
            return snapshotData().isEmpty();
        }

        /** Check if this is {@link #NULL}. */
        @Override
        public boolean isNull() {
            return this == NULL;
        }
    }

    /** Contains a {@code byte[]} representation of the
     * {@linkplain Snapshot#snapshotData() snapshot data}. */
    public static final class BinarySnapshot extends Snapshot<byte[]> {

        private static final byte[] NULL_ARRAY = new byte[]{};
        private static final BinarySnapshot NULL = new BinarySnapshot();
        
        public BinarySnapshot(final String id, final Class<?> type, final int typeVersion, final byte[] data, final int dataVersion) {
            super(id, type, typeVersion, data, dataVersion);
        }
        
        private BinarySnapshot() {
            super("", Object.class, 1, NULL_ARRAY, 1);
        }

        /** Returns {@code true} if and only if the {@code String} represenation of
         * {@linkplain Snapshot#snapshotData() snaphot data} is an empty {@code byte array}.
         */
        @Override
        public boolean isEmpty() {
            return snapshotData().length == 0;
        }

        /** Check if this is {@link #NULL}. */
        @Override
        public boolean isNull() {
          return this == NULL;
        }
    }
}
