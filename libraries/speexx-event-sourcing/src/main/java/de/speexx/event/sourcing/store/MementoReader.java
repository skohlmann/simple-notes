/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store;

import de.speexx.event.sourcing.Memento;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;

/**
 * A {@code Memento<M>} reader for a given storage type. The specific storage type
 * provides its typed instance.
 * <p>The reader reads sequentially over all {@code Memento<M>} instances in the
 * entire storage, from the first available {@code Memento<M>} to the current last
 * available {@code Memento<M>}. The reader is prepared to read all newly appended
 * {@code Memento<M>} instances beyond that point when they become available.</p>
 * @author sascha.kohlmann
 * @param <M> the {@code Memento} type
 */
public interface MementoReader<M extends Memento<?>> extends AutoCloseable {
    
    /**
    * A special ID to {@linkplain #seekTo(java.lang.String) seek to} the first ID
    * position of the storage.
    * <p>All implementation must support this special ID.</p> */
    static final String BEGIN = "<";

    /**
    * A special ID to {@linkplain #seekTo(java.lang.String) seek to} the last ID
    * position of the storage.
    * <p>All implementation must support this special ID.</p> */
    static final String END = ">";

    /**
    * A special ID to query the current ID position of the logbook without
    * repositioning.
    * <p>All implementation must support this special ID.</p>
    * @see #seekTo(java.lang.String) */
    static final String QUERY = "=";

   /** Gives the next available {@code Memento<M>} instance if available. Otherwise an
    * empty {@code Optional}.
    * <p>The next {@code Memento<M>} is relative to the one previously read by the same
    * reader, or the first {@code Memento<M>} in the storage if none have previously
    * been read.</p> */
    Optional<M> readNext();
    
   /** Gives the next available {@code Memento<M>} instance if available. Otherwise an
    * empty {@code Optional}.
    * <p>The next {@code Memento<M>} is relative to the one previously read by the same
    * reader, or the first {@code Memento<M>} in the storage if none have previously
    * been read.</p> 
    * @param fromId the String ID of the {@code Memento<M>} to which the {@linkplain #seekTo(java.lang.String) seek} prepares to next read */
    Optional<M> readNext(final String fromId);
    
   /** Gives the next available {@code Memento<M>} instances as a {@code List} if available.
    * Otherwise an empty {@code List}.
    * <p>The next {@code Memento<M>} is relative to the one previously read by the same
    * reader, or the first {@code Memento<M>} in the storage if none have previously
    * been read.</p>
    * <p>The {@code maximumEntries} should be used to indicate the total number of
    * {@code Memento<M>} instances that can be consumed in a timely fashion by the caller,
    * which is a natural back-pressure prevention mechanism.</p>
    * @param maximumEntries the int indicating the maximum number of {@code Entry<T>} instances to read
    * @return a list. Never {@code null} */
    List<M> readNext(final int maximumEntries);

   /** Gives the next available {@code Memento<M>} instances as a {@code List} if available.
    * Otherwise an empty {@code List}.
    * <p>The next {@code Memento<M>} is relative to the one previously read by the same
    * reader, or the first {@code Memento<M>} in the storage if none have previously
    * been read.</p>
    * <p>The {@code maximumEntries} should be used to indicate the total number of
    * {@code Memento<M>} instances that can be consumed in a timely fashion by the caller,
    * which is a natural back-pressure prevention mechanism.</p>
    * @param maximumEntries the int indicating the maximum number of {@code Entry<T>} instances to read
    * @param fromId the String ID of the {@code Memento<M>} to which the {@linkplain #seekTo(java.lang.String) seek} prepares to next read
    * @return a list. Never {@code null} */
    List<M> readNext(final String fromId, final int maximumEntries);

    /**
     * Rewinds the reader so that the next available {@code Memento<M>} is the first
     * one in the storage.
     * <p>Sending {@code rewind()} is the same as sending {@code #seekTo(BEGIN)}.</p> */
    void rewind();

   /**
    * Gives the new position of the reader after attempting to seek to the {@code Memento<M>}
    * of the given ID, such that the next available {@code Memento<M>} is the one of the
    * given ID. 
    * If the ID does not exist, the position is set to just beyond the last
    * {@code Memento<M>} instance in the logbook (see {@code END}), or to
    * being the first (see {@code BEGIN}) if none currently exist.
    * @param id the String ID of the {@code Memento<M>} instance to which the seek prepares to next read
    */
    String seekTo(final String id);

    /** The size of the {@code Memento} instances. If the size is unknown or not
     * queryable, a {@link OptionalLong#empty()} is answered. */
    OptionalLong size();
    
    /** The name of this reader.
     * @return the name */
    String name();
}
        