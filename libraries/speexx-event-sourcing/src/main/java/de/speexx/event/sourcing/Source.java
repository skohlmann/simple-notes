/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

/**
 * Base of any concrete source of truth in a bounded context. Such source of truth might be entities 
 * or domain events.
 * <p>The concrete type is represented by the {@code T} parameter and is also implements by this interface.</p>
 * @param <T> the type of source of truth
 * @author sascha.kohlmann
 */
public interface Source<T> {

    /**
     * Version of the structure of the source of truth to handle compatibility issues.
     * @return the version
     */
    int sourceVersion();

    OffsetDateTime sourcedAt();

    /** Answers the name of the source. Default implementation returns {@link Class#getSimpleName()}.
     * <p>Overwrite for own name schema.</p>
     * @return the name of the source.
     */
    public default String sourceName() {
        return getClass().getSimpleName();
    }

    /**
     * Support for <em>Null Pattern</em>.
     * @return always {@code true} if instance of {@link #none()}.
     */
    public default boolean isNull() {
        return false;
    }
    
    /**
     * Support for Null Object pattern.
     */
    public static <T> Source<T> nullSource() {
        return new NullSource<>();
    }

    public static <T> List<Source<T>> none() {
        return Collections.emptyList();
    }
}
