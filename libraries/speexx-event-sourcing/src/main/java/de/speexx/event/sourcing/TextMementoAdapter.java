/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import de.speexx.json.seralizer.JsonSerializer;
import de.speexx.serializer.Deserializer;
import de.speexx.serializer.Serializer;

import static de.speexx.common.Assertions.assertNotNull;


/**
 * The adapter transforms {@link Source Sources} to {@link Memento Mementos}. It based on native type Mementos.
 * The memento data is represented as JSON objects.
 * @author sascha.kohlmann
 */
@SuppressWarnings("rawtypes")
public class TextMementoAdapter implements MementoAdapter {

    private final Serializer<String> serializer;
    private final Deserializer<String> deserializer;
    
    public TextMementoAdapter(final JsonSerializer serializer) {
        this.serializer = assertNotNull(serializer, "JsonSerializer must be provided");
        this.deserializer = serializer;
    }
    
    /**
     * Native type name supporting adapter. The implementation call {@link Class#forName(java.lang.String) } to get
     * the native Java type of the Source.
     * @param memento teh mememto to adapt.
     * @return a source
     */
    @Override
    public Source fromMemento(final Memento memento) {
        try {
            final Class<?> type = Class.forName(memento.typeName());
            return (Source) deserializer().deserialize((String) memento.mementoData(), type);
        } catch (final ClassNotFoundException ex) {
            throw new IllegalStateException("Cannot convert to type: " + memento.typeName());
        }
    }

    /** Native adapter. The implementation calls {@code Source.getClass()} the get the native type of the source.
     * @param source the source to adapt
     * @return a native type memento.
     */ 
    @Override
    public BaseMemento.TextMemento toMemento(final Source source) {
        final var text = serializer().serialize(source);
        return new BaseMemento.TextMemento(source.getClass(), 1, text);
    }    

    /** Native adapter. The implementation calls {@code Source.getClass()} the get the native type of the source.
     * @param source the source to adapt
     * @param id the ID ofthe source
     * @return a native type memento.
     */
    @Override
    public BaseMemento.TextMemento toMemento(final Source source, final String id) {
        final var text = serializer().serialize(source);
        return new BaseMemento.TextMemento(id, source.getClass(), 1, text);
    }    

    protected final Serializer<String> serializer() {
        return this.serializer;
    }

    protected final Deserializer<String> deserializer() {
        return this.deserializer;
    }
}
