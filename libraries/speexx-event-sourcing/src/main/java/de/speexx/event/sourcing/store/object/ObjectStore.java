/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object;

import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.Source;
import de.speexx.event.sourcing.store.StoreException;
import de.speexx.event.sourcing.store.StoreResult;
import static java.util.Arrays.asList;
import java.util.List;

/**
 * The top-level object store used to store all of its {@code Memento<M>} instances for
 * {@code event sourced} and/or {@code command sourced} components. Each use of
 * the object store stores some number of {@code Memento<M>} instances.
 * <p>Implementations store a object name and {@link Memento#typeName() memento type} tuple only once.</p>
 * <p>The object store may also be queried for a {@link #objectStoreReader(String) ObjectStoreReader}
 * and a {@link #objectReader() ObjectReader}.</p>
 * @param <S> the concrete type of {@code Memento<M>} stored, which maybe be {@code String}
 * @author sascha.kohlmann
 */
public interface ObjectStore<S> {

   /**
    * Stores the single {@code Source<S>} as an {@code Memento<M>} to of object name and type name tuple.
    * @param objectName the String name of the object to store
    * @param source the {@code Source<S>} to store as a {@code Memento<M>}
    * @param interest the {@code StoreResultInterest<ST>} used to convey the result of the operation
    * @param <S> the {@code Source} type */
    default <S> void store(final String objectName, final Source<S> source, final StoreResultInterest interest) {
        storeAll(objectName, asList(source), interest);
    }

   /**
    * Stores all {@code Source<S>}s as an {@code Memento<M>} to of object name and type name tuple.
    * @param objectName the String name of the object to store
    * @param sources the {@code Source<S>}s to store as a {@code Memento<M>}s
    * @param interest the {@code StoreResultInterest<ST>} used to convey the result of the operation
    * @param <S> the {@code Source} type */
    <S> void storeAll(final String objectName, final List<Source<S>> sources, final StoreResultInterest interest);

   /**
    * Gives the {@code ObjectStoreReader<M>} named {@code name} for this object store.
    * If the reader named {@code name} does not yet exist, it is first created. Readers
    * with different names enables reading from different positions and for different
    * reasons.
    * <p>E.g. such an instance might be used for a feed reader getting all events.</p> 
    * @param name the String name of the {@code ObjectStoreReader<M>} to answer
    * @param <M> the concrete type of {@code Memento<?>} of the {@code ObjectStoreReader<M>}
    * @return a object store reader instance
    * @throws StoreException if and only if a reader can't be created */
    <M extends Memento<?>> ObjectStoreReader<M>  objectStoreReader(final String name) throws StoreException;

    /** *  Gives a {@code StreamReader} for this logbook. */
    <M extends Memento<?>> ObjectReader<M> objectReader();

    /** Default {@code String} based {@code Memento} logbook. */
    public static interface TextObjectStore extends ObjectStore<String> { }

    @FunctionalInterface
    public static interface StoreResultInterest {
        <S> void storeResultedIn(final StoreResult result, final String objectName, final List<Source<S>> sources);
    }
}
