/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.jdbc.postgres;

import de.speexx.common.annotation.Generated;
import de.speexx.event.sourcing.BaseMemento;
import de.speexx.event.sourcing.BaseMemento.TextMemento;
import de.speexx.event.sourcing.store.ResultType;
import de.speexx.event.sourcing.store.StoreException;
import de.speexx.event.sourcing.store.StoreResult;
import de.speexx.event.sourcing.store.logbook.LogbookReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.logging.Level;
import java.util.logging.Logger;

import static de.speexx.event.sourcing.Memento.typed;

/**
 *
 * @author sascha.kohlmann
 */
public class PostgresLogbookReader implements LogbookReader<TextMemento> {

    private static final String QUERY_COUNT =
            "SELECT COUNT(*) FROM tbl_event_logbook";

    private static final String QUERY_CURRENT_OFFSET =
            "SELECT reader_offset "
            + "FROM tbl_event_logbook_offsets "
            + "WHERE reader_name = ?";

    private static final String DELETE_OFFSET =
            "DELETE FROM tbl_event_logbook_offsets "
            + "WHERE reader_name = ?";

    private static final String UPDATE_CURRENT_OFFSET =
            "INSERT INTO tbl_event_logbook_offsets(reader_offset, reader_name, reader_last_op) "
            + "VALUES(?, ?, ?) "
            + "ON CONFLICT (reader_name) DO UPDATE SET reader_offset = ?, reader_last_op = ?";

    private static final String QUERY_SINGLE =
            "SELECT id, memento_data, memento_type, memento_type_version, event_timestamp "
            + "FROM tbl_event_logbook "
            + "WHERE event_timestamp >= ? ORDER BY event_timestamp ASC";

    private static final String QUERY_BATCH = QUERY_SINGLE;

    private static final String QUERY_LAST_OFFSET =
            "SELECT MAX(entry_timestamp) FROM tbl_event_logbook";
    
    private static final OptionalLong ZERO = OptionalLong.of(0);

    private Connection connection;
    private String name;
    private long offset;

    public PostgresLogbookReader(final String name, final Connection connection) {
        setName(name);
        setConnection(connection);
        setOffset(retrieveCurrentOffset());
    }

    @Override
    public Optional<TextMemento> readNext() {
        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_SINGLE)) {
            stmt.setLong(1, offset());
            stmt.setFetchSize(1);
            try (final ResultSet resultSet = stmt.executeQuery()) {
                
                if (resultSet.next()) {
                    setOffset(resultSet.getLong("event_timestamp") + 1);
                    updateCurrentOffset();
                    return Optional.of(entryFromResultSet(resultSet));
                }
            }
        } catch (final Exception ex) {
            Logger.getLogger(getClass().getSimpleName()).log(Level.WARNING, ex, () -> "Unable to readNext. Reason: " + ex.getLocalizedMessage());
        }

        return Optional.empty();
    }

    @Override
    public Optional<TextMemento> readNext(final String fromId) {
        seekTo(fromId);
        return readNext();
    }

    @Override
    public List<TextMemento> readNext(final int maximumEntries) {
        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_BATCH)) {
            stmt.setLong(1, offset());
            stmt.setMaxRows(maximumEntries);
            stmt.setFetchSize(maximumEntries);

            try (final ResultSet resultSet = stmt.executeQuery()) {

                final List<TextMemento> mementos = new ArrayList<>();
                while (mementos.size() != maximumEntries && resultSet.next()) {
                    mementos.add(entryFromResultSet(resultSet));
                    if (resultSet.isLast()) {
                        setOffset(resultSet.getLong("event_timestamp") + 1);
                    }
                }

                updateCurrentOffset();
                return mementos;
            }
        } catch (final Exception ex) {
            Logger.getLogger(getClass().getSimpleName()).log(Level.WARNING, ex, () -> "Unable to readNext. Reason: " + ex.getLocalizedMessage());
        }
        return Collections.emptyList();
    }

    @Override
    public List<TextMemento> readNext(final String fromId, final int maximumEntries) {
        seekTo(fromId);
        return readNext(maximumEntries);
    }

    @Override
    public void rewind() {
        setOffset(1);
        updateCurrentOffset();
    }

    @Override
    public String seekTo(final String id) {
        final MementoId mementoId = new MementoId(id);
        final String offsetId = "" + mementoId.mementoOffset();
        switch (offsetId) {
            case BEGIN:
                rewind();
                break;
            case END:
                setOffset(retrieveLatestOffset() + 1);
                updateCurrentOffset();
                break;
            case QUERY:
                break;
            default:
                setOffset(Long.parseLong(offsetId));
                updateCurrentOffset();
                break;
        }

        return String.valueOf(offset());
    }

    @Override
    public OptionalLong size() {
        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_COUNT);
             final ResultSet resultSet = stmt.executeQuery()) {
            if (resultSet.next()) {
                return OptionalLong.of(resultSet.getLong(1));
            }
        } catch (final Exception ex) {
            Logger.getLogger(getClass().getSimpleName()).log(Level.WARNING, ex, () -> "Unable to get size. Reason: " + ex.getLocalizedMessage());
        }

        return ZERO;
    }
    
    @Override
    public void delete() {
        try (final PreparedStatement stmt = connection().prepareStatement(DELETE_OFFSET)) {
            stmt.setString(1, name());
            stmt.executeQuery();
        } catch (final Exception ex) {
            Logger.getLogger(getClass().getSimpleName()).log(Level.WARNING, ex, () -> "Unable to delete. Reason: " + ex.getLocalizedMessage());
        }
    }

    @Override
    public void close() throws Exception {
        try {
            connection().close();
        } catch (final SQLException ex) {
            Logger.getLogger(getClass().getSimpleName()).log(Level.WARNING, ex, () -> "Unable to close connection. Reason: " + ex.getLocalizedMessage());
        }
    }

    private TextMemento entryFromResultSet(final ResultSet resultSet) throws SQLException, ClassNotFoundException {
        
        final String id = resultSet.getString("id");
        final long timestampe = resultSet.getLong("event_timestamp");
        final String mementoData = resultSet.getString("memento_data");
        final String mementoType = resultSet.getString("memento_type");
        final int mementoDataTypeVersion = resultSet.getInt("memento_type_version");

        return new BaseMemento.TextMemento(new MementoId(id, timestampe).asString(), typed(mementoType), mementoDataTypeVersion, mementoData);
    }

    private long retrieveCurrentOffset() {

        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_CURRENT_OFFSET)) {
            stmt.setString(1, name());
            final ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return resultSet.getLong(1);
            }
        } catch (final Exception e) {
            e.printStackTrace(System.err);
            // TODO:logging
        }

        return 1;
    }

    private void updateCurrentOffset() {
        try (final PreparedStatement stmt = connection().prepareStatement(UPDATE_CURRENT_OFFSET)) {
            stmt.setLong(1, offset());
            stmt.setString(2, name());
            final LocalDateTime now = LocalDateTime.now();
            stmt.setObject(3, now);
            stmt.setLong(4, offset());
            stmt.setObject(5, now);

            stmt.executeUpdate();
            connection().commit();
        } catch (final Exception ex) {
            Logger.getLogger(getClass().getSimpleName()).log(Level.WARNING, ex, () -> "Unable to updateCurrentOffset. Reason: " + ex.getLocalizedMessage());
        }
    }

    private long retrieveLatestOffset() {
        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_LAST_OFFSET);
             final ResultSet resultSet = stmt.executeQuery()) {
            if (resultSet.next()) {
                return resultSet.getLong(1);
            }
        } catch (final Exception ex) {
            Logger.getLogger(getClass().getSimpleName()).log(Level.WARNING, ex, () -> "Unable to retrieveLatestOffset. Reason: " + ex.getLocalizedMessage());
        }

        return offset();
    }
    
    private void setOffset(final long offset) {
        if (offset < 1) {
            throw new StoreException(new StoreResult(ResultType.FAILURE), "Offset can't be lower 1: " + offset);
        }
        this.offset = offset;
    }
    
    public long offset() {
        return this.offset;
    }
    
    @Override
    public String name() {
        return this.name;
    }

    private void setName(final String name) {
        this.name = name;
    }

    private Connection connection() {
        return connection;
    }

    private void setConnection(final Connection con) {
        this.connection = con;
    }

    @Override
    @Generated
    public String toString() {
        return "PostgresLogBookReader{" + "name=" + name + ", offset=" + offset + '}';
    }
    
    static final class MementoId {
        
        private static final String DELIMITER = ":";
        
        private final String mementoId;
        private final long mementoOffset;
        
        public MementoId(final String id) {
            this.mementoId = mementoIdPart(id);
            this.mementoOffset = mementoOffsetPart(id);
        }
        
        public MementoId(final String id, final long offset) {
            this.mementoId = Objects.requireNonNull(id);
            this.mementoOffset = offset;
        }
        
        private String mementoIdPart(final String id) {
            final int delimiterIndex = id.indexOf(DELIMITER);
            return id.substring(0, delimiterIndex);
        }

        private long mementoOffsetPart(final String id) {
            final int delimiterIndex = id.indexOf(DELIMITER);
            final String offset = id.substring(delimiterIndex + 1);
            return Long.parseLong(offset);
        }

        public String mementoId() {
            return this.mementoId;
        }

        public long mementoOffset() {
            return this.mementoOffset;
        }

        public String asString() {
            return mementoId() + DELIMITER + mementoOffset();
        }

        @Override
        public String toString() {
            return "MementoId{" + "mementoId=" + mementoId + ", mementoOffset=" + mementoOffset + '}';
        }
    }
}
