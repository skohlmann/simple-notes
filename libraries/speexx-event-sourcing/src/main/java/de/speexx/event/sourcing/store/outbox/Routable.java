/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.outbox;

import de.speexx.common.annotation.Nullable;
import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.Snapshot;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.List.copyOf;
import static java.util.List.of;

/**
 * Defines routable data.
 * @author sascha.kohlmann
 */
public class Routable<M extends Memento<?>, S extends Snapshot<?>> {

    private final String id;
    private final LocalDateTime createdOn;
    private final S snapshot;
    private final List<M> mementos;

    public Routable(final String id, final LocalDateTime createdOn, @Nullable final S snapshot, final List<M> mementos) {
        this.id = Objects.requireNonNull(id, "ID must be provided");
        this.createdOn = Objects.requireNonNull(createdOn, "Created On must be provided");
        this.snapshot = snapshot;
        this.mementos = mementos != null ? copyOf(mementos) : of();
    }

    public String id() {
        return id;
    }

    public LocalDateTime createdOn() {
        return createdOn;
    }

    public Optional<S> snapshot() {
        return Optional.ofNullable(snapshot);
    }

    public List<M> mementos() {
        return mementos;
    }

    @SuppressWarnings("unchecked")
    public <S> Snapshot<S> typedSnapshot() {
        return (Snapshot<S>) this.snapshot;
    }

    @Override
    public String toString() {
        return "Routable{" + "id=" + id + ", createdOn=" + createdOn + ", snapshot=" + snapshot + ", mementos=" + mementos + '}';
    }
}
