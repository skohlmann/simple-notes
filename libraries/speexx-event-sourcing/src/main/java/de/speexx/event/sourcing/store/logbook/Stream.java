/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook;

import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.Snapshot;
import java.util.List;

import static de.speexx.common.Assertions.assertNotNull;
import static java.util.List.copyOf;

/**
 * The {@code Mementos} and an optional {@code Snapshot} of a full or partial stream of a
 * given named stream.
 * @author sascha.kohlmann
 */
public class Stream<T> {
    
    private final Snapshot<T> snapshot;
    private final List<Memento<T>> mementos;
    private final String streamName;
    private final int streamVersion;

    /**
     * Creates a new stream
     * @param streamName the name of the stream
     * @param streamVersion the version of the stream
     * @param mementos the mementos of the stream
     * @param snapshot a snapshot of the stream. Might be a support for the Null Object pattern
     */
    public Stream(final String streamName, final int streamVersion, final List<Memento<T>> mementos, final Snapshot<T> snapshot) {
        this.snapshot = assertNotNull(snapshot, "Snapshot must be provided event it is Snapshot.TextSnapshot.NULL");
        this.mementos = copyOf(mementos);
        this.streamName = assertNotNull(streamName, "stream name must be provided");
        this.streamVersion = streamVersion;
    }

    /** The optional snapshot of the stream if any. */
    public Snapshot<T> snapshot() {
        return this.snapshot;
    }

    /** The {@code List<Memento<T>>} of the mementos of the named stream, and possibly just a sub-stream. 
     * The list is unmodifiable */
    public List<Memento<T>> mementos() {
        return mementos;
    }

    /** The strean name. */
    public String streamName() {
        return streamName;
    }

    /** The current stream version. */
    public int streamVersion() {
        return streamVersion;
    }

    /** Whether or not hold a non-empty snapshot. */
    public boolean hasSnapshot() {
        return !snapshot().isEmpty();
    }

    /** Amount of mementos within this stream. */
    public int size() {
        return mementos().size();
    }

    @Override
    public String toString() {
        return "Stream{" + "snapshot=" + snapshot + ", mementos=" + mementos + ", streamName=" + streamName + ", streamVersion=" + streamVersion + '}';
    }
}
