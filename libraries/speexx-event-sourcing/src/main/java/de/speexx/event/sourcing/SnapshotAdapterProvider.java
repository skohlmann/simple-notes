/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import de.speexx.json.seralizer.JsonSerializer;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNullElse;

/**
 * Provider for dedicated {@link SnapshotAdapter}.
 * <p>{@link TextSnapshotAdapter} is implicit available as default implementation. See notes on
 * {@link TextSnapshotAdapter} for dealing with native types.</p>
 * @author sascha.kohlmann
 * @see TextSnapshotAdapter
 */
public class SnapshotAdapterProvider {
    
    private final Map<Class<?>, SnapshotAdapter<?,?>> adapters;
    private final Map<String, SnapshotAdapter<?,?>> namedAdapters;
    private final TextSnapshotAdapter defaultTextSnapshotAdapter;


    public SnapshotAdapterProvider() {
        this(JsonSerializer.instance());
    }

    public SnapshotAdapterProvider(final JsonSerializer serializer) {
        this.adapters = new HashMap<>();
        this.namedAdapters = new HashMap<>();
        this.defaultTextSnapshotAdapter = new TextSnapshotAdapter(requireNonNullElse(serializer, JsonSerializer.instance()));
    }

    public <S,ST extends Snapshot<?>> SnapshotAdapterProvider registerAdapter(final Class<S> snapshotType, final SnapshotAdapter<S,ST> adapter) {
        this.adapters.put(snapshotType, adapter);
        this.namedAdapters.put(snapshotType.getName(), adapter);
        return this;
    }
    
    public <S,ST extends Snapshot<?>> SnapshotAdapterProvider registerAdapter(final String snapshotName, final SnapshotAdapter<S,ST> adapter) {
        this.namedAdapters.put(snapshotName, adapter);
        return this;
    }
    
    public <S,ST extends Snapshot<?>> ST asRaw(final String id, final S snapshot, final int snapshotVersion) {
        final SnapshotAdapter<S,ST>  adapter = (SnapshotAdapter<S,ST>) adapter((Class<S>) snapshot.getClass());
        return adapter.toRawSnapshot(id, snapshot, snapshotVersion);
    }

    public <S,ST extends Snapshot<?>> S fromRaw(final ST snapshot) {
        final SnapshotAdapter<S, ST> adapter = namedAdapter(snapshot);
        return adapter.fromRawSnapshot(snapshot);
    }

    private <S, ST extends Snapshot<?>> SnapshotAdapter<S, ST> adapter(final Class<?> type) {
        return (SnapshotAdapter<S, ST>) adapters().getOrDefault(type, defaultSnapshotAdapter());
    }

    private <S, ST extends Snapshot<?>> SnapshotAdapter<S, ST> namedAdapter(final ST snapshot) {
        return (SnapshotAdapter<S, ST>) namedAdapters().getOrDefault(snapshot.typeName(), defaultSnapshotAdapter());
    }

    private Map<Class<?>, SnapshotAdapter<?,?>> adapters() {
        return this.adapters;
    }
    
    private Map<String, SnapshotAdapter<?,?>> namedAdapters() {
        return this.namedAdapters;
    }

    private TextSnapshotAdapter defaultSnapshotAdapter() {
        return this.defaultTextSnapshotAdapter;
    }
}
