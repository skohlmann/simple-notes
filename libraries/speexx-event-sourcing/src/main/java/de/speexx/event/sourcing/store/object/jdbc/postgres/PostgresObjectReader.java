/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object.jdbc.postgres;

import de.speexx.event.sourcing.BaseMemento.TextMemento;
import static de.speexx.event.sourcing.Memento.typed;
import de.speexx.event.sourcing.store.ResultType;
import de.speexx.event.sourcing.store.StoreException;
import de.speexx.event.sourcing.store.StoreResult;
import de.speexx.event.sourcing.store.object.ObjectState;
import static de.speexx.event.sourcing.store.object.ObjectState.nullState;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import de.speexx.event.sourcing.store.object.ObjectReader;

/**
 *
 * @author sascha.kohlmann
 */
public class PostgresObjectReader implements ObjectReader<TextMemento> {

    private static final String FETCH_EVENTS_BY_STATE_NAME =
            "SELECT memento_data, memento_type_version, memento_type, id FROM " + PostgresObjectStore.TBL_EVENT_STATE + " WHERE object_name = ? ORDER BY memento_created_at_nanos ASC";
    
    private Connection connection;
    
    public PostgresObjectReader(final Connection conn) {
        setConnection(conn);
    }

    @Override
    public ObjectState<TextMemento> objectFor(final String objectName) {
        final List<TextMemento> mementos = fetchMementosForName(objectName);
        if (mementos.isEmpty()) {
            return nullState();
        }
        return new ObjectState(objectName, Collections.unmodifiableList(mementos));
    }

    List<TextMemento> fetchMementosForName(final String objectName) {

        final Connection conn = connection();
        try (final PreparedStatement stmt = conn.prepareStatement(FETCH_EVENTS_BY_STATE_NAME)) {
            stmt.setString(1, objectName);
            
            try (final ResultSet rs = stmt.executeQuery()) {
                return mementosFrom(rs);
            }
            
        } catch (final SQLException | ClassNotFoundException ex) {
            throw new StoreException(new StoreResult(ResultType.FAILURE), ex.getLocalizedMessage());
        }
    }
    
    List<TextMemento> mementosFrom(final ResultSet rs) throws SQLException, ClassNotFoundException {
        final List<TextMemento> mementos = new ArrayList<>();
        while(rs.next()) {
            final TextMemento memento = entryFromResultSet(rs);
            mementos.add(memento);
        }
        return mementos;
    }

    private TextMemento entryFromResultSet(final ResultSet resultSet) throws SQLException, ClassNotFoundException {
        
        final String mementoData = resultSet.getString("memento_data");
        final String mementoType = resultSet.getString("memento_type");
        final int mementoVersion = resultSet.getInt("memento_type_version");
        final String id = resultSet.getString("id");

        return new TextMemento(id, typed(mementoType), mementoVersion, mementoData);
    }
    
    private Connection connection() {
        return this.connection;
    }

    private void setConnection(final Connection conn) {
        this.connection = Objects.requireNonNull(conn, "Connection must be provided");
    }
}

