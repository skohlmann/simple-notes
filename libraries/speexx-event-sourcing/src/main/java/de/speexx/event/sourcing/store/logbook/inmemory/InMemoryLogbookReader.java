/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.inmemory;

import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.store.logbook.LogbookReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalLong;

import static de.speexx.common.Assertions.assertNotNull;
import static java.lang.Integer.parseInt;
import static java.util.Optional.empty;
import static java.util.OptionalLong.of;

/**
 *
 * @author sascha.kohlmann
 */
public final class InMemoryLogbookReader<T extends Memento<?>> implements LogbookReader<T> {

    private int currentIndex;
    private boolean closed = false;
    private final List<Memento<T>> logbook;
    private final Map<String, LogbookReader> readers;
    private final String name;

    InMemoryLogbookReader(final List<Memento<T>> logbook, final Map<String, LogbookReader> readers, final String name) {
        this.logbook = assertNotNull(logbook, "Logbook must be provided");
        this.name = assertNotNull(name, "Name must be provided");
        this.readers = assertNotNull(readers, "LogbookReaders must be provided");
        rewind();
    }
    
    @Override
    public void delete() {
        try {
            this.readers.remove(this.name);
            close();
        } catch (final Exception ex) { /* Never happen in memory */ }
    }

    @Override
    public Optional<T> readNext() {
        closedGuard();
        if (this.currentIndex < logbook.size()) {
            return Optional.of((T) this.logbook.get(this.currentIndex++));
        }
        return empty();
    }

    @Override
    public Optional<T> readNext(final String fromId) {
        seekTo(fromId);
        return readNext();
    }

    @Override
    public List<T> readNext(final int maximumEntries) {
        closedGuard();
        final var mementos = new ArrayList<T>();
        for (var idx = 0; idx < maximumEntries; ) {
            final var next = readNext();
            if (next.isPresent()) {
                mementos.add(next.get());
            } else {
                break;
            }
        }
        
        return mementos;
    }

    @Override
    public List<T> readNext(final String fromId, final int maximumEntries) {
        seekTo(fromId);
        return readNext(maximumEntries);
    }

    @Override
    public void rewind() {
        closedGuard();
        this.currentIndex = 0;
    }

    @Override
    public String seekTo(final String id) {
        closedGuard();
        switch(id) {
            case END: this.currentIndex = this.logbook.size() - 1; break;
            case BEGIN: rewind(); break;
            case QUERY: break;
            default: this.currentIndex = parseInt(id);
        }
        return "" + this.currentIndex;
    }

    @Override
    public OptionalLong size() {
        closedGuard();
        return of(this.logbook.size());
    }

    @Override
    public String name() {
        closedGuard();
        return this.name;
    }

    @Override
    public void close() throws Exception {
        this.closed = true;
    }
    
    private void closedGuard() {
        if (this.closed) {
            throw new IllegalStateException("reader closed");
        }
    }
}
