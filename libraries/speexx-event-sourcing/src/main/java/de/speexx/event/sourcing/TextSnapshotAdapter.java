/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import de.speexx.json.seralizer.JsonSerializer;
import de.speexx.serializer.Deserializer;
import de.speexx.serializer.Serializer;

import static de.speexx.common.Assertions.assertNotNull;

/**
 * The adapter transforms an {@link Object} into a {@link Snapshot}.  It based on native type snapshots.
 * The snapshot data may be represented as JSON objects.
 * @author sascha.kohlmann
 */
public class TextSnapshotAdapter implements SnapshotAdapter<Object, Snapshot.TextSnapshot> {

    private final Serializer<String> serializer;
    private final Deserializer<String> deserializer;
    
    public TextSnapshotAdapter(final JsonSerializer serializer) {
        this.serializer = assertNotNull(serializer, "JsonSerializer must be provided");
        this.deserializer = serializer;
    }

    /**
     * @return 1
     */
    @Override
    public int typeVersion() {
        return 1;
    }

    @Override
    public Object fromRawSnapshot(final Snapshot.TextSnapshot raw) {
        try {
            return fromRawSnapshot(raw, Class.forName(raw.typeName()));
        } catch (final ClassNotFoundException ex) {
            throw new IllegalStateException("Cannot convert to Snapshot type: " + raw.typeName(), ex);
        }
    }

    @Override
    public <ST> ST fromRawSnapshot(final Snapshot.TextSnapshot raw, final Class<ST> snapshotType) {
        return deserializer().deserialize(raw.snapshotData(), snapshotType);
    }

    @Override
    public Snapshot.TextSnapshot toRawSnapshot(final Object snapshot, final int snapshotVersion) {
        final String serialization = serializer().serialize(snapshot);
        return new Snapshot.TextSnapshot("", snapshot.getClass(), typeVersion(), serialization, snapshotVersion);
    }

    @Override
    public Snapshot.TextSnapshot toRawSnapshot(final String id, final Object snapshot, final int snapshotVersion) {
        final String snapshotData = serializer().serialize(snapshot);
        return new Snapshot.TextSnapshot(id, snapshot.getClass(), typeVersion(), snapshotData, snapshotVersion);
    }
    
    protected final Serializer<String> serializer() {
        return this.serializer;
    }

    protected final Deserializer<String> deserializer() {
        return this.deserializer;
    }
}
