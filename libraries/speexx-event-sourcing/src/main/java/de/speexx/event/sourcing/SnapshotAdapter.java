/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

/**
 * Adapts the native snapshot to the raw {@code Snapshot<?>}, and the raw
 * {@code Snapshot<?>} to the native snapshot.
 * @author sascha.kohlmann
 * @param <S> the native snapshot type to adapt into a {@code Snapshot}
 * @param <RS> the raw snapshot type
 * 
 */
public interface SnapshotAdapter<S, RS extends Snapshot<?>> {

    /** The version of the raw snapshot type.
     * @return the version */
    int typeVersion();

    /** Adapts a raws snapshot into a native snapshot.
     * @param raw the raw snapshot
     * @return the native snapshot */
    S fromRawSnapshot(final RS raw);

    /** Converts a raws snapshot into a native snapshot for a concrete snapshot type.
     * @param <ST> the type of the native snapshot
     * @param raw the raw snapshot
     * @param snapshotType the {@code Class<ST>} type to which to convert
     * @return the native snapshot */
    <ST> ST fromRawSnapshot(final RS raw, final Class<ST> snapshotType);

    /**
     * Converts a native snapshot into a raw snapshot.
     * @param id the String identity of the snapshot
     * @param snapshot the {@code S} native snapshot instance
     * @param snapshotVersion the snapshot version
     * @return the raw snapshot
     */
    RS toRawSnapshot(final String id, final S snapshot, final int snapshotVersion);

    /**
     * Converts a native snapshot into a raw snapshot.
     * @param snapshot the {@code S} native snapshot instance
     * @param snapshotVersion the snapshot version
     * @return the raw snapshot
     */
    RS toRawSnapshot(final S snapshot, final int snapshotVersion);
}
