/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.outbox;

/**
 * Indicates the {@link RoutedResultType#SUCCESS} or {@link RoutedResultType#FAILURE} of a {@link RoutingResult}.
 * @author sascha.kohlmann
 */
public enum RoutedResultType {
    
    FAILURE {
        @Override public boolean isFailure() { return true; }
    },
    SUCCESS {
        @Override public boolean isSuccess() { return true; }
    };

    public boolean isFailure() { return false; }
    public boolean isSuccess() { return false; }
}
