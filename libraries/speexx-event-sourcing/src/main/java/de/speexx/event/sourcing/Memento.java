/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

/**
 * Represents source of truth as a sourcing entry to append to a logbook.
 * @author sascha.kohlmann
 * @param <T> the type of the memento
 */
public interface Memento<T> {
    
    /** The raw data of the memento state.
     * @return the raw data of the memento state
     */
    T mementoData();

    /** Answers the emptiness of the memento. */
    boolean isEmpty();
    
    /** The name of the native memento state.
     * @return the version */
    String typeName();

    /** The version of the memento.
     * @return the version */
    int mementoVersion();
    
    /** The ID of the memento.
     * @return the ID */
    String id();

    /** Null Object pattern support.
     * @return default implementation always returns {@code false}. */
    public default boolean isNull() {
        return false;
    }
    
    @SuppressWarnings("unchecked")
    public static <C> Class<C> typed(final String type) {
        try {
            return (Class<C>) Class.forName(type);
        } catch (final Exception e) {
            throw new IllegalStateException("Cannot get class for type: " + type);
        }
    }
}
