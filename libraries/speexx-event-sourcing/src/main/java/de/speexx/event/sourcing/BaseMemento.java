/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import static de.speexx.common.Assertions.assertNotNull;

/** A abstract base of all logbook memento types.
 * @param <T> the concrete type of {@code Memento<T>} stored and read.
 * @see TextMemento BinaryMemento NullMemento
 * @author sascha.kohlmann
 */
public abstract sealed class BaseMemento<T> implements Memento<T> permits BaseMemento.TextMemento,
                                                                          BaseMemento.BinaryMemento,
                                                                          BaseMemento.NullMemento {
    
    private static final String UNKNOWN_ID = "";

    private String id;
    private final T mementoData;
    private final String typeName;
    private final int mementoVersion;

    protected BaseMemento(final Class<?> nativeType, final int mementoVersion, final T mementoData) {
        this(UNKNOWN_ID, nativeType, mementoVersion, mementoData);
    }

    protected BaseMemento(final String id, final Class<?> nativeType, final int mementoVersion, final T mementoData) {
        if (mementoVersion < 1) {throw new IllegalArgumentException("Version must be > 0");}
        this.mementoVersion = mementoVersion;
        this.id = assertNotNull(id, "An ID must be provided");
        this.typeName = assertNotNull(nativeType, "A native type must be provided").getName();
        this.mementoData = assertNotNull(mementoData, "Memento data must be provided");
    }

    @Override
    public T mementoData() {
        return this.mementoData;
    }

    @Override
    public String typeName() {
        return this.typeName;
    }
    
    @Override
    public int mementoVersion() {
        return this.mementoVersion;
    }

    @Override
    public String id() {
        return this.id;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" + "id=" + id + ", mementoData=" + mementoData + ", typeName=" + typeName + ", mementoVersion=" + mementoVersion + '}';
    }

    /**
     * The text String form of {@code Memento<T>}.
     */
    public static final class TextMemento extends BaseMemento<String> {

        public TextMemento(final Class<?> type, final int mementoVersion, final String mementoData) {
              this(UNKNOWN_ID, type, mementoVersion, mementoData);
        }

        public TextMemento(final String id, final Class<?> type, final int mementoVersion, final String mementoData) {
            super(id, type, mementoVersion, mementoData);
        }

        @Override
        public boolean isEmpty() {
            return mementoData().isEmpty();
        }
    }

    /**
     * The byte array form of {@code Memento<T>}.
     */
    public static final class BinaryMemento extends BaseMemento<byte[]> {

        public BinaryMemento(final Class<?> type, final int mementoVersion, final byte[] mementoData) {
              this(UNKNOWN_ID, type, mementoVersion, mementoData);
        }

        public BinaryMemento(final String id, final Class<?> type, final int mementoVersion, final byte[] mementoData) {
            super(id, type, mementoVersion, mementoData);
        }

        @Override
        public boolean isEmpty() {
            return mementoData().length == 0;
        }
    }

    /**
     * The Null Object form of {@code Entry<T>}.
     */
    public static final class NullMemento<T> extends BaseMemento<T> {

        public static final NullMemento<String> TEXT = new NullMemento<>("");

        private NullMemento(final T entryData) {
            super(UNKNOWN_ID, Object.class, 1, entryData);
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public boolean isNull() {
            return true;
        }
    }
}
