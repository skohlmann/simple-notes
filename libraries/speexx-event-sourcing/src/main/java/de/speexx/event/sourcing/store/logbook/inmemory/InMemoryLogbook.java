/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.inmemory;

import de.speexx.event.sourcing.BaseMemento;
import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.MementoAdapterProvider;
import de.speexx.event.sourcing.Snapshot;
import de.speexx.event.sourcing.SnapshotAdapterProvider;
import de.speexx.event.sourcing.Source;
import de.speexx.event.sourcing.store.StoreException;
import de.speexx.event.sourcing.store.logbook.Logbook;
import de.speexx.event.sourcing.store.logbook.LogbookReader;
import de.speexx.event.sourcing.store.outbox.Routable;
import de.speexx.event.sourcing.store.outbox.Router;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static de.speexx.common.Assertions.assertNotNull;
import static de.speexx.event.sourcing.Memento.typed;
import static de.speexx.event.sourcing.store.StoreResult.ofSuccess;
import static java.time.LocalDateTime.now;
import static java.util.Optional.empty;
import static java.util.Optional.of;

/**
 *
 * @author sascha.kohlmann
 * @param <T> Only {@link BaseMemento.TextMemento} and {@link BaseMemento.BinaryMemento} are supported.
 */
public final class InMemoryLogbook<T, R extends Snapshot<T>> implements Logbook<T> {

    private final MementoAdapterProvider mementoAdapterProvider;
    private final SnapshotAdapterProvider snapshotAdapterProvider;
    private final Router router;
    
    private final List<Memento<T>> logbook;
    private final Map<String, LogbookReader<? extends Memento<?>>> logbookReaders;
    private final Map<String, Map<Integer, Integer>> indices;
    private final Map<String, R> snapshots;
  
    public InMemoryLogbook(final MementoAdapterProvider mementoAdapterProvider, final SnapshotAdapterProvider snapshotAdapterProvider) {
        this(NoRouter.INSTANCE, mementoAdapterProvider, snapshotAdapterProvider);
    }
    
    public InMemoryLogbook(final Router router, final MementoAdapterProvider mementoAdapterProvider, final SnapshotAdapterProvider snapshotAdapterProvider) {
        this.mementoAdapterProvider = assertNotNull(mementoAdapterProvider, "MementoAdapterProvider must be provided");
        this.snapshotAdapterProvider = assertNotNull(snapshotAdapterProvider, "SnapshotAdapterProvider must be provided");
        this.router = assertNotNull(router, "Router must be provided");
        
        this.logbook = new ArrayList<>();
        this.indices = new HashMap<>();
        this.logbookReaders = new HashMap<>();
        this.snapshots = new HashMap<>();
    }
    
    @Override
    public <S> void appendAll(final String streamName, final int streamVersion, final List<Source<S>> sources, final AppendResultInterest interest) {
        final var mementos = sources.stream().map(source -> (Memento<T>) this.mementoAdapterProvider.asMemento(source)).toList();
        insert(streamName, streamVersion, mementos);
        route(streamName, streamVersion, mementos, null);
        interest.appendResultedIn(ofSuccess(), streamName, streamVersion, sources, empty());
    }

    @Override
    public <S, ST> void appendAllWith(final String streamName, final int streamVersion, List<Source<S>> sources, final ST snapshot, final AppendResultInterest interest) {
        final var mementos = sources.stream().map(source -> (Memento<T>) this.mementoAdapterProvider.asMemento(source)).toList();
        insert(streamName, streamVersion, mementos);

        final R raw;
        final Optional<ST> result;
        if (snapshot != null) {
            raw = this.snapshotAdapterProvider.asRaw(streamName, snapshot, streamVersion);
            this.snapshots.put(streamName, raw);
            result = of(snapshot);
        } else {
            raw = null;
            result = empty();
        }

        route(streamName, streamVersion, mementos, raw);
        interest.appendResultedIn(ofSuccess(), streamName, streamVersion, sources, result);
    }

    /**
     * Always an instance of {@link InMemoryLogbookReader}.
     * @param <M> the memeento type
     * @param name the name of the reader
     * @return an instance of {@link InMemoryLogbookReader}
     * @throws StoreException if and only if a problem occurs
     */
    @Override
    public <M extends Memento<?>> LogbookReader<M> logbookReader(final String name) throws StoreException {
        var reader = this.logbookReaders.get(name);
        if (reader == null) {
            reader = new InMemoryLogbookReader(this.logbook, this.logbookReaders, name);
            this.logbookReaders.put(name, reader);
        }
        return (LogbookReader<M>) reader;
    }

    @Override
    public <M extends Memento<?>> InMemoryStreamReader<M> streamReader() {
        return new InMemoryStreamReader(this.logbook, this.indices, this.snapshots);
    }
    
    private void insert(final String streamName, final int streamVersion, final List<Memento<T>> mementos) {
        int index = 0;
        for (final var memento : mementos) {
            final var versionIndices = this.indices.computeIfAbsent(streamName, k -> new HashMap<>());
            versionIndices.put(streamVersion + index, this.logbook.size());
            this.logbook.add(toIndexedMemento(memento, this.logbook.size()));
            index++;
        }
    }
    
    Memento<T> toIndexedMemento(final Memento<T> memento, final int index) {
        if (memento instanceof BaseMemento.TextMemento text) {
            return (Memento<T>) new BaseMemento.TextMemento("" + index, typed(text.typeName()), text.mementoVersion(), text.mementoData());
        } if (memento instanceof BaseMemento.BinaryMemento binary) {
            return (Memento<T>) new BaseMemento.BinaryMemento("" + index, typed(binary.typeName()), binary.mementoVersion(), binary.mementoData());
        }
        throw new IllegalArgumentException("Unsupported Memento type: " + memento.getClass().getName());
    }
    
    private void route(final String streamName, final int streamVersion, final List<Memento<T>> entries, final R snapshot) {
        final var routable = new Routable<Memento<T>, R>(streamName + ":" + streamVersion,  now(), snapshot, entries);
        this.router.route(routable);
    }
    
    private static final class NoRouter implements Router<Routable> {
        public static NoRouter INSTANCE = new NoRouter();

        @Override
        public void route(final Routable routable) throws RuntimeException {}
    }
}
