/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.outbox;

import java.util.Optional;

/**
 * The result of the {@link OutboxController#process(Router, RoutingResult) } execution.
 * @author sascha.kohlmann
 */
public interface RoutingResult {

    /**
     * The amount of successful routed {@link Routable Routables}.
     * @return value is greater or equal 0
     */
    long routed();

    /**
     * The final routing result type.
     * @return the result type of the routing process. Must never be {@code null}
     */
    RoutedResultType resultType();

    /**
     * In case of a not full successful routing process execution an optional exception may give more insight of
     * the processing problems.
     * <p>Should only be not empty if {@link RoutedResultType} is {@link RoutedResultType#FAILURE}.</p>
     * @return a routing exception if
     */
    Optional<Exception> exception();
}
