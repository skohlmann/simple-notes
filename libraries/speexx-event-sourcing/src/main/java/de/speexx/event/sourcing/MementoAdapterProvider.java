/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;

import de.speexx.json.seralizer.JsonSerializer;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNullElse;

/**
 * Provider for dedicated {@link MementoAdapter}.
 * <p>{@link TextMementoAdapter} is implicit available as default implementation. See notes on
 * {@link TextMementoAdapter} for dealing with native types.</p>
 * @author sascha.kohlmann
 * @see TextMementoAdapter
 */
public class MementoAdapterProvider {
    
    private final Map<Class<?>, MementoAdapter<?,?>> adapters;
    private final Map<String, MementoAdapter<?,?>> namedAdapters;
    private final MementoAdapter<Source<?>, BaseMemento.TextMemento> defaultTextMementoAdapter;

    public MementoAdapterProvider() {
        this(JsonSerializer.instance());
    }

    public MementoAdapterProvider(final JsonSerializer serializer) {
        this.adapters = new HashMap<>();
        this.namedAdapters = new HashMap<>();
        this.defaultTextMementoAdapter = new TextMementoAdapter(requireNonNullElse(serializer, JsonSerializer.instance()));
    }
    
    public <S extends Source<?>, M extends Memento<?>> MementoAdapterProvider registerAdapter(final Class<S> sourceType, final MementoAdapter<S, M> adapter) {
        this.adapters.put(sourceType, adapter);
        this.namedAdapters.put(sourceType.getName(), adapter);
        return this;
    }
    
    public <S extends Source<?>, M extends Memento<?>> MementoAdapterProvider registerAdapter(final String sourceName, final MementoAdapter<S, M> adapter) {
        this.namedAdapters.put(sourceName, adapter);
        return this;
    }
    
    public <S extends Source<?>, M extends Memento<?>> M asMemento(final S source) {
        MementoAdapter<S, M> adapter = (MementoAdapter<S, M>) namedAdapters().get(source.sourceName());
        if (adapter ==  null) {
            adapter = (MementoAdapter<S, M>) adapters().getOrDefault((Class<?>) source.getClass(), defaultMementoAdapter());
        }
        return adapter.toMemento(source);
    }
                                                  
    public <S extends Source<?>, M extends Memento<?>> S asSource(final M memento) {
        final MementoAdapter<S, M> adapter = (MementoAdapter<S, M>) namedAdapters().getOrDefault(memento.typeName(), defaultMementoAdapter());
        return adapter.fromMemento(memento);
    }

    private Map<Class<?>, MementoAdapter<?,?>> adapters() {
        return this.adapters;
    }
    
    private Map<String, MementoAdapter<?,?>> namedAdapters() {
        return this.namedAdapters;
    }

    private MementoAdapter<Source<?>, BaseMemento.TextMemento> defaultMementoAdapter() {
        return this.defaultTextMementoAdapter;
    }
}
