/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object;

import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.store.MementoReader;

/**
 * The reader for a given {@code ObjectStore<S>}, which is provided by its
 * {@link ObjectStore#objectStoreReader(java.lang.String) objectStoreReader(name)}.
 * This reads sequentially over all {@code Memento<M>} instances in the entire object store,
 * from the first written {@code Memento<M>} to the current last written 
 * {@code Memento<M>}, and is prepared to read all newly stored {@code Memento<M>}
 * instances beyond that point when they become available.
 *
 * @param <M> the concrete type of {@code Memento<M>} stored and read, which maybe be {@code String}
 * @author sascha.kohlmann
 */
public interface ObjectStoreReader<M extends Memento<?>> extends MementoReader<M> {

    /** Deletes current reader. After calling {@code delete} all other 
     * reader operations are of undefined behavior. */
    void delete();
}
