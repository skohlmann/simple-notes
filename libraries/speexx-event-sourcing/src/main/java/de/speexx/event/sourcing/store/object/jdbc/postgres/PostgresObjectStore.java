/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object.jdbc.postgres;

import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.MementoAdapterProvider;
import de.speexx.event.sourcing.Source;
import de.speexx.event.sourcing.store.ResultType;
import de.speexx.event.sourcing.store.StoreResult;
import java.sql.Connection;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.time.OffsetDateTime;
import static java.time.OffsetDateTime.ofInstant;
import java.time.ZoneId;
import static java.time.ZoneId.of;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import de.speexx.event.sourcing.store.object.ObjectReader;
import de.speexx.event.sourcing.store.object.ObjectStore.TextObjectStore;
import de.speexx.event.sourcing.store.object.ObjectStoreReader;

/**
 * Current implementation requires a cooperative handling of {@link Connection} regarding
 * commits. This means: the method {@link Connection#setAutoCommit(boolean)} will be set to {@code false}.
 * 
 * <p>The implementation handles the append operation in an own transaction for all given data (mementos, snapshot).</p>
 * 
 * <p>Future implementation may support uncooperative handling but also may require backwards incompatible API changes</p>
 * @author sascha.kohlmann */
public class PostgresObjectStore implements TextObjectStore {
    
    static final String TBL_EVENT_STATE = "tbl_event_object";
    private static final long NANO_MULTIPLIER = 1_000_000_000;

    private static final String UPSERT_EVENT =
            "INSERT INTO " + TBL_EVENT_STATE + " AS po (id, object_name, memento_data, memento_type, memento_type_version, memento_created_at_nanos, memento_created_zone) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?) "
            + "ON CONFLICT ON CONSTRAINT unique_object_name_type "
            + "DO UPDATE SET memento_data = ?, memento_type_version = ?, memento_created_at_nanos = ?, memento_created_zone = ? WHERE po.object_name = ? AND po.memento_type = ? AND po.memento_created_at_nanos <= ?";

    private MementoAdapterProvider mementoAdapterProvider;
    private volatile Connection connection;

    public PostgresObjectStore(final MementoAdapterProvider provider, final Connection conn) {
        setMementoAdapterProvider(provider);
        setConnection(conn);
    }
    
    @Override
    public <S> void storeAll(final String objectName, List<Source<S>> sources, StoreResultInterest interest) {
        final Consumer<Exception> whenFailed = (e) -> storeAllResultedInFailure(objectName, sources, interest, e);
        final List<ExtMemento> mementos = asMementos(sources, whenFailed);
        insertMementos(objectName, mementos, whenFailed);
        interest.storeResultedIn(new StoreResult(ResultType.SUCCESS), objectName, sources);
    }

    @Override
    public <M extends Memento<?>> ObjectStoreReader<M> objectStoreReader(String name) {
        return (ObjectStoreReader<M>) new PostgresObjectStoreReader(connection(), name);
    }

    @Override
    public <M extends Memento<?>> ObjectReader<M> objectReader() {
        return (ObjectReader<M>) new PostgresObjectReader(connection());
    }

    private <S> List<ExtMemento> asMementos(final List<Source<S>> sources, final Consumer<Exception> whenFailed) {
        return sources.stream().map(source -> asMemento(source, whenFailed)).collect(Collectors.toList());
    }

    private <S> ExtMemento asMemento(final Source<S> source, final Consumer<Exception> whenFailed) {
        try {
            final Memento memento = mementoAdapterProvider().asMemento(source);
            final OffsetDateTime sourcedAt = source.sourcedAt();
            final Class<?> type = source.getClass();
            return new ExtMemento(sourcedAt, memento);
        } catch (final Exception e) {
            whenFailed.accept(e);
            throw new IllegalArgumentException(e);
        }
    }

    List<UUID> insertMementos(final String objectName, final List<ExtMemento> mementos, final Consumer<Exception> whenFailed) {
        final Connection conn = connection();
        try (final PreparedStatement stmt = conn.prepareStatement(UPSERT_EVENT)) {
            final List<UUID> uuids = new ArrayList<>();
            mementos.forEach(memento -> {
                try {
                    final UUID uuid = fillUpsertStatementWithMementoData(stmt, objectName, memento);
                    stmt.addBatch();
                    uuids.add(uuid);
                } catch (final SQLException ex) {
                    throw new RuntimeException(ex);
                }
            });
            stmt.executeBatch();
            return uuids;
        } catch (final SQLException e) {
            whenFailed.accept(e);
            return Collections.emptyList();
        }
    }
    
    UUID fillUpsertStatementWithMementoData(final PreparedStatement stmt, final String objectName, ExtMemento memento) throws SQLException {
        final UUID uuid =  UUID.randomUUID();
        stmt.setObject(1, uuid);
        stmt.setString(2, objectName);
        stmt.setString(3, memento.mementoData());
        stmt.setString(4, memento.typeName());
        stmt.setInt(5, memento.mementoVersion());
        final long sourcedAt = toNanos(memento.sourcedAt());
        final String zoneId = memento.sourcedAt().getOffset().getId();
        stmt.setLong(6, sourcedAt);
        stmt.setString(7, zoneId);
        
        stmt.setString(8, memento.mementoData());
        stmt.setInt(9, memento.mementoVersion());
        stmt.setLong(10, sourcedAt);
        stmt.setString(11, zoneId);
        
        stmt.setString(12, objectName);
        stmt.setString(13, memento.typeName());
        stmt.setLong(14, sourcedAt);
        
        return uuid;
    }
    
    static OffsetDateTime nanosToOffsetDatetime(final long nanos, final String zoneId) {
        final long seconds = nanos / NANO_MULTIPLIER;
        final long nanoAdjustment = nanos % NANO_MULTIPLIER;
        
        final Instant instant = Instant.ofEpochSecond(seconds, nanoAdjustment);
        final ZoneId zone = of(zoneId);
        
        return ofInstant(instant, zone);
    }

    private <S, ST> void storeResultedInFailure(
            final String objectName,
            final Source<S> source,
            final StoreResultInterest interest,
            final Exception e) {

        interest.storeResultedIn(new StoreResult(ResultType.FAILURE, e), objectName, Arrays.asList(source));
    }

    private <S, ST> void storeAllResultedInFailure(
            final String objectName,
            final List<Source<S>> sources,
            final StoreResultInterest interest,
            final Exception e) {

        interest.storeResultedIn(new StoreResult(ResultType.FAILURE, e), objectName, sources);
    }

    private MementoAdapterProvider mementoAdapterProvider() {
        return this.mementoAdapterProvider;
    }

    private void setMementoAdapterProvider(final MementoAdapterProvider mementoAdapterProvider) {
        this.mementoAdapterProvider = mementoAdapterProvider;
    }

    private Connection connection() {
        return this.connection;
    }

    private void setConnection(final Connection conn) {
        this.connection = Objects.requireNonNull(conn, "Connection must be provided");
    }
    
    private static final class ExtMemento implements Memento<String> {
        
        private final OffsetDateTime sourcedAt;
        private final Memento<String> memento;
        
        private ExtMemento(final OffsetDateTime sourcedAt, final Memento<String> memento) {
              this.memento = Objects.requireNonNull(memento, "Memento must be provided");
              this.sourcedAt = Objects.requireNonNull(sourcedAt, "SourcedAt must be provided");
        }
        
        public OffsetDateTime sourcedAt() {
            return this.sourcedAt;
        }

        @Override
        public String mementoData() {
            return this.memento.mementoData();
        }

        @Override
        public boolean isEmpty() {
            return memento.isEmpty();
        }

        @Override
        public String typeName() {
            return memento.typeName();
        }

        @Override
        public int mementoVersion() {
            return memento.mementoVersion();
        }

        @Override
        public String id() {
            return memento.id();
        }

        @Override
        public boolean isNull() {
            return memento.isNull();
        }
    }

    static long toNanos(final OffsetDateTime datetime) {
        final Instant instant = datetime.toInstant();
        final long seconds = instant.getEpochSecond();
        final long nanos = (long) instant.getNano();
        final long secondsAsNanos = seconds * NANO_MULTIPLIER;
        return secondsAsNanos + nanos;
    }
}
