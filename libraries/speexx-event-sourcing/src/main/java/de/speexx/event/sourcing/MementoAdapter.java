/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing;


/**
 * Adapts native {@code Source<?>} state into a raw {@code   Memento<?>} and vice versa.
 * @author sascha.kohlmann
 * @param <S> the native {@code Source<?>}
 * @param <M> the raw {@code Memento<?>}
 */
public interface MementoAdapter<S extends Source<?>, M extends Memento<?>> {
 
   /** Converts the raw {@code M Memento<?>} state into a native source state {@code S}.
    * @param memento the {@code M Memento<?>} to convert from
    * @return a native state */
    S fromMemento(final M memento);

   /** Converts a native state {@code S} into a raw {@code M Memento<?>}.
    * @param source the source state {@code S} to convert from
    * @return a memento */
    M toMemento(final S source);

   /** Converts a native state {@code S} into a raw {@code M Memento<?>}.
    * @param source the source state {@code S} to convert from
    * @param id ID of the source
    * @return a memento */
    M toMemento(final S source, final String id);
}
