/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store;

/**
 *
 * @author sascha.kohlmann
 */
public enum ResultType {
    
    CONCURRENCY_VIOLATION {
        @Override public boolean isConcurrencyViolation() { return true; }
    },
    FAILURE {
        @Override public boolean isFailure() { return true; }
    },
    NOT_FOUND {
        @Override public boolean isNotFound() { return true; }
    },
    SUCCESS {
        @Override public boolean isSuccess() { return true; }
    };

    public boolean isConcurrencyViolation() { return false; }
    public boolean isFailure() { return false; }
    public boolean isNotFound() { return false; }
    public boolean isSuccess() { return false; }
}
