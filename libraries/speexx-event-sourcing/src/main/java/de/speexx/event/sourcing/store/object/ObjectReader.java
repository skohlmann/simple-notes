/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object;

/**
 * The reader for a specific named {@code ObjectState<T>} of the {@code ObjectStore<T>}
 * as provided by {@link ObjectStore#objectReader() objectReader()}.
 * The reader read all existing {@code Memento<T>} instances of a given
 * {@code ObjectState<T>}.
 * @param <T> the concrete type of {@code ObjectState<T>} stored and read, which maybe be {@code String}
 * @author sascha.kohlmann
 */
public interface ObjectReader<T> {
 
    /** *  Gives the full {@code ObjectState<T>} of the object with {@code objectName}.
     * @param objectName the name of the {@code ObjectState<T>} to answer. {@link ObjectState#isNull() Null ObjectState} if no object available. */
    ObjectState<T> objectFor(final String objectName);
}
