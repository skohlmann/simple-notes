/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.jdbc.postgres;

import de.speexx.event.sourcing.store.outbox.RoutedResultType;
import de.speexx.event.sourcing.store.outbox.RoutingResult;
import java.util.Optional;

/**
 * Implementation to support {@link PostgresOutboxController}.
 * @author sascha.kohlmann
 */
public class PostgresRoutingResult implements RoutingResult {
    
    private volatile long routed = 0;
    private RoutedResultType resultType = RoutedResultType.FAILURE;
    private Exception exception = null;
    

    @Override
    public final long routed() {
        return this.routed;
    }

    /**
     * The default {@code RoutedResultType} is {@link RoutedResultType#FAILURE} until {@link #success()}
     * is not called.
     */
    @Override
    public final RoutedResultType resultType() {
        return this.resultType;
    }

    @Override
    public final Optional<Exception> exception() {
        return Optional.ofNullable(this.exception);
    }

    /**
     * Inrease the routed counter by 1.
     * <p>Overriding methods must call super method to have an effect on {@link #routed() }</p>
     * @throws IllegalStateException if and only if the routed counter {@linkplain Long#MAX_VALUE maximum} is still reached
     */
    public void increaseRouted() throws IllegalStateException {
        if (routed() == Long.MAX_VALUE) {
            throw new IllegalStateException("Unable to increase routed count. Maximum reached: " + routed());
        }
        this.routed++;
    }
    
    /**
     * Sets the {@link #resultType() RoutedResultType} to {@link RoutedResultType#SUCCESS}.
     * <p>Overriding methods must call super method to have an effect on {@link #resultType() }</p>
     * @see #resultType()
     */
    public void success() {
        this.resultType = RoutedResultType.SUCCESS;
    }
    
    /**
     * Sets the result exception if available.
     * <p>Overriding methods must call super method to have an effect on {@link #exception() }</p>
     * @param e the exception to set
     * @throws IllegalStateException if and only if an exception still set
     */
    public void withException(final Exception e) throws IllegalStateException {
        if (exception().isPresent()) {
            throw new IllegalStateException("Exception available: " + exception().get());
        }
        this.exception = e;
    }
}
