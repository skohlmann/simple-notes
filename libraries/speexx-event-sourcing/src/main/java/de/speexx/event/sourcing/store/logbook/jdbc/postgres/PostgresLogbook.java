/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.logbook.jdbc.postgres;

import de.speexx.common.annotation.Generated;
import de.speexx.event.sourcing.Memento;
import de.speexx.event.sourcing.MementoAdapterProvider;
import de.speexx.event.sourcing.Snapshot;
import de.speexx.event.sourcing.SnapshotAdapterProvider;
import de.speexx.event.sourcing.Source;
import de.speexx.event.sourcing.store.ResultType;
import de.speexx.event.sourcing.store.StoreResult;
import de.speexx.event.sourcing.store.logbook.Logbook;
import de.speexx.event.sourcing.store.logbook.LogbookReader;
import de.speexx.event.sourcing.store.logbook.StreamReader;
import de.speexx.event.sourcing.store.outbox.Routable;
import de.speexx.event.sourcing.store.outbox.Router;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Current implementation requires a cooperative handling of {@link Connection} regarding
 * commits. This means: the method {@link Connection#setAutoCommit(boolean)} must be
 * provided with {@code false}.
 * 
 * <p>The implementation handles the append operation in an own transaction for all given data (mementos, snapshot). In case
 * a {@link PostgresOutboxController} is available the routing will be done in an own transaction. Therefor the
 * implementation may call {@link Connection#commit()} twice during the execution of a {@code append} operation.</p>
 * 
 * <p>Future implementation may support uncooperative handling but also may require backwards incompatible API changes</p>
 * 
 * <p><strong>Note:</strong> the implementation is not thread safe. Also not thread save together with operations of
 * {@link PostgresLogbookReader} and {@link PostgresStreamReader}. These implementations shares a database connection.</p>
 * @author sascha.kohlmann */
public class PostgresLogbook implements Logbook<String> {

    private static final String INSERT_EVENT = 
            "INSERT INTO tbl_event_logbook(id, stream_name, stream_version, memento_data, memento_type, memento_type_version, memento_created_at, memento_created_at_offset, event_timestamp) "
            + "VALUES(?, ?, ?, ?::JSONB, ?, ?, ?, ?, ?)";

    private static final String INSERT_SNAPSHOT =
            "INSERT INTO tbl_event_logbook_snapshots(stream_name, snapshot_type, snapshot_type_version, snapshot_data, snapshot_data_version) "
            + "VALUES(?, ?, ?, ?::JSONB, ?) "
            + "ON CONFLICT (stream_name) DO UPDATE SET snapshot_type = ? , snapshot_type_version = ? , snapshot_data = ?::JSONB , snapshot_data_version = ?";

    private final static String INSERT_ROUTABLE =
            "INSERT INTO tbl_event_logbook_outbox "
            + "(id, created_timestamp, route_id, snapshot_id, snapshot_type, snapshot_type_version, snapshot_data, snapshot_data_version, memento_ids) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?::JSONB, ?, ?)";
    
    private Router router;
    private PostgresOutboxController outboxController;
    private PostgresRoutingResult routingResult;
    private MementoAdapterProvider mementoAdapterProvider;
    private SnapshotAdapterProvider snapshotAdapterProvider;
    private volatile Connection connection;
    private boolean toOutbox;
    
    /**
     * Simple stores the given mementos and optional snapshots in a Postgres database.
     * <p>Outbox Pattern is not supported.</p>
     * @param mementoAdapterProvider adapter for sources to mementos and vice versa
     * @param snapshotAdapterProvider adapter for objects to snapshots and vice versa
     * @param conn a database coonnection
     * @throws IllegalStateException if the connection delivers {@code getAutocommit() == true}.
     */
    public PostgresLogbook(final MementoAdapterProvider mementoAdapterProvider, final SnapshotAdapterProvider snapshotAdapterProvider, final Connection conn) {
        this(false, mementoAdapterProvider, snapshotAdapterProvider, conn);
    }

    /**
     * Simple stores the given mementos and optional snapshots in a Postgres database.
     * <p>In case the given {@code toOutbox} is {@code true} the implementation will store {@code Routables} to
     * handle with the {@code OutboxController} in an <em>asynchronous</em> way.</p>
     * @param toOutbox if {@code true} the implementation stores the appended mementos and optional snapshot also in the outbox.
     * @param mementoAdapterProvider adapter for sources to mementos and vice versa
     * @param snapshotAdapterProvider adapter for objects to snapshots and vice versa
     * @param conn a database coonnection
     * @throws IllegalStateException if the connection delivers {@code getAutocommit() == true}.
     */
    public PostgresLogbook(final boolean toOutbox, final MementoAdapterProvider mementoAdapterProvider, final SnapshotAdapterProvider snapshotAdapterProvider, final Connection conn) {
        this(null, null, mementoAdapterProvider, snapshotAdapterProvider, conn);
        setToOutbox(toOutbox);
    }

    /**
     * Simple stores the given mementos and optional snapshots in a Postgres database.
     * <p>In case the given {@code outboxController} is not {@code null} the implementation will store {@code Routables} to
     * handle with the {@code OutboxController} in a <em>synchronous</em> way. This means, that the implementations of the
     * <em>append</em> calls the {@link de.speexx.event.sourcing.store.outbox.OutboxController#process(de.speexx.event.sourcing.store.outbox.Router)} method.</p>
     * @param router a crouter
     * @param outboxController a controller
     * @param mementoAdapterProvider adapter for sources to mementos and vice versa
     * @param snapshotAdapterProvider adapter for objects to snapshots and vice versa
     * @param conn a database coonnection
     * @throws IllegalStateException if the connection delivers {@code getAutocommit() == true} or router is available and outbox countroller not (or vice versa).
     */
    public PostgresLogbook(final PostgresOutboxController outboxController, final Router router, final MementoAdapterProvider mementoAdapterProvider, final SnapshotAdapterProvider snapshotAdapterProvider, final Connection conn) {
        this(outboxController, new PostgresRoutingResult(), router, mementoAdapterProvider, snapshotAdapterProvider, conn);
    }

    /**
     * Simple stores the given mementos and optional snapshots in a Postgres database.
     * <p>In case the given {@code outboxController} is not {@code null} the implementation will store {@code Routables} to
     * handle with the {@code OutboxController} in a <em>synchronous</em> way. This means, that the implementations of the
     * <em>append</em> calls the {@link de.speexx.event.sourcing.store.outbox.OutboxController#process(de.speexx.event.sourcing.store.outbox.Router)} method.
     * Determine the routing result with <em>routingResult</em> if set.</p>
     * @param router a crouter
     * @param outboxController a controller
     * @param routingResult determine the routing result
     * @param mementoAdapterProvider adapter for sources to mementos and vice versa
     * @param snapshotAdapterProvider adapter for objects to snapshots and vice versa
     * @param conn a database coonnection
     * @throws IllegalStateException if the connection delivers {@code getAutocommit() == true} or router is available and outbox countroller not (or vice versa).
     */
    public PostgresLogbook(final PostgresOutboxController outboxController, final PostgresRoutingResult routingResult, final Router router, final MementoAdapterProvider mementoAdapterProvider, final SnapshotAdapterProvider snapshotAdapterProvider, final Connection conn) {
        setMementoAdapterProvider(mementoAdapterProvider);
        setSnapshotAdapterProvider(snapshotAdapterProvider);
        setConnection(conn);
        setOutboxController(outboxController);
        setRoutingResult(routingResult);
        setRouter(router);
        
        if ((router().isPresent() && !outboxController().isPresent()) || (!router().isPresent() && outboxController().isPresent())) {
            throw new IllegalStateException("Router (available: " + router().isPresent() + ") and OutboxController (available: " + outboxController().isPresent() + ")must by available");
        }
        
        if (routingResult() == null) {
            throw new IllegalStateException("RoutingResult must be provided.");
        }
    }
    
    @Override
    public <S> void appendAll(final String streamName, final int streamVersion, final List<Source<S>> sources, final AppendResultInterest interest) {
        final Consumer<Exception> whenFailed = (e) -> appendAllResultedInFailure(streamName, streamVersion, sources, null, interest, e);
        final List<Memento<String>> mementos = asMementos(sources, whenFailed);
        final List<UUID> mementoIds = new ArrayList<>();
        int version = streamVersion;
        for (final Memento memento : mementos) {
             final UUID mementoId = insertMemento(streamName, version++, memento, whenFailed);
             mementoIds.add(mementoId);
        }

        if (mustRoute()) {
            final Routable<Memento<String>, Snapshot.TextSnapshot> routable = buildRoutable(streamName, streamVersion, mementos, Snapshot.TextSnapshot.NULL);
            insertRoutableToOutbox(routable, mementoIds, whenFailed);
        }
        
        doCommitIf(whenFailed);
        processOutboxIf();
        interest.appendResultedIn(new StoreResult(ResultType.SUCCESS), streamName, streamVersion, sources, Optional.empty());
    }

    @Override
    public <S, ST> void appendAllWith(final String streamName, final int streamVersion, final List<Source<S>> sources, final ST snapshot, final AppendResultInterest interest) {
        final Consumer<Exception> whenFailed = (e) -> appendAllResultedInFailure(streamName, streamVersion, sources, snapshot, interest, e);
        final List<Memento<String>> mementos = asMementos(sources, whenFailed);
        final List<UUID> mementoIds = new ArrayList<>();
        int version = streamVersion;
        for (final Memento memento : mementos) {
             final UUID mementoId = insertMemento(streamName, version++, memento, whenFailed);
             mementoIds.add(mementoId);
        }
        final Optional<Snapshot.TextSnapshot> textSnapshot = asSnapshot(streamName, snapshot, streamVersion, whenFailed);
        textSnapshot.ifPresent(s -> insertSnapshot(streamName, s, whenFailed));

        if (mustRoute()) {
            final Routable<Memento<String>, Snapshot.TextSnapshot> routable = buildRoutable(streamName, streamVersion, mementos, Snapshot.TextSnapshot.NULL);
            insertRoutableToOutbox(routable, mementoIds, whenFailed);
        }
        
        doCommitIf(whenFailed);
        processOutboxIf();
        interest.appendResultedIn(new StoreResult(ResultType.SUCCESS), streamName, streamVersion, sources, Optional.empty());
    }

    @Override
    @SuppressWarnings("unchecked")
    public <M extends Memento<?>> LogbookReader<M> logbookReader(final String name) {
        return (LogbookReader<M>) new PostgresLogbookReader(name, connection());
    }

    @Override
    @SuppressWarnings("unchecked")
    public <M extends Memento<?>> StreamReader<M> streamReader() {
        return (StreamReader<M>) new PostgresStreamReader(connection());
    }

    private Optional<PostgresOutboxController> outboxController() {
        return Optional.ofNullable(this.outboxController);
    }

    private void setRoutingResult(final PostgresRoutingResult routingResult) {
        this.routingResult = routingResult;
    }
    
    private PostgresRoutingResult routingResult() {
        return this.routingResult;
    }
    
    private void setOutboxController(final PostgresOutboxController controller) {
        this.outboxController = controller;
    }
    
    private Optional<Router> router() {
        return Optional.ofNullable(this.router);
    }
    
    private void setRouter(final Router router) {
        this.router = router;
    }
    
    private void setToOutbox(final boolean toOutbox) {
        this.toOutbox = toOutbox;
    }
    
    private boolean isToOutbox() {
        return this.toOutbox;
    }
    
    private boolean mustRoute() {
        return isToOutbox() || outboxController().isPresent();
    }

    private MementoAdapterProvider mementoAdapterProvider() {
        return this.mementoAdapterProvider;
    }

    private void setMementoAdapterProvider(final MementoAdapterProvider mementoAdapterProvider) {
        this.mementoAdapterProvider = mementoAdapterProvider;
    }

    private SnapshotAdapterProvider snapshotAdapterProvider() {
        return this.snapshotAdapterProvider;
    }

    private void setSnapshotAdapterProvider(final SnapshotAdapterProvider snapshotAdapterProvider) {
        this.snapshotAdapterProvider = snapshotAdapterProvider;
    }

    private Connection connection() {
        return this.connection;
    }

    private void setConnection(final Connection conn) {
        try {
            if (conn.getAutoCommit()) {
                throw new IllegalStateException("Postgres Connection not autocommit == false");
            }
        } catch (final SQLException e) {
            throw new IllegalStateException("Unable to get connection autocommit status", e);
        }
        this.connection = conn;
    }

    private void processOutboxIf() {
        try {
            outboxController().ifPresent(controller -> controller.process(router().get(), routingResult()));
        } catch (final RuntimeException e) {
            e.printStackTrace(System.out);
            throw e;
        }
    }

    protected final void insertRoutableToOutbox(final Routable<Memento<String>, Snapshot.TextSnapshot> routable, final List<UUID> mementoIds, final Consumer<Exception> whenFailed) {
        if (routable == null) {
            return;
        }
        
        if (!mustRoute()) {
            return;
        }

        try (final PreparedStatement stmt = connection().prepareStatement(INSERT_ROUTABLE)) {

            final UUID id = UUID.randomUUID();
            stmt.setObject(1, id);
            stmt.setObject(2, routable.createdOn());
            stmt.setString(3, routable.id());

            final Snapshot<String> snapshot = routable.typedSnapshot();
            if (routable.snapshot().isPresent() && !snapshot.isNull()) {
                stmt.setString(4, snapshot.id());
                stmt.setString(5, snapshot.typeName());
                stmt.setInt(6, snapshot.typeVersion());
                stmt.setString(7, snapshot.snapshotData());
                stmt.setInt(8, snapshot.dataVersion());
            } else {
                stmt.setString(4, null);
                stmt.setString(5, null);
                stmt.setInt(6, 0);
                stmt.setString(7, null);
                stmt.setInt(8, 0);
            }

            if (routable.mementos() != null && !routable.mementos().isEmpty()) {
                stmt.setString(9, mementoIds.stream()
                        .map(uuid -> uuid.toString())
                        .collect(Collectors.joining(PostgresOutboxController.ROUTABLE_MEMENTOS_ID_DELIMITER)));
            } else {
                stmt.setString(9, "");
            }

            if (stmt.executeUpdate() != 1) {
                throw new IllegalStateException("Postgres event - Could not insert snapshot");
            }
        } catch (final SQLException e) {
            whenFailed.accept(e);
            throw new IllegalStateException(e);
        }
    }

    protected final UUID insertMemento(final String streamName, final int streamVersion, final Memento<String> memento, final Consumer<Exception> whenFailed) {
        try (final PreparedStatement stmt = connection().prepareStatement(INSERT_EVENT)) {

            final UUID id = UUID.randomUUID();
            stmt.setObject(1, id);
            stmt.setString(2, streamName);
            stmt.setInt(3, streamVersion);
            stmt.setObject(4, memento.mementoData());
            stmt.setString(5, memento.typeName());
            stmt.setInt(6, memento.mementoVersion());
            final OffsetDateTime now = OffsetDateTime.now();
            stmt.setObject(7, now);
            stmt.setInt(8, now.getOffset().getTotalSeconds());
            stmt.setLong(9, offsetDatetimeToNanoLongs(now));
            
            if (stmt.executeUpdate() != 1) {
                throw new IllegalStateException("Postgres event - Could not insert event");
            }

            return id;
        } catch (final SQLException e) {
            whenFailed.accept(e);
            throw new IllegalStateException(e);
        }
    }
    
    protected final void insertSnapshot(final String streamName, final Snapshot.TextSnapshot snapshot, final Consumer<Exception> whenFailed) {

        try (final PreparedStatement stmt = connection().prepareStatement(INSERT_SNAPSHOT)) {
            stmt.setString(1, streamName);
            stmt.setString(2, snapshot.typeName());
            stmt.setInt(3, snapshot.typeVersion());
            stmt.setString(4, snapshot.snapshotData());
            stmt.setInt(5, snapshot.dataVersion());

            stmt.setString(6, snapshot.typeName());
            stmt.setInt(7, snapshot.typeVersion());
            stmt.setString(8, snapshot.snapshotData());
            stmt.setInt(9, snapshot.dataVersion());

            if (stmt.executeUpdate() != 1) {
                throw new IllegalStateException("Postgres event - Could not insert snapshot");
            }
        } catch (final SQLException e) {
            whenFailed.accept(e);
            throw new IllegalStateException(e);
        }
    }

    private <S> Memento<String> asMemento(final Source<S> source, final Consumer<Exception> whenFailed) {
        try {
            final Memento<String> memento = mementoAdapterProvider().asMemento(source);
            return memento;
        } catch (final Exception e) {
            whenFailed.accept(e);
            throw new IllegalArgumentException(e);
        }
    }

    private <S> Optional<Snapshot.TextSnapshot> asSnapshot(final String streamName, final S snapshot, final int streamVersion, final Consumer<Exception> whenFailed) {
        try {
            return Optional.ofNullable(snapshotAdapterProvider().asRaw(streamName, snapshot, streamVersion));
        } catch (final Exception e) {
            whenFailed.accept(e);
            throw new IllegalArgumentException(e);
        }
    }

    private <S> List<Memento<String>> asMementos(final List<Source<S>> sources, final Consumer<Exception> whenFailed) {
        final List<Memento<String>> mementos = sources.stream().map(source -> asMemento(source, whenFailed)).collect(Collectors.toList());
        return mementos;
    }

    private <S, ST> void appendResultedInFailure(
            final String streamName,
            final int streamVersion,
            final Source<S> source,
            final ST snapshot,
            final AppendResultInterest interest,
            final Exception e) {

        interest.appendResultedIn(new StoreResult(ResultType.FAILURE, e), streamName, streamVersion, Arrays.asList(source), snapshot == null ? Optional.empty() : Optional.of(snapshot));
    }

    private <S, ST> void appendAllResultedInFailure(
            final String streamName,
            final int streamVersion,
            final List<Source<S>> sources,
            final ST snapshot,
            final AppendResultInterest interest,
            final Exception e) {

        interest.appendResultedIn(new StoreResult(ResultType.FAILURE, e), streamName, streamVersion, sources, snapshot == null ? Optional.empty() : Optional.of(snapshot));
    }

    private void doCommitIf(final Consumer<Exception> whenFailed) {
        final Connection conn = connection();
        try {
            if (!conn.getAutoCommit()) {
                conn.commit();
            }
        } catch (final SQLException e) {
            try {
                conn.rollback();
            } catch (final SQLException ex) {
                whenFailed.accept(e);
                throw new IllegalStateException(ex);
            }
            whenFailed.accept(e);
            throw new IllegalStateException(e);
        }
    }

    private Routable<Memento<String>, Snapshot.TextSnapshot> buildRoutable(final String streamName, final int streamVersion, final List<Memento<String>> entries, final Snapshot.TextSnapshot snapshot) {

        if (mustRoute()) {
            final String id = routeIdFor(streamName, streamVersion);
            return new Routable<>(id, LocalDateTime.now(), snapshot, entries);
        }

        return null;
    }

    private String routeIdFor(final String streamName, final int streamVersion) {
        return streamName + ":" + streamVersion + ":" + UUID.randomUUID().toString();
    }
    
    long offsetDatetimeToNanoLongs(final OffsetDateTime datetime) {
        final long millis = datetime.toEpochSecond();
        int nanos = datetime.getNano();
        nanos = nanos < 1_000 ? nanos : nanos / 1_000;
        return millis * 1_000_000 + nanos;
    }

    @Override
    @Generated
    public String toString() {
        return "PostgresLogbook{" + "outboxController=" + outboxController + ", mementoAdapterProvider=" + mementoAdapterProvider + ", snapshotAdapterProvider=" + snapshotAdapterProvider + ", connection=" + connection + ", toOutbox=" + toOutbox + '}';
    }
}
