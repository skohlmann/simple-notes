/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store;

import de.speexx.common.annotation.Generated;
import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

import static de.speexx.common.Assertions.assertNotNull;

/**
 * @author sascha.kohlmann
 */
public class StoreResult implements Serializable {

    private final Exception exception;
    private final ResultType resultType;
    
    /** @throws NullPointerException if and only if {@code resultType} is of {@code null} */
    public StoreResult(final ResultType resultType) {
        this(resultType, null);
    }

    /** @throws NullPointerException if and only if {@code resultType} is of {@code null} */
    public StoreResult(final ResultType resultType, final Exception exception) {
        this.exception = exception;
        this.resultType = assertNotNull(resultType, "Result type must be provided");
    }
    
    public Optional<Exception> exception() {
        return Optional.ofNullable(this.exception);
    }
    
    public ResultType resultType() {
        return this.resultType;
    }
    
    /** {@code null} safe factory. */
    public static StoreResult ofFailure(final Exception ex) {
        return new StoreResult(ResultType.FAILURE, ex);
    }
    
    /** {@code null} safe factory. */
    public static StoreResult ofNotFound(final Exception ex) {
        return new StoreResult(ResultType.NOT_FOUND, ex);
    }
    
    /** {@code null} safe factory. */
    public static StoreResult ofNotFound() {
        return new StoreResult(ResultType.NOT_FOUND);
    }
    
    /** {@code null} safe factory. */
    public static StoreResult ofSuccess() {
        return new StoreResult(ResultType.SUCCESS);
    }

    @Override
    @Generated
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.exception);
        hash = 67 * hash + Objects.hashCode(this.resultType);
        return hash;
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StoreResult other = (StoreResult) obj;
        if (!Objects.equals(this.exception, other.exception)) {
            return false;
        }
        if (this.resultType != other.resultType) {
            return false;
        }
        return true;
    }

    @Override
    @Generated
    public String toString() {
        return "StoreResult{" + "exception=" + exception + ", resultType=" + resultType + '}';
    }
}
