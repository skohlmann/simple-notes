/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.object.jdbc.postgres;

import de.speexx.event.sourcing.BaseMemento.TextMemento;
import static de.speexx.event.sourcing.Memento.typed;
import de.speexx.event.sourcing.store.ResultType;
import de.speexx.event.sourcing.store.StoreException;
import de.speexx.event.sourcing.store.StoreResult;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalLong;
import de.speexx.event.sourcing.store.object.ObjectStoreReader;

/**
 * @author sascha.kohlmann
 */
public class PostgresObjectStoreReader implements ObjectStoreReader<TextMemento> {

    static final String TBL_EVENT_STATE_STORE_OFFSETS = "tbl_event_object_offsets";

    private static final String QUERY_COUNT = "SELECT COUNT(*) FROM " + PostgresObjectStore.TBL_EVENT_STATE;
    private static final String DELETE_OFFSET = "DELETE FROM " + TBL_EVENT_STATE_STORE_OFFSETS + " WHERE reader_name = ?";


    private static final String UPSERT_CURRENT_OFFSET =
            "INSERT INTO " + TBL_EVENT_STATE_STORE_OFFSETS + " (reader_name, reader_offset, reader_counter, reader_last_op) "
            + "VALUES(?, ?, ?, NOW()) "
            + "ON CONFLICT (reader_name) DO UPDATE SET reader_offset = ?, reader_counter = ?, reader_last_op = NOW()";

    private static final String QUERY_CURRENT_OFFSET = "SELECT reader_offset, reader_counter FROM " + TBL_EVENT_STATE_STORE_OFFSETS + " WHERE reader_name = ?";
    private static final String QUERY_LAST_OFFSET = "SELECT count(*) FROM " + PostgresObjectStore.TBL_EVENT_STATE;
    
    private static final String QUERY_MEMENTOS =
            "SELECT memento_data, memento_type_version, memento_type, id, memento_created_at_nanos "
            + "FROM " + PostgresObjectStore.TBL_EVENT_STATE + " "
            + "WHERE memento_created_at_nanos >= ? ORDER BY memento_created_at_nanos ASC LIMIT ?";
    
    private Connection connection;

    private final String readerName;
    private long offset;
    private int counter;
    
    public PostgresObjectStoreReader(final Connection conn, final String readerName) {
        this.readerName = Objects.requireNonNull(readerName, "Name must be provided");
        setConnection(conn);
        retrieveCurrentOffset();
    }

    @Override
    public Optional<TextMemento> readNext() {
        final List<TextMemento> next = readNext(1);
        if (next.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(next.get(0));
    }

    @Override
    public Optional<TextMemento> readNext(final String fromId) {
        seekTo(fromId);
        return readNext();
    }

    @Override
    public List<TextMemento> readNext(final int maximumEntries) {
        final List<TextMemento> mementos = new ArrayList<>(maximumEntries);
        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_MEMENTOS)) {
            stmt.setLong(1, offset());
            // Assumption: there are not 20 event created within the same nanosecond since Unix Epoche.
            // This is ugly but a quick fix.
            final int maximumEntriesWorkaround = maximumEntries + 20;
            stmt.setMaxRows(maximumEntriesWorkaround);
            stmt.setFetchSize(maximumEntriesWorkaround);
            stmt.setInt(2, maximumEntriesWorkaround); // workaround for AWS Postgres to limit resultSet (setMaxRows of jdbc driver is ignored)

            try (final ResultSet rs = stmt.executeQuery()) {
                int localCounter = 0;
                while (rs.next()) {
                    long newOffset = rs.getLong("memento_created_at_nanos");
                    if (newOffset > this.offset) {
                        resetOffset(newOffset);
                        localCounter = 0;
                    }
                    if (localCounter < this.counter) {
                        localCounter++;
                    } else {
                        if (this.offset == newOffset) {
                            this.counter++;
                            localCounter++;
                        }
                        mementos.add(entryFromResultSet(rs));
                    }
                        
                    if (mementos.size() == maximumEntries) {
                        break;
                    }
                }
            }
        } catch (final Exception ex) {
            throw new StoreException(new StoreResult(ResultType.FAILURE), ex.getLocalizedMessage());
        }
        updateToCurrentOffset();
        return mementos;
    }

    @Override
    public List<TextMemento> readNext(final String fromId, final int maximumEntries) {
        seekTo(fromId);
        return readNext(maximumEntries);
    }

    @Override
    public void rewind() {
        resetOffset(0);
        updateToCurrentOffset();
    }

    @Override
    public String seekTo(final String id) {
        switch (id) {
            case BEGIN:
                rewind();
                break;
            case END:
                resetOffset(retrieveLatestOffset() + 1);
                updateToCurrentOffset();
                break;
            case QUERY:
                break;
            default:
                resetOffset(Long.parseLong(id));
                updateToCurrentOffset();
                break;
        }

        return String.valueOf(offset());
    }

    @Override
    public void delete() {
        try (final PreparedStatement stmt = connection().prepareStatement(DELETE_OFFSET)) {
            stmt.setString(1, name());
            stmt.execute();
        } catch (final Exception ex) {
            throw new StoreException(new StoreResult(ResultType.FAILURE, ex), ex.getLocalizedMessage());
        }
    }

    @Override
    public OptionalLong size() {
        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_COUNT);
             final ResultSet resultSet = stmt.executeQuery()) {
            if (resultSet.next()) {
                return OptionalLong.of(resultSet.getLong(1));
            }
        } catch (final Exception ex) {
            throw new StoreException(new StoreResult(ResultType.FAILURE), "Offset can't be lower 1: " + offset);
        }

        return OptionalLong.of(0);
    }

    private TextMemento entryFromResultSet(final ResultSet resultSet) throws SQLException, ClassNotFoundException {
        
        final String mementoData = resultSet.getString("memento_data");
        final String mementoType = resultSet.getString("memento_type");
        final int mementoVersion = resultSet.getInt("memento_type_version");
        final String id = resultSet.getString("id");

        return new TextMemento(id, typed(mementoType), mementoVersion, mementoData);
    }

    private void updateToCurrentOffset() {
        try (final PreparedStatement stmt = connection().prepareStatement(UPSERT_CURRENT_OFFSET)) {
            stmt.setString(1, name());
            stmt.setLong(2, offset());
            stmt.setInt(3, this.counter);
            stmt.setLong(4, offset());
            stmt.setInt(5, this.counter);

            final int result = stmt.executeUpdate();
        } catch (final Exception ex) {
            throw new StoreException(new StoreResult(ResultType.FAILURE, ex), ex.getLocalizedMessage());
        }
    }

    private void retrieveCurrentOffset() {

        boolean doInit = true;
        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_CURRENT_OFFSET)) {
            stmt.setString(1, name());
            try (final ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    doInit = false;
                    this.offset = rs.getLong("reader_offset");
                    this.counter = rs.getInt("reader_counter");
                } 
            }
        } catch (final Exception ex) {
            throw new StoreException(new StoreResult(ResultType.FAILURE, ex), ex.getLocalizedMessage());
        }
        if (doInit) {
            this.offset = 0;
            this.counter = 0;
            updateToCurrentOffset();
        }
    }
    
    private long retrieveLatestOffset() {
        try (final PreparedStatement stmt = connection().prepareStatement(QUERY_LAST_OFFSET);
             final ResultSet resultSet = stmt.executeQuery()) {
            if (resultSet.next()) {
                return resultSet.getLong(1);
            }
        } catch (final Exception ex) {
            throw new StoreException(new StoreResult(ResultType.FAILURE, ex), ex.getLocalizedMessage());
        }

        return offset();
    }

    private void resetOffset(final long offset) {
        if (offset < 0) { throw new StoreException(new StoreResult(ResultType.FAILURE), "Offset can't be lower 1: " + offset); }
        this.offset = offset;
        this.counter = 0;
    }
    
    public long offset() {
        return this.offset;
    }

    @Override
    public String name() {
        return this.readerName;
    }

    @Override
    public void close() throws Exception {
        connection.close();
    }
    
    private Connection connection() {
        return this.connection;
    }

    private void setConnection(final Connection conn) {
        this.connection = Objects.requireNonNull(conn, "Connection must be provided");
    }

    @Override
    public String toString() {
        return "PostgresObjectStoreReader{" + "readerName=" + readerName + ", offset=" + offset + ", counter=" + counter + '}';
    }
}
