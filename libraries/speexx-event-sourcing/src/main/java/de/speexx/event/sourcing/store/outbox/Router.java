/*
 * This file is part of the SpeexX Event Sourcing.
 * 
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License (MPL), v. 2.0 and the GNU Lesser General Public License (LGPL)
 * Version 3 or newer.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://mozilla.org/MPL/2.0/
 * If a copy of the LGPL was not distributed with this file, you can obtain one at
 * http://www.gnu.org/licenses/lgpl-3.0.de.html
 */
package de.speexx.event.sourcing.store.outbox;

/**
 * Defines routable support.
 * <p>Implementation must guarantee an at-least-once scenario and routers are responsible
 * for their own state persistence. This means that after the router call has returned
 * to its caller without a thrown exception, the router cannot expect the routable to be
 * delivered again.</p>
 * @author sascha.kohlmann
 */
public interface Router<R extends Routable> {

    /** Routes a routable. */
    void route(final R routable) throws RuntimeException;
}
