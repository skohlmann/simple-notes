
= Build

Build requires an installed and running Docker which version must be at least 1.6.0.

= Build site documentation

To build the site documentation https://graphviz.org/[graphviz] must be installed and the `dot` command tool must be available via `PATH` environment variable.
