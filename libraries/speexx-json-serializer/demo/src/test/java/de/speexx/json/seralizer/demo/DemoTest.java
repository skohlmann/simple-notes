package de.speexx.json.seralizer.demo;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.seralizer.JsonSerializer;
import java.time.OffsetDateTime;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class DemoTest {
    
    @Test
    public void serialize_deserialize() {
        // Given
        final var demo = new Demo("simple value", OffsetDateTime.now());
        
        // When
        final var json = JsonSerializer.instance().serialize(demo);
        System.out.println("JSON: " + json);
        final var deserialized = JsonSerializer.instance().deserialize(json, Demo.class);
        
        assertThat(deserialized.value).isEqualTo("simple value");
    }
}
