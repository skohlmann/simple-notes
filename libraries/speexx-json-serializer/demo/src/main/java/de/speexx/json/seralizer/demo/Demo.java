package de.speexx.json.seralizer.demo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.OffsetDateTime;

/**
 *
 * @author sascha.kohlmann
 */
public class Demo {
    
    @JsonProperty("value") public String value;
    @JsonProperty("aday") public OffsetDateTime datetime;

    public Demo(@JsonProperty("value") final String value, @JsonProperty("aday") final OffsetDateTime datetime) {
        this.value = value;
        this.datetime = datetime;
    }
}
