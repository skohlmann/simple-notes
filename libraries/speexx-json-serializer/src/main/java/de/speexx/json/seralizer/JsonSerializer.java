/*
 * This file is part of the SpeexX JSON Serializer.
 * 
 * SpeexX JSON Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Serializer. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.seralizer;
    
import de.speexx.json.seralizer.jackson.JacksonJsonSerializer;
import de.speexx.serializer.Deserializer;
import de.speexx.serializer.Serializer;

/**
 * Simple <a href='http://json.org/'>JSON</a> serializer and deserializer.
 * The default implementation of {@link #instance()} is backed with 
 * <a href='https://github.com/FasterXML/jackson'>FasterXML Jackson</a> as serializer/deserializer.
 * 
 * <p>Implementation must support all Java standard types and parts of the {@code java.time}
 * package like {@link java.time.LocalTime}, {@link java.time.LocalDateTime},
 * {@link java.time.Month}, {@link java.time.OffsetDateTime} and {@link java.time.ZonedDateTime}.</p>
 * @author sascha.kohlmann
 */
public interface JsonSerializer extends Serializer<String>, Deserializer<String> {
    
    public static JsonSerializer instance() {
        return JacksonJsonSerializer.instance();
    }

    /**
     * Serialize a given Object into a JSON string representation.
     * @param object the object to serialize
     * @return a JSON formatted string representation
     * @throws IllegalArgumentException if and only if the given structure can't be serialized.
     */
    @Override
    String serialize(final Object object);

    /**
     * Deserialize a given JSON string respresentation into an instance of the given {@code Class}.
     * @param json the JSON string representation to deserialize
     * @param target the targettype to serialize into
     * @param <T> the type
     * @return a new instance of <em>target</em>
     * @throws IllegalArgumentException if and only if the given JSON string can't be deserialized.
     */
    @Override
    <T extends Object> T deserialize(final String json, final Class<T> target);
}
