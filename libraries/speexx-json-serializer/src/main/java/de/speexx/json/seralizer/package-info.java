/*
 * This file is part of the SpeexX JSON Serializer.
 * 
 * SpeexX JSON Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Serializer. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * <a href='http://json.org/'>JSON</a> serializer and deserializer.
 */
package de.speexx.json.seralizer;
