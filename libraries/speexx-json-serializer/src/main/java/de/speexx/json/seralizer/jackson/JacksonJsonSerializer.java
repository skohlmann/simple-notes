/*
 * This file is part of the SpeexX JSON Serializer.
 * 
 * SpeexX JSON Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Serializer. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.seralizer.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import static com.fasterxml.jackson.databind.SerializationFeature.FAIL_ON_EMPTY_BEANS;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import de.speexx.json.seralizer.JsonSerializer;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;

/**
 * @author sascha.kohlmann
 */
public class JacksonJsonSerializer implements JsonSerializer {
    
    private static JacksonJsonSerializer INSTANCE;

    public static synchronized JacksonJsonSerializer instance() {
        if (INSTANCE == null) {
            INSTANCE = new JacksonJsonSerializer();
        }

        return JacksonJsonSerializer.INSTANCE;
    }
    private ObjectMapper objectMapper;

    private JacksonJsonSerializer() {
       super();
       build();
    }
    
    private void build() {
        final SimpleModule mod = new SimpleModule();
        mod.addSerializer(LocalTime.class, new LocalTimeSerializer());
        mod.addSerializer(LocalDate.class, new LocalDateSerializer());
        mod.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
        mod.addSerializer(OffsetTime.class, new OffsetTimeSerializer());
        mod.addSerializer(OffsetDateTime.class, new OffsetDateTimeSerializer());
        mod.addSerializer(ZonedDateTime.class, new ZonedDateTimeSerializer());
        mod.addDeserializer(LocalTime.class, new LocalTimeDeserializer());
        mod.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer());
        mod.addDeserializer(LocalDate.class, new LocalDateDeserializer());
        mod.addDeserializer(OffsetDateTime.class, new OffsetDateTimeDeserializer());
        mod.addDeserializer(OffsetTime.class, new OffsetTimeDeserializer());
        mod.addDeserializer(ZonedDateTime.class, new ZonedDateTimeDeserializer());

        final ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(mod);
        mapper.registerModule(new Jdk8Module());
        mapper.disable(FAIL_ON_EMPTY_BEANS);
//        mapper.setVisibility(ALL, ANY);

        this.objectMapper = mapper;
    }

    @Override
    public String serialize(final Object object) {
        try {
            return objectMapper().writeValueAsString(object);
        } catch (final JsonProcessingException ex) {
            throw new IllegalArgumentException("Unable to serialize structure.", ex);
        }
    }

    @Override
    public <T extends Object> T deserialize(final String json, final Class<T> target) {
        try {
            return objectMapper().readValue(json, target);
        } catch (final IOException ex) {
            throw new IllegalArgumentException("Unable to deserialize structure.", ex);
        }
    }

    private ObjectMapper objectMapper() {
        return this.objectMapper;
    }
}
