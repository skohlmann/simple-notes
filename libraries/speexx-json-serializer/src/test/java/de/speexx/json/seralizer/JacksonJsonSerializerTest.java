/*
 * This file is part of the SpeexX JSON Serializer.
 * 
 * SpeexX JSON Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX JSON Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX JSON Serializer. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.json.seralizer;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;

import static com.google.common.truth.Truth.assertThat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.Optional;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class JacksonJsonSerializerTest {

    @Test
    public void LocalDate_serialization_deserialization() {
        // Given
        final LocalDate date = LocalDate.now();
        final LocalDateContainer container = new LocalDateContainer();
        container.setDate(date);
        
        // When
        final String json = JsonSerializer.instance().serialize(container);
        final LocalDateContainer deserialized = JsonSerializer.instance().deserialize(json, LocalDateContainer.class);
        
        // Then
        assertThat(container.date()).isEqualTo(deserialized.date());
    }
    
    @Test
    public void LocalDateTime_serialization_deserialization() {
        // Given
        final LocalDateTime datetime = LocalDateTime.now();
        final LocalDateTimeContainer container = new LocalDateTimeContainer();
        container.setDatetime(datetime);
        
        // When
        final String json = JsonSerializer.instance().serialize(container);
        final LocalDateTimeContainer deserialized = JsonSerializer.instance().deserialize(json, LocalDateTimeContainer.class);
        
        // Then
        assertThat(container.datetime()).isEqualTo(deserialized.datetime());
    }
    
    @Test
    public void LocalTime_serialization_deserialization() {
        // Given
        final LocalTime time = LocalTime.now();
        final LocalTimeContainer container = new LocalTimeContainer();
        container.setTime(time);
        
        // When
        final String json = JsonSerializer.instance().serialize(container);
        final LocalTimeContainer deserialized = JsonSerializer.instance().deserialize(json, LocalTimeContainer.class);
        
        // Then
        assertThat(container.time()).isEqualTo(deserialized.time());
    }
    
    @Test
    public void Month_serialization_deserialization() {
        // Given
        final Month month = Month.MARCH;
        final MonthContainer container = new MonthContainer();
        container.setMonth(month);
        
        // When
        final String json = JsonSerializer.instance().serialize(container);
        final MonthContainer deserialized = JsonSerializer.instance().deserialize(json, MonthContainer.class);
        
        // Then
        assertThat(container.month()).isEqualTo(deserialized.month());
    }
    
    @Test
    public void OffsetDateTime_serialization_deserialization() {
        // Given
        final OffsetDateTime datetime = OffsetDateTime.now();
        final OffsetDateTimeContainer container = new OffsetDateTimeContainer();
        container.setDatetime(datetime);
        
        // When
        final String json = JsonSerializer.instance().serialize(container);
        final OffsetDateTimeContainer deserialized = JsonSerializer.instance().deserialize(json, OffsetDateTimeContainer.class);
        
        // Then
        assertThat(container.datetime()).isEqualTo(deserialized.datetime());
    }
    
    @Test
    public void ZonedDateTime_serialization_deserialization() {
        // Given
        final ZonedDateTime datetime = ZonedDateTime.now();
        final ZonedDateTimeContainer container = new ZonedDateTimeContainer();
        container.setDatetime(datetime);
        
        // When
        final String json = JsonSerializer.instance().serialize(container);
        final ZonedDateTimeContainer deserialized = JsonSerializer.instance().deserialize(json, ZonedDateTimeContainer.class);
        
        // Then
        assertThat(container.datetime()).isEqualTo(deserialized.datetime());
    }

    @Test
    public void Optional_seralization_deserialization_with_value() {
        // Given
        final OptionalContainer optCont = new OptionalContainer();
        optCont.setData("test");
        
        // When
        final String json = JsonSerializer.instance().serialize(optCont);
        final OptionalContainer deserialized = JsonSerializer.instance().deserialize(json, OptionalContainer.class);

        // Then
        assertThat(deserialized.data().isPresent()).isTrue();
        assertThat(deserialized.data().get()).isEqualTo("test");
    }

    @Test
    public void Optional_seralization_deserialization_with_null_value() {
        // Given
        final OptionalContainer optCont = new OptionalContainer();
        optCont.setData(null);
        
        // When
        final String json = JsonSerializer.instance().serialize(optCont);
        final OptionalContainer deserialized = JsonSerializer.instance().deserialize(json, OptionalContainer.class);

        // Then
        assertThat(deserialized.data().isPresent()).isFalse();
    }

    public static final class LocalDateContainer {
        
        @JsonProperty private LocalDate date;
        
        public LocalDate date() {
            return this.date;
        }
        
        void setDate(final LocalDate date) {
            this.date = date;
        }
    }

    public static final class LocalDateTimeContainer {
        
        @JsonProperty private LocalDateTime datetime;
        
        public LocalDateTime datetime() {
            return this.datetime;
        }
        
        void setDatetime(final LocalDateTime datetime) {
            this.datetime = datetime;
        }
    }

    public static final class ZonedDateTimeContainer {
        
        @JsonProperty private ZonedDateTime datetime;
        
        public ZonedDateTime datetime() {
            return this.datetime;
        }
        
        void setDatetime(final ZonedDateTime datetime) {
            this.datetime = datetime;
        }
    }

    public static final class OffsetDateTimeContainer {
        
        @JsonProperty private OffsetDateTime datetime;
        
        public OffsetDateTime datetime() {
            return this.datetime;
        }
        
        void setDatetime(final OffsetDateTime datetime) {
            this.datetime = datetime;
        }
    }

    public static final class LocalTimeContainer {
        
        @JsonProperty private LocalTime time;
        
        public LocalTime time() {
            return this.time;
        }
        
        void setTime(final LocalTime time) {
            this.time = time;
        }
    }

    public static final class MonthContainer {
        
        @JsonProperty private Month month;
        
        public Month month() {
            return this.month;
        }
        
        void setMonth(final Month month) {
            this.month = month;
        }
    }
    
    public static final class OptionalContainer {
        
        @JsonProperty("data") private String data;
        
        public void setData(final String data) {
            this.data = data;
        }
        
        public Optional<String> data() {
            return Optional.ofNullable(this.data);
        }
    }
}
