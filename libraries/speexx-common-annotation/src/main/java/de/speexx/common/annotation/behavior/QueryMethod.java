/* 
 * This file is part of the SpeexX Common annotations.
 * 
 * SpeexX Common annotatiions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common annotatiions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common annotatiions. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.annotation.behavior;

import de.speexx.common.annotation.IDE;
import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.apiguardian.api.API;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.SOURCE;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 * Marks a <em>side effect free</em> method to retrieve information from objects.
 * Query methods should have a return value.
 * <p>By convention, normal getter methods (returning data of an object) should not be annotated with this type.</p>
 * @author sascha.kohlmann
 * @see CommandMethod
 * @author sascha.kohlmann
 */
@IDE
@Inherited
@Documented
@Target({METHOD})
@Retention(SOURCE)
@API(status=EXPERIMENTAL)
public @interface QueryMethod {}
