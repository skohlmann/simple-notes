/* 
 * This file is part of the SpeexX Common annotations.
 * 
 * SpeexX Common annotatiions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common annotatiions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common annotatiions. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.apiguardian.api.API;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.LOCAL_VARIABLE;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.CLASS;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * Documents that the parameter of a method or constructor or the return type of a method can be of value {@code null}.
 * <p>By convention, this annotation applied only when the value should <em>always</em> be checked against {@code null}
 * because the developer could do nothing to prevent null from happening.
 * Otherwise, too eager {@link Nullable} usage could lead to too many false positives from static analysis tools.</p>
 * <p><b>Note:</b> For return type a better option is always to return a {@link java.util.Optional Optional}.</p>
 * <p>Apart from documentation purposes this annotation is intended to be used by static analysis tools
 * to validate against element contract violations.</p>
 * @author sascha.kohlmann
 */
@Inherited
@Documented
@Retention(CLASS)
@API(status=STABLE)
@Target({PARAMETER, TYPE_USE, LOCAL_VARIABLE, FIELD})
public @interface Nullable {}
