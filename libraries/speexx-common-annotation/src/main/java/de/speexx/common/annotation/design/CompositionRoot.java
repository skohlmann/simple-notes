/* 
 * This file is part of the SpeexX Common annotations.
 * 
 * SpeexX Common annotatiions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common annotatiions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common annotatiions. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.annotation.design;

import de.speexx.common.annotation.IDE;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.apiguardian.api.API;

import static java.lang.annotation.RetentionPolicy.CLASS;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 * Only one {@code CompositionRoot} annotation should be available within an application.
 * 
 * <dl>
 *   <dt>Composition Root</dt>
 *   <dd>
 *     <p>A Composition Root is a (preferably) unique location in an application where modules are composed together.</p>
 *     <p>Source: <a href='https://freecontent.manning.com/dependency-injection-in-net-2nd-edition-understanding-the-composition-root/'>Dependency Injection - Principles, Practicces and Patterns</a></p>
 *   </dd>
 * </dl>
 */
@IDE
@Documented
@Retention(CLASS)
@API(status=EXPERIMENTAL)
@Target({ElementType.PACKAGE, ElementType.TYPE})
public @interface CompositionRoot {}
