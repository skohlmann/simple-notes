/* 
 * This file is part of the SpeexX Common annotations.
 * 
 * SpeexX Common annotatiions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common annotatiions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common annotatiions. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.annotation.behavior;

import de.speexx.common.annotation.IDE;
import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.apiguardian.api.API;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.SOURCE;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 * A command method manipulates a {@linkplain de.speexx.common.annotation.Mutable mutable object}
 * (e.g. entity) with the command value as parameter and should return {@code void} or an event.
 * A command method may have a side effect on the object.
 * <p>The name of a command method should be an <em>imperative</em>.</p>
 * <p>Apart from documentation purposes this annotation is intended to be used by static analysis tools
 * to validate against element contract violations.</p>
 * @author sascha.kohlmann
 * @see ModifierMethod
 * @see QueryMethod
 * @see de.speexx.common.annotation.Mutable
 */
@IDE
@Inherited
@Documented
@Target({METHOD})
@Retention(SOURCE)
@API(status=EXPERIMENTAL)
public @interface CommandMethod {}
