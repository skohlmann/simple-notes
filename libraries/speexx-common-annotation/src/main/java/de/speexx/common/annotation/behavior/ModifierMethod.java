/* 
 * This file is part of the SpeexX Common annotations.
 * 
 * SpeexX Common annotatiions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common annotatiions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common annotatiions. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.annotation.behavior;

import de.speexx.common.annotation.IDE;
import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.apiguardian.api.API;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.SOURCE;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 * A modifer method is able to change a {@linkplain de.speexx.common.annotation.Immutable immutable object}
 * (e.g. value objects) by creating a new instance with the value of the parameter.
 * In other words: a modifer method has never a side effect on the called instance.
 * <p>The name of a modifier method should be an declarative. To create a good name for modifier methods,
 * fill in the following template: “I want <em>this</em> …, but …”. <br>
 * E.g. for a <em>Product</em> object with a given <em>name</em> there should be a name change. The resulting sentence is:
 * "I want this Product, but with a <em>changed</em> name."<br>
 * As a result the name of the modifying method should be {@code withChangedName} or {@code nameChanged}.</p>
 * @see CommandMethod
 * @see de.speexx.common.annotation.Immutable
 * @author sascha.kohlmann
 */
@IDE
@Inherited
@Documented
@Target({METHOD})
@Retention(SOURCE)
@API(status=EXPERIMENTAL)
public @interface ModifierMethod {}
