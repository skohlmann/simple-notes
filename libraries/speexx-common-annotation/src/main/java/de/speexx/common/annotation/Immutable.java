/* 
 * This file is part of the SpeexX Common annotations.
 * 
 * SpeexX Common annotatiions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common annotatiions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common annotatiions. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.apiguardian.api.API;

import static java.lang.annotation.RetentionPolicy.CLASS;
import static org.apiguardian.api.API.Status.STABLE;


/**
 * Identifies an objects where the properties of the objects are not mutable after creation of the object instance.
 * <p><b>Reference:</b> {@link java.lang.String}</p>
 * <p>Apart from documentation purposes this annotation is intended to be used by static analysis tools to validate
 * against element contract violations.</p>
 * @author sascha.kohlmann
 * @see java.lang.String
 */
@Inherited
@Documented
@API(status=STABLE)
@Retention(CLASS)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
public @interface Immutable {}
