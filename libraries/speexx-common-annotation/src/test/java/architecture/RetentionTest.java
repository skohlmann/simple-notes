/* 
 * This file is part of the SpeexX Common annotations.
 * 
 * SpeexX Common annotatiions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common annotatiions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common annotatiions. If not, see <https://www.gnu.org/licenses/>.
 */
package architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import de.speexx.common.annotation.IDE;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 *
 * @author sascha.kohlmann
 */
public class RetentionTest {

    @Test
    public void all_classes() {
        final JavaClasses importedClasses = new ClassFileImporter().importPackages(IDE.class.getPackageName());
        importedClasses.forEach(javaClass -> {
            final Class<?> annotation = javaClass.reflect();
            if (!"package-info".equals(annotation.getSimpleName())) {
                assertThat(annotation.isAnnotation() || annotation.isEnum()).isTrue();

                if (annotation.isAnnotation()) {
                    final Retention retention = annotation.getAnnotation(Retention.class);
                    assertThat(retention).isNotNull();
            
                    final RetentionPolicy policy = retention.value();
                    assertThat(policy).isNotEqualTo(RetentionPolicy.RUNTIME);
                }
            }
        });
    }
}
