/*
 * This file is part of the SpeexX Common.
 * 
 * SpeexX Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Offers a basic API for translating a data structure or object state into a
 * format that can be stored (for example, in a file or memory data buffer) or
 * transmitted (for example, across a computer network) and reconstructed later
 * (possibly in a different computer environment). When the resulting series of
 * bits is reread according to the serialization format, it can be used to
 * create a semantically identical clone of the original object.
 */
module speexx.serializer.api {
    requires org.apiguardian.api;

    exports de.speexx.serializer;
}
