/*
 * This file is part of the SpeexX Serializer.
 * 
 * SpeexX Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Serializer. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.serializer;

import org.apiguardian.api.API;

import static org.apiguardian.api.API.Status.STABLE;

/**
 * The interface offers a method to transform an object state into a format
 * which can be stored in a data base or transmitted over a neetwork environment.
 * The format of the transformation can be deserialized afterwards.
 * @author sascha.kohlmann
 * @see Deserializer
 * @param <T> the object representation of the serialized object. E.g. this can be {@code String} or a byte array.
 */
@API(status=STABLE)
public interface Serializer<T> {

    /**
     * Serialize (marshall) the object state into a format which can be stored in
     * a database or transmitted over a network.
     * @param obj the object state to serialize
     * @return the serialized format of the object state
     */
    T serialize(final Object obj);
}
