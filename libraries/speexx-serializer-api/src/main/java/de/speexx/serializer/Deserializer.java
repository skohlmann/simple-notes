/*
 * This file is part of the SpeexX Serializer.
 * 
 * SpeexX Serializer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Serializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Serializer. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.serializer;

import org.apiguardian.api.API;

import static org.apiguardian.api.API.Status.STABLE;

/**
 * The interface offers a method to transform a format which was stored in a database
 * or transmitteed ofer a network connection back to an object state representation.
 * @author sascha.kohlmann
 * @see Serializer
 * @param <T> the format representation of the serialized object. E.g. this can be {@code String} or a byte array.
 */
@API(status=STABLE)
public interface Deserializer<T> {
    
    /**
     * Deserialize (unmarshall) a representation format which could be stored in
     * a database or transmitted over a network connection back into an object
     * state.
     * @param <SO> the concrete state object type to deserialize into
     * @param serialized a serializd representation format
     * @param template a type template to deserialize the <em>serialized</em> format into.
     * @return an instance of the <em>template</em> for the <em>serialized</em> format
     */
    <SO extends Object> SO deserialize(final T serialized, final Class<SO> template);
}
