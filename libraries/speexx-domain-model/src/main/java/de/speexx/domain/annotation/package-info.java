/*
 * This file is part of the SpeexX Common Domain Model.
 * 
 * SpeexX Common Domain Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common Domain Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common Domain Model. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Domain Driven Design relevant documentation annotations. Based on the book
 * <a href='https://en.wikipedia.org/wiki/Special:BookSources/978-032-112521-7'>Domain-Driven Design.
 * Tackling Complexity in the Heart of Software</a> by Eric Evans.
 */
package de.speexx.domain.annotation;
