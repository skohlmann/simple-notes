/*
 * This file is part of the SpeexX Common Domain Model.
 * 
 * SpeexX Common Domain Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common Domain Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common Domain Model. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import static de.speexx.common.Assertions.assertNotBlank;
import static de.speexx.common.Assertions.assertNotNull;
import de.speexx.common.annotation.Generated;
import java.util.Objects;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * Defines a general ID contract for the SpeexX Domain Model environment.
 * <p>The implementation uses Jackson annotations for JSON seraialization and deserialization.
 * All subclasses should be compatible with this behavior.</p>
 * @author sascha.kohlmann
 */
@API(status=STABLE)
public abstract class AbstractId implements Identity {

    @JsonProperty
    @jakarta.validation.constraints.NotBlank
    @JsonPropertyDescription("The ID value should (RFC 2119) a character sequence of none whitespace Unicode only characters with a maximum length of 42 characters.")
    private String id;

    /**
     * Checks the given value to be 
     * <ol>
     *   <li>not to be {@code null}</li>
     *   <li>not to contain only whitespace characters</li>
     *   <li>the <em>optional</em> requirements implemented by {@link #validateId(java.lang.String) } are fulfilled</li>
     * </ol>
     * 
     * @param id the ID valkue to set
     */
    protected AbstractId(@JsonProperty(value = "id", required = true) final String id) {
        super();
        setId(id);
    }
    
    /**
     * <p>The ID value <em><a href='https://tools.ietf.org/html/rfc2119'>should</a></em> be a character sequence of
     * none whitespace characters with a maximum length of 42 characters.
     * Characters can be all Unicode 11 (or higher).</p>
     * @return the value of the ID. Never {@code null}.
     */
    @Override
    public String id() {
        return this.id;
    }

    /**
     * Validates the given ID. The default implementation does nothing.
     * @param id the ID to validate.
     */
    protected void validateId(final String id) {
        // implemented by subclasses for validation.
        // throws a runtime exception if invalid.
    }
    
    @Override
    @Generated
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractId other = (AbstractId) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{id=" + id + "}";
    }
    
    protected final void setId(final String id) {
        assertNotNull(id, "ID is null");
        assertNotBlank(id, "ID is blank");
        this.validateId(id);
        this.id = id;
    }
}
