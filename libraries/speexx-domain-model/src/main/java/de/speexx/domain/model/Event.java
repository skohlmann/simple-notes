/*
 * This file is part of the SpeexX Common Domain Model.
 * 
 * SpeexX Common Domain Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common Domain Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common Domain Model. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.domain.model;

import java.time.OffsetDateTime;

/**
 *
 * @author sascha.kohlmann
 */
public interface Event {
    
    @jakarta.validation.constraints.NotBlank
    String eventName();
    
    /**
     * The occurence of the event. If not defined otherwise, it's the timestamp of the creation of the event.
     * This can be overwritten by the deserialization process.
     * @return the occurence datetime
     */
    @jakarta.validation.constraints.NotNull
    OffsetDateTime occuredOn();
    
    /**
     * The event version. 1 or higher.
     * @return the version
     */
    @jakarta.validation.constraints.NotNull
    int eventVersion();
}
