/*
 * This file is part of the SpeexX Common Domain Model.
 * 
 * SpeexX Common Domain Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common Domain Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common Domain Model. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.domain.model;

import org.apiguardian.api.API;

import static org.apiguardian.api.API.Status.EXPERIMENTAL;


/**
 * Delivers an {@link Identity}.
 * The interface <a href='https://tools.ietf.org/rfc/rfc2119.txt'>should</a> be used for objects
 * annotated with {@link org.jmolecules.ddd.annotation.Entity}.
 * @author sascha.kohlmann
 * @see de.speexx.domain.model.sourced.EventSourcedEntity
 */
@API(status=EXPERIMENTAL)
public interface IdentityProvider {

    /**
     * Delivers the identity of an object.
     * <p><strong>Implementation note:</strong> implementing types must gurantee never returning {@code null}.
     * @return the identity
     */
    Identity identity();
}
