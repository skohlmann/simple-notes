/*
 * This file is part of the SpeexX Common Domain Model.
 * 
 * SpeexX Common Domain Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common Domain Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common Domain Model. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.domain.model;

import de.speexx.domain.annotation.DomainDrivenDesign;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;

/**
 * Documents a class as a domain event.
 * @author sascha.kohlmann
 */
@Documented
@Retention(RUNTIME)
@DomainDrivenDesign
@Target({ElementType.TYPE})
public @interface EventName {
    /** Defines a logical name of the event.
     * @return the name of the event. Must not be {@linkplain String#isEmpty() empty} or {@linkplain String#isBlank() blank}. */
    String value();
}
