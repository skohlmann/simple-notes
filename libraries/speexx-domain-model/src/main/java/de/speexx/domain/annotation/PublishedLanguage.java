/*
 * This file is part of the SpeexX Common Domain Model.
 * 
 * SpeexX Common Domain Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common Domain Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common Domain Model. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.domain.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.apiguardian.api.API;

import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 * A published language. Published languages should be backward compatible.
 * @author sascha.kohlmann
 */
@Documented
@Retention(RUNTIME)
@DomainDrivenDesign
@API(status=EXPERIMENTAL)
@Target({ElementType.TYPE})
public @interface PublishedLanguage {

    /** An optional name.
     * Tool support should fetch the relavant from other APIs. E.g {@link de.speexx.domain.model.EventName#value()}.
     * @return the name of the API  */
    String name() default "";

    /** An optional version.
     * @return the name of the API  */
    String version() default "";

    /** Defines the name of a group of schemas like {@code domainevent} or {@code command}.
     * @return a group name */
    String group() default "";
}
