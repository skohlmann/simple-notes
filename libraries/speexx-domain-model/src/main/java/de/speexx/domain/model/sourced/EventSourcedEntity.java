/*
 * This file is part of the SpeexX Common Domain Model.
 * 
 * SpeexX Common Domain Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common Domain Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common Domain Model. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.domain.model.sourced;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.speexx.domain.model.Event;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * The source entity can be created within a stream of {@link Event events}.
 * 
 * <p>The methods handling the {@code DomainEvent} must follow the rules of {@link Mutator} annotation.</p>
 * 
 * <p>For every {@code DomainEvent} only one {@code Mutator} method is supported.
 * If and only if two annotated methods are available, the behavior is not defined.</p>
 */
@SuppressWarnings("rawtypes")
public abstract class EventSourcedEntity {
    
    private static final Map<String, Method> MUTATOR_METHODS = new HashMap<>();
    private static final Map<Class<?>, Field[]> SNAPSHOT_FIELDS = new HashMap<>();

    @JsonIgnore private transient List<Event> mutatingEvents;
    @JsonIgnore private transient int unmutatedVersion;

    /** Creates a general instance of the entity. */
    protected EventSourcedEntity() {
        super();
        this.setMutatingEvents(new ArrayList<>(2));
        this.setUnmutatedVersion(0);
    }

    /** Creates entity based on the snapshot data of the given snapshot version.
     * All {@linkplain Class#getDeclaredFields() declared fields} of the given
     * snapshot are copied to this instance.
     * @param snapshot the snapshot
     * @param streamVersion the version of the event stream
     */
    protected EventSourcedEntity(final EventSourcedEntity snapshot, final int streamVersion) {
        this();
        initFromSnapshot(snapshot);
        this.setUnmutatedVersion(streamVersion);
    }

    /** Creates an entity of the <em>event stream</em>.
     * The events are propagated to {@link #mutateWhen(DomainEvent)} in the order of the given list.
     * <p><strong>Note:</strong> the implementation does not reorder the events by the {@link Event#occuredOn()} datetime.</p>
     * @param eventStream a list of events in the order the occured
     * @param streamVersion the version of the event stream  */
    protected EventSourcedEntity(final List<? extends Event> eventStream, final int streamVersion) {
        this();
        eventStream.forEach((event) -> this.mutateWhen(event));
        this.setUnmutatedVersion(streamVersion);
    }
    
    /** Creates entity based on the snapshot data of the given snapshot version and the events created after the snapshot generation.
     * <p>The following process is implemented:</p>
     * <ol>
     *   <li>All {@linkplain Class#getDeclaredFields() declared fields} of the given  snapshot are copied to this instance.</li>
     *   <li>All events from the given event stream are propagated to the {@link #mutateWhen(Event)} method.</li>
     * </ol>
     * <p><strong>Note:</strong> the implementation does not reorder the events by the {@link Event#occuredOn()} datetime.</p>
     * @param snapshot the snapshot
     * @param eventStream a list of events in the order the occured
     * @param streamVersion the version of the event stream */
    protected EventSourcedEntity(final EventSourcedEntity snapshot, final List<? extends Event> eventStream, final int streamVersion) {
        this(snapshot, streamVersion);
        eventStream.forEach(event -> this.mutateWhen(event));
    }

    private void initFromSnapshot(final EventSourcedEntity snapshot) {
        if (this.getClass() != snapshot.getClass()) {
            throw new IllegalArgumentException("Snapshot not of same type");
        }
        
        final Field[] fields = fieldsForType(this.getClass());
        for (final Field field : fields) {
            final Class<?> declaredType = field.getDeclaringClass();
            try {
                if (declaredType.isPrimitive()) {
                    if (declaredType == Integer.TYPE) {field.setInt(this, field.getInt(snapshot));} else
                    if (declaredType == Long.TYPE) {field.setLong(this, field.getLong(snapshot));} else
                    if (declaredType == Double.TYPE) {field.setDouble(this, field.getDouble(snapshot));} else
                    if (declaredType == Float.TYPE) {field.setFloat(this, field.getFloat(snapshot));} else
                    if (declaredType == Boolean.TYPE) {field.setBoolean(this, field.getBoolean(snapshot));} else
                    if (declaredType == Short.TYPE) {field.setShort(this, field.getShort(snapshot));} else
                    if (declaredType == Character.TYPE) {field.setChar(this, field.getChar(snapshot));} else
                    if (declaredType == Byte.TYPE) {field.setByte(this, field.getByte(snapshot));}
                } else {
                    field.set(this, field.get(snapshot));
                }
            } catch (final IllegalArgumentException | IllegalAccessException ex) {
                throw new IllegalStateException("Unable to init with snapshot because: " + ex.getLocalizedMessage(), ex);
            }
        }
    }
    
    private Field[] fieldsForType(final Class<?> clazz) {
        return SNAPSHOT_FIELDS.getOrDefault(clazz, fetchFields(clazz));
    }
    
    Field[] fetchFields(final Class<?> clazz) {
        final Set<Field> fields = new HashSet<>();
        for (final Field field : clazz.getDeclaredFields()) {
            if (!Modifier.isTransient(field.getModifiers()) && !field.isSynthetic() && !Modifier.isFinal(field.getModifiers())) {
                field.setAccessible(true);
                fields.add(field);
            }
        }
        
        return fields.toArray(new Field[fields.size()]);
    }

    /** The version of the unmutated events. The value normally is when creating
     * the instance.
     * @return the unmutated version */
    public final int unmutatedVersion() {
        return this.unmutatedVersion;
    }
    
    /** The version of the mutating events. The first mutating event, handeled by {@link #apply(Event)}.
     * @return the mutated version  */
    public final int mutatedVersion() {
        return this.unmutatedVersion() + 1;
    }

    /** A list of all mutating events handeled by {@link #apply(Event)}.
     * @return never {@code null} but might be empty. */
    public final List<Event> mutatingEvents() {
        return this.mutatingEvents;
    }

    protected final void apply(final Event event) {
        this.mutatingEvents().add(event);
        this.mutateWhen(event);
    }

    private void setMutatingEvents(final List<Event> mutatingEvents) {
        this.mutatingEvents = mutatingEvents;
    }

    private void setUnmutatedVersion(final int streamVersion) {
        this.unmutatedVersion = streamVersion;
    }

    protected final void mutateWhen(final Event event) {

        final Class<? extends EventSourcedEntity> rootType = this.getClass();
        final Class<? extends Event> eventType = event.getClass();
        final Method mutatorMethod = fetchMutatorMethod(rootType, eventType);
        
        try {
            mutatorMethod.invoke(this, event);

        } catch (final InvocationTargetException e) {
            if (e.getCause() != null) {
                throw new RuntimeException("Method " + mutatorMethod + "(" + eventType.getSimpleName() + ") failed. See cause: " + e.getMessage(), e.getCause());
            }

            // tricky to test
            throw new RuntimeException("Method " + mutatorMethod + "(" + eventType.getSimpleName() + ") failed. See cause: " + e.getMessage(), e);

        } catch (final IllegalAccessException e) {
            // tricky to test
            throw new RuntimeException("Method " + mutatorMethod + "(" + eventType.getSimpleName() + ") failed because of illegal access. See cause: " + e.getMessage(), e);
        }
    }

    Method fetchMutatorMethod(final Class<? extends EventSourcedEntity> rootType, final Class<? extends Event> eventType) {
        final String key = rootType.getName() + ":" + eventType.getName();
        final Method mutatorMethod = MUTATOR_METHODS.get(key);
        if (mutatorMethod == null) {
            return this.cacheMutatorMethodFor(key, rootType, eventType);
        }
        return mutatorMethod;
    }

    private Method cacheMutatorMethodFor(final String key, Class<? extends EventSourcedEntity> rootType, Class<? extends Event> eventType) {

        synchronized(MUTATOR_METHODS) {
            final Method method = this.hiddenOrPublicMethod(rootType, eventType);
            method.setAccessible(true);
            MUTATOR_METHODS.put(key, method);
            return method;
        }
    }

    private Method hiddenOrPublicMethod(final Class<? extends EventSourcedEntity> rootType, final Class<? extends Event> eventType) {
        final Optional<Method> declared = filterMutatorMethod(rootType.getDeclaredMethods(), eventType);
        if (declared.isPresent()) {
            return declared.get();
        }
        return filterMutatorMethod(rootType.getMethods(), eventType)
                    .orElseThrow(() -> new NoSuchMethodError("No mutator in " + rootType.getName() + " for DomainEvent " + eventType.getName()));
    }
    
    private Optional<Method> filterMutatorMethod(final Method[] methods, final Class<? extends Event> eventType) {
        for (final Method method : methods) {
            if (method.getAnnotation(Mutator.class) != null && method.getParameterCount() == 1) {
                final Parameter[] parameters = method.getParameters();
                if (parameters[0].getType() == eventType) {
                    return Optional.of(method);
                }
            }
        }
        return Optional.empty();
    }
}
