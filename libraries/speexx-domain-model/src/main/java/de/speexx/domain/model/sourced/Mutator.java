/*
 * This file is part of the SpeexX Common Domain Model.
 * 
 * SpeexX Common Domain Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common Domain Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common Domain Model. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.domain.model.sourced;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * Defines mutator methods in for handling domain events.
 *
 * Mutator methods are non-static and should be {@code protected} and may be
 * {@code private} or {@code public}. Mutator methods must have one parameter of
 * a specialized {@link de.speexx.domain.model.DomainEvent} type.
 * <p>{@code public} or {@code protected} Mutator methods should be {@code final}.</p>
 * @author sascha.kohlmann
 * @see EventSourcedEntity
 */
@Retention(RUNTIME)
@API(status=STABLE)
@Target(ElementType.METHOD)
public @interface Mutator {}
