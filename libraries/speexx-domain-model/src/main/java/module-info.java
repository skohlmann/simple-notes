module speexx.domain.model {

    requires org.apiguardian.api;
    requires jakarta.validation;
    requires com.fasterxml.jackson.annotation;
    requires speexx.common;
    requires speexx.common.annotation;
    requires jmolecules.ddd;
    requires jmolecules.events;

    exports de.speexx.domain.model;
    exports de.speexx.domain.model.sourced;
    exports de.speexx.domain.annotation;

    opens de.speexx.domain.model;
    opens de.speexx.domain.model.sourced;
}
