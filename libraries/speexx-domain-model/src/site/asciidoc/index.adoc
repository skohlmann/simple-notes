= SpeexX Domain Model

This project supports the development of https://en.wikipedia.org/wiki/Domain-driven_design[Domain Driven Design] projects of SpeexX Common. 

It supports  link:apidocs/de/speexx/common/domain/annotation/package-summary.html[annotations] for better identifactions of a domain model and the design of the model in the application.
Use these annotations together with the link:/speexx-common-annotation/index.html[SpeexX Common Annotation].

The package link:apidocs/de/speexx/common/domain/model/package-summary.html[`de.speexx.common.domain.model`] supports the development of an event sourced entity model. 

See link:apidocs/index.html[Javadoc] for further documentation.

