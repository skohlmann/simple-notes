/*
 * This file is part of the SpeexX Common Domain Model.
 * 
 * SpeexX Common Domain Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common Domain Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common Domain Model. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.seralizer.JsonSerializer;
import java.lang.reflect.Field;
import java.time.OffsetDateTime;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class AbstractDomainEventTest {
    
    @Test
    public void default_creation() {
        // When
        final DomainEvent evt = new TestDomainEvent();

        // Then
        assertThat(evt.eventVersion()).isEqualTo(1);
        assertThat(evt.eventName()).isEqualTo("TestDomainEvent");
        assertThat(evt.occuredOn()).isNotNull();
    }

    @Test
    public void create_with_version() {
        // When
        final DomainEvent evt = new TestDomainEvent(2);

        // Then
        assertThat(evt.eventVersion()).isEqualTo(2);
        assertThat(evt.eventName()).isEqualTo("TestDomainEvent");
        assertThat(evt.occuredOn()).isNotNull();
    }

    @Test
    public void create_with_wrong_version() {
        assertThrows(IllegalArgumentException.class, () -> new TestDomainEvent(0));
    }

    @Test
    public void create_with_minimum_version() {
        new TestDomainEvent(1);
    }

    @Test
    public void create_with_version_name_and_occured_on() {
        // Give
        final OffsetDateTime dateTime = OffsetDateTime.now();
        
        // When
        final DomainEvent evt = new TestDomainEvent(3, dateTime, "test");

        // Then
        assertThat(evt.eventVersion()).isEqualTo(3);
        assertThat(evt.eventName()).isEqualTo("test");
        assertThat(evt.occuredOn()).isEqualTo(dateTime);
    }

    @Test
    public void create_DomainEvent_annotated() {
        // When
        final DomainEvent evt = new TestDomainEventWithDomainEventAnnotation();

        // Then
        assertThat(evt.eventName()).isEqualTo("simple_event");
    }

    @Test
    public void create_DomainEvent_with_version_but_no_occurence() {
        assertThrows(IllegalArgumentException.class, () -> new TestDomainEvent(1, null));
    }

    @Test
    public void create_DomainEvent_with_version_and_name_but_no_occurence() {
        assertThrows(IllegalArgumentException.class, () -> new TestDomainEvent(1, null, "eventname"));
    }

    @Test
    public void create_DomainEvent_with_version_and_occurence_but_no_name() {
        assertThrows(IllegalArgumentException.class, () -> new TestDomainEvent(1, OffsetDateTime.now(), null));
    }
    
    @Test
    public void serialize_deserialize() {
        // Given
        final var event = new TestDomainEvent("value");
        
        // When
        final var json = JsonSerializer.instance().serialize(event);
        final var deserialized = JsonSerializer.instance().deserialize(json, TestDomainEvent.class);
        
        // Then
        assertThat(deserialized.property()).isEqualTo("value");
    }
    
    @Test
    public void json_names() throws NoSuchFieldException {

        final Class<?> eventClass = DomainEvent.Descriptor.class;

        final Field eventNameField = eventClass.getDeclaredField("eventName");
        final JsonProperty eventNameFieldJsonPropertyAnnotation = eventNameField.getAnnotation(JsonProperty.class);
        assertThat(eventNameFieldJsonPropertyAnnotation.value()).isEqualTo("event-name");
        
        final Field eventVersionField = eventClass.getDeclaredField("eventVersion");
        final JsonProperty eventVersionFieldJsonPropertyAnnotation = eventVersionField.getAnnotation(JsonProperty.class);
        assertThat(eventVersionFieldJsonPropertyAnnotation.value()).isEqualTo("event-version");

        final Field occuredOnField = eventClass.getDeclaredField("occuredOn");
        final JsonProperty occuredOnFieldFieldJsonPropertyAnnotation = occuredOnField.getAnnotation(JsonProperty.class);
        assertThat(occuredOnFieldFieldJsonPropertyAnnotation.value()).isEqualTo("occured-on");
    }
    
    @Test
    public void descriptor_name() throws NoSuchFieldException {

        final Class<?> eventClass = DomainEvent.class;

        final Field eventNameField = eventClass.getDeclaredField("descriptor");
        final JsonProperty eventNameFieldJsonPropertyAnnotation = eventNameField.getAnnotation(JsonProperty.class);
        assertThat(eventNameFieldJsonPropertyAnnotation.value()).isEqualTo("event-descriptor");
    }
    
    static class TestDomainEvent extends DomainEvent {
        
        @JsonProperty("property")
        private String property;
        
        protected TestDomainEvent() {
            super();
            this.property = "value";
        }

        protected TestDomainEvent(final int eventVersion) {
            super(eventVersion);
            this.property = "value";
        }

        protected TestDomainEvent(final int eventVersion, final OffsetDateTime occuredOn) {
            super(eventVersion, occuredOn);
        }

        protected TestDomainEvent(final OffsetDateTime occuredOn) {
            super(occuredOn);
            this.property = "value";
        }

        protected TestDomainEvent(final int eventVersion, final OffsetDateTime occuredOn, final String eventName) {
            super(eventVersion, occuredOn, eventName);
            this.property = "value";
        }
        
        protected TestDomainEvent(final String property) {
            super();
            this.property = property;
        }

        public String property() {
            return this.property;
        }
    }

    @EventName("simple_event")
    static class TestDomainEventWithDomainEventAnnotation extends TestDomainEvent {
        protected TestDomainEventWithDomainEventAnnotation() {
            super();
        }
    }
}
