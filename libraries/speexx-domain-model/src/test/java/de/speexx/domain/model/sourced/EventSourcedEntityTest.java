/*
 * This file is part of the SpeexX Common Domain Model.
 * 
 * SpeexX Common Domain Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common Domain Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common Domain Model. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.domain.model.sourced;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.truth.Truth.assertThat;
import de.speexx.domain.model.DomainEvent;
import de.speexx.domain.model.Event;
import de.speexx.json.seralizer.JsonSerializer;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class EventSourcedEntityTest {

    @Test
    public void no_mutator_method() {
        final class DummyEventSourcedRootEntity extends EventSourcedEntity {
            public DummyEventSourcedRootEntity(final List<Event> eventStream, final int version) {
                super(eventStream, version);
            }
        };
        
        final SimpleEvent event = new SimpleEvent();        
        
        assertThrows(NoSuchMethodError.class, () -> new DummyEventSourcedRootEntity(asList(event), 1));
    }
    
    @Test
    public void mutator_but_no_parameter() {
        final class DummyEventSourcedRootEntity extends EventSourcedEntity {
            public DummyEventSourcedRootEntity(final List<Event> eventStream, final int version) {
                super(eventStream, version);
            }
            @Mutator
            protected void mutator() {}
        };
        
        final SimpleEvent event = new SimpleEvent();        
        assertThrows(NoSuchMethodError.class, () -> new DummyEventSourcedRootEntity(asList(event), 1));
    }
    
    @Test
    public void mutator_but_illegal_parameter_type() {
        final class DummyEventSourcedRootEntity extends EventSourcedEntity {
            public DummyEventSourcedRootEntity(final List<Event> eventStream, final int version) {
                super(eventStream, version);
            }
            @Mutator
            protected void mutator(final String illegalType) {}
        };
        
        final SimpleEvent event = new SimpleEvent();        
        assertThrows(NoSuchMethodError.class, () -> new DummyEventSourcedRootEntity(asList(event), 1));
    }
    
    @Test
    public void mutator_with_public_method_for_valid_event() {
        final class DummyEventSourcedRootEntity extends EventSourcedEntity {
            public String value;
            public DummyEventSourcedRootEntity(final List<Event> eventStream, final int version) {
                super(eventStream, version);
            }
            @Mutator
            public void mutator(final SimpleEvent validType) {
                this.value = validType.value();
            }
        };
        
        final SimpleEvent event = new SimpleEvent();        
        assertThat(new DummyEventSourcedRootEntity(asList(event), 1).value).isEqualTo("retval");
    }

    static List<Event> asList(final SimpleEvent... events) {
        return Arrays.asList(events);
    }
    
    @Test
    public void mutator_with_two_method_for_valid_events() {
        final class DummyEventSourcedRootEntity extends EventSourcedEntity {
            public String value;
            public DummyEventSourcedRootEntity(final List<Event> eventStream, final int version) {
                super(eventStream, version);
            }
            @Mutator
            protected void mutator(final ExtSimpleEvent validType) {
                this.value = validType.value();
            }
            @Mutator
            private void mutator(final SimpleEvent validType) {
                this.value = validType.value();
            }
        };
        
        final SimpleEvent event = new SimpleEvent();        
        assertThat(new DummyEventSourcedRootEntity(asList(event), 1).value).isEqualTo("retval");
    }
    
    @Test
    public void mutator_with_protected_method_for_valid_event() {
        final class DummyEventSourcedRootEntity extends EventSourcedEntity {
            public String value;
            public DummyEventSourcedRootEntity(final List<Event> eventStream, final int version) {
                super(eventStream, version);
            }
            @Mutator
            private void mutator(final SimpleEvent validType) {
                this.value = validType.value();
            }
        };
        
        final SimpleEvent event = new SimpleEvent();        
        assertThat(new DummyEventSourcedRootEntity(asList(event), 1).value).isEqualTo("retval");
    }

    @Test
    public void mutator_throwing_cause_containing_InnocationTargetException() {
        final class DummyEventSourcedRootEntity extends EventSourcedEntity {
            public DummyEventSourcedRootEntity(final List<Event> eventStream, final int version) {
                super(eventStream, version);
            }
            @Mutator
            private void mutator(final InvocationTargetExceptionThrowingWithCauseEvent throwing) throws InvocationTargetException {
                throwing.value();
            }
        };
        
        final InvocationTargetExceptionThrowingWithCauseEvent event = new InvocationTargetExceptionThrowingWithCauseEvent();        
        final RuntimeException e = assertThrows(RuntimeException.class, () -> new DummyEventSourcedRootEntity(Arrays.asList(event), 1));
        assertThat(e.getCause().getClass()).isSameInstanceAs(NegativeArraySizeException.class);
    }

    @Test
    public void with_snapshot_and_events() {
        final SnapshotableEntity snapshot = new SnapshotableEntity();
        snapshot.setSnapshotField("test");
        
        final SnapshotableEntity.EventFieldChanged changeEvent = new SnapshotableEntity.EventFieldChanged();
        changeEvent.setField("abc");
        
        final SnapshotableEntity entity = new SnapshotableEntity(snapshot, Arrays.asList(changeEvent), 2);
        
        assertThat(entity.unmutatedVersion()).isEqualTo(2);
        assertThat(entity.snapshotField()).isEqualTo("test");
        assertThat(entity.eventField()).isEqualTo("abc");
    }

    @Test
    public void is_serializable() throws Exception {
        final SnapshotableEntity snapshot = new SnapshotableEntity();
        final String json = JsonSerializer.instance().serialize(snapshot);
        JsonSerializer.instance().deserialize(json, SnapshotableEntity.class);
    }
    
    public static class SnapshotableEntity extends EventSourcedEntity {
        @JsonProperty private String snapshotField;
        @JsonProperty private String eventField;
        
        public SnapshotableEntity(final SnapshotableEntity snapshot, final List<Event> eventStream, final int streamVersion) {
            super(snapshot, eventStream, streamVersion);
        }
        
        private SnapshotableEntity() {}

        public String snapshotField() {
            return snapshotField;
        }

        public void setSnapshotField(String snapshotField) {
            this.snapshotField = snapshotField;
        }

        public String eventField() {
            return eventField;
        }

        @Mutator
        public void when(final EventFieldChanged eventFieldChanged) {
            this.eventField = eventFieldChanged.getField();
        }
        
        public static class EventFieldChanged extends DomainEvent {
            private String field;
            public String getField() {return field;}
            public void setField(String field) {this.field = field;}

        }
    }

    static class SimpleEvent extends DomainEvent {
        public String value() {return "retval";}
    }
    
    static class ExtSimpleEvent extends SimpleEvent {
        @Override
        public String value() {return "extretval";}
   }

    static class InvocationTargetExceptionThrowingWithCauseEvent extends DomainEvent {
        public String value() {
            throw new NegativeArraySizeException();
        }
    }
}
