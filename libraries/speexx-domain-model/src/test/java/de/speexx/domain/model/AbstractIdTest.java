/*
 * This file is part of the SpeexX Common Domain Model.
 * 
 * SpeexX Common Domain Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Common Domain Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with SpeexX Common Domain Model. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.seralizer.JsonSerializer;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class AbstractIdTest {

    @Test
    public void creation() {
        // Given 
        final AbstractId id = new AbstractId("abc") {};
        
        // Then
        assertThat(id.id()).isEqualTo("abc");
    }
    
    @Test
    public void creation_with_null() {
        assertThrows(IllegalArgumentException.class, () -> new AbstractId(null) {});
    }
    
    @Test
    public void serialize_deserialize() {
        // Given 
        final AbstractId id = new SerializeTestAbstractId("abc");

        // When
        final String json = JsonSerializer.instance().serialize(id);
        final AbstractId deserialzed = JsonSerializer.instance().deserialize(json, SerializeTestAbstractId.class);
        
        // Then
        assertThat(deserialzed).isEqualTo(id);
    }
    
    @Test
    public void toString_name() {
        // Given 
        final AbstractId id = new SerializeTestAbstractId("abc");

        // Then
        assertThat(id.toString()).startsWith("SerializeTestAbstractId");
    }
    
    @Test
    public void validate_called() {
        assertThrows(IllegalStateException.class, () -> new ValidatingTestAbstractId("123"));
    }
    
    final static class SerializeTestAbstractId extends AbstractId {
        public SerializeTestAbstractId(@JsonProperty("id") final String id) {
            super(id);
        }
    }

    final static class ValidatingTestAbstractId extends AbstractId {
        
        public ValidatingTestAbstractId(@JsonProperty("id") final String id) {
            super(id);
        }
        
        @Override
        public void validateId(final String id) {
            throw new IllegalStateException("failed");
        }
    }
}
