/**
 * My module provides support for dynamic and compact access to data in JSON documents.
 * 
 * <p>My main entry point is {@link de.speexx.json.adapter.JsonPointerSupport}.
 * It allows you to write compact adapters for JSON structures.</p>
 * <p><strong>Example</strong></p>
 * <pre>
 *     final var structure = new JsonAdapter().jsonFrom("""
 *         {
 *           \"uri\" : \"https://example.com/\",
 *           \"value\" : 1.2
 *         }
 *     """);
 *
 *     jsonPointerSupport.optionalUriForPointer(structure, "/uri").send().ifPresent(pojo::setUri);
 *     jsonPointerSupport.optionalBigDecimalForPointer(structure, "/value").send().ifPresent(pojo::setPrice);
 * </pre>
 * 
 * <p>My {@link de.speexx.json.adapter.event.DomainEventSupport} adds also support for dealing with {@code AbstractDomainEvent}s.</p>
 * <p>Your adapter implementations may also inherit from {@link de.speexx.json.adapter.JsonPointerSupport} or from {@link de.speexx.json.adapter.event.DomainEventSupport} for more compact code.</p>
 * 
 * @see <a href='https://tools.ietf.org/html/rfc6901'>RFC&nbsp;6901</a> (JavaScript Object Notation (JSON) Pointer)
 */
module speexx.json.adapter {
    requires org.apiguardian.api;
    requires speexx.common;
    requires speexx.common.annotation;
    requires jakarta.json;

    exports de.speexx.json.adapter;
    exports de.speexx.json.adapter.event;
}
