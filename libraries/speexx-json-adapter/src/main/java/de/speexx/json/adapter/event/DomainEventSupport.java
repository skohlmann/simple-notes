package de.speexx.json.adapter.event;

import de.speexx.json.adapter.JsonPointerSupport;
import de.speexx.json.adapter.ValueContainer.IntegerContainer;
import de.speexx.json.adapter.ValueContainer.OffsetDateTimeContainer;
import de.speexx.json.adapter.ValueContainer.StringContainer;
import de.speexx.json.adapter.helper.DatetimeAdapter;
import jakarta.json.JsonStructure;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * Support for {@code de.speexx.domain.model.DomainEvent} events or other structures which
 * contains the top level JSON properties:
 * <ul>
 *   <li>{@code event-name}</li>
 *   <li>{@code event-version} as an integer value.</li>
 *   <li>{@code occured-on} in format of {@link java.time.format.DateTimeFormatter#ISO_OFFSET_DATE_TIME}</li>
 * </ul>
 * @author sascha.kohlmann
 */
@API(status=STABLE)
public class DomainEventSupport extends JsonPointerSupport {

    private final DatetimeAdapter datetimeAdapter = new DatetimeAdapter();

    /**
     * Returns the name of the {@code AbstractDomainEvent}.
     * @param json JSON structure of a serialized {@code AbstractDomainEvent}
     * @return a conatainer with the name of the event
     * @throws IllegalArgumentException if and only if the given structure doesn't conatain a {@code event-name}.
     */
    public StringContainer eventName(final JsonStructure json) {
        return stringForPointer(json, "/event-descriptor/event-name");
    }
    
    /**
     * Returns the version of the {@code AbstractDomainEvent}.
     * @param json JSON structure of a serialized {@code AbstractDomainEvent}
     * @return a conatainer with the version of the event
     * @throws IllegalArgumentException if and only if the given structure doesn't conatain a {@code event-version}.
     */
    public IntegerContainer eventVersion(final JsonStructure json) {
        return numberForPointer(json, "/event-descriptor/event-version").asInteger();
    }
    
    public boolean isEventWithName(final JsonStructure json, final String name) {
        return name.equals(eventName(json).get());
    }

    /**
     * Returns the occured at datetime of the {@code AbstractDomainEvent}.
     * @param json JSON structure of a serialized {@code AbstractDomainEvent}
     * @return a conatainer with the occured at datetime of the event
     * @throws IllegalArgumentException if and only if the given structure doesn't conatain a {@code occured-on}
     *         value of format {@link java.time.format.DateTimeFormatter#ISO_OFFSET_DATE_TIME}.
     */
    public OffsetDateTimeContainer occuredOn(final JsonStructure json) {
        final var creationDate = stringForPointer(json, "/event-descriptor/occured-on").get();
        final var datetime = datetimeAdapter().offsetDateTimeContainerFrom(creationDate).get();
        return new OffsetDateTimeContainer(datetime);
    }

    DatetimeAdapter datetimeAdapter() {
        return this.datetimeAdapter;
    }
}
