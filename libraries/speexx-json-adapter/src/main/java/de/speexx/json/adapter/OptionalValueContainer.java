package de.speexx.json.adapter;

import de.speexx.common.annotation.Generated;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonStructure;
import jakarta.json.JsonValue;
import static jakarta.json.JsonValue.ValueType.ARRAY;
import static jakarta.json.JsonValue.ValueType.OBJECT;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Optional;
import static java.util.Optional.ofNullable;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * My implementors gurantee that {@link #get()} never returns {@code null} so my users can implement a fluent call.
 * I also allow sending data to consumer via my {@link #send()} method by creating fluent API use.
 * 
 * <p><strong>Example</strong></p>
 * <pre>
 * final var pointerSupport = new JsonPointerSupport();
 * final var consumer = new SimpleConsumer();
 * 
 * pointerSupport.optionalStringForPointer(jsonStructure, "/data").send().ifPresent(consumer::setData);
 * </pre>
 * 
 * This part of my behavior allows very compact adapter code from JSON structures.
 *
 * @author sascha.kohlmann
 * @param <T> the type
 * @see JsonPointerSupport
 */
@API(status=STABLE)
public interface OptionalValueContainer<T> extends ValueContainer<Optional<T>> {

    Optional<T> send();

    /**
     * @author sascha.kohlmann
     * @param <T>
     */
    @API(status=STABLE)
    public static abstract class BaseOptionalValueContainer<T> implements OptionalValueContainer<T> {

        private final Optional<T> value;

        public BaseOptionalValueContainer(final T value) {
            super();
            this.value = ofNullable(value);
        }

        @Override
        public Optional<T> send() {
            return get();
        }

        @Override
        public Optional<T> get() {
            return this.value;
        }

        @Override
        @Generated
        public String toString() {
            return getClass().getSimpleName() + "{value=" + value + '}';
        }
    }

    @API(status=STABLE)
    public static final class OptionalStringContainer extends BaseOptionalValueContainer<String> {
        public OptionalStringContainer(final String value) { super(value); }
    }

    @API(status=STABLE)
    public static final class OptionalStructureContainer extends BaseOptionalValueContainer<JsonStructure> {
        public OptionalStructureContainer(final JsonStructure value) { super(value); }

        public boolean isJsonArray() {
            if (get().isPresent()) {
                final var jsonValue = get().get();
                return jsonValue.getValueType() == ARRAY;
            }
            return false;
        }

        public boolean isJsonObject() {
            if (get().isPresent()) {
                final var jsonValue = get().get();
                return jsonValue.getValueType() == OBJECT;
            }
            return false;
        }
        
        /**
         * @return never {@code null}
         * @throws IllegalStateException if and only if the given structure is not of type {@link JsonArray}
         */
        public JsonArray asJsonArray() {
            if (isJsonArray()) {
                return get().get().asJsonArray();
            }
            throw new IllegalStateException("Not of JsonArray: " + this);
        }

        /**
         * @return never {@code null}
         * @throws IllegalStateException if and only if the given structure is not of type {@link JsonArray}
         */
        public ArrayContainer asArrayContainer() {
            return new ArrayContainer(asJsonArray());
        }

        /**
         * @return never {@code null}
         * @throws IllegalStateException if and only if the given structure is not of type {@link JsonObject}
         */
        public JsonObject asJsonObject() {
            if (isJsonObject()) {
                return get().get().asJsonObject();
            }
            throw new IllegalStateException("Not of JsonObject: " + this);
        }
    }
    
    @API(status=STABLE)
    public static final class OptionalJsonValueContainer extends BaseOptionalValueContainer<JsonValue> {
        public OptionalJsonValueContainer(final JsonValue value) { super(value); }

        public boolean isJsonStructure() {
            return isJsonArray() || isJsonObject();
        }

        private boolean isJsonArray() {
            if (get().isPresent()) {
                final var jsonValue = get().get();
                return jsonValue.getValueType() == ARRAY;
            }
            return false;
        }

        private boolean isJsonObject() {
            if (get().isPresent()) {
                final var jsonValue = get().get();
                return jsonValue.getValueType() == OBJECT;
            }
            return false;
        }

        /**
         * @return never {@code null}
         * @throws IllegalStateException if and only if the given structure is not of type {@link JsonStructure}
         */
        public OptionalStructureContainer asJsonStructure() {
            if (isJsonArray()) {
                return new OptionalStructureContainer(get().get().asJsonArray());
            } else if (isJsonObject()) {
                return new OptionalStructureContainer(get().get().asJsonObject());
            }

            throw new IllegalStateException("Not of JsonStructure: " + this);
        }
    }
    
    @API(status=STABLE)
    public static final class OptionalLocalDateContainer extends BaseOptionalValueContainer<LocalDate> {
        public OptionalLocalDateContainer(final LocalDate value) { super(value); }
    }

    @API(status=STABLE)
    public static final class OptionalOffsetDateTimeContainer extends BaseOptionalValueContainer<OffsetDateTime> {
        public OptionalOffsetDateTimeContainer(final OffsetDateTime value) { super(value); }
    }

    @API(status=STABLE)
    public static final class OptionalBigDecimalContainer extends BaseOptionalValueContainer<BigDecimal> {
        public OptionalBigDecimalContainer(BigDecimal value) { super(value); }
    }

    @API(status=STABLE)
    public static final class OptionalBigIntegerContainer extends BaseOptionalValueContainer<BigInteger> {
        public OptionalBigIntegerContainer(BigInteger value) { super(value); }
    }

    @API(status=STABLE)
    public static final class OptionalIntegerContainer extends BaseOptionalValueContainer<Integer> {
        public OptionalIntegerContainer(Integer value) { super(value); }
    }

    @API(status=STABLE)
    public static final class OptionalUriContainer extends BaseOptionalValueContainer<URI> {
        public OptionalUriContainer(String value) { this((value != null && !value.isBlank()) ? URI.create(value) : null); }
        public OptionalUriContainer(URI value) { super(value); }
    }

    @API(status=STABLE)
    public static final class OptionalNumberContainer extends BaseOptionalValueContainer<Number> {

        public OptionalNumberContainer(final Number number) {
            super(number);
        }

        /**
         * I converte all Java 15 {@link Number} implementations into a {@code BigDecimal} container without
         * loosing precision. For all not Java 15 implemented {@code Number} types I may lose precision.
         * @return a container
         */
        public OptionalBigDecimalContainer asBigDecimal() {
            if (get().isEmpty()) {
                return new OptionalBigDecimalContainer(null);
            }
            final Number number = get().get();
            if (number instanceof BigDecimal) {
                return new OptionalBigDecimalContainer((BigDecimal) number);
            } else if (number instanceof BigInteger) {
                return new OptionalBigDecimalContainer(new BigDecimal((BigInteger) number));
            }
            return new OptionalBigDecimalContainer(new BigDecimal(number.doubleValue()));
        }

        /**
         * I convert all Java 15 {@link Number} implementations into a {@code BigInteger} container without
         * loosing precision. For all not Java 15 implemented {@code Number} types I may lose precision.
         * @return a container
         */
        public OptionalBigIntegerContainer asBigInteger() {
            if (get().isEmpty()) {
                return new OptionalBigIntegerContainer(null);
            }
            final Number number = get().get();
            if (number instanceof BigDecimal) {
                return new OptionalBigIntegerContainer(((BigDecimal) number).toBigInteger());
            } else if (number instanceof BigInteger) {
                return new OptionalBigIntegerContainer((BigInteger) number);
            }
            return new OptionalBigIntegerContainer(BigInteger.valueOf(number.longValue()));
        }

        /**
         * I converte all Java 15 {@link Number} implementations into a {@code Integer} container. I may lose precision.
         * @return a container
         */
        public OptionalIntegerContainer asInteger() {
            final var optionalBigInteger = asBigInteger().get();
            if (optionalBigInteger.isPresent()) {
                return new OptionalIntegerContainer(optionalBigInteger.get().intValue());
            }
            return new OptionalIntegerContainer(null);
        }
    }
}
