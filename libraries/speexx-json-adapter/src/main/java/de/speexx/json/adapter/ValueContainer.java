package de.speexx.json.adapter;

import static de.speexx.common.Assertions.assertNotNull;
import de.speexx.common.annotation.Generated;
import jakarta.json.JsonArray;
import jakarta.json.JsonNumber;
import jakarta.json.JsonObject;
import jakarta.json.JsonString;
import jakarta.json.JsonValue;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.time.OffsetDateTime;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * My implementors gurantee that {@link #get()} never returns {@code null} so my users can implement a fluent call.
 * I also allow sending data to consumer via my {@link #send(Consumer)} method.
 * 
 * <p><strong>Example</strong></p>
 * <pre>
 * final var pointerSupport = new JsonPointerSupport();
 * final var consumer = new SimpleConsumer();
 * 
 * pointerSupport.stringForPointer(jsonStructure, "/data").send(consumer::setData);
 * </pre>
 * 
 * This part of my behavior allows very compact adapter code from JSON structures.
 *
 * @author sascha.kohlmann
 * @param <T> the type
 * @see JsonPointerSupport
 */
@API(status=STABLE)
public interface ValueContainer<T> extends Supplier<T> {
    
    /**
     * I send my data to the given consumer.
     * @param consumer the consumer to recieve my data */
    public default void send(final Consumer<T> consumer) {
        consumer.accept(get());
    }

    /**
     * Base conatiner for types.
     * @param <T> my type
     */
    @API(status=STABLE)
    public static abstract class BaseValueContainer<T> implements ValueContainer<T> {

        private final T value;

        /**
         * I create a new instance of the conatiner.
         * @param value must not be {@code null}
         * @throws IllegalArgumentException if and only if the given <em>value</em> is {@code null}
         */
        public BaseValueContainer(final T value) {
            super();
            this.value = assertNotNull(value, "Value for container type " + simpleClassName() + " must be provided");
        }

        @Override
        public T get() {
            return this.value;
        }

        private String simpleClassName() {
            return getClass().getSimpleName();
        }

        @Override
        @Generated
        public String toString() {
            return simpleClassName() + "{" + "value=" + value + '}';
        }
    }

    /**
     * I create a new simple value container.
     * @param <T> my type
     * @param value must not be {@code null}
     * @return the container. Never {@code null}
     * @throws IllegalArgumentException if and only if the given <em>value</em> is {@code null}
     */
    public static <T> ValueContainer<T> of(final T value) {
        return new BaseValueContainer<>(value) {};
    }

    /** I am a container for a {@link java.math.BigDecimal} value. */
    @API(status=STABLE)
    public static final class BigDecimalContainer extends BaseValueContainer<BigDecimal> {

        /** @throws IllegalArgumentException if and only if the given <em>value</em> is {@code null} */
        public BigDecimalContainer(final BigDecimal value) {
            super(value);
        }
    }

    /** I am a container for a {@link java.math.BigInteger} value. */
    @API(status=STABLE)
    public static final class BigIntegerContainer extends BaseValueContainer<BigInteger> {

        /** @throws IllegalArgumentException if and only if the given <em>value</em> is {@code null} */
        public BigIntegerContainer(final BigInteger value) {
            super(value);
        }
    }

    /** I am a container for a {@link java.lang.Integer} value. */
    @API(status=STABLE)
    public static final class IntegerContainer extends BaseValueContainer<Integer> {

        /** @throws IllegalArgumentException if and only if the given <em>value</em> is {@code null} */
        public IntegerContainer(final Integer value) {
            super(value);
        }
    }
    
    /** I am a container for a {@link java.lang.Number} value. */
    @API(status=STABLE)
    public final static class NumberContainer extends BaseValueContainer<Number> {

        /** @throws IllegalArgumentException if and only if the given <em>number</em> is {@code null} */
        public NumberContainer(final Number number) {
            super(number);
        }

        /**
         * I converte all Java 15s {@link Number} implementations into a {@code BigDecimal} container without
         * loosing precision. For all not Java 15 implemented {@code Number} types I may lose precision.
         * @return a container. Never {@code null}
         */
        public BigDecimalContainer asBigDecimal() {
            final Number number = get();
            if (number instanceof BigDecimal) {
                return new BigDecimalContainer((BigDecimal) number);
            } else if (number instanceof BigInteger) {
                return new BigDecimalContainer(new BigDecimal((BigInteger) number));
            }
            return new BigDecimalContainer(new BigDecimal(number.doubleValue()));
        }

        /**
         * I convert all Java 15 {@link Number} implementations into a {@code BigInteger} container without
         * loosing precision. For all not Java 15 implemented {@code Number} types I may lose precision.
         * @return a container. Never {@code null}
         */
        public BigIntegerContainer asBigInteger() {
            final Number number = get();
            if (number instanceof BigDecimal) {
                return new BigIntegerContainer(((BigDecimal) number).toBigInteger());
            } else if (number instanceof BigInteger) {
                return new BigIntegerContainer((BigInteger) number);
            }
            return new BigIntegerContainer(BigInteger.valueOf(number.longValue()));
        }

        /**
         * I converte all Java 15 {@link Number} implementations into a {@code Integer} container. I may lose precision.
         * @return a container. Never {@code null}
         */
        public IntegerContainer asInteger() {
            final var bigInteger = asBigInteger();
            return new IntegerContainer(bigInteger.get().intValue());
        }
    }

    /** I am a container for a {@link java.lang.String} value. */
    @API(status=STABLE)
    public static final class StringContainer extends BaseValueContainer<String> {
        /** @throws IllegalArgumentException if and only if the given <em>string</em> is {@code null} */
        public StringContainer(final String string) { super(string); }
    }

    /** I am a container for a {@link java.time.OffsetDateTime} value. */
    @API(status=STABLE)
    public static final class OffsetDateTimeContainer extends BaseValueContainer<OffsetDateTime> {
        /** @throws IllegalArgumentException if and only if the given <em>datetime</em> is {@code null} */
        public OffsetDateTimeContainer(final OffsetDateTime datetime) { super(datetime); }
    }

    /** I am a container for a {@link java.net.URI} value. */
    @API(status=STABLE)
    public static final class UriContainer extends BaseValueContainer<URI> {
        /** @throws IllegalArgumentException if and only if the given <em>uri</em> is {@code null} */
        public UriContainer(final URI uri) { super(uri); }
    }

    /** I am a container for a {@link jakarta.json.JsonArray} value.
     * I provide helper methods to deliver Json typical objects in a compact way.
     * @see #stream() */
    @API(status=STABLE)
    public static final class ArrayContainer extends BaseValueContainer<JsonArray> {
        
        /** I am a helper function to map {@code JsonValue} to {@code BigDecimal}. 
         * @see ArrayContainer#asBigDecimalStream() */
        @API(status=EXPERIMENTAL) public static final Function<JsonValue, BigDecimal> ToBigDecimal = value -> ((JsonNumber) value).bigDecimalValue();
        /** I am a helper function to map {@code JsonValue} to {@code String}. */
        @API(status=EXPERIMENTAL) public static final Function<JsonValue, String> ToString = value -> ((JsonString) value).getString();
        /** I am a helper function to map {@code JsonValue} to {@code JsonObject}. */
        @API(status=EXPERIMENTAL) public static final Function<JsonValue, JsonObject> ToJsonObject = value -> value.asJsonObject();
        
        /** I create a container for a {@code JsonArray} instance.
         * @param array the array. Must not be {@code null}.
         * @throws IllegalArgumentException if and only if the given <em>array</em> is {@code null} */
        @API(status=STABLE)
        public ArrayContainer(final JsonArray array) { super(array); }
        
        /** My implementation provides a shortcut for {@code ArrayContainer.get().stream()…} 
         * @return a stream of {@code JsonValue} instances
         * @see #asStringStream()
         * @see #asJsonObjectStream()
         * @see #asBigDecimalStream() */
        @API(status=EXPERIMENTAL) public Stream<JsonValue> stream() { return get().stream(); }

        /** My implementation provides a shortcut for {@code ArrayContainer.stream().map(ToString::apply)…}
         * @return a stream of strings
         * @see ToString 
         * @see #stream() */
        @API(status=EXPERIMENTAL) public Stream<String> asStringStream() { return get().stream().map(ToString::apply); }

        /** My implementation provides a shortcut for {@code ArrayContainer.stream().map(ToBigDecimal::apply)…}
         * @return a stream of {@code BigDecimal}
         * @see ToBigDecimal 
         * @see #stream() */
        @API(status=EXPERIMENTAL) public Stream<BigDecimal> asBigDecimalStream() { return get().stream().map(ToBigDecimal::apply); }

        /** My implementation provides a shortcut for {@code ArrayContainer.stream().map(ToJsonObject::apply)…}
         * @return a stream of {@code JsonObject}
         * @see ToJsonObject 
         * @see #stream() */
        @API(status=EXPERIMENTAL) public Stream<JsonObject> asJsonObjectStream() { return get().stream().map(ToJsonObject::apply); }
    }
}
