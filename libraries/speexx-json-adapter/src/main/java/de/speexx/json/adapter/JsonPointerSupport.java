package de.speexx.json.adapter;

import de.speexx.json.adapter.OptionalValueContainer.OptionalJsonValueContainer;
import de.speexx.json.adapter.OptionalValueContainer.OptionalLocalDateContainer;
import de.speexx.json.adapter.OptionalValueContainer.OptionalNumberContainer;
import de.speexx.json.adapter.OptionalValueContainer.OptionalOffsetDateTimeContainer;
import de.speexx.json.adapter.OptionalValueContainer.OptionalStringContainer;
import de.speexx.json.adapter.OptionalValueContainer.OptionalStructureContainer;
import de.speexx.json.adapter.OptionalValueContainer.OptionalUriContainer;
import de.speexx.json.adapter.ValueContainer.ArrayContainer;
import de.speexx.json.adapter.ValueContainer.NumberContainer;
import de.speexx.json.adapter.ValueContainer.StringContainer;
import de.speexx.json.adapter.helper.DatetimeAdapter;
import jakarta.json.Json;
import jakarta.json.JsonException;
import jakarta.json.JsonPointer;
import jakarta.json.JsonStructure;
import jakarta.json.JsonValue;
import static jakarta.json.JsonValue.EMPTY_JSON_ARRAY;
import static jakarta.json.JsonValue.NULL;
import static jakarta.json.JsonValue.ValueType.ARRAY;
import static jakarta.json.JsonValue.ValueType.OBJECT;
import static java.time.LocalDate.parse;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * Support for working with JSON documents and JSON pointer as defined in
 * <a href='https://tools.ietf.org/html/rfc6901'>RFC&nbsp;6901</a>.
 * <p>See usage examples in {@link ValueContainer} and {@link OptionalValueContainer}.</p>
 * @author sascha.kohlmann
 */
@API(status=STABLE)
public class JsonPointerSupport {
    
    private final JsonAdapter adapter = new JsonAdapter();

    /**
     * Lookup for a value of a JSON pointer. In case it is a string, number or a boolean JSON value a 
     * container with the string representation of the data is returned.
     * @param json the JSON structure
     * @param jsonPointer the JSON pointer
     * @return a container with the string representation. Never {@code null}
     * @throws IllegalArgumentException if the data can't be converted to string or the data is not available.
     * @see #optionalStringForPointer(jakarta.json.JsonStructure, java.lang.String)
     * @see ValueContainer
     */
    public final StringContainer stringForPointer(final JsonStructure json, final String jsonPointer) throws IllegalArgumentException {
        return new StringContainer(this.adapter.stringFrom(valueForPointer(json, jsonPointer)));
    }

    /**
     * Lookup for a value of a JSON pointer. In case it is a string, number or a boolean JSON value a 
     * container with an optional string representation of the data is returned.
     * @param json the JSON structure
     * @param jsonPointer the JSON pointer
     * @return a container with an optional string representation. Never {@code null}
     * @throws IllegalArgumentException if the data can't be converted to string
     * @see OptionalValueContainer
     */
    public final OptionalStringContainer optionalStringForPointer(final JsonStructure json, final String jsonPointer) {
        return new OptionalStringContainer(this.adapter.stringFrom(valueForPointer(json, jsonPointer)));
    }

    /**
     * Lookup for an URI value of a JSON pointer. If a string is available for the pointer it is converted to a
     * {@link java.net.URI}. Otherwise the container optional {@link java.util.Optional#isEmpty() is empty}.
     * Empty strinngs or {@link java.lang.String#isBlank() blank} strings are handled like {@code null} an the
     * optional is empty.
     * @param json the JSON structure
     * @param jsonPointer the JSON pointer
     * @return a container with an optional {@link java.net.URI} representation. Never {@code null}
     * @throws IllegalArgumentException If the given string violates RFC&nbsp;2396
     * @see OptionalValueContainer
     */
    public final OptionalUriContainer optionalUriForPointer(final JsonStructure json, final String jsonPointer) {
        return new OptionalUriContainer(this.adapter.stringFrom(valueForPointer(json, jsonPointer)));
    }

    /**
     * Lookup for a local date value of a JSON pointer. If no date is available the {@link java.util.Optional#isEmpty() is empty}.
     * The implementation support the ISO 8601 long date format (e.g. {@code 2020-10-08}.
     * Also ISO datetimes in long format are supported. If the datetime delimiter is not {@code T (U+0054)} but 
     * {@code U+0020 (SPACE)} the behavior is also supported.
     * @param json the JSON structure
     * @param jsonPointer the JSON pointer
     * @return a container with an optional {@link java.time.LocalDate} representation. Never {@code null}
     * @throws IllegalArgumentException if and only if the value is not convertable into a local date.
     * @see java.time.format.DateTimeFormatter#ISO_LOCAL_DATE
     * @see OptionalValueContainer
     */
    public final OptionalLocalDateContainer optionalLocalIsoDateForPointer(final JsonStructure json, final String jsonPointer) {
        final var string = this.adapter.stringFrom(valueForPointer(json, jsonPointer));
        if (string != null && !string.isEmpty()) {
            try {
                return new OptionalLocalDateContainer(parse(cleanedIsoDate(string), DateTimeFormatter.ISO_LOCAL_DATE));
            } catch (final DateTimeParseException ex) {
                throw new IllegalArgumentException("No ISO LocalDate format: " + string);
            }
        }
        return new OptionalLocalDateContainer(null);
    }

    /**
     * Lookup for a offset local datetime value of a JSON pointer. If no date is available the
     * {@link java.util.Optional#isEmpty() is empty}.
     * The implementation support the ISO 8601 long date format (e.g. {@code 2020-08-27T08:02:15.563804Z}.
     * @param json the JSON structure
     * @param jsonPointer the JSON pointer
     * @return a container with an optional {@link java.time.LocalDate} representation. Never {@code null}
     * @throws IllegalArgumentException if and only if the value is not convertable into an offset datetime.
     * @see java.time.format.DateTimeFormatter#ISO_OFFSET_DATE_TIME
     * @see OptionalValueContainer
     */
    public final OptionalOffsetDateTimeContainer optionalOffsetDateTimeForPointer(final JsonStructure json, final String jsonPointer) {
        final var string = this.adapter.stringFrom(valueForPointer(json, jsonPointer));
        if (string != null && !string.isEmpty()) {
            return new DatetimeAdapter().optionalOffsetDateTimeContainerFrom(string);
        }
        return new OptionalOffsetDateTimeContainer(null);
    }
    
    /**
     * Lookup for a JSON structur value of a JSON pointer. If no data is available the
     * {@link java.util.Optional#isEmpty() is empty}.
     * @param json the JSON structure
     * @param jsonPointer the JSON pointer
     * @return a container with an optional {@link jakarta.json.JsonStructure} representation. Never {@code null}
     * @throws IllegalArgumentException if and only if the value is not convertable into a JSON structure.
     * @see OptionalValueContainer
     * @see #arrayForPointer(jakarta.json.JsonStructure, java.lang.String)
     */
    public final OptionalStructureContainer optionalStructureForPointer(final JsonStructure json, final String jsonPointer) {
        final var value = valueForPointer(json, jsonPointer);
        if (value == NULL) {
            return new OptionalStructureContainer(null);
        }
        if (value.getValueType() == ARRAY || value.getValueType() == OBJECT) {
            return new OptionalStructureContainer((JsonStructure) value);
        }
        throw new IllegalArgumentException("Pointer target is not a JSON structure but " + value.getValueType());
    }

    /**
     * Lookup for a JSON value of a JSON pointer. If no data is available the {@link java.util.Optional#isEmpty() is empty}.
     * @param json the JSON structure
     * @param jsonPointer the JSON pointer
     * @return a container with an optional {@link jakarta.json.JsonValue} representation. Never {@code null}
     * @see OptionalJsonValueContainer
     */
    public final OptionalJsonValueContainer optionalJsonValueForPointer(final JsonStructure json, final String jsonPointer) {
        final var value = valueForPointer(json, jsonPointer);
        if (value == NULL) {
            return new OptionalJsonValueContainer(null);
        }
        return new OptionalJsonValueContainer(value);
    }
    
    /**
     * Lookup for a number value of a JSON pointer.
     * In case the value is a number JSON value a container with the number representation of the data is returned.
     * In case the value is a string JSON value the implementation tries to convert it into a number.
     * @param json the JSON structure
     * @param jsonPointer the JSON pointer
     * @return a container with the number representation. Never {@code null}
     * @throws IllegalArgumentException if the data can't be converted to number or the data is not available.
     * @see #optionalNumberForPointer(jakarta.json.JsonStructure, java.lang.String)
     * @see ValueContainer
     */
    public final NumberContainer numberForPointer(final JsonStructure json, final String jsonPointer) {
        return new NumberContainer(this.adapter.numberFrom(valueForPointer(json, jsonPointer)));
    }

    /**
     * Lookup for an JSON array of a JSON pointer.
     * In case the structure doesn't exist for the given pointer thee array of the container is empty.
     * In case the structuree is not of type array an exception will be thrown
     * @param json the JSON structure
     * @param jsonPointer the JSON pointer
     * @return a container with the array representation. Never {@code null}
     * @throws IllegalArgumentException if the pointer target is not empty or not of type array.
     * @see #optionalStructureForPointer(jakarta.json.JsonStructure, java.lang.String)
     * @see ValueContainer
     */
    public final ArrayContainer arrayForPointer(final JsonStructure json, final String jsonPointer) {
        final var optionalStructure = optionalStructureForPointer(json, jsonPointer);
        if (optionalStructure.get().isPresent()) {
            final var structure = optionalStructure.get().get();
            if (structure.getValueType() == ARRAY) {
                return new ArrayContainer(structure.asJsonArray());
            }
            
            throw new IllegalArgumentException("Structure for pointer not of type Array");
        }
        
        return new ArrayContainer(EMPTY_JSON_ARRAY);
    }
    
    /**
     * Lookup for a number value of a JSON pointer.
     * In case the value is a number JSON value a container with the number representation of the data is returned.
     * In case the value is a string JSON value the implementation tries to convert it into a number.
     * If the value is value is not available the container {@link java.util.Optional#isEmpty() is empty}.
     * @param json the JSON structure
     * @param jsonPointer the JSON pointer
     * @return a container with the number representation or an empty container. Never {@code null}
     * @throws IllegalArgumentException if the data can't be converted to number
     * @see #optionalNumberForPointer(jakarta.json.JsonStructure, java.lang.String)
     * @see OptionalValueContainer
     */
    public final OptionalNumberContainer optionalNumberForPointer(final JsonStructure json, final String jsonPointer) {
        return new OptionalNumberContainer(this.adapter.numberFrom(valueForPointer(json, jsonPointer)));
    }
    
    static String cleanedIsoDate(final String date) {
        final var stripped = date.strip();
        final var spaceDelimited = stripped.indexOf(" ");
        if (spaceDelimited >= 0) {
            return stripped.substring(0, spaceDelimited);
        }
        final var tDelimited = stripped.indexOf("T");
        if (tDelimited >= 0) {
            return stripped.substring(0, tDelimited);
        }
        return stripped;
    }    

    /**
     * @return Never {@code null} but may be {@code jakarta.json.JsonValue.NULL}
     */
    public final JsonValue valueForPointer(final JsonStructure json, final String jsonPointer) {
        
        try {
            final JsonPointer pointer = Json.createPointer(jsonPointer);
            if (pointer.containsValue(json)) {
                return pointer.getValue(json);
            }
        } catch (final JsonException ex) {
            // Can be ignored
        }
        return NULL;
    }
    
    /**
     * @return Never {@code null} but may be an empty list.
     */
    public final List<JsonValue> listForPointer(final JsonStructure json, final String jsonPointer) {
        try {
            final JsonPointer pointer = Json.createPointer(jsonPointer);
            if (pointer.containsValue(json)) {
                return this.adapter.listFrom(json.getValue(jsonPointer));
            }
        } catch (final JsonException ex) {
            // Can be ignored
        }
        return List.of();
    }
}
