package de.speexx.json.adapter;

import static de.speexx.common.Assertions.assertNotNull;
import de.speexx.common.annotation.Nullable;
import de.speexx.common.annotation.design.gof.Adapter;
import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonNumber;
import jakarta.json.JsonString;
import jakarta.json.JsonStructure;
import jakarta.json.JsonValue;
import static jakarta.json.JsonValue.ValueType.ARRAY;
import static jakarta.json.JsonValue.ValueType.FALSE;
import static jakarta.json.JsonValue.ValueType.NULL;
import static jakarta.json.JsonValue.ValueType.NUMBER;
import static jakarta.json.JsonValue.ValueType.STRING;
import static jakarta.json.JsonValue.ValueType.TRUE;
import jakarta.json.stream.JsonParsingException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * Adapter with support for hanndling JSON-P values.
 * @author sascha.kohlmann 
 */
@Adapter
@API(status=STABLE)
public final class JsonAdapter {

    /**
     * Helper to transforms a JSON string into a {@code JsonStructure}.
     * @param jsonData must not be {@code null}
     * @return a structure
     * @throws IllegalArgumentException if and only if creating of a structure is not possible or if <em>jsonData</em> is {@code null}
     */
    public final JsonStructure jsonFrom(final String jsonData) {
        final StringReader sr = new StringReader(assertNotNull(jsonData, "JSON Data must be provided"));
        return jsonFrom(sr);
    }

    /**
     * Helper to transforms a {@code Readeer} with JSON content into a {@code JsonStructure}.
     * @param reader must not be {@code null}
     * @return a structure
     * @throws IllegalArgumentException if and only if creating of a structure is not possible or if <em>reader</em> is {@code null}
     */
    public final JsonStructure jsonFrom(final Reader reader) {
        try {
            return Json.createReader(assertNotNull(reader, "Reader must be provided")).read();
        } catch (final JsonParsingException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    /**
     * Converts a JSON value into a string.
     * @param value a JSON value
     * @return a string or {@code null} if and only if <em>value</em> is a {@code null} reference
     *         or of type {@code jakarta.json.JsonValue.ValueType.NULL}.
     * @throws IllegalArgumentException if and only if the value can't be converted into a string
     */
    public final @Nullable String stringFrom(final JsonValue value) {
        
        if (value == null || value.getValueType() == NULL) {
            return null;
        }
        
        if (null != value.getValueType()) switch (value.getValueType()) {
            case STRING:
                return JsonAdapter.this.stringFrom((JsonString) value);
            case NUMBER:
                return stringFrom((JsonNumber) value);
            case TRUE:
            case FALSE:
                return booleanFrom(value).toString();
            default:
                break;
        }

        throw new IllegalArgumentException("Unable to convert to String");
    }
    
    /**
     * Converts a JSON value into a boolean.
     * @param value a JSON value
     * @return a Boolean or {@code null} if and only if <em>value</em> is a {@code null} reference
     *         or of type {@code jakarta.json.JsonValue.ValueType.NULL} or a
     *         {@code JsonString} contains a {@link java.lang.String#isBlank() blank} value.
     * @throws IllegalArgumentException if and only if the value can't be converted into a boolean
     */
    public final @Nullable Boolean booleanFrom(final JsonValue value) {
        if (value == null || value.getValueType() == NULL) {
            return null;
        }

        if (value.getValueType() == FALSE) {
            return Boolean.FALSE;
        }

        if (value.getValueType() == TRUE) {
            return Boolean.TRUE;
        }

        try {
            final var string = ((JsonString) value).getString();
            if (string == null || string.isBlank()) {
                return null;
            }
            if ("true".equalsIgnoreCase(string)) {
                return Boolean.TRUE;
            }
            if ("false".equalsIgnoreCase(string)) {
                return Boolean.FALSE;
            }
            throw new IllegalArgumentException("Unable to convert " + value.getValueType() + " to boolean");

        } catch (final Exception e) {
            throw new IllegalArgumentException("Unable to convert " + value.getValueType() + " to boolean", e);
        }
    }

    /**
     * Converts a JSON value into a number.
     * @param value a JSON value
     * @return a {@link java.math.BigDecimal} or {@code null} if and only if <em>value</em> is a {@code null} reference
     *         or of type {@code jakarta.json.JsonValue.ValueType.NULL} or a
     *         {@code JsonString} contains a {@link java.lang.String#isBlank() blank} value.
     * @throws IllegalArgumentException if and only if the value can't be converted into a number
     */
    public final @Nullable BigDecimal numberFrom(final JsonValue value) {
        if (value == null || value.getValueType() == NULL) {
            return null;
        }

        if (value.getValueType() == NUMBER) {
            return ((JsonNumber) value).bigDecimalValue();
        }

        try {
            final var string = ((JsonString) value).getString();
            if (string == null || string.isBlank()) {
                return null;
            }
            return new BigDecimal(string);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Unable to convert " + value.getValueType() + " to Number");
        }
    }

    /**
     * Converts a JSON String into a string.
     * @param jsonString must not be {@code null}
     * @return a string
     * @throws NullPointerException if and only if <em>jsonString</em> is {@code null}.
     */
    public final String stringFrom(final JsonString jsonString) {
        return jsonString.getString();
    }

    /**
     * Converts JSON Number into a string.
     * @param number must not be {@code null}
     * @return a string
     * @throws NullPointerException if and only if <em>number</em> is {@code null}.
     */
    public final String stringFrom(final JsonNumber number) {
        return number.bigDecimalValue().toPlainString();
    }
    
    /**
     * Converts JSON value into a list of JSON values.
     * If <em>value</em> is of {@code null} or {@code jakarta.json.JsonValue.ValueType.NULL}, the returning list is empty.
     * If <em>value</em> is not of {@code jakarta.json.JsonValue.ValueType.ARRAY} and not of {@code jakarta.json.JsonValue.ValueType.NULL}
     * the given parameter will be the only element inside the list.
     * @param value the value
     * @return a list of JSON Values. Never {@code null}
     */
    public final List<JsonValue> listFrom(final JsonValue value) {
        if (value == null || value.getValueType() == NULL) {
            return List.of();
        }
        if (value.getValueType() == ARRAY) {
            final JsonArray array = (JsonArray) value;
            return array.getValuesAs(JsonValue.class);
        }
        return List.of(value);
    }

    /**
     * Converts JSON value list into a list of strings. The JsonValue must be of type
     * {@code STRING}, {@code NUMBER}, {@code TRUE}, {@code FALSE} or {@code NULL}.
     * If <em>value</em> is of {@code null} the returning list is empty.
     * @param values the values
     * @return a list of strings and optional {@code null} values. Never {@code null}. May be empty.
     * @throws IllegalArgumentException if and only if the value can't be converted into a string
     */
    public final List<String> stringListFrom(final List<JsonValue> values) {
        if (values == null) {
            return List.of();
        }
        return values.stream().map(value -> JsonAdapter.this.stringFrom(value)).collect(Collectors.toList());
    }
}
