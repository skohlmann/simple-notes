package de.speexx.json.adapter.helper;

import de.speexx.common.annotation.design.gof.Adapter;
import de.speexx.json.adapter.OptionalValueContainer.OptionalOffsetDateTimeContainer;
import de.speexx.json.adapter.ValueContainer.OffsetDateTimeContainer;
import java.time.OffsetDateTime;
import static java.time.OffsetDateTime.parse;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 *
 * @author sascha.kohlmann
 */
@Adapter
public final class DatetimeAdapter {

    public OffsetDateTimeContainer offsetDateTimeContainerFrom(final String s) {
        return new OffsetDateTimeContainer(stringToOffsetDateTime(s));
    }

    public OptionalOffsetDateTimeContainer optionalOffsetDateTimeContainerFrom(final String s) {
        return new OptionalOffsetDateTimeContainer(stringToOffsetDateTime(s));
    }

    public static OffsetDateTime stringToOffsetDateTime(final String s) {
        try {
            return parse(s, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        } catch (final DateTimeParseException ex) {
            throw new IllegalArgumentException("Unable to create OffsetDatetime from " +  s, ex);
        }
    }
}
