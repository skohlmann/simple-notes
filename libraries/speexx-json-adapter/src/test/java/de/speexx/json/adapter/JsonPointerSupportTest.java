package de.speexx.json.adapter;

import static com.google.common.truth.Truth.assertThat;
import jakarta.json.Json;
import jakarta.json.JsonString;
import jakarta.json.JsonStructure;
import static jakarta.json.JsonValue.NULL;
import static jakarta.json.JsonValue.ValueType.ARRAY;
import static jakarta.json.JsonValue.ValueType.STRING;
import java.net.URI;
import java.time.LocalDate;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class JsonPointerSupportTest {
    
    @Test
    public void simple_string_for_pointer() {
        // Given
        final var json = "{\"data1\":\"name\",\"data2\":2}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var valueOfPointer = new JsonPointerSupport().stringForPointer(structure, "/data1");
        
        // Then
        assertThat(valueOfPointer.get()).isEqualTo("name");
    }

    @Test
    public void simple_string_for_pointer_with_number() {
        // Given
        final var json = "{\"data1\":\"name\",\"data2\":2}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var valueOfPointer = new JsonPointerSupport().stringForPointer(structure, "/data2");
        
        // Then
        assertThat(valueOfPointer.get()).isEqualTo("2");
    }
    
    @Test
    public void stringForPointer_no_data_for_pointer_available() {
        // Given
        final var json = "{}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // Then
        assertThrows(IllegalArgumentException.class, () -> new JsonPointerSupport().stringForPointer(structure, "/data"));
    }

    @Test
    public void optionalStringForPointer_string_for_pointer() {
        // Given
        final var json = "{\"data1\":\"name\",\"data2\":2}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var valueOfPointer = new JsonPointerSupport().optionalStringForPointer(structure, "/data1");
        
        // Then
        assertThat(valueOfPointer.get().get()).isEqualTo("name");
    }

    @Test
    public void optionalStringForPointer_no_data_for_pointer_available() {
        // Given
        final var json = "{}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var valueOfPointer = new JsonPointerSupport().optionalStringForPointer(structure, "/data1");
        
        // Then
        assertThat(valueOfPointer.get().isEmpty()).isTrue();
    }

    @Test
    public void optionalUriForPointer_no_data_for_pointer_available() {
        // Given
        final var json = "{}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var uriContainer = new JsonPointerSupport().optionalUriForPointer(structure, "/data1");
        
        // Then
        assertThat(uriContainer.get().isEmpty()).isTrue();
    }

    @Test
    public void optionalUriForPointer_uri_for_pointer() {
        // Given
        final var json = "{\"uri\":\"http://example.com/\"}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var uriContainer = new JsonPointerSupport().optionalUriForPointer(structure, "/uri");
        
        // Then
        assertThat(uriContainer.get().get()).isEqualTo(URI.create("http://example.com/"));
    }

    @Test
    public void optionalLocalIsoDateForPointer_localdate_for_pointer_to_correct_ISO_8601_long_date() {
        // Given
        final var json = "{\"date\":\"2020-10-08\"}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var dateContainer = new JsonPointerSupport().optionalLocalIsoDateForPointer(structure, "/date");
        
        // Then
        assertThat(dateContainer.get().get()).isEqualTo(LocalDate.of(2020, Month.OCTOBER, 8));
    }

    @Test
    public void optionalLocalIsoDateForPointer_localdate_for_pointer_to_correct_ISO_8601_long_date_with_leading_whitespace() {
        // Given
        final var json = "{\"date\":\" 2020-10-08\"}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var dateContainer = new JsonPointerSupport().optionalLocalIsoDateForPointer(structure, "/date");
        
        // Then
        assertThat(dateContainer.get().get()).isEqualTo(LocalDate.of(2020, Month.OCTOBER, 8));
    }

    @Test
    public void optionalLocalIsoDateForPointer_localdate_for_pointer_to_correct_ISO_8601_long_datetime() {
        // Given
        final var json = "{\"date\":\"2020-10-08T14:14:00\"}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var dateContainer = new JsonPointerSupport().optionalLocalIsoDateForPointer(structure, "/date");
        
        // Then
        assertThat(dateContainer.get().get()).isEqualTo(LocalDate.of(2020, Month.OCTOBER, 8));
    }

    @Test
    public void optionalLocalIsoDateForPointer_localdate_for_pointer_to_correct_ISO_8601_long_datetime_space_delimited() {
        // Given
        final var json = "{\"date\":\"2020-10-08 14:14:00\"}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var dateContainer = new JsonPointerSupport().optionalLocalIsoDateForPointer(structure, "/date");
        
        // Then
        assertThat(dateContainer.get().get()).isEqualTo(LocalDate.of(2020, Month.OCTOBER, 8));
    }

    @Test
    public void optionalLocalIsoDateForPointer_localdate_for_not_available_pointer() {
        // Given
        final var json = "{}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var dateContainer = new JsonPointerSupport().optionalLocalIsoDateForPointer(structure, "/date");
        
        // Then
        assertThat(dateContainer.get().isEmpty()).isTrue();
    }
    
    @Test
    public void optionalLocalIsoDateForPointer_localdate_is_illegal_value() {
        // Given
        final var json = "{\"date\":\"2022A3328 14:14:00\"}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // Then
        assertThrows(IllegalArgumentException.class, () -> new JsonPointerSupport().optionalLocalIsoDateForPointer(structure, "/date"));
    }
    
    @Test
    public void optionalOffsetDateTimeForPointer_offsetdatetime_for_pointer_to_correct_ISO_8601_long_offset_datetime() {
        // Given
        final var json = "{\"date\":\"2020-08-27T08:02:15.563804Z\"}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var datetimeContainer = new JsonPointerSupport().optionalOffsetDateTimeForPointer(structure, "/date");
        
        // Then
        assertThat(datetimeContainer.get().get()).isEqualTo(OffsetDateTime.of(2020, 8, 27, 8, 2, 15, 563804000, ZoneOffset.UTC));
    }

    @Test
    public void optionalOffsetDateTimeForPointer_offsetdatetime_for_pointer_to_illegal_ISO_8601_long_offset_datetime() {
        // Given
        final var json = "{\"date\":\"ABCD-08-27T08:02:15.563804Z\"}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        assertThrows(IllegalArgumentException.class, () -> new JsonPointerSupport().optionalOffsetDateTimeForPointer(structure, "/date"));
        
    }
    
    @Test
    public void optionalOffsetDateTimeForPointer_offsetdatetime_for_pointer_to_empty_ISO_8601_long_offset_datetime() {
        // Given
        final var json = "{}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var datetimeContainer = new JsonPointerSupport().optionalOffsetDateTimeForPointer(structure, "/date");
        
        // Then
        assertThat(datetimeContainer.get().isEmpty()).isTrue();
    }

    @Test
    public void optionalNumberForPointer_number_for_pointer_value_is_string() {
        // Given
        final var json = "{\"number\":\"2\"}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var numberContainer = new JsonPointerSupport().optionalNumberForPointer(structure, "/number");
        
        // Then
        assertThat(numberContainer.get().get().longValue()).isEqualTo(2);
    }
    
    @Test
    public void optionalNumberForPointer_number_for_pointer_value_is_number() {
        // Given
        final var json = "{\"number\":3}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var numberContainer = new JsonPointerSupport().optionalNumberForPointer(structure, "/number");
        
        // Then
        assertThat(numberContainer.get().get().longValue()).isEqualTo(3);
    }

    @Test
    public void optionalNumberForPointer_number_for_not_available_pointer() {
        // Given
        final var json = "{}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var numberContainer = new JsonPointerSupport().optionalNumberForPointer(structure, "/number");
        
        // Then
        assertThat(numberContainer.get().isEmpty()).isTrue();
    }

    @Test
    public void optionalNumberForPointer_number_for_not_convertable_value() {
        // Given
        final var json = "{\"number\":\"jgsdjg\"}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // Then
        assertThrows(IllegalArgumentException.class, () -> new JsonPointerSupport().optionalNumberForPointer(structure, "/number"));
    }

    @Test
    public void simple_number_for_pointer() {
        // Given
        final var json = "{\"data1\":\"name\",\"data2\":2}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var valueOfPointer = new JsonPointerSupport().numberForPointer(structure, "/data2");
        
        // Then
        assertThat(valueOfPointer.get().longValue()).isEqualTo(2);
    }

    @Test
    public void simple_number_for_pointer_with_number() {
        // Given
        final var json = "{\"data1\":\"name\",\"data2\":2}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var valueOfPointer = new JsonPointerSupport().numberForPointer(structure, "/data2");
        
        // Then
        assertThat(valueOfPointer.get().intValue()).isEqualTo(2);
    }
    
    @Test
    public void numberForPointer_no_data_for_pointer_available() {
        // Given
        final var json = "{}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // Then
        assertThrows(IllegalArgumentException.class, () -> new JsonPointerSupport().numberForPointer(structure, "/data"));
    }

    @Test
    public void valueForPointer() {
        // Given
        final var json = "{\"data1\":\"name\",\"data2\":2}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var valueOfPointer = new JsonPointerSupport().valueForPointer(structure, "/data1");
        
        // Then
        assertThat(valueOfPointer.getValueType()).isEqualTo(STRING);
    }

    @Test
    public void valueForPointer_not_available() {
        // Given
        final var json = "{}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var valueOfPointer = new JsonPointerSupport().valueForPointer(structure, "/data");
        
        // Then
        assertThat(valueOfPointer).isEqualTo(NULL);
    }

    @Test
    public void listForPointer() {
        // Given
        final var json = "{\"list\":[{\"data1\":\"name\"},{\"data2\":2}]}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var list = new JsonPointerSupport().listForPointer(structure, "/list");
        
        // Then
        assertThat(list).hasSize(2);
    }

    @Test
    public void listForPointer_not_available() {
        // Given
        final var json = "{}";
        final var structure = (JsonStructure) new JsonAdapter().jsonFrom(json);
        
        // When
        final var list = new JsonPointerSupport().listForPointer(structure, "/data");
        
        // Then
        assertThat(list).isEmpty();
    }

    @Test
    public void optionalStructureForPointer_with_valid_structure() {
        // Given
        final var json = "{\"array\":[\"value 1\",\"value 2\"]}";
        final var structure = new JsonAdapter().jsonFrom(json);
        
        // When
        final var optionalStruture =  new JsonPointerSupport().optionalStructureForPointer(structure, "/array");
        
        // Then
        assertThat(optionalStruture.get().get().getValueType()).isEqualTo(ARRAY);
    }

    @Test
    public void optionalStructureForPointer_with_null_structure() {
        // Given
        final var json = "{\"array\":[\"value 1\",\"value 2\"]}";
        final var structure = new JsonAdapter().jsonFrom(json);
        
        // When
        final var optionalStruture =  new JsonPointerSupport().optionalStructureForPointer(structure, "/dummy");
        
        // Then
        assertThat(optionalStruture.get().isPresent()).isFalse();
    }

    @Test
    public void optionalStructureForPointer_with_no_structure() {
        // Given
        final var json = "{\"array\":false}";
        final var structure = new JsonAdapter().jsonFrom(json);
        
        // When
        assertThrows(IllegalArgumentException.class, () -> new JsonPointerSupport().optionalStructureForPointer(structure, "/array"));
    }

    @Test
    public void arrayForPointer_with_array() {
        // Given
        final var json = "{\"array\":[\"value 1\",\"value 2\"]}";
        final var structure = new JsonAdapter().jsonFrom(json);
        
        // When
        final var optionalStruture =  new JsonPointerSupport().arrayForPointer(structure, "/array");
        
        // Then
        assertThat(optionalStruture.get()).hasSize(2);
    }

    @Test
    public void arrayForPointer_with_null_array() {
        // Given
        final var json = "{\"array\":[\"value 1\",\"value 2\"]}";
        final var structure = new JsonAdapter().jsonFrom(json);
        
        // When
        final var optionalStruture =  new JsonPointerSupport().arrayForPointer(structure, "/dummy");
        
        // Then
        assertThat(optionalStruture.get()).hasSize(0);
    }

    @Test
    public void arrayForPointer_with_not_array() {
        // Given
        final var json = "{\"array\":{\"objeect\":\"test\"}}";
        final var structure = new JsonAdapter().jsonFrom(json);
        
        // When
        assertThrows(IllegalArgumentException.class, () -> new JsonPointerSupport().arrayForPointer(structure, "/array"));
    }

    @Test
    public void jsonValueForPointer() {
        // Given
        final var json = Json.createObjectBuilder().add("key", "value").build();
        
        // When
        final var optionalValue =  new JsonPointerSupport().optionalJsonValueForPointer(json, "/key");
        
        // Then
        assertThat(((JsonString) optionalValue.get().get()).getString()).isEqualTo("value");
    }
}
