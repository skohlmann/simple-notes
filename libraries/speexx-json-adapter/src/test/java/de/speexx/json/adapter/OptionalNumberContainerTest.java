package de.speexx.json.adapter;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.adapter.OptionalValueContainer.OptionalNumberContainer;
import java.math.BigDecimal;
import static java.math.BigDecimal.ONE;
import java.math.BigInteger;
import static java.math.BigInteger.TEN;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class OptionalNumberContainerTest {

    @Test
    public void optionalNumberContainer_with_BigDecimal_as_BigDecimalContainer() {
        assertThat(new OptionalNumberContainer(ONE).asBigDecimal().get().get()).isEqualTo(ONE);
    }

    @Test
    public void optionalNumberContainer_with_BigInteger_as_BigDecimalContainer() {
        assertThat(new OptionalNumberContainer(TEN).asBigDecimal().get().get()).isEqualTo(BigDecimal.TEN);
    }

    @Test
    public void optionalNumberContainer_with_number_as_BigDecimalContainer() {
        assertThat(new OptionalNumberContainer((Number) 1).asBigDecimal().get().get()).isEqualTo(BigDecimal.ONE);
    }

    @Test
    public void optionalNumberContainer_as_BigDecimalContainer_with_null_value() {
        assertThat(new OptionalNumberContainer(null).asBigDecimal().get().isEmpty()).isTrue();
    }

    @Test
    public void optionalNumberContainer_with_BigDecimal_as_BigIntegerContainer() {
        assertThat(new OptionalNumberContainer(ONE).asBigInteger().get().get()).isEqualTo(BigInteger.ONE);
    }

    @Test
    public void optionalNumberContainer_with_BigInteger_as_BigIntegerContainer() {
        assertThat(new OptionalNumberContainer(TEN).asBigInteger().get().get()).isEqualTo(BigInteger.TEN);
    }

    @Test
    public void optionalNumberContainer_with_Number_as_BigIntegerContainer() {
        assertThat(new OptionalNumberContainer((Number) 1).asBigInteger().get().get()).isEqualTo(BigInteger.ONE);
    }

    @Test
    public void optionalNumberContainer_as_BigIntegerContainer_with_null_value() {
        assertThat(new OptionalNumberContainer(null).asBigInteger().get().isEmpty()).isTrue();
    }

    @Test
    public void optionalNumberContainer_with_Number_as_IntegerContainer() {
        assertThat(new OptionalNumberContainer((Number) 1).asInteger().get().get()).isEqualTo(1);
    }

    @Test
    public void optionalNumberContainer_as_IntegerContainer_with_null_value() {
        assertThat(new OptionalNumberContainer(null).asInteger().get().isEmpty()).isTrue();
    }
}
