package de.speexx.json.adapter;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.adapter.ValueContainer.NumberContainer;
import java.math.BigDecimal;
import static java.math.BigDecimal.ONE;
import java.math.BigInteger;
import static java.math.BigInteger.TEN;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class NumberContainerTest {
    
    @Test
    public void numberContainer_with_BigDecimal_as_BigDecimalContainer() {
        assertThat(new NumberContainer(ONE).asBigDecimal().get()).isEqualTo(ONE);
    }

    @Test
    public void numberContainer_with_BigInteger_as_BigDecimalContainer() {
        assertThat(new NumberContainer(TEN).asBigDecimal().get()).isEqualTo(BigDecimal.TEN);
    }

    @Test
    public void numberContainer_with_number_as_BigDecimalContainer() {
        assertThat(new NumberContainer((Number) 1).asBigDecimal().get()).isEqualTo(BigDecimal.ONE);
    }

    @Test
    public void numberContainer_with_BigDecimal_as_BigIntegerContainer() {
        assertThat(new NumberContainer(ONE).asBigInteger().get()).isEqualTo(BigInteger.ONE);
    }

    @Test
    public void numberContainer_with_BigInteger_as_BigIntegerContainer() {
        assertThat(new NumberContainer(TEN).asBigInteger().get()).isEqualTo(BigInteger.TEN);
    }

    @Test
    public void numberContainer_with_Number_as_BigIntegerContainer() {
        assertThat(new NumberContainer((Number) 1).asBigInteger().get()).isEqualTo(BigInteger.ONE);
    }

    @Test
    public void numberContainer_with_Number_as_IntegerContainer() {
        assertThat(new NumberContainer((Number) 1).asInteger().get()).isEqualTo(1);
    }
}
