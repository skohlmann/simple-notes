package de.speexx.json.adapter;

import de.speexx.json.adapter.ValueContainer;
import de.speexx.json.adapter.ValueContainer.NumberContainer;
import org.junit.jupiter.api.Test;
import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.adapter.ValueContainer.BaseValueContainer;
import de.speexx.json.adapter.ValueContainer.UriContainer;
import static java.math.BigDecimal.ONE;
import static java.net.URI.create;

/**
 *
 * @author sascha.kohlmann
 */
public class ValueContainerTest {
    
    @Test
    public void numberContainer_int_number_to_BigDecimal() {
        // Given
        final var container = new NumberContainer(1);
        
        // Then
        assertThat(container.asBigDecimal().get()).isEqualTo(ONE);
    }

    @Test
    public void numberContainer_long_number_to_BigDecimal() {
        // Given
        final var container = new NumberContainer(1L);
        
        // Then
        assertThat(container.asBigDecimal().get()).isEqualTo(ONE);
    }
    
    @Test
    public void get_from_BaseValueContainer() {
        // Given
        final var container = ValueContainer.of("test");
        
        // Then
        assertThat(container.get()).isEqualTo("test");
    }
    
    @Test
    public void send_from_BaseValueContainer() {
        // Given
        final var container = new BaseValueContainer<String>("value") {};
        final class ValueConsumer {
            public String value;
            public void setValue(final String newValue) {
                this.value = newValue;
            }
        }
        
        final var valueConsumer = new ValueConsumer();
        
        // When
        container.send(valueConsumer::setValue);
        
        // Then
        assertThat(valueConsumer.value).isEqualTo("value");
    }
    
    @Test
    public void simple_UriContainer() {
        // Given
        final var uriContainer = new UriContainer(create("http://example.com"));
        
        // Then
        assertThat(uriContainer.get()).isEqualTo(create("http://example.com"));
    }
}
