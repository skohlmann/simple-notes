package de.speexx.json.adapter;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.adapter.OptionalValueContainer.BaseOptionalValueContainer;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class OptionalValueContainerTest {
    
    @Test
    public void get_from_BaseOptionalValueContainer() {
        // Given
        final var container = new BaseOptionalValueContainer<String>("test") {};
        
        // Then
        assertThat(container.get().isPresent()).isTrue();
        assertThat(container.get().get()).isEqualTo("test");
    }
    
    @Test
    public void send_from_BaseOptionalValueContainer() {
        // Given
        final var container = new BaseOptionalValueContainer<String>("value") {};
        final var valueConsumer = new ValueConsumer();
        
        // When
        container.send().ifPresent(valueConsumer::setValue);
        
        // Then
        assertThat(valueConsumer.value).isEqualTo("value");
    }

    @Test
    public void get_from_BaseOptionalValueContainer_with_null_value() {
        // Given
        final var container = new BaseOptionalValueContainer<String>(null) {};
        final var valueConsumer = new ValueConsumer();
        
        // When
        container.send().ifPresent(valueConsumer::setValue);

        // Then
        assertThat(container.get().isEmpty()).isTrue();
        assertThat(valueConsumer.value).isNull();
    }

     static final class ValueConsumer {
        public String value = null;
        public void setValue(final String newValue) {
            this.value = newValue;
        }
    }
}
