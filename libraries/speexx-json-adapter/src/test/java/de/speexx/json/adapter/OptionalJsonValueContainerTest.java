package de.speexx.json.adapter;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.adapter.OptionalValueContainer.OptionalJsonValueContainer;
import jakarta.json.Json;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class OptionalJsonValueContainerTest {

    @Test
    public void for_simple_value() {
        // Given
        final var jsonValue = Json.createValue("test");
        
        // When
        final var valueContainer = new OptionalJsonValueContainer(jsonValue);
        
        // Then
        assertThat(valueContainer.isJsonStructure()).isFalse();
        
        // And Then
        assertThrows(IllegalStateException.class, () -> valueContainer.asJsonStructure());
    }

    @Test
    public void for_structured_array_null_value() {
        // Given
        final var valueContainer = new OptionalJsonValueContainer(null);
        
        // Then
        assertThrows(IllegalStateException.class, () -> valueContainer.asJsonStructure());
    }
    
    @Test
    public void for_structured_array_value() {
        // Given
        final var jsonValue = Json.createArrayBuilder().add(1).add(2).build();
        
        // When
        final var valueContainer = new OptionalJsonValueContainer(jsonValue);
        
        // Then
        assertThat(valueContainer.isJsonStructure()).isTrue();
        
        // And Then
        assertThat(valueContainer.asJsonStructure().isJsonArray()).isTrue();
    }

    @Test
    public void for_structured_object_value() {
        // Given
        final var jsonValue = Json.createObjectBuilder().add("key", "value").build();
        
        // When
        final var valueContainer = new OptionalJsonValueContainer(jsonValue);
        
        // Then
        assertThat(valueContainer.isJsonStructure()).isTrue();
        
        // And Then
        assertThat(valueContainer.asJsonStructure().isJsonObject()).isTrue();
    }
}
