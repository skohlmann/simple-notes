package de.speexx.json.adapter;

import de.speexx.json.adapter.JsonAdapter;
import de.speexx.json.adapter.ValueContainer.ArrayContainer;
import static de.speexx.json.adapter.ValueContainer.ArrayContainer.ToString;
import static java.util.stream.Collectors.toList;
import org.junit.jupiter.api.Test;
import static com.google.common.truth.Truth.assertThat;
import java.math.BigDecimal;

/**
 *
 * @author sascha.kohlmann
 */
public class ValueContainer_ArrayContainer_Test {

    @Test
    public void as_stream() {
        // Given
        final var array = new JsonAdapter().jsonFrom(STRING_JSON_ARRAY).asJsonArray();
        
        // When
        final var strings = new ArrayContainer(array).stream().map(ToString::apply).collect(toList());
        
        // Then
        assertThat(strings).containsAnyOf("first", "second");
    }
    
    @Test
    public void as_string_stream() {
        // Given
        final var array = new JsonAdapter().jsonFrom(STRING_JSON_ARRAY).asJsonArray();
        
        // When
        final var strings = new ArrayContainer(array).asStringStream().collect(toList());
        
        // Then
        assertThat(strings).containsAnyOf("first", "second");
    }
    
    @Test
    public void as_bigDecimal_stream() {
        // Given
        final var array = new JsonAdapter().jsonFrom(NUMBER_JSON_ARRAY).asJsonArray();
        
        // When
        final var bigDecimals = new ArrayContainer(array).asBigDecimalStream().collect(toList());
        
        // Then
        assertThat(bigDecimals).containsAnyOf(new BigDecimal("1.1"), new BigDecimal("2.2"));
    }
    
    
    @Test
    public void as_jsonObject_stream() {
        // Given
        final var adapter = new JsonAdapter();
        final var array = adapter.jsonFrom(OBJECT_JSON_ARRAY).asJsonArray();
        
        // When
        final var jsonObjects = new ArrayContainer(array).asJsonObjectStream().collect(toList());
        
        // Then
        assertThat(jsonObjects).containsAnyOf(adapter.jsonFrom("{\"name\":\"first\"}"), adapter.jsonFrom("{\"name\":\"second\"}"));
    }
    

    private static final String STRING_JSON_ARRAY = "[\"first\",\"second\"]";
    private static final String NUMBER_JSON_ARRAY = "[1.1,2.2]";
    private static final String OBJECT_JSON_ARRAY = "[{\"name\":\"first\"},{\"name\":\"second\"}]";
}
