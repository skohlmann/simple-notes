package de.speexx.json.adapter.event;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.adapter.JsonAdapter;
import java.time.OffsetDateTime;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class DomainEventSupportTest {

    @Test
    public void all_values() {
        // Given
        final var currentDatetime = OffsetDateTime.now().format(ISO_OFFSET_DATE_TIME);
        final var json = "{\"event-descriptor\":{\"event-name\":\"name\",\"event-version\":2,\"occured-on\":\"" + currentDatetime + "\"}}";
        final var structure = new JsonAdapter().jsonFrom(json);
        final var eventSupport = new DomainEventSupport();
        
        // Then
        assertThat(eventSupport.eventName(structure).get()).isEqualTo("name");
        assertThat(eventSupport.eventVersion(structure).get()).isEqualTo(2);
        assertThat(eventSupport.occuredOn(structure).get().format(ISO_OFFSET_DATE_TIME)).isEqualTo(currentDatetime);
    }

    @Test
    public void not_occureed_on() {
        // Given
        final var structure = new JsonAdapter().jsonFrom("{}");
        final var eventSupport = new DomainEventSupport();
        
        // Then
        assertThrows(IllegalArgumentException.class, () -> eventSupport.occuredOn(structure));
    }

    @Test
    public void not_event_name() {
        // Given
        final var structure = new JsonAdapter().jsonFrom("{}");
        final var eventSupport = new DomainEventSupport();
        
        // Then
        assertThrows(IllegalArgumentException.class, () -> eventSupport.eventName(structure));
    }

    @Test
    public void not_event_version() {
        // Given
        final var structure = new JsonAdapter().jsonFrom("{}");
        final var eventSupport = new DomainEventSupport();
        
        // Then
        assertThrows(IllegalArgumentException.class, () -> eventSupport.eventVersion(structure));
    }
}
