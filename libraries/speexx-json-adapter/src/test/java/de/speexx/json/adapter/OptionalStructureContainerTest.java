package de.speexx.json.adapter;

import static com.google.common.truth.Truth.assertThat;
import de.speexx.json.adapter.OptionalValueContainer.OptionalStructureContainer;
import jakarta.json.Json;
import java.math.BigDecimal;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class OptionalStructureContainerTest {
    
    @Test
    public void simple_array() {
        // Given
        final var structure = Json.createArrayBuilder(asList(1, 2)).build();
        
        // When
        final var container = new OptionalStructureContainer(structure);
        
        // Then
        assertThat(container.asJsonArray()).hasSize(2);
    }

    @Test
    public void simple_object() {
        // Given
        final var structure = Json.createObjectBuilder().add("key", "value").build();
        
        // When
        final var container = new OptionalStructureContainer(structure);
        
        // Then
        assertThat(container.asJsonObject().getString("key")).isEqualTo("value");
    }

    @Test
    public void as_arrayContainer() {
        // Given
        final var structure = Json.createArrayBuilder(asList(1, 2)).build();
        
        // When
        final var container = new OptionalStructureContainer(structure);
        
        // Then
        assertThat(container.asArrayContainer().stream().collect(toList())).hasSize(2);
    }

    @Test
    public void failing_asJsonObject() {
        // Given
        final var structure = Json.createArrayBuilder(asList(1, 2)).build();
        
        // When
        final var container = new OptionalStructureContainer(structure);
        
        // Then
        assertThrows(IllegalStateException.class, () -> container.asJsonObject());
    }

    @Test
    public void failing_asJsonObject_with_null() {
        // Given
        final var container = new OptionalStructureContainer(null);
        
        // Then
        assertThrows(IllegalStateException.class, () -> container.asJsonObject());
    }
    
    @Test
    public void failing_asJsonArray() {
        // Given
        final var structure = Json.createObjectBuilder().add("key", BigDecimal.ONE).build();
        
        // When
        final var container = new OptionalStructureContainer(structure);
        
        // Then
        assertThrows(IllegalStateException.class, () -> container.asJsonArray());
    }
    
    @Test
    public void failing_asJsonArray_with_null() {
        // Given
        // When
        final var container = new OptionalStructureContainer(null);
        
        // Then
        assertThrows(IllegalStateException.class, () -> container.asJsonArray());
    }
}
