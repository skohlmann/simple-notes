package de.speexx.json.adapter;

import static com.google.common.truth.Truth.assertThat;
import jakarta.json.Json;
import jakarta.json.JsonValue;
import static jakarta.json.JsonValue.FALSE;
import static jakarta.json.JsonValue.NULL;
import static jakarta.json.JsonValue.TRUE;
import java.io.Reader;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class JsonAdapterTest {
 
    @Test
    public void string_to_structure() {
        // Given
        final var json = "{}";
        
        // When
        final var structure = new JsonAdapter().jsonFrom(json);
        
        // Then
        assertThat(structure).isNotNull();
    }

    @Test
    public void illegal_json_content() {
        // Given
        final var json = "{\"}";
        
        // Then
        assertThrows(IllegalArgumentException.class, () -> new JsonAdapter().jsonFrom(json));
    }

    @Test
    public void null_json_string() {
        assertThrows(IllegalArgumentException.class, () -> new JsonAdapter().jsonFrom((String) null));
    }

    @Test
    public void null_json_reader() {
        assertThrows(IllegalArgumentException.class, () -> new JsonAdapter().jsonFrom((Reader) null));
    }
    
    @Test
    public void stringFrom_with_jsonString() {
        // Given
        final var value = Json.createValue("test");
        
        // When
        final var string = new JsonAdapter().stringFrom((JsonValue) value);
        
        // Then
        assertThat(string).isEqualTo("test");
    }
    
    @Test
    public void stringFrom_with_null() {
        // When
        final var string = new JsonAdapter().stringFrom((JsonValue) null);
        
        // Then
        assertThat(string).isNull();
    }

    @Test
    public void stringFrom_with_Number_value() {
        // Given
        final var value = Json.createValue(123);

        // When
        final var string = new JsonAdapter().stringFrom((JsonValue) value);
        
        // Then
        assertThat(string).isEqualTo("123");
    }

    @Test
    public void stringFrom_with_jsonValue_NULL() {
        // When
        final var string = new JsonAdapter().stringFrom(NULL);
        
        // Then
        assertThat(string).isNull();
    }

    @Test
    public void stringFrom_with_jsonValue_TRUE() {
        // When
        final var string = new JsonAdapter().stringFrom(TRUE);
        
        // Then
        assertThat(string).isEqualTo("true");
    }

    @Test
    public void stringFrom_with_jsonValue_FALSE() {
        // When
        final var string = new JsonAdapter().stringFrom(FALSE);
        
        // Then
        assertThat(string).isEqualTo("false");
    }

    @Test
    public void booleanFrom_with_jsonValue_string_true() {
        // Given
        final var value = Json.createValue("true");

        // When
        final var bool = new JsonAdapter().booleanFrom(value);
        
        // Then
        assertThat(bool).isEqualTo(true);
    }

    @Test
    public void booleanFrom_with_jsonValue_string_false() {
        // Given
        final var value = Json.createValue("falSE");

        // When
        final var bool = new JsonAdapter().booleanFrom(value);
        
        // Then
        assertThat(bool).isEqualTo(false);
    }

    @Test
    public void booleanFrom_with_jsonValue_null() {
        // When
        final var bool = new JsonAdapter().booleanFrom(null);
        
        // Then
        assertThat(bool).isNull();
    }

    @Test
    public void booleanFrom_with_jsonValue_string_blank() {
        // Given
        final var blank = Json.createValue("\t");

        // When
        final var bool = new JsonAdapter().booleanFrom(blank);
        
        // Then
        assertThat(bool).isNull();
    }

    @Test
    public void jsonValue_string_is_illegal_boolean_value_to_boolean() {
        // Given
        final var illegal = Json.createValue("23");

        // Then
        assertThrows(IllegalArgumentException.class, () -> new JsonAdapter().booleanFrom(illegal));
    }

    @Test
    public void numberFrom_with_jsonValue_number() {
        // Given
        final var jsonNumber = Json.createValue(1);

        // When
        final var number = new JsonAdapter().numberFrom(jsonNumber);
        
        // Then
        assertThat(number.intValue()).isEqualTo(1);
    }
    
    @Test
    public void numberFrom_with_jsonValue_string() {
        // Given
        final var jsonNumber = Json.createValue("1.23");

        // When
        final var number = new JsonAdapter().numberFrom(jsonNumber);
        
        // Then
        assertThat(number.intValue()).isEqualTo(1);
    }
    
    @Test
    public void numberFrom_with_jsonValue_no_number_string() {
        // Given
        final var jsonNumber = Json.createValue("shudghjgfsd");

        // Then
        assertThrows(IllegalArgumentException.class, () -> new JsonAdapter().numberFrom(jsonNumber));
    }
    
    @Test
    public void numberFrom_with_jsonValue_null() {
        // When
        final var number = new JsonAdapter().numberFrom(null);
        
        // Then
        assertThat(number).isNull();
    }
    
    @Test
    public void numberFrom_with_jsonValue_NULL() {
        // When
        final var number = new JsonAdapter().numberFrom(null);
        
        // Then
        assertThat(number).isNull();
    }

    @Test
    public void jsonValue_string_blank_to_java_number() {
        // Given
        final var blank = Json.createValue("");

        // When
        final var number = new JsonAdapter().numberFrom(blank);
        
        // Then
        assertThat(number).isNull();
    }
    
    @Test
    public void listFrom_with_null() {
        // When
        final var list = new JsonAdapter().listFrom(null);
        
        // Then
        assertThat(list).isEmpty();
    }
    
    @Test
    public void listFrom_with_JsonValue_NULL() {
        // When
        final var list = new JsonAdapter().listFrom(NULL);
        
        // Then
        assertThat(list).isEmpty();
    }
    
    @Test
    public void listFrom_with_JsonValue_single_string() {
        // Given
        final var string = Json.createValue("test");
        
        // When
        final var list = new JsonAdapter().listFrom(string);
        
        // Then
        assertThat(list.get(0)).isEqualTo(string);
    }

    @Test
    public void stringListFrom_with_JsonValue_null() {
        // When
        final var list = new JsonAdapter().stringListFrom(null);
        
        // Then
        assertThat(list).isEmpty();
    }

    @Test
    public void stringListFrom_with_JsonValue_multiple_values() {
        // Given
        final var value1 = Json.createValue("test");
        final var value2 = Json.createValue(1);
        final var array = Json.createArrayBuilder().add(value1).add(value2).build();
        final var adapter = new JsonAdapter();
        
        // When
        final var list = adapter.stringListFrom(adapter.listFrom(array));
        
        // Then
        assertThat(list.get(0)).isEqualTo("test");
        assertThat(list.get(1)).isEqualTo("1");
    }

    @Test
    public void stringListFrom_with_JsonValue_not_supported_values() {
        // Given
        final var value1 = Json.createValue("test");
        final var value2 = Json.createValue(1);
        final var array = Json.createArrayBuilder().add(value1).build();
        final var illegalArray = Json.createArrayBuilder().add(value2).add(array).build();
        final var adapter = new JsonAdapter();
        
        // Then
        assertThrows(IllegalArgumentException.class, () -> adapter.stringListFrom(adapter.listFrom(illegalArray)));
    }
}
