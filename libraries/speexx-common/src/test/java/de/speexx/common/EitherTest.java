/*
 * Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package de.speexx.common;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth8.assertThat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class EitherTest {

    @Test
    public void leftValueNull_ThrowsException() {
        Assertions.assertThrows(NullPointerException.class, () -> Either.left(null));
    }

    @Test
    public void rightValueNull_ThrowsException() {
        Assertions.assertThrows(NullPointerException.class, () -> Either.right(null));
    }

    @Test
    public void mapWhenLeftValuePresent_OnlyCallsLeftFunction() {
        final Either<String, Integer> either = Either.left("left val");
        final Boolean mapped = either.map(s -> {
                                              assertThat(s).isEqualTo("left val");
                                              return Boolean.TRUE;
                                          },
                                          this::assertNotCalled);
        assertThat(mapped).isTrue();
    }

    @Test
    public void mapWhenRightValuePresent_OnlyCallsRightFunction() {
        final Either<String, Integer> either = Either.right(42);
        final Boolean mapped = either.map(this::assertNotCalled,
                                          i -> {
                                              assertThat(i).isEqualTo(42);
                                              return Boolean.TRUE;
                                          });
        assertThat(mapped).isTrue();
    }

    @Test
    public void mapLeftWhenLeftValuePresent_OnlyMapsLeftValue() {
        final String value = "left val";
        final Either<String, Integer> either = Either.left(value);
        either.mapLeft(String::hashCode)
              .apply(l -> assertThat(l).isEqualTo(value.hashCode()),
                     this::assertNotCalled);
    }

    @Test
    public void mapLeftWhenLeftValueNotPresent_DoesNotMapValue() {
        final Integer value = 42;
        final Either<String, Integer> either = Either.right(value);
        either.mapLeft(this::assertNotCalled)
              .apply(this::assertNotCalled,
                     i -> assertThat(i).isEqualTo(42));
    }

    @Test
    public void mapRightWhenRightValuePresent_OnlyMapsLeftValue() {
        final Integer value = 42;
        final Either<String, Integer> either = Either.right(value);
        either.mapRight(i -> "num=" + i)
              .apply(this::assertNotCalled,
                     v -> assertThat(v).isEqualTo("num=42"));
    }

    @Test
    public void mapRightWhenRightValueNotPresent_DoesNotMapValue() {
        final String value = "left val";
        final Either<String, Integer> either = Either.left(value);
        either.mapRight(this::assertNotCalled)
              .apply(s -> assertThat(s).isEqualTo(value),
                     this::assertNotCalled);
    }

    @Test
    public void fromNullable() {
        assertThat(Either.fromNullable("left", null)).hasValue(Either.left("left"));
        assertThat(Either.fromNullable(null, "right")).hasValue(Either.right("right"));
        assertThat(Either.fromNullable(null, null)).isEmpty();
        Assertions.assertThrows(IllegalArgumentException.class, () -> Either.fromNullable("left", "right"));
    }

    @Test
    public void eitherLeft_returnLeft() {
        assertThat(Either.left("test").left()).hasValue("test");
        assertThat(Either.left("test").right()).isEmpty();
    }

    @Test
    public void eitherRight_returnRight() {
        assertThat(Either.right("test").right()).hasValue("test");
        assertThat(Either.right("test").left()).isEmpty();
    }

    private <InT, OutT> OutT assertNotCalled(InT in) {
        throw new AssertionError("Mapping function should not have been called");
    }
}
