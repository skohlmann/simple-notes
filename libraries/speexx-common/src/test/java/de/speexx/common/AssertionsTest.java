package de.speexx.common;

import static com.google.common.truth.Truth.assertThat;
import static de.speexx.common.Assertions.assertMatch;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class AssertionsTest {

    @Test
    public void assertRange_long_with_supplier_below_min() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(1L, 2L, 3L, () -> "below min"));
        assertThat(e.getMessage()).isEqualTo("below min");
    }

    @Test
    public void assertRange_long_with_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(1L, 2L, 3L, () -> null));
        assertThat(e.getMessage()).isEqualTo("1");
    }

    @Test
    public void assertRange_long_with_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(1L, 2L, 3L, (String) null));
        assertThat(e.getMessage()).isEqualTo("1");
    }
    
    @Test
    public void assertRange_long_with_supplier_equal_min() {
        Assertions.assertRange(2L, 2L, 3L, () -> "ok");
    }
    
    @Test
    public void assertRange_long_with_supplier_higher_min() {
        Assertions.assertRange(3L, 2L, 3L, () -> "ok");
    }
    
    @Test
    public void assertRange_long_with_supplier_below_max() {
        Assertions.assertRange(2L, 2L, 3L, () -> "ok");
    }
    
    @Test
    public void assertRange_long_with_supplier_equal_max() {
        Assertions.assertRange(3L, 2L, 3L, () -> "ok");
    }

    @Test
    public void assertRange_long_with_supplier_higher_max() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(4L, 2L, 3L, () -> "higher max"));
        assertThat(e.getMessage()).isEqualTo("higher max");
    }

    @Test
    public void assertRange_long_with_supplier_null_higher_max() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(4L, 2L, 3L, () -> null));
        assertThat(e.getMessage()).isEqualTo("4");
    }

    @Test
    public void assertRange_long_with_message_null_higher_max() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(4L, 2L, 3L, (String) null));
        assertThat(e.getMessage()).isEqualTo("4");
    }

    @Test
    public void assertRange_long_simple() {
        Assertions.assertRange(3L, 2L, 4L, "ok");
    }

    @Test
    public void assertRange_with_supplier_below_min() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(1, 2, 3, () -> "below min"));
        assertThat(e.getMessage()).isEqualTo("below min");
    }
    
    @Test
    public void assertRange_below_min_with_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(1, 2, 3, () -> null));
        assertThat(e.getMessage()).isEqualTo("1");
    }
    
    @Test
    public void assertRange_below_min_with_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(1, 2, 3, (String) null));
        assertThat(e.getMessage()).isEqualTo("1");
    }
    
    @Test
    public void assertRange_with_supplier_equal_min() {
        Assertions.assertRange(2, 2, 3, () -> "ok");
    }
    
    @Test
    public void assertRange_with_supplier_higher_min() {
        Assertions.assertRange(3, 2, 3, () -> "ok");
    }
    
    @Test
    public void assertRange_with_supplier_below_max() {
        Assertions.assertRange(2, 2, 3, () -> "ok");
    }
    
    @Test
    public void assertRange_with_supplier_equal_max() {
        Assertions.assertRange(3, 2, 3, () -> "ok");
    }
    
    @Test
    public void assertRange_with_supplier_higher_max() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(4, 2, 3, () -> "higher max"));
        assertThat(e.getMessage()).isEqualTo("higher max");
    }

    @Test
    public void assertRange_higher_max_with_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(4, 2, 3, () -> null));
        assertThat(e.getMessage()).isEqualTo("4");
    }

    @Test
    public void assertRange_higher_max_with_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertRange(4, 2, 3, (String) null));
        assertThat(e.getMessage()).isEqualTo("4");
    }

    @Test
    public void assertRange_simple() {
        Assertions.assertRange(3, 2, 4, "ok");
    }
    
    @Test
    public void assertNotNull_null() {
        final var e = assertThrows(ArgumentNullException.class, () -> Assertions.assertNotNull(null, "is null"));
        assertThat(e.getMessage()).isEqualTo("is null");
    }

    @Test
    public void assertNotNull_null_with_message_null() {
        final var e = assertThrows(ArgumentNullException.class, () -> Assertions.assertNotNull(null, (String) null));
        assertThat(e.getMessage()).isEqualTo("null");
    }
    
    
    @Test
    public void assertNotNull_not_null() {
        Assertions.assertNotNull("", "not null");
    }
    
    @Test
    public void assertNotNull_null_with_supplier() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotNull(null, () -> "is null"));
        assertThat(e.getMessage()).isEqualTo("is null");
    }
    
    @Test
    public void assertNotNull_null_with_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotNull(null, () -> null));
        assertThat(e.getMessage()).isEqualTo("null");
    }
    
    @Test
    public void assertNotNull_with_supplier_not_null() {
        Assertions.assertNotNull("", () -> "not null");
    }

    @Test
    public void assertNull_not_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNull("", "is not null"));
        assertThat(e.getMessage()).isEqualTo("is not null");
    }
    
    @Test
    public void assertNull_not_null_with_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNull("", (String) null));
        assertThat(e.getMessage()).isEqualTo("not null");
    }
    
    @Test
    public void assertNull_null() {
        Assertions.assertNull(null, "not null");
    }
    
    @Test
    public void assertNull_not_null_with_supplier() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNull("", () -> "is not null"));
        assertThat(e.getMessage()).isEqualTo("is not null");
    }
    
    @Test
    public void assertNull_not_null_with_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNull("", () -> null));
        assertThat(e.getMessage()).isEqualTo("not null");
    }
    
    @Test
    public void assertNull_with_supplier_null() {
        Assertions.assertNull(null, () -> "not null");
    }
    
    @Test
    public void assertNotEquals_equals() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEquals("abc", "abc", "equals"));
        assertThat(e.getMessage()).isEqualTo("equals");
    }
    
    @Test
    public void assertNotEquals_not_equals() {
        Assertions.assertNotEquals("abc", "def", "equals");
    }
    
    @Test
    public void assertNotEquals_equals_with_supplier() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEquals("abc", "abc", () -> "equals"));
        assertThat(e.getMessage()).isEqualTo("equals");
    }
    
    @Test
    public void assertNotEquals_equals_with_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEquals("abc", "abc", () -> null));
        assertThat(e.getMessage()).isEqualTo("not equal");
    }
    
    @Test
    public void assertNotEquals_equals_with_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEquals("abc", "abc", (String) null));
        assertThat(e.getMessage()).isEqualTo("not equal");
    }
    
    @Test
    public void assertNotEquals_with_supplier_not_equals() {
        Assertions.assertNotEquals("abc", "def", () -> "equals");
    }
    
    @Test
    public void assertNotEmpty_null_Collection() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty((Collection) null, () -> "null value"));
        assertThat(e.getMessage()).isEqualTo("null value");
    }

    @Test
    public void assertNotEmpty_empty_Collection() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty(Collections.emptySet(), () -> "null value"));
        assertThat(e.getMessage()).isEqualTo("null value");
    }
    
    @Test
    public void assertNotEmpty_empty_Collection_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty(Set.of(), () -> null));
        assertThat(e.getMessage()).isEqualTo("empty");
    }
    
    @Test
    public void assertNotEmpty_empty_Collection_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty(Set.of(), (String) null));
        assertThat(e.getMessage()).isEqualTo("empty");
    }
    
    @Test
    public void assertNotEmpty_Collection() {
        final Collection<Integer> collection = new ArrayList<>(Arrays.asList(1));
        assertThat(Assertions.assertNotEmpty(collection, "nothing to print")).isSameInstanceAs(collection);
    }
    
    @Test
    public void assertNotEmpty_with_supplier_null_String() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty((String) null, () -> "null value"));
        assertThat(e.getMessage()).isEqualTo("string is null");
    }

    @Test
    public void assertNotEmpty_empty_with_whitespace_String_with_supplier() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty(" ", () -> "empty String"));
        assertThat(e.getMessage()).isEqualTo("empty String");
    }

    @Test
    public void assertNotEmpty_empty_with_whitespace_String_with_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty(" ", () -> null));
        assertThat(e.getMessage()).isEqualTo("empty");
    }

    @Test
    public void assertNotEmpty_empty_with_whitespace_String_with_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty(" ", (String) null));
        assertThat(e.getMessage()).isEqualTo("empty");
    }

    @Test
    public void assertNotEmpty_with_supplier_not_empty_String() {
        Assertions.assertNotEmpty("abc", () -> "ok");
    }
    
    @Test
    public void assertNotEmpty_null_String() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty((String) null, "null value"));
        assertThat(e.getMessage()).isEqualTo("string is null");
    }

    @Test
    public void assertNotEmpty_empty_with_whitespace() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty(" ", "is empty"));
        assertThat(e.getMessage()).isEqualTo("is empty");
    }

    @Test
    public void assertNotEmpty_empty_with_whitespace_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty(" ", () -> null));
        assertThat(e.getMessage()).isEqualTo("empty");
    }

    @Test
    public void assertNotEmpty_empty_with_whitespace_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotEmpty(" ", (String) null));
        assertThat(e.getMessage()).isEqualTo("empty");
    }

    @Test
    public void assertNotEmpty_not_empty_String() {
        Assertions.assertNotEmpty("abc", "ok");
    }
    
    @Test
    public void assertLength_min_max_lower_min() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("ab", 3, 5, "too short value"));
        assertThat(e.getMessage()).isEqualTo("too short value");
    }

    @Test
    public void assertLength_min_max_lower_min_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("ab", 3, 5, () -> null));
        assertThat(e.getMessage()).isEqualTo("too short");
    }

    @Test
    public void assertLength_min_max_lower_min_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("ab", 3, 5, (String) null));
        assertThat(e.getMessage()).isEqualTo("too short");
    }

    @Test
    public void assertLength_min_max_equal_min() {
        Assertions.assertLength("abc", 3, 5, "ok");
    }

    @Test
    public void assertLength_min_max_higher_min() {
        Assertions.assertLength("abcd", 3, 5, "ok");
    }

    @Test
    public void assertLength_min_max_lower_max() {
        Assertions.assertLength("abcd", 3, 5, "ok");
    }
    
    @Test
    public void assertLength_min_max_equal_max() {
        Assertions.assertLength("abcde", 3, 5, "");
    }
    
    @Test
    public void assertLength_min_max_higher_max() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("abcdef", 3, 5, "too long value"));
        assertThat(e.getMessage()).isEqualTo("too long value");
    }    

    @Test
    public void assertLength_min_max_with_supplier_lower_min() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("ab", 3, 5, () -> "too short value"));
        assertThat(e.getMessage()).isEqualTo("too short value");
    }

    @Test
    public void assertLength_min_max_lower_min_with_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("ab", 3, 5, () -> null));
        assertThat(e.getMessage()).isEqualTo("too short");
    }

    @Test
    public void assertLength_min_max_lower_min_with_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("ab", 3, 5, (String) null));
        assertThat(e.getMessage()).isEqualTo("too short");
    }

    @Test
    public void assertLength_min_max_greater_min_with_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("abcdef", 3, 5, () -> null));
        assertThat(e.getMessage()).isEqualTo("too long");
    }

    @Test
    public void assertLength_min_max_greater_min_with_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("abcdef", 3, 5, (String) null));
        assertThat(e.getMessage()).isEqualTo("too long");
    }

    @Test
    public void assertLength_min_max_with_supplier_equal_min() {
        Assertions.assertLength("abc", 3, 5, () -> "ok");
    }

    @Test
    public void assertLength_min_max_with_supplier_higher_min() {
        Assertions.assertLength("abcd", 3, 5, () -> "ok");
    }

    @Test
    public void assertLength_min_max_with_supplier_lower_max() {
        Assertions.assertLength("abcd", 3, 5, () -> "ok");
    }
    
    @Test
    public void assertLength_min_max_with_supplier_equal_max() {
        Assertions.assertLength("abcde", 3, 5, () -> "");
    }
    
    @Test
    public void assertLength_min_max_with_supplier_higher_max() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("abcdef", 3, 5, () -> "too long value"));
        assertThat(e.getMessage()).isEqualTo("too long value");
    }    

    @Test
    public void assertLength_with_supplier_lower_max() {
        Assertions.assertLength("ab", 3, () -> "");
    }
    
    @Test
    public void assertLength_with_supplier_equal_max() {
        Assertions.assertLength("abc", 3, () -> "");
    }
    
    @Test
    public void assertLength_with_supplier_higher_max() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("abcd", 3, () -> "too long value"));
        assertThat(e.getMessage()).isEqualTo("too long value");
    }    

    @Test
    public void assertLength_lower_max() {
        Assertions.assertLength("ab", 3, "");
    }
    
    @Test
    public void assertLength_equal_max() {
        Assertions.assertLength("abc", 3, "");
    }
    
    @Test
    public void assertLength_higher_max() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertLength("abcd", 3, "too long value"));
        assertThat(e.getMessage()).isEqualTo("too long value");
    }    
    
    @Test
    public void assertdFalse_with_false() {
        Assertions.assertFalse(false, "must be false");
    }

    @Test
    public void assertdFalse_with_supplier_with_false() {
        Assertions.assertFalse(false, () -> "supplier false");
    }
    
    @Test
    public void assertdFalse_with_true() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertFalse(true, "must be false"));
        assertThat(e.getMessage()).isEqualTo("must be false");
    }

    @Test
    public void assertdFalse_with_supplier_with_true() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertFalse(true, () -> "supplier false"));
        assertThat(e.getMessage()).isEqualTo("supplier false");
    }

    @Test
    public void assertdFalse_with_true_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertFalse(true, () -> null));
        assertThat(e.getMessage()).isEqualTo("true");
    }

    @Test
    public void assertdFalse_with_true_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertFalse(true, (String) null));
        assertThat(e.getMessage()).isEqualTo("true");
    }

    @Test
    public void assertdTrue_with_true() {
        Assertions.assertTrue(true, "must be false");
    }

    @Test
    public void assertdTrue_with_supplier_with_true() {
        Assertions.assertTrue(true, () -> "");
    }
    
    @Test
    public void assertdTrue_with_false() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertTrue(false, "must be true"));
        assertThat(e.getMessage()).isEqualTo("must be true");
    }

    @Test
    public void assertdTrue_with_false_supplier() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertTrue(false, () -> "supplier true"));
        assertThat(e.getMessage()).isEqualTo("supplier true");
    }

    @Test
    public void assertdTrue_with_false_supplier_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertTrue(false, () -> null));
        assertThat(e.getMessage()).isEqualTo("false");
    }

    @Test
    public void assertdTrue_with_false_message_null() {
        final var e = assertThrows(IllegalArgumentException.class, () -> Assertions.assertTrue(false, (String) null));
        assertThat(e.getMessage()).isEqualTo("false");
    }

    @Test
    public void assertStateFalse_with_false() {
        Assertions.assertStateFalse(false, "must be false");
    }

    @Test
    public void assertStateFalse_with_supplier_with_false() {
        Assertions.assertStateFalse(false, () -> "supplier false");
    }
    
    @Test
    public void assertStateFalse_with_true() {
        final IllegalStateException e = assertThrows(IllegalStateException.class, () -> Assertions.assertStateFalse(true, "must be false"));
        assertThat(e.getMessage()).isEqualTo("must be false");
    }

    @Test
    public void assertStateFalse_with_true_supplier() {
        final IllegalStateException e = assertThrows(IllegalStateException.class, () -> Assertions.assertStateFalse(true, () -> "supplier false"));
        assertThat(e.getMessage()).isEqualTo("supplier false");
    }

    @Test
    public void assertStateFalse_with_true_supllier_null() {
        final IllegalStateException e = assertThrows(IllegalStateException.class, () -> Assertions.assertStateFalse(true, () -> null));
        assertThat(e.getMessage()).isEqualTo("true");
    }

    @Test
    public void assertStateFalse_with_true_message_null() {
        final IllegalStateException e = assertThrows(IllegalStateException.class, () -> Assertions.assertStateFalse(true, (String) null));
        assertThat(e.getMessage()).isEqualTo("true");
    }

    @Test
    public void assertStateTrue_with_true() {
        Assertions.assertStateTrue(true, "must be false");
    }

    @Test
    public void assertStateTrue_with_supplier_with_true() {
        Assertions.assertStateTrue(true, () -> "");
    }
    
    @Test
    public void assertStateTrue_with_false() {
        final var e = assertThrows(IllegalStateException.class, () -> Assertions.assertStateTrue(false, "must be true"));
        assertThat(e.getMessage()).isEqualTo("must be true");
    }

    @Test
    public void assertStateTrue_with_false_supplier() {
        final var e = assertThrows(IllegalStateException.class, () -> Assertions.assertStateTrue(false, () -> "supplier true"));
        assertThat(e.getMessage()).isEqualTo("supplier true");
    }

    @Test
    public void assertStateTrue_with_false_supplier_null() {
        final var e = assertThrows(IllegalStateException.class, () -> Assertions.assertStateTrue(false, () -> null));
        assertThat(e.getMessage()).isEqualTo("false");
    }

    @Test
    public void assertStateTrue_with_false_meessagee_null() {
        final var e = assertThrows(IllegalStateException.class, () -> Assertions.assertStateTrue(false, (String) null));
        assertThat(e.getMessage()).isEqualTo("false");
    }
    
    @Test
    public void assertContainsNoNull_with_valid_data() {
        Assertions.assertContainsNoNull(Arrays.asList("1", "2"), "valid");
    }
    
    @Test
    public void assertContainsNoNull_with_null_collection() {
        assertThrows(IllegalArgumentException.class, () -> Assertions.assertContainsNoNull(null, "null"));
    }
    
    @Test
    public void assertContainsNoNull_with_null_in_collection() {
        assertThrows(IllegalArgumentException.class, () -> Assertions.assertContainsNoNull(Arrays.asList("1", null), "null"));
    }
    
    @Test
    public void assertIsNotBlank_with_whitspace_only_message() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotBlank("\u0020\t\n\f\r\u000B\u001C\u001D\u001E\u001F", "is blank"));
        assertThat(ex.getMessage()).isEqualTo("is blank");
    }

    @Test
    public void assertIsNotBlank_with_whitspace_only_message_null() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotBlank("\u0020\t\n\f\r\u000B\u001C\u001D\u001E\u001F", (String) null));
        assertThat(ex.getMessage()).isEqualTo("blank");
    }

    @Test
    public void assertIsNotBlank_with_whitspace_only_supplier_null() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> Assertions.assertNotBlank("\u0020\t\n\f\r\u000B\u001C\u001D\u001E\u001F", () -> null));
        assertThat(ex.getMessage()).isEqualTo("blank");
    }

    @Test
    public void assertIsNotBlank_with_none_whitspace() {
        assertThat(Assertions.assertNotBlank("\u0020not blank", "blank")).isEqualTo(" not blank");
    }

    @Test
    public void assertMatch_with_null_pattern_pattern() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> assertMatch("test", (Pattern) null, "null"));
        assertThat(ex.getLocalizedMessage()).isEqualTo("Pattern is null");
    }

    @Test
    public void assertMatch_with_null_pattern_string() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> assertMatch("test", (String) null, "null"));
        assertThat(ex.getLocalizedMessage()).isEqualTo("Pattern is null");
    }
    
    @Test
    public void assertMatch_with_null_sequence_message() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> assertMatch(null, Pattern.compile("test"), "null"));
        assertThat(ex.getLocalizedMessage()).isEqualTo("Sequence is null");
    }

    @Test
    public void assertMatch_with_not_matching_sequence_message_null() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> assertMatch("u", Pattern.compile("test"), (String) null));
        assertThat(ex.getLocalizedMessage()).isEqualTo("not matching");
    }

    @Test
    public void assertMatch_with_not_matching_sequence_supplier_null() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> assertMatch("u", Pattern.compile("test"), () -> null));
        assertThat(ex.getLocalizedMessage()).isEqualTo("not matching");
    }

    @Test
    public void assertMatch_with_no_match() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> assertMatch("dummy", Pattern.compile("test"), "no match"));
        assertThat(ex.getLocalizedMessage()).isEqualTo("no match");
    }

    @Test
    public void assertMatch_with_match() {
        assertThat(assertMatch("test", Pattern.compile("test"), "no match")).isEqualTo("test");
    }

     @Test
    public void assertMatch_with_string_pattern_match() {
        assertThat(assertMatch("test", "test", "no match")).isEqualTo("test");
    }
   
    @Test
    public void assertMatch_with_illegal_string_pattern() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> assertMatch("test", "[\\u]", "null"));
        assertThat(ex).isInstanceOf(PatternSyntaxException.class);
    }
    
    @Test
    public void assertSize_collection_with_null() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> Assertions.assertSize((Collection) null, 1, 1, "null!"));
        assertThat(ex.getLocalizedMessage()).isEqualTo("Collection must be provided");
    }
    
    @Test
    public void assertSize_collection_with_correct_range() {
        Assertions.assertSize(Arrays.asList(1, 2), 1, 2, "");
    }
    
    @Test
    public void assertSize_collection_with_incorrect_range_message() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> Assertions.assertSize(Arrays.asList(1, 2), 3, 4, "incorrect!"));
        assertThat(ex.getLocalizedMessage()).isEqualTo("incorrect!");
    }
    
    @Test
    public void assertSize_collection_with_incorrect_range_message_null() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> Assertions.assertSize(Arrays.asList(1, 2), 3, 4, (String) null));
        assertThat(ex.getLocalizedMessage()).isEqualTo("2");
    }
    
    @Test
    public void assertSize_collection_with_incorrect_range_supplier_null() {
        final var ex = assertThrows(IllegalArgumentException.class, () -> Assertions.assertSize(Arrays.asList(1, 2), 3, 4, () -> null));
        assertThat(ex.getLocalizedMessage()).isEqualTo("2");
    }
}
