= SpeexX Simple Dependency Injection

== Runtime

* Releases are based on the lastest release Java version.

== Version schema

A https://semver.org[semantic version schema] where

* MAJOR is the version of the latest compatible Java version
* MINOR backward compatible API changes
* PATCH backward compatible bug fixes

E.g. a version might be 17.0.1 which means: this version is Java 17 or higher compatble.

New features might be backported to older versions with increased MINOR version.

See Maven https://maven.apache.org/enforcer/enforcer-rules/versionRanges.html[Version Range Specification] specification (Enforcer Pluging).
