/*
 * This file is part of the SpeexX Simple Dependency Injection project.
 * 
 * SpeexX Simple Dependency Injection project is free software:
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Dependency Injection project is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warrantyof MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpeexX Simple Dependency Injection project.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.di.simple.impl;

import de.speexx.common.Either;
import de.speexx.common.annotation.Generated;
import de.speexx.di.simple.ConstructorTemplate;
import de.speexx.di.simple.InjectionException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * @author sascha.kohlmann
 */
public final class ConstructorTemplateImpl implements ConstructorTemplate {

    private static final Supplier<Object> NULL_SUPPLIER = () -> null;
    
    private List<Either<Supplier<Object>, de.speexx.di.simple.InstanceTemplateReference>> parameterValues = new ArrayList<>();
    
    private ConstructorTemplateImpl(final ConstructorTemplateImplBuilder builder) {
        builder.parameter.stream().forEachOrdered(parameter -> {
            
            if (parameter instanceof Supplier supplier) {
                parameterValues.add(Either.left(supplier));
            } else if (parameter instanceof de.speexx.di.simple.InstanceTemplateReference instanceTemplateReference) {
                parameterValues.add(Either.right(instanceTemplateReference));
            } else if (parameter == null) {
                parameterValues.add(Either.left(NULL_SUPPLIER));
            } else {
                throw new InjectionException("Unsupported type: " + parameter.getClass());
            }
        });
    }

    @Override
    public List<Either<Supplier<Object>, de.speexx.di.simple.InstanceTemplateReference>> parameters() {
        return List.copyOf(this.parameterValues);
    }

    @Override
    @Generated
    public String toString() {
        return "ConstructorTemplateImpl{" + "parameterValues=" + parameterValues + '}';
    }
    

    public final static class ConstructorTemplateImplBuilder extends BaseBuilder<ConstructorTemplate> implements ConstructorTemplate.Builder {

        private List<Object> parameter = new ArrayList<>();
        
        @Override
        public ConstructorTemplate build() {
            return new ConstructorTemplateImpl(this);
        }

        @Override
        public Builder addParameter(final Supplier supplier) {
            buildGuard();
            this.parameter.add(supplier);
            return this;
        }

        @Override
        public Builder addParameter(final de.speexx.di.simple.InstanceTemplateReference reference) {
            buildGuard();
            this.parameter.add(reference);
            return this;
        }

        @Override
        @Generated
        public String toString() {
            return "ConstructorTemplateImplBuilder{" + "parameter=" + parameter + '}';
        }
    }
}
