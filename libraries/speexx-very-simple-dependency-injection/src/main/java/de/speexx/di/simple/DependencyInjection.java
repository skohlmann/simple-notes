/*
 * This file is part of the SpeexX Simple Dependency Injection project.
 * 
 * SpeexX Simple Dependency Injection project is free software:
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Dependency Injection project is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warrantyof MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpeexX Simple Dependency Injection project.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.di.simple;

import de.speexx.di.simple.impl.DependencyInjectionBuilder;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 *
 * @author sascha.kohlmann
 */
@API(status=EXPERIMENTAL)
public interface DependencyInjection {
    
    public static Builder builder() {
        return new DependencyInjectionBuilder();
    }
        
    <T> T instanceFor(final Class<T> templateType);

    @API(status=EXPERIMENTAL)
    public static interface Builder {
        
        Builder add(final TypeTemplateResolver resolver);
        Builder add(final Instance instance);
        
        DependencyInjection build();
    }
}
