/*
 * This file is part of the SpeexX Simple Dependency Injection project.
 * 
 * SpeexX Simple Dependency Injection project is free software:
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Dependency Injection project is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warrantyof MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpeexX Simple Dependency Injection project.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.di.simple;

import java.util.HashSet;
import java.util.Set;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 *
 * @author sascha.kohlmann
 */
@API(status=EXPERIMENTAL)
public final class StaticTypeTemplateResolverBuilder {
    
    private boolean built = false;
    private final Set<InjectionTemplate> templates = new HashSet<>();

    private StaticTypeTemplateResolverBuilder() {}
    
    public static StaticTypeTemplateResolverBuilder builder() {
        return new StaticTypeTemplateResolverBuilder();
    }

    public StaticTypeTemplateResolverBuilder register(final InjectionTemplate template) {
        if (template != null) {
            this.templates.add(template);
        }
        return this;
    }
    
    public TypeTemplateResolver build() {
        buildGuard();
        this.built = true;
        return () -> Set.copyOf(this.templates);
    }
    
    private final void buildGuard() {
        if (this.built) {
            throw new IllegalStateException("build() called twice");
        }
    }
}
