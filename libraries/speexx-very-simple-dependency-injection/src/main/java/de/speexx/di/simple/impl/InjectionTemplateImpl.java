/*
 * This file is part of the SpeexX Simple Dependency Injection project.
 * 
 * SpeexX Simple Dependency Injection project is free software:
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Dependency Injection project is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warrantyof MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpeexX Simple Dependency Injection project.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.di.simple.impl;

import static de.speexx.common.Assertions.assertNotNull;
import de.speexx.common.annotation.Generated;
import de.speexx.di.simple.ConstructorTemplate;
import static de.speexx.di.simple.ConstructorTemplate.emptyConstructorTemplate;
import de.speexx.di.simple.InjectionTemplate;
import java.util.Objects;
import java.util.Optional;

/**
 *
 * @author sascha.kohlmann
 */
public final class InjectionTemplateImpl implements InjectionTemplate {
    
    private final Class<?> implementationType;
    private final de.speexx.di.simple.InstanceTemplateReference templateType;
    private final ConstructorTemplate constructorTemplate;
    
    private InjectionTemplateImpl(final InjectionTemplateBuilder builder) {
        this.implementationType = assertNotNull(builder.instanceClass, "Clazz must be provided");
        this.constructorTemplate = builder.constructorTemplate != null ? builder.constructorTemplate : emptyConstructorTemplate();
        this.templateType =  builder.templateClass;
    }

    @Override
    public Class<?> implementationType() {
        return this.implementationType;
    }

    @Override
    public Optional<de.speexx.di.simple.InstanceTemplateReference> templateReference() {
        return Optional.ofNullable(this.templateType);
    }

    @Override
    public ConstructorTemplate constructor() {
        return this.constructorTemplate;
    }

    @Override
    @Generated
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.implementationType);
        hash = 29 * hash + Objects.hashCode(this.templateType);
        hash = 29 * hash + Objects.hashCode(this.constructorTemplate);
        return hash;
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InjectionTemplateImpl other = (InjectionTemplateImpl) obj;
        if (!Objects.equals(this.implementationType, other.implementationType)) {
            return false;
        }
        if (!Objects.equals(this.templateType, other.templateType)) {
            return false;
        }
        return Objects.equals(this.constructorTemplate, other.constructorTemplate);
    }

    @Override
    @Generated
    public String toString() {
        return "InjectionTemplateImpl{" + "implementationType=" + implementationType + ", templateType=" + templateType + ", constructorTemplate=" + constructorTemplate + '}';
    }
    
    public static final class InjectionTemplateBuilder extends BaseBuilder<InjectionTemplate> implements InjectionTemplate.Builder {

        private final Class<?> instanceClass;
        private de.speexx.di.simple.InstanceTemplateReference templateClass;
        private ConstructorTemplate constructorTemplate;

        public InjectionTemplateBuilder(final Class<?> clazz) {
            this.instanceClass = clazz;
        }

        @Override
        public InjectionTemplate build() {
            built();
            return new InjectionTemplateImpl(this);
        }

        @Override
        public InjectionTemplate.Builder with(final ConstructorTemplate template) {
            buildGuard();
            this.constructorTemplate = template;
            return this;
        }

        @Override
        public Builder with(final de.speexx.di.simple.InstanceTemplateReference templateClass) {
            buildGuard();
            this.templateClass = templateClass;
            return this;
        }

        @Override
        public String toString() {
            return "InjectionTemplateBuilder{" + "instanceClass=" + instanceClass + ", templateClass=" + templateClass + ", constructorTemplate=" + constructorTemplate + '}';
        }
    }
}
