/*
 * This file is part of the SpeexX Simple Dependency Injection project.
 * 
 * SpeexX Simple Dependency Injection project is free software:
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Dependency Injection project is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warrantyof MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpeexX Simple Dependency Injection project.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.di.simple.impl;

import de.speexx.common.annotation.Generated;
import de.speexx.di.simple.DependencyInjection;
import de.speexx.di.simple.DependencyInjection.Builder;
import de.speexx.di.simple.InjectionTemplate;
import de.speexx.di.simple.Instance;
import de.speexx.di.simple.TypeTemplateResolver;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sascha.kohlmann
 */
public final class DependencyInjectionBuilder extends BaseBuilder<DependencyInjection> implements DependencyInjection.Builder {

    private final List<InjectionTemplate> templates = new ArrayList<>();
    private final List<Instance> instances = new ArrayList<>();
    
    @Override
    public DependencyInjection.Builder add(final TypeTemplateResolver resolver) {
        buildGuard();
        if (resolver != null) {
            this.templates.addAll(resolver.templates());
        }
        return this;
    }
    
    @Override
    public Builder add(final Instance instance) {
        buildGuard();
        if (instance != null) {
            this.instances.add(instance);
        }
        return this;
    }

    List<InjectionTemplate> templates() {
        return List.copyOf(this.templates);
    }
    
    List<Instance> instances() {
        return List.copyOf(this.instances);
    }
    
    @Override
    public DependencyInjection build() {
        built();
        return new DependencyInjectionImpl(this);
    }

    @Override
    @Generated
    public String toString() {
        return "DependencyInjectionBuilder{" + "templates=" + templates + ", instances=" + instances + '}';
    }
}
