/*
 * This file is part of the SpeexX Simple Dependency Injection project.
 * 
 * SpeexX Simple Dependency Injection project is free software:
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Dependency Injection project is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warrantyof MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpeexX Simple Dependency Injection project.
 * If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Support for a very simple dependency injector. The injector will only
 * support <a href='https://en.wikipedia.org/wiki/Dependency_injection#Types_of_dependency_injection'>constructor injection</a>
 * and will not come with any annotations to prevent vendor lock-in.
 * 
 * <p>The current implementation supports code as configuration, late binding and auto wire.
 * A configuration binding should be possible with implementing {@link TypeTemplateResolver}.</p>
 * 
 * <p>Entry point is the {@link DependencyInjection} and the builder 
 * {@link DependencyInjection#builder()} method.</p>
 * 
 * <p><strong>NOTE:</strong> Everything in this package is experimental.</p>
 */
package de.speexx.di.simple;

