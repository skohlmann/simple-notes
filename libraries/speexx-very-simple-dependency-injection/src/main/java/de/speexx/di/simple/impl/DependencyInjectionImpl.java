/*
 * This file is part of the SpeexX Simple Dependency Injection project.
 * 
 * SpeexX Simple Dependency Injection project is free software:
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Dependency Injection project is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warrantyof MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpeexX Simple Dependency Injection project.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.di.simple.impl;

import static de.speexx.common.Assertions.assertFalse;
import static de.speexx.common.Assertions.assertNotNull;
import static de.speexx.common.Assertions.assertTrue;
import de.speexx.common.annotation.Generated;
import de.speexx.di.simple.DependencyInjection;
import de.speexx.di.simple.InjectionException;
import de.speexx.di.simple.InjectionTemplate;
import de.speexx.di.simple.Instance;
import de.speexx.di.simple.NoMatchingConstructorException;
import de.speexx.di.simple.NoTemplateException;
import static java.lang.String.format;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 *
 * @author sascha.kohlmann
 */
public final class DependencyInjectionImpl implements DependencyInjection {
    
    private final Function<Supplier<Object>, Object> SUPPLIER_FUNC = supplier -> supplier.get();
    private final Function<de.speexx.di.simple.InstanceTemplateReference, Object> INSTANCE_FOR_FUNC = templateType -> instanceFor(templateType.reference());
    
    private final Map<ClassTuple, InjectionTemplate> typeTemplates = new HashMap<>();
    private final Map<ClassTuple, Instance> instances = new HashMap<>();
    
    public DependencyInjectionImpl(final DependencyInjectionBuilder builder) {
        assertNotNull(builder, "Builder must be provided");
        builder.templates().stream()
                .filter(template -> template != null)
                .peek(template -> legalTypeCheck(template))
                .peek(template -> reachableCheck(template))
                .flatMap(template -> toAssignable(template.implementationType(), template, new HashSet<>()).stream())
                .forEach(assignable -> this.typeTemplates.put(assignable.tuple(), assignable.typeTemplate()));
        builder.instances().stream()
                .filter(instance -> instance != null)
                .peek(instance -> legalTypeCheck(instance.getClass()))
                .peek(instance -> reachableCheck(instance))
                .flatMap(instance -> toAssignable(instance.get().getClass(), instance).stream())
                .forEach(instance -> this.instances.put(instance.tuple(), instance.instance()));
    }

    @Override
    public <T> T instanceFor(final Class<T> templateType) {
        final var tuple = new ClassTuple(templateType);
        final var inst = this.instances.get(tuple);
        if (inst != null) {
            return templateType.cast(inst.get());
        }

        try {
            final var typeTemplate = this.typeTemplates.get(tuple);
            if (typeTemplate == null) {
                throw new NoTemplateException(format("No instance or template for type %s", templateType));
            }
            
            final var instance = createInstanceFor(typeTemplate);
            return templateType.cast(instance);
        } catch (final IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException ex) {
            throw new InjectionException(ex);
        }
    }

    private Object createInstanceFor(final InjectionTemplate template) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        final var instanceClass = template.implementationType();

        final var values = template.constructor().parameters().stream().map(either -> either.map(SUPPLIER_FUNC, INSTANCE_FOR_FUNC)).toArray();
        final var possibleConstructor = findConstructor(instanceClass, values);
        if (possibleConstructor.isEmpty()) {
            throwNoMatchingConstructorException(instanceClass, values);
        }
        
        return possibleConstructor.get().newInstance(values);
    }
    
    void throwNoMatchingConstructorException(final Class<?> clazz, final Object[] values) {
        throw new NoMatchingConstructorException(String.format("Type: %s; Parameter: %s", clazz, Arrays.toString(typesForObjects(values))));
    }
    
    Class<?>[] typesForObjects(final Object[] objects) {
        return (Class<?>[]) Arrays.stream(objects)
                .map(object -> object != null ? object.getClass() : null)
                .toArray();
    }
    
    Optional<Constructor<?>> findConstructor(final Class<?> clazz, final Object[] values) {
        return Arrays.stream(clazz.getConstructors())
                .filter(cconstructor -> cconstructor.getParameterCount() == values.length)
                .findFirst();
    }
    
    private void legalTypeCheck(final InjectionTemplate template) {
        legalTypeCheck(template.implementationType());
    }

    private void legalTypeCheck(final Class<?> clazz) {
        assertFalse(clazz.isAnnotation(), () -> format("Not supported type: annotation - %s", clazz));
        assertFalse(clazz.isEnum(), () -> format("Not supported type: enum - %s", clazz));
        assertFalse(clazz.isArray(), () -> format("Not supported type: array - %s", clazz));
        assertFalse(Modifier.isAbstract(clazz.getModifiers()), () -> format("Not supported: abstract type - %s", clazz));
        assertFalse(clazz.isHidden(), () -> format("Not supported: hidden type - %s", clazz));
        assertFalse(clazz.isPrimitive(), () -> format("Not supported: primitive type - %s", clazz));
        assertFalse(clazz.isLocalClass(), () -> format("Not supported: local class - %s", clazz));
    }

    Set<AssignableInstance> toAssignable(final Class<?> clazz, final Instance instance) {
        final var assignables = new HashSet<AssignableInstance>();
        assignables.add(new AssignableInstance(new ClassTuple(clazz), instance));
        instance.instanceReference().ifPresent(instanceReference -> assignables.add(new AssignableInstance(new ClassTuple(instanceReference.getClass()), instance)));
        return assignables;
    }
    
    Set<AssignableTemplate> toAssignable(final Class<?> clazz, final InjectionTemplate template, final Set<AssignableTemplate> assignables) {
        assert clazz != null;
        assert template != null;

        if (clazz.equals(template.implementationType())) {
            template.templateReference().isPresent();
            if (template.templateReference().isPresent()) {
                final var templateType = template.templateReference().get().reference();
                addToAssignables(templateType, template, assignables);
                return assignables;
            } else {
                addToAssignables(template.implementationType(), template, assignables);
            }
        }
        
        final var superClass = clazz.getSuperclass();
        if (superClass != null && superClass != Object.class && !superClass.isEnum() && !superClass.isRecord()) {
            addToAssignables(superClass, template, assignables);
            toAssignable(superClass, template, assignables);
        } 

        final var interfaces = clazz.getInterfaces();
        if (interfaces != null) {
            Arrays.stream(interfaces)
                    .filter(ifc -> ifc != Annotation.class)
                    .forEach(ifc -> {
                        addToAssignables(ifc, template, assignables);
                        toAssignable(ifc, template, assignables);
                    });
        } 
        
        return assignables;
    }

    private void addToAssignables(final Class<?> clazz, final InjectionTemplate template, final Set<AssignableTemplate> assignables) {
        final var tuple = new ClassTuple(clazz);
        final var assignable = new AssignableTemplate(tuple, template);
        assignables.add(assignable);
    }
    
    private void reachableCheck(final InjectionTemplate template) {
        template.templateReference()
                .ifPresent(referenceType -> assertTrue(referenceType.reference().isAssignableFrom(template.implementationType()),
                                                      () -> String.format("Implementation %s doesn't implement/inherit of/from %s",
                                                                          referenceType.reference().getName(),
                                                                          template.implementationType().getName())));
    }

    private <T> void reachableCheck(final Instance<T> instance) {
        instance.instanceReference()
                .ifPresent(instanceReference -> assertTrue(instanceReference.reference().isAssignableFrom(instance.get().getClass()),
                                                            () -> String.format("Implementation %s doesn't implement/inherit of/from %s",
                                                                                instanceReference.reference().getName(),
                                                                                instance.get().getClass().getName())));
    }

    @Override
    @Generated
    public String toString() {
        return "DependencyInjectionImpl{" + "typeTemplates=" + typeTemplates + '}';
    }

    private record ClassTuple(Class<?> clazz) {}
 
    private record AssignableTemplate(ClassTuple tuple, InjectionTemplate typeTemplate) {}
    private record AssignableInstance(ClassTuple tuple, Instance instance) {}
}
