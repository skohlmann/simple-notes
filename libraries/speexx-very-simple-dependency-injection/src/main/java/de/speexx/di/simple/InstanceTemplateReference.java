/*
 * This file is part of the SpeexX Simple Dependency Injection project.
 * 
 * SpeexX Simple Dependency Injection project is free software:
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Dependency Injection project is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warrantyof MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpeexX Simple Dependency Injection project.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.di.simple;

import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 *
 * @param reference the template reference
 * @author sascha.kohlmann
 */
@API(status=EXPERIMENTAL)
public record InstanceTemplateReference(Class<?> reference) {}
