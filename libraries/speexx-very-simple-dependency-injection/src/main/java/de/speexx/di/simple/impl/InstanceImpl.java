/*
 * This file is part of the SpeexX Simple Dependency Injection project.
 * 
 * SpeexX Simple Dependency Injection project is free software:
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Dependency Injection project is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warrantyof MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpeexX Simple Dependency Injection project.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.di.simple.impl;

import de.speexx.common.annotation.Generated;
import de.speexx.di.simple.Instance;
import java.util.Optional;

/**
 *
 * @author sascha.kohlmann
 */
public final class InstanceImpl<T> implements Instance<T> {
    
    private final T instance;
    private final de.speexx.di.simple.InstanceTemplateReference reference;
    
    private InstanceImpl(final InstanceBuilderImpl builder) {
        this.instance = (T) builder.instance;
        this.reference = builder.reference;
    }

    @Override
    public T get() {
        return this.instance;
    }

    @Override
    public Optional<de.speexx.di.simple.InstanceTemplateReference> instanceReference() {
        return Optional.ofNullable(this.reference);
    }

    @Override
    @Generated
    public String toString() {
        return "InstanceImpl{" + "instance=" + instance + ", reference=" + reference + '}';
    }

    public static final class InstanceBuilderImpl<I> extends BaseBuilder<Instance> implements Builder<I> {

        private final I instance;
        private de.speexx.di.simple.InstanceTemplateReference reference;
        
        public  InstanceBuilderImpl(final I instance) {
            this.instance = instance;
        }
         
        @Override
        public Instance build() {
            built();
            return new InstanceImpl(this);
        }

        @Override
        public Builder with(final de.speexx.di.simple.InstanceTemplateReference templateType) {
            buildGuard();
            this.reference = templateType;
            return this;
        }

        @Override
        @Generated
        public String toString() {
            return "InstanceBuilderImpl{" + "instance=" + instance + ", reference=" + reference + '}';
        }
    }
}
