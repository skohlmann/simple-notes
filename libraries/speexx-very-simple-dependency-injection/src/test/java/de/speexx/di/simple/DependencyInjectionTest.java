/*
 * This file is part of the SpeexX Simple Dependency Injection project.
 * 
 * SpeexX Simple Dependency Injection project is free software:
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Dependency Injection project is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warrantyof MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpeexX Simple Dependency Injection project.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.di.simple;

import static com.google.common.truth.Truth.assertThat;
import org.junit.jupiter.api.Test;

/**
 *
 * @author sascha.kohlmann
 */
public class DependencyInjectionTest {
    
    @Test
    public void no_template_type_with_default_constructor() {
        // Given
        final var di = DependencyInjection.builder()
                .add(StaticTypeTemplateResolverBuilder.builder()
                        .register(InjectionTemplate.builder(RootClass.class)
                                .build())
                        .build())
                .build();
        
        System.out.println(di);
        // When
        final var instance = di.instanceFor(RootClass.class);
        
        // Then
        assertThat(instance).isNotNull();
    }

    @Test
    public void with_template_type_with_default_constructor() {
        // Given
        final var di = DependencyInjection.builder()
                .add(StaticTypeTemplateResolverBuilder.builder()
                        .register(InjectionTemplate.builder(RootClassWithInterface.class)
                                .build())
                        .build())
                .build();
        
        // When
        final var instance = di.instanceFor(RootInterface.class);
        
        // Then
        assertThat(instance).isNotNull();
    }

    @Test
    public void with_template_type_with_constructor() {
        // Given
        final var di = DependencyInjection.builder()
                .add(StaticTypeTemplateResolverBuilder.builder()
                        .register(InjectionTemplate.builder(RootClassWithInterface.class)
                                .build())
                        .register(InjectionTemplate.builder(WithConstructor.class)
                                .with(ConstructorTemplate.builder()
                                        .addParameter(new InstanceTemplateReference(RootInterface.class))
                                        .addParameter(() -> 2)
                                        .build())
                                .build())
                        .build())
                .build();
        
        // When
        final var instance = di.instanceFor(ConstructorBase.class);
        
        // Then
        assertThat(instance).isNotNull();
        assertThat(instance.get()).isInstanceOf(RootInterface.class);
        assertThat(instance).isInstanceOf(WithConstructor.class);
        assertThat(((WithConstructor) instance).i).isEqualTo(2);
    }
    
    @Test
    public void with_instance() {
        // Given
        final var di = DependencyInjection.builder()
                .add(Instance.builder(new RootClass())
                        .build())
                .build();
        
        // When
        final var instance = di.instanceFor(RootClass.class);
        
        // Then
        assertThat(instance).isNotNull();
    }
    
    public static interface RootInterface {}
    public static class RootClass {}
    public static class RootClassWithInterface implements RootInterface {}

    public static interface ConstructorBase {RootInterface get();}
    public static class WithConstructor implements ConstructorBase {

        private RootInterface ifc;
        private int i;
        
        public WithConstructor(final RootInterface rootInterfaceImpl, final int i) {
            this.ifc = rootInterfaceImpl;
            this.i = i;
        }
        
        @Override
        public RootInterface get() {
            return this.ifc;
        }
        
        public int i() {
            return this.i;
        }
    }
}
