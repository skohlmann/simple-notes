/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.service.note;

import de.speexx.simplenotes.domain.model.note.NoteEvent;
import de.speexx.simplenotes.domain.model.note.NoteId;
import java.util.List;

import static de.speexx.common.Assertions.assertContainsNoNull;
import static de.speexx.common.Assertions.assertNotNull;
import static java.lang.String.format;
import static java.util.List.copyOf;

/**
 * Container for events within the same {@link NoteId}.
 * @author sascha.kohlmann
 */
public record NoteEvents(NoteId noteId, List<NoteEvent> events) {

    /**
     * @param noteId the identifier of the note events.
     * @param events the events
     * @throws IllegalArgumentException if and only if a parameter is {@code null} or an event reference is {@code null}
     * @throws IllegalStateException if and only if the events have note the same identifier as the provided identifier
     */
    public NoteEvents(NoteId noteId, List<NoteEvent> events) {
        assertContainsNoNull(events, "Event list must be provided and not contain 'null' reference");
        final var identifier = noteId.id();
        events.stream().forEach(event -> {
            if (!identifier.equals(event.noteId().id())) {
                throw new IllegalStateException(format("Notes must have same ID (%s): %s", identifier, event));
            }
        });
        this.noteId = assertNotNull(noteId, "NoteId must be provided");
        this.events = copyOf(events);
    }
}
