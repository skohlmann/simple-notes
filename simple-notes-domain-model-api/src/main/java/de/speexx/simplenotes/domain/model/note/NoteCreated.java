/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.model.note;

import de.speexx.simplenotes.domain.model.creator.Creator;
import java.time.OffsetDateTime;
import org.jmolecules.event.annotation.DomainEvent;

/**
 * Indicates the creation of a {@code Note}.
 * @author sascha.kohlmann
 */
@DomainEvent
public interface NoteCreated extends NoteEvent {

    /** The text of the note.
     * @return the text. Never {@code null} */
    Text text();

    /** The datetime the note was created.
     * @return the creation datetime of the note. Never {@code null} */
    OffsetDateTime createdAt();
    
    /** The creator of the note.
     * @return the note creator. Never {@code null} */
    Creator creator();
}
