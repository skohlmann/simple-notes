/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.service.note;

import de.speexx.simplenotes.domain.model.note.Note;
import de.speexx.simplenotes.domain.model.note.NoteId;
import java.util.Optional;
import org.jmolecules.ddd.annotation.Repository;

/**
 * Adding {@code NoteEvent}s to an underlying persistence system and retrieving
 * a {@code Note}s from the underlying persistence system.
 * @author sascha.kohlmann
 */
@Repository
public interface NoteRepository {
    
    /**
     * Adds the events to an underlying persistence system.
     * @param noteEvents the events to add
     * @throws RepositoryException if a problem occurs while adding the events to
     *                             the underlying persistence system.
     */
    void add(final NoteEvents noteEvents) throws RepositoryException; 

    /**
     * Retrievs a {@code Note} of the provided identifier from the underlying
     * persistence system if the @{@code Note} exists.
     * @param noteId the identifier of the note to retrieve
     * @return an {@code Optional} with the {@code Note} if available in the
     *         underlying persistence system. Otherwise an empty {@code Optional}.
     * @throws RepositoryException if a problem occurs while retrieving the
     *                             {@code Note} from the underlying persistence
     *                             system.
     */
    Optional<Note> noteOf(final NoteId noteId) throws RepositoryException;
}
