/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.model.creator;

import org.jmolecules.ddd.annotation.ValueObject;

/**
 * Every note in the context has creator.
 * @author sascha.kohlmann
 */
@ValueObject
public interface Creator {

    /** A unique identifier for the creator.
     * @return the unique identifier. Never {@code null}. */
    CreatorId creatorId();

    /** The name of the creato.
     * @return the name. Should not be {@code null} */
    String name();
}
