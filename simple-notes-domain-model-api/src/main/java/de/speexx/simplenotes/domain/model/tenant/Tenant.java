/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.model.tenant;

import de.speexx.simplenotes.domain.model.tenant.unknown.UnknownTenant;

/**
 * Makes the <em>Simple Notes</em> context multitenancy capable.
 * As not all use cases of the <em>Simple Notes</em> context requires multitenancy,
 * an {@linkplain #unknownTenant() unknown tenant} is upported.
 * @author sascha.kohlmann
 */
public interface Tenant {
    
    /**
     * The return value should never be {@code null}. An empty string should
     * represent an {@linkplain #unknownTenant() unknown tenant} in use cases
     * where multitenancy is not required.
     * @return the tenant identifier
     * @see #isUnknown() 
     */
    String id();
    
    /**
     * If and only if the tenant represents an {@linkplain #unknownTenant() unknown tenant}
     * the value is {@code true}.
     * @return {@code false} for the default implementation
     */
    public default boolean isUnknown() {
        return false;
    }

    /**
     * Returns a tenant implementation rpresenting an unknown tenant.
     * Such an unkown tenant may be used where multitenancy is not requiredd.
     * @return an unknown tenant where {@link #isUnknown() } returns {@code false} and {@link #id() } returns an empty string.
     */
    public static Tenant unknownTenant() {
        return UnknownTenant.INSTANCE;
    }
}
