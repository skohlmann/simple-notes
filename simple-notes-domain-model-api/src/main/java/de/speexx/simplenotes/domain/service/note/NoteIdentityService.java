/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.service.note;

import de.speexx.simplenotes.domain.model.note.Note;
import de.speexx.simplenotes.domain.model.note.NoteId;
import java.util.UUID;
import org.jmolecules.ddd.annotation.Service;

/**
 * Supplies {@link NoteId NoteIds}.
 * @author sascha.kohlmann
 * @see Note
 */
@Service
public interface NoteIdentityService {
    
    /**
     * The default implementation generaties the Note ID with
     * <pre>
     * var id = UUID.randomUUID().toString();
     * </pre>
     * @return a {@link Note} identifier based on a random {@link UUID}.
     */
    public default NoteId nextNoteId() {
        final var id = UUID.randomUUID().toString();
        return () -> id;
    }
}
