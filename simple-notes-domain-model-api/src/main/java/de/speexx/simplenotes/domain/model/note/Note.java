/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.model.note;

import de.speexx.simplenotes.domain.model.creator.Creator;
import java.time.OffsetDateTime;
import org.jmolecules.ddd.annotation.AggregateRoot;

/**
 * State of a note. 
 * @author sascha.kohlmann
 */
@AggregateRoot
public interface Note {

    /** Return the text of a note.
     * @return the note text. Must never be {@code null} */
    Text text();

    /** The datetime when the note has reached the system.
     * @return the creation datetime. Must never be {@code null} */
    OffsetDateTime createdAt();
    
    /** A unique ID of the note.
     * @return the ID of the note. Must never be {@code null}  */
    NoteId noteId();

    /** The creator of the note with a name. 
     * @return the creator of the note. Must never be {@code null}  */
    Creator creator();
}
