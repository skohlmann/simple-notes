/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.command.note;

import de.speexx.common.annotation.Immutable;
import de.speexx.simplenotes.domain.model.creator.Creator;
import de.speexx.simplenotes.domain.model.note.Note;
import java.time.OffsetDateTime;
import java.util.Optional;

/**
 * The command contains all relevant information to create a new <em>idependent</em> {@link Note}.
 * @author sascha.kohlmann
 */
@Immutable
public final class CreateNote {
    
    public final Optional<String> text;
    public final Optional<OffsetDateTime> creationDatetime;
    public final Optional<Creator> creator;

    public static Builder builder() {
        return new Builder();
    }
    
    private CreateNote(final Builder builder) {
        this.text = Optional.ofNullable(builder.text);
        this.creationDatetime = Optional.ofNullable(builder.creationDatetime);
        this.creator = Optional.ofNullable(builder.creator);
    }
    
    public static final class Builder {

        private String text;
        private OffsetDateTime creationDatetime;
        private Creator creator;
        
        private boolean built = false;
        
        private Builder() {};
        
        public CreateNote build() {
            buildGuard();
            this.built = true;
            return new CreateNote(this);
        }
        
        public Builder withText(final String text) {
            buildGuard();
            this.text = text;
            return this;
        }

        public Builder withCreator(final Creator creator) {
            buildGuard();
            this.creator = creator;
            return this;
        }

        public Builder withCreationDatetime(final OffsetDateTime creationDatetime) {
            buildGuard();
            this.creationDatetime = creationDatetime;
            return this;
        }
        
        private void buildGuard() {
            if (this.built) {
                throw new IllegalStateException("built");
            }
        }
    }
}
