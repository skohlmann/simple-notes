/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * All {@linkplain de.speexx.simplenotes.domain.model.note.Note notes} must be created by a creator.
 * This creator is not part of this bounded context an must be managed in an external environment.
 * However use the {@link de.speexx.simplenotes.domain.service.collaborator.CollaboratorService CollaboratorService}
 * to lookup for creators.
 */
package de.speexx.simplenotes.domain.model.creator;
