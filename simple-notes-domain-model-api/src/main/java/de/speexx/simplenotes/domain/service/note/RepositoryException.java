/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.service.note;

/**
 * Indicates a problem withing {@link NoteRepository} operations.
 * @author sascha.kohlmann
 */
public class RepositoryException extends RuntimeException {

    /**
     * Creates a new instance of <code>RepositoryException</code> without detail
     * message.
     */
    public RepositoryException() {
    }

    /**
     * Constructs an instance of <code>RepositoryException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public RepositoryException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>RepositoryException</code> with the specified detail message and the causal throwable.
     * @param message the detail message
     * @param cause the cause
     */
    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs an instance of <code>RepositoryException</code> with the causal throwable.
     * @param cause the cause
     */
    public RepositoryException(Throwable cause) {
        super(cause);
    }
}
