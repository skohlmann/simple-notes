/*
 * This file is part of the SpeexX Simple Notes Project.
 * 
 * SpeexX Simple Notes Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * SpeexX Simple Notes Project is distributed in the hope that it will be,
 * useful but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along
 * along with SpeexX Simple Notes Project. If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.simplenotes.domain.model.tenant.unknown;

import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 *
 * @author sascha.kohlmann
 */
public class UnknownTenantTest {

    @Test
    public void unknown_grant() {
        assertThat(UnknownTenant.INSTANCE.isUnknown()).isTrue();
        assertThat(UnknownTenant.INSTANCE.id()).isEmpty();
    }
}
